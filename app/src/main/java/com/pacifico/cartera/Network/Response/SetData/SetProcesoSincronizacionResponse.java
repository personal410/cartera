package com.pacifico.cartera.Network.Response.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by DSB on 01/07/2016.
 */
    public class SetProcesoSincronizacionResponse {
    @SerializedName("FechaSincronizacion")
    @Expose
    public String FechaSincronizacion;
    @SerializedName("MensajeSincronizacion")
    @Expose
    public String MensajeSincronizacion;
    @SerializedName("FlagExito")
    @Expose
    public String FlagExito;
}