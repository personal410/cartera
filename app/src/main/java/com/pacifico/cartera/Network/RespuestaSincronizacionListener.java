package com.pacifico.cartera.Network;

/**
 * Created by victorsalazar on 9/08/16.
 */
public interface RespuestaSincronizacionListener{
    void terminoSincronizacion(int codigo, String mensaje);
}