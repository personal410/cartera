package com.pacifico.cartera.Network.Response;

//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//import com.fasterxml.jackson.annotation.JsonProperty;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Joel on 24/05/2016.
 */

//@JsonIgnoreProperties(ignoreUnknown = true)
public class RestDataInicialResponse_test {
    @SerializedName("Message")
    @Expose
    private String test;

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

}



