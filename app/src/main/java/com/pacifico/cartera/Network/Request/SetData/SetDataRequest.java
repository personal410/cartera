package com.pacifico.cartera.Network.Request.SetData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vctrls3477 on 8/06/16.
 */
public class SetDataRequest{
    @SerializedName("RequestData")
    @Expose
    private RequestDataRequest requestData;

    public RequestDataRequest getRequestData() {
        return requestData;
    }
    public void setRequestData(RequestDataRequest requestData) {
        this.requestData = requestData;
    }
}