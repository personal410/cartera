package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.HistorialBean;

/**
 * Created by dsb on 15/05/2016.
 */
public class HistorialAdapter extends BaseAdapter {

    private LayoutInflater vi;
    private ArrayList<HistorialBean> listaHistorial = new ArrayList<HistorialBean>();
    private Activity activity;

    public HistorialAdapter(Activity activity, ArrayList<HistorialBean> list) {
        super();
        this.activity=activity;
        if(list!=null){
            this.setListaHistorial(list);
        }
        vi = (LayoutInflater)activity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.ppct_hist_item_row, null);

            holder.tvEtapa = (TextView) convertView.findViewById(R.id.tvEtapa);
            holder.vwEtapa = convertView.findViewById(R.id.vwEtapa);
            holder.tvEstado = (TextView) convertView.findViewById(R.id.tvEstado);
            holder.vwEstado = convertView.findViewById(R.id.vwEstado);
            holder.tvFechaEvento = (TextView) convertView.findViewById(R.id.tvFechaEvento);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }


        HistorialBean item = getListaHistorial().get(position);

        holder.tvEtapa.setText(item.getDesEtapa().toString());

        holder.vwEtapa.setBackgroundDrawable(item.getDrwEtapa());

        holder.tvEstado.setText(item.getDesEstado2().toString());

        holder.vwEstado.setBackgroundDrawable(item.getDrwEstado());

        holder.tvFechaEvento.setText(item.getDesFechaEvento());


        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getListaHistorial().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return getListaHistorial().get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public ArrayList<HistorialBean> getListaHistorial() {
        return listaHistorial;
    }

    public void setListaHistorial(ArrayList<HistorialBean> listaHistorial) {
        this.listaHistorial = listaHistorial;
    }

    /** Helper class acting as a holder of the information for each row */
    private class ViewHolder {
        public TextView tvEtapa;
        public View vwEtapa;
        public TextView tvEstado;
        public View vwEstado;
        public TextView tvFechaEvento;
    }

}

