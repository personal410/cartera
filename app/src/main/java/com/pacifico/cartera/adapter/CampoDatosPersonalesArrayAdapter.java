package com.pacifico.cartera.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import com.pacifico.cartera.R;

/**
 * Created by victorsalazar on 5/08/16.
 */
public class CampoDatosPersonalesArrayAdapter extends ArrayAdapter<String>{
    ArrayList<String> arrCampoDatosPersonales;
    int tipoLista;
    public CampoDatosPersonalesArrayAdapter(Context context, ArrayList<String> arrCampoDatosPersonalesTemp, int nTipoLista){
        super(context, -1);
        arrCampoDatosPersonales = arrCampoDatosPersonalesTemp;
        tipoLista = nTipoLista;
    }
    @Override
    public int getCount() {
        return arrCampoDatosPersonales.size();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.row_campo_datos_personales, parent, false);
        TextView txtCampoDatosPersonales = (TextView)rowView.findViewById(R.id.txtCampoDatosPersonales);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)txtCampoDatosPersonales.getLayoutParams();
        layoutParams.gravity = tipoLista == 1 ? Gravity.START : Gravity.CENTER;
        txtCampoDatosPersonales.setPadding(tipoLista == 1 ? 100 : 0, 0, 0, 0);
        txtCampoDatosPersonales.setText(arrCampoDatosPersonales.get(position));
        return rowView;
    }
}