package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.FamiliarCarteraBean;

import static com.pacifico.cartera.utils.Util.capitalizedString;

/**
 * Created by dsb on 12/07/2016.
 */
public class HijoAdapter extends BaseAdapter {

    private LayoutInflater vi;
    private ArrayList<FamiliarCarteraBean> listaHijo = new ArrayList<FamiliarCarteraBean>();
    private Activity activity;

    public HijoAdapter(Activity activity, ArrayList<FamiliarCarteraBean> list) {
        super();
        this.activity=activity;
        if(list!=null){
            this.setListaHijo(list);
        }
        vi = (LayoutInflater)activity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.ppct_dfam_item_row, null);

            holder.tvHijoNombre = (TextView) convertView.findViewById(R.id.tvHijoNombre);
            holder.tvHijoEdad = (TextView) convertView.findViewById(R.id.tvHijoEdad);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        insertarDatos(holder,position);


        return convertView;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getListaHijo().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return getListaHijo().get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public ArrayList<FamiliarCarteraBean> getListaHijo() {
        return listaHijo;
    }

    public void setListaHijo(ArrayList<FamiliarCarteraBean> listaHijo) {
        this.listaHijo = listaHijo;
    }

    /** Helper class acting as a holder of the information for each row */
    private class ViewHolder {
        public TextView tvHijoNombre;
        public TextView tvHijoEdad;
    }

    private void insertarDatos(ViewHolder holder , int position){

        FamiliarCarteraBean item = getListaHijo().get(position);
        if ( item.getFlagActivo()==1) {
            holder.tvHijoNombre.setVisibility(View.VISIBLE);
            holder.tvHijoNombre.setText(capitalizedString(item.getNombres().toString()));
            holder.tvHijoEdad.setVisibility(View.VISIBLE);
            if(item.getEdad()==1){
                holder.tvHijoEdad.setText(String.valueOf(item.getEdad()+" Año"));
            }else {
                holder.tvHijoEdad.setText(String.valueOf(item.getEdad() + " Años"));
            }
        }


    }


}
