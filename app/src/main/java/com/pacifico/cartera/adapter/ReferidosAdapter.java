package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.ReferidosBean;

import static com.pacifico.cartera.utils.Util.capitalizedString;

/**
 * Created by dsb on 15/05/2016.
 */
public class ReferidosAdapter extends BaseAdapter
{

    private LayoutInflater vi;
    private ArrayList<ReferidosBean> lstReferidos = new ArrayList<>();
    private Activity activity;
     String Hijos;
     String Ing;
    String Ed;

    public ReferidosAdapter(Activity activity,ArrayList<ReferidosBean> list) {
        super();
        if(list!=null){
            this.setLstReferidos(list);
        }
        vi = (LayoutInflater)activity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.ppct_refe_item_row, null);

            holder.tvProspecto = (TextView) convertView.findViewById(R.id.tvProspecto);
            holder.tvTelefono = (TextView) convertView.findViewById(R.id.tvTelefono);
            holder.tvHijos = (TextView) convertView.findViewById(R.id.tvHijos);
            holder.tvEdad = (TextView) convertView.findViewById(R.id.tvEdad);
            holder.tvIngresos = (TextView) convertView.findViewById(R.id.tvIngresos);
            holder.tvContactado = (TextView) convertView.findViewById(R.id.tvContactado);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        ReferidosBean item = getLstReferidos().get(position);

     String nombresArray[] = item.getNomProspecto().split(" ");
        String nombres ="";
        for(int i = 0; i < nombresArray.length; i++){
            nombres = nombres +" "+capitalizedString(nombresArray[i]);
        }


        holder.tvProspecto.setText(nombres);
        holder.tvTelefono.setText(item.getTelefono());
        holder.tvHijos.setText(ValoresHijos(""+item.getFlagTieneHijos()));
        holder.tvEdad.setText(ValoresEdad(""+item.getCodEdad()));
        holder.tvIngresos.setText(ValoresTipoEmpleo(""+item.getTipoEmpleo()));
        holder.tvContactado.setText(ValoresHijos(""+item.getCodContactado()));

        return convertView;
    }

    private class ViewHolder {
        public TextView tvProspecto;
        public TextView tvTelefono;
        public TextView tvContactado;
        public TextView tvIngresos;
        public TextView tvHijos;
        public TextView tvEdad;
    }

    public ArrayList<ReferidosBean> getLstReferidos() {
        return lstReferidos;
    }

    public void setLstReferidos(ArrayList<ReferidosBean> lstReferidos) {
        this.lstReferidos = lstReferidos;
    }

    @Override
    public int getCount() {
        return getLstReferidos().size();
    }

    @Override
    public Object getItem(int position) {
        return getLstReferidos().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    public String ValoresHijos(String telefono){
        switch(telefono){

            case "0" :
                Hijos = "No";
                return Hijos;
            case "1" :
                Hijos="Si";
                return Hijos;
        }
        return null;
    }

    public String ValoresIngresos(String ingresos){

    switch(ingresos){

    case "1" :
        Ing = "Menor a S/ 6,000";
        return Ing;
    case "2" :
        Ing="Entre S/ 6,000 y S/ 9,000";
        return Ing;
    case "3" :
        Ing="Entre S/ 9,001 y S/ 12,000";
        return Ing;
    case "4" :
        Ing="Mayores a S/ 12,001";
        return Ing;
            }

        return null;
    }

    public String ValoresEdad(String edad){

        switch(edad){

            case "1" :
                Ed = "Menores a 25 años";
                return Ed;
            case "2" :
                Ed="Entre 25 y 30 años";
                return Ed;
            case "3" :
                Ed="Entre 31 y 40 años";
                return Ed;
            case "4" :
                Ed="Mayores a 41 años";
                return Ed;
        }
        return null;
    }

    public String ValoresTipoEmpleo(String tipoEmpleo){

        switch(tipoEmpleo){

            case "1" :
                Ed = "Dependiente";
                return Ed;
            case "2" :
                Ed="Independiente";
                return Ed;
        }
        return null;
    }

}

