package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.TusContactosBean;
import com.pacifico.cartera.utils.Constantes;

import static com.pacifico.cartera.utils.Util.strNULL;

/**
 * Created by dsb on 02/05/2016.
 */
public class TusContactosAdapter extends BaseAdapter {

    private LayoutInflater layoutInflater;
    private ArrayList<TusContactosBean> listaTusContactos = new ArrayList<>();
    private Activity activity;
    private short campoOrden;

    public TusContactosAdapter(Activity activity, ArrayList<TusContactosBean> list) {
        super();
        this.activity = activity;
        if (list != null) {
            this.setListaTusContactos(list);
        }
        layoutInflater = activity.getLayoutInflater();
        campoOrden = Constantes.campoOrdenNombreAsc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.tuco_item_row, null);

            holder.tvPrimeraLetraAsc = (TextView) convertView.findViewById(R.id.tvPrimeraLetra);
            holder.tvProspecto = (TextView) convertView.findViewById(R.id.tvProspecto);
            holder.tvEtapa = (TextView) convertView.findViewById(R.id.tvEtapa);
            holder.vwEtapa = convertView.findViewById(R.id.vwEtapa);
            holder.tvEstado1 = (TextView) convertView.findViewById(R.id.tvEstado1);
            holder.tvEstado2 = (TextView) convertView.findViewById(R.id.tvEstado2);
            holder.vwEstado = convertView.findViewById(R.id.vwEstado);
            holder.tvUltimoEstado = (TextView) convertView.findViewById(R.id.tvUltimoEstado);
            holder.tvIngresos = (TextView) convertView.findViewById(R.id.tvIngresos);
            holder.tvHijos = (TextView) convertView.findViewById(R.id.tvHijos);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        TusContactosBean item = getListaTusContactos().get(position);

        holder.tvPrimeraLetraAsc.setText(item.getPrimeraLetraAsc());
        if (campoOrden == Constantes.campoOrdenNombreAsc) {
            holder.tvPrimeraLetraAsc.setText(item.getPrimeraLetraAsc());
        } else if (campoOrden == Constantes.campoOrdenNombreDesc) {
            holder.tvPrimeraLetraAsc.setText(item.getPrimeraLetraDesc());
        } else {
            holder.tvPrimeraLetraAsc.setText("");
        }

        String  nombres = item.getNomProspecto();
        String[] nombresApell = nombres.split(" ");
        String nombreFinal = "";

        for (int i = 0; i < nombresApell.length; i++) {
            //nombresApell[i]= nombresApell[i].substring(0,1).toUpperCase()+""+nombresApell[i].substring(1).toLowerCase();
            //Log.i("TAG", "nuevos" +nombresApell[i]);
            if (nombresApell[i].trim().equals("")) {
                nombreFinal = nombreFinal+"";
            }else{
                nombreFinal = nombreFinal + " " + strNULL(nombresApell[i].substring(0,1)).toUpperCase()+""+strNULL(nombresApell[i].substring(1).toLowerCase());
            }
        }

        holder.tvProspecto.setText(nombreFinal.trim());
        if (item.getDesEtapa() != null) {
            holder.tvEtapa.setText(item.getDesEtapa().toString());
        }

        holder.tvEstado1.setVisibility(View.VISIBLE);
        holder.tvEstado2.setVisibility(View.VISIBLE);
        holder.vwEtapa.setBackgroundDrawable(item.getDrwEtapa());
        Log.i("TAG","resultado:"+item.getDesResultado()+"*");
        if (item.getDesResultado() != null && item.getDesResultado().length()>0) {
        //if (item.getDesEstado2() != null) {
            //holder.tvEstado2.setText(item.getDesEstado2().toString());

            holder.tvEstado2.setText(item.getDesResultado().toString());
        }else{
            holder.tvEstado2.setVisibility(View.GONE);
        }



        if (item.getDesEstado() == null || item.getDesEstado().length() <= 0) {

            holder.tvEstado1.setVisibility(View.GONE);
            holder.tvEstado2.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

        } else {
            holder.tvEstado1.setVisibility(View.VISIBLE);
            holder.tvEstado1.setText(item.getDesEstado());
            //holder.tvEstado1.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

        }

        holder.vwEstado.setBackgroundDrawable(item.getDrwEstado());

        holder.tvUltimoEstado.setText(item.getDesFecUltEstado());
        if(item.getDesFecUltEstado().length() == 0 && item.getCodEtapa() == 1){
            holder.tvUltimoEstado.setText(item.getFechaCreacionProspecto());
        }

        item.getDesIngresos().replace("Entre","").trim();
        Log.i("Tag","old"+item.getDesIngresos().replace("Entre","").trim().replace("y","a"));
        holder.tvIngresos.setText(item.getDesIngresos().replace("Entre","").trim());

        holder.tvHijos.setText(item.getDesTieneHijos().toString());

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getListaTusContactos().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return getListaTusContactos().get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public ArrayList<TusContactosBean> getListaTusContactos() {
        return listaTusContactos;
    }

    public void setListaTusContactos(ArrayList<TusContactosBean> listaTusContactos) {
        this.listaTusContactos = listaTusContactos;
    }

    /**
     * Helper class acting as a holder of the information for each row
     */
    private class ViewHolder {
        public TextView tvPrimeraLetraAsc;
        public TextView tvProspecto;
        public TextView tvEtapa;
        public View vwEtapa;
        public TextView tvEstado1;
        public TextView tvEstado2;
        public View vwEstado;
        public TextView tvUltimoEstado;
        public TextView tvIngresos;
        public TextView tvHijos;
    }

    public void ordenarPorProspectoAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getNomProspecto().compareToIgnoreCase(object2.getNomProspecto());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenNombreAsc;
        notifyDataSetChanged();

    }

    public void ordenarPorPropectoDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getNomProspecto().compareToIgnoreCase(object1.getNomProspecto());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenNombreDesc;
        notifyDataSetChanged();
    }

    public void ordenarPorEtapaAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getDesEtapa().compareToIgnoreCase(object2.getDesEtapa());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorEtapaDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getDesEtapa().compareToIgnoreCase(object1.getDesEtapa());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorEstadoAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getDesEstados().compareToIgnoreCase(object2.getDesEstados());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorEstadoDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getDesEstados().compareToIgnoreCase(object1.getDesEstados());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorFecUltEstadoAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                if (object1.getFecUltEstado() != null && object2.getFecUltEstado() != null)
                    return object1.getFecUltEstado().compareTo(object2.getFecUltEstado());
                else
                    return -1;

            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorFecUltEstadoDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                if (object1.getFecUltEstado() != null && object2.getFecUltEstado() != null)
                    return object2.getFecUltEstado().compareTo(object1.getFecUltEstado());
                else
                    return -1;
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorIngresosAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getCodIngresos().compareTo(object2.getCodIngresos());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorIngresosDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getCodIngresos().compareTo(object1.getCodIngresos());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorTieneHijosAsc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getCodTieneHijos().compareTo(object2.getCodTieneHijos());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

    public void ordenarPorTieneHijosDesc() {
        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getCodTieneHijos().compareTo(object1.getCodTieneHijos());
            }
        };
        Collections.sort(getListaTusContactos(), comparator);
        campoOrden = Constantes.campoOrdenOtro;
        notifyDataSetChanged();
    }

}

