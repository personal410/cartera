package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.IndiceDatoBean;

/**
 * Created by dsb on 12/07/2016.
 */
public class IndicadoresLeyendaAdapter extends BaseAdapter {

    private LayoutInflater vi;
    private List<IndiceDatoBean> lista = new ArrayList<IndiceDatoBean>();
    private Activity activity;
    private int itemResourceId;
    private Drawable[] drwIcons;

    public IndicadoresLeyendaAdapter(Activity activity, List<IndiceDatoBean> list, int itemResourceId, Drawable[] drwIcons) {
        super();
        this.activity=activity;
        this.itemResourceId = itemResourceId;
        this.drwIcons = drwIcons;
        if(list!=null){
            this.setLista(list);
        }
        vi = (LayoutInflater)activity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();

            convertView = vi.inflate(getItemResourceId(), null);
            holder.vwIcon = convertView.findViewById(R.id.vwIcon);
            holder.tvDescripcion = (TextView) convertView.findViewById(R.id.tvDescripcion);
            holder.tvCantidad = (TextView) convertView.findViewById(R.id.tvCantidad);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        IndiceDatoBean item = getLista().get(position);
        if(drwIcons!=null && drwIcons.length>position){
            holder.vwIcon.setBackground(drwIcons[position]);
        }

        holder.tvDescripcion.setText(item.getDescripcion());
        holder.tvCantidad.setText(String.valueOf(item.getValor()));


        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getLista().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return getLista().get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public List<IndiceDatoBean> getLista() {
        return lista;
    }

    public void setLista(List<IndiceDatoBean> lista) {
        this.lista = lista;
    }

    public int getItemResourceId() {
        return itemResourceId;
    }

    public void setItemResourceId(int itemResourceId) {
        this.itemResourceId = itemResourceId;
    }

    /** Helper class acting as a holder of the information for each row */
    private class ViewHolder {
        public View vwIcon;
        public TextView tvDescripcion;
        public TextView tvCantidad;
    }

}
