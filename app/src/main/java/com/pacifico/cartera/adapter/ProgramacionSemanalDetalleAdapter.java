package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionSemanalDetalleBean;
import com.pacifico.cartera.Util.Util;

import static com.pacifico.cartera.utils.Util.capitalizedString;

/**
 * Created by dsb on 18/05/2016.
 */
public class ProgramacionSemanalDetalleAdapter extends BaseAdapter {

    private LayoutInflater vi;
    private ArrayList<ProgramacionSemanalDetalleBean> listaDetalle = new ArrayList<>();
    private Activity activity;

    public ProgramacionSemanalDetalleAdapter(Activity activity, ArrayList<ProgramacionSemanalDetalleBean> list) {
        super();
        this.activity=activity;
        if(list!=null){
            this.setListaDetalle(list);
        }
        vi = (LayoutInflater)activity.getLayoutInflater();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = vi.inflate(R.layout.idcd_psem_deta_dial_row, null);
            holder.tvProspecto = (TextView) convertView.findViewById(R.id.tvProspecto);
            holder.tvReferido = (TextView) convertView.findViewById(R.id.tvReferido);
            holder.tvFuente = (TextView) convertView.findViewById(R.id.tvFuente);
            holder.tvEtapa = (TextView) convertView.findViewById(R.id.tvEtapa);
            holder.vwEtapa = convertView.findViewById(R.id.vwEtapa);
            holder.tvFecha = (TextView) convertView.findViewById(R.id.tvFecha);
            holder.tvEstado1 = (TextView) convertView.findViewById(R.id.tvEstado1);
            holder.tvEstado2 = (TextView) convertView.findViewById(R.id.tvEstado2);
            holder.vwEstado = convertView.findViewById(R.id.vwEstado);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvReferido.setVisibility(View.VISIBLE);
        holder.tvEstado1.setVisibility(View.VISIBLE);

        ProgramacionSemanalDetalleBean item = getListaDetalle().get(position);

        /*String nombreArray[] = item.getNomProspecto().split(" ");
        String prospecto ="";
        for(int i = 0;i<nombreArray.length;i++){
           prospecto = prospecto + " " +capitalizedString(nombreArray[i]);

        }
        holder.tvProspecto.setText(prospecto);
        */

        holder.tvProspecto.setText(Util.capitalizedString(item.getNomProspecto().trim()));
        if (item.getIdReferenciadorDispositivo() != null)
            holder.tvReferido.setText(Util.capitalizedString(item.getNomReferenciador().trim()));
        else
        {
            holder.tvReferido.setVisibility(View.GONE);
            holder.tvProspecto.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        }

        //if (item.getIdReferenciadorDispositivo() == "0")
            //holder.tvReferido.setText("");
        holder.tvFuente.setText(item.getDesFuente());
        holder.tvEtapa.setText(item.getDesEtapa());
        holder.vwEtapa.setBackgroundDrawable(item.getDrwEtapa());

        String date = "";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date newDate = format.parse(item.getFecha().substring(0,16));

            format = new SimpleDateFormat("E, dd MMM hh:mm a");
            date = format.format(newDate);
            item.setDesFecha(date);

        }catch (Exception e){
                e.printStackTrace();
        }

        holder.tvFecha.setText(date);

        holder.tvEstado2.setText(item.getDesEstado2());

        //Log.d("PSDA", item.getCodProspecto() + " item.getDesEstado(): " + item.getDesEstado() + " item.getDesEstado2(): " + item.getDesEstado2());
        //Log.d("PSDA", item.getIdProspectoDispositivo() + " item.getDesEstado(): " + item.getDesEstado() + " item.getDesEstado2(): " + item.getDesEstado2());

        if(item.getDesEstado() == null || item.getDesEstado().length()<=0){
            Log.d("PSDA","hide estado1");
            holder.tvEstado1.setVisibility(View.GONE);
            holder.tvEstado2.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);

        }else{
            Log.d("PSDA","show estado1");
            holder.tvEstado1.setVisibility(View.VISIBLE);
            holder.tvEstado1.setText(item.getDesEstado().toString());
            holder.tvEstado2.setGravity(Gravity.TOP | Gravity.LEFT);
        }

        holder.vwEstado.setBackgroundDrawable(item.getDrwEstado());

        return convertView;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return getListaDetalle().size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return getListaDetalle().get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public ArrayList<ProgramacionSemanalDetalleBean> getListaDetalle() {
        return listaDetalle;
    }

    public void setListaDetalle(ArrayList<ProgramacionSemanalDetalleBean> listaDetalle) {
        this.listaDetalle = listaDetalle;
    }

    /** Helper class acting as a holder of the information for each row */
    private class ViewHolder {
        public TextView tvProspecto;
        public TextView tvReferido;
        public TextView tvFuente;
        public TextView tvEtapa;
        public View vwEtapa;
        public TextView tvFecha;
        public TextView tvEstado1;
        public TextView tvEstado2;
        public View vwEstado;

    }

}
