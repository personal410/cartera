package com.pacifico.cartera.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import com.pacifico.cartera.R;

public class ProgramacionSemanalAdapter extends BaseAdapter {

    public ArrayList<ArrayList<String>> listCitas;

    private Activity activity;

    private class ViewHolder {
        TextView tvMar1ra;
        TextView tvMar2da;
        TextView tvMie1ra;
        TextView tvMie2da;
        TextView tvJue1ra;
        TextView tvJue2da;
        TextView tvVie1ra;
        TextView tvVie2da;
        TextView tvSab1ra;
        TextView tvSab2da;
        TextView tvDom1ra;
        TextView tvDom2da;
        TextView tvLun1ra;
        TextView tvLun2da;
    }


    public ProgramacionSemanalAdapter(Activity activity, ArrayList<ArrayList<String>> listCitas) {
        super();
        this.activity = activity;
        this.listCitas = listCitas;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return listCitas.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return listCitas.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub


        ViewHolder holder;
        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.idcd_psem_item_leyenda_row, null);
            holder = new ViewHolder();
            holder.tvMar1ra = (TextView) convertView.findViewById(R.id.tvMar1ra);
            holder.tvMar2da = (TextView) convertView.findViewById(R.id.tvMar2da);
            holder.tvMie1ra = (TextView) convertView.findViewById(R.id.tvMie1ra);
            holder.tvMie2da = (TextView) convertView.findViewById(R.id.tvMie2da);
            holder.tvJue1ra = (TextView) convertView.findViewById(R.id.tvJue1ra);
            holder.tvJue2da = (TextView) convertView.findViewById(R.id.tvJue2da);
            holder.tvVie1ra = (TextView) convertView.findViewById(R.id.tvVie1ra);
            holder.tvVie2da = (TextView) convertView.findViewById(R.id.tvVie2da);
            holder.tvSab1ra = (TextView) convertView.findViewById(R.id.tvSab1ra);
            holder.tvSab2da = (TextView) convertView.findViewById(R.id.tvSab2da);
            holder.tvDom1ra = (TextView) convertView.findViewById(R.id.tvDom1ra);
            holder.tvDom2da = (TextView) convertView.findViewById(R.id.tvDom2da);
            holder.tvLun1ra = (TextView) convertView.findViewById(R.id.tvLun1ra);
            holder.tvLun2da = (TextView) convertView.findViewById(R.id.tvLun2da);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ArrayList<String> listData = listCitas.get(position);

        //Log.d("PSA", "getView positon: " + position + " lstCitas: "  + listCitas + " listData: " + listData);

        if (listData != null) {
            holder.tvMar1ra.setText(listData.get(0));
            holder.tvMar2da.setText(listData.get(1));
            holder.tvMie1ra.setText(listData.get(2));
            holder.tvMie2da.setText(listData.get(3));
            holder.tvJue1ra.setText(listData.get(4));
            holder.tvJue2da.setText(listData.get(5));
            holder.tvVie1ra.setText(listData.get(6));
            holder.tvVie2da.setText(listData.get(7));
            holder.tvSab1ra.setText(listData.get(8));
            holder.tvSab2da.setText(listData.get(9));
            holder.tvDom1ra.setText(listData.get(10));
            holder.tvDom2da.setText(listData.get(11));
            holder.tvLun1ra.setText(listData.get(12));
            holder.tvLun2da.setText(listData.get(13));
        }


        return convertView;
    }


}