package com.pacifico.cartera.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.pacifico.cartera.Model.Bean.Auxiliar.CalendarioProcesadoBean;

public class IndicadoresSemanaHelper extends CalendarioHelper {

    private CalendarioProcesadoBean calendarioInicio;
    private CalendarioProcesadoBean calendarioFin;

    public IndicadoresSemanaHelper(Context context, Resources resources) {
        super(context, resources);
    }

    public void init() {
        super.init();
        obtenerUltimasCuatroSemanas();
    }

    public String obtenerTitulo() {
        if (getCalendarioInicio() == null || getCalendarioFin() == null) {
            return null;
        }

        StringBuffer sbTitulo = new StringBuffer("(");
        sbTitulo.append(DateUtils.getDateFormat01Upper(getCalendarioInicio().getFechaInicioSemana()));
        sbTitulo.append(" - ");
        sbTitulo.append(DateUtils.getDateFormat01Upper(getCalendarioFin().getFechaFinSemana()));
        sbTitulo.append(")");
        return sbTitulo.toString();

    }

    public int obtenerIncrementoCalendario(int idCalendario, int diferencia) {
        int incremento = diferencia;

        int idCalendarioInicio = idCalendario - diferencia;

        if (idCalendarioInicio <= 1) {
            incremento = 0;
        } else if (idCalendarioInicio < (diferencia + 2)) {
            incremento = 1;
        }

        Log.d("PSF", " idCalendario: " + idCalendarioInicio + " diferencia: " + diferencia + " incremento: " + incremento);
        return incremento;
    }

    public void obtenerUltimasCuatroSemanas() {
       // List<CalendarioProcesadoBean> lstCalendarioProcesadoBean = new ArrayList<>();
        int idCalendarioActual = getCurrentCalendario().getIdCalendario();

        int incrementoInicio = obtenerIncrementoCalendario(idCalendarioActual, -4);
        //if(incrementoInicio>0) {
        this.setCalendarioInicio(incrementarIdCalendarioActual(incrementoInicio));
        //}

        int incrementoFin = obtenerIncrementoCalendario(idCalendarioActual, -1);
        //if(incrementoFin>0) {
        this.setCalendarioFin(incrementarIdCalendarioActual(incrementoFin));
        //}
    }

    public CalendarioProcesadoBean getCalendarioInicio() {
        return calendarioInicio;
    }

    public void setCalendarioInicio(CalendarioProcesadoBean calendarioInicio) {
        this.calendarioInicio = calendarioInicio;
    }

    public CalendarioProcesadoBean getCalendarioFin() {
        return calendarioFin;
    }

    public void setCalendarioFin(CalendarioProcesadoBean calendarioFin) {
        this.calendarioFin = calendarioFin;
    }
}
