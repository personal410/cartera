package com.pacifico.cartera.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.github.mikephil.charting.data.BarEntry;

import org.joda.time.DateTime;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraCitaReunionController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraProspectoController;
import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.CalendarioProcesadoBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.DiaSemanaBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionCalendarioBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionSemanalDetalleBean;

/**
 * Created by dsb on 13/06/2016.
 */
public class ProgramacionSemanalHelper extends CalendarioHelper {


    public ProgramacionSemanalHelper(Context context, Resources resources) {
        super(context, resources);
    }

    public String getNumeroCita(String etapa, short numeroEntrevista) {
        return etapa + String.valueOf(numeroEntrevista);
    }

    public String getTextoCitas(long cantidad) {
        String texto = "-";
        if (cantidad > 0L) {
            texto = String.valueOf(cantidad);
        }

        return texto;
    }


    public ProgramacionCalendarioBean obtenerProgramacionCalendario(){
        CarteraCitaReunionController carteraCitaReunionController = new CarteraCitaReunionController();
        CarteraProspectoController prospectoController = new CarteraProspectoController();

        List<DiaSemanaBean> lstDiaSemana = new ArrayList<>();
        // int diaSemana = getCurrentCalendario().getFechaInicioSemana().getDayOfMonth();
        // int diaFin = getCurrentCalendario().getFechaFinSemana().getDayOfMonth();

        short numeroDiasSemana = 1;
        short numeroDiasSemanaDesdeCero = 0;
        DateTime currentDateTime = getCurrentCalendario().getFechaInicioSemana();

        ContextCompat.getColor(getContext(), R.color.gris_claro_texto5);

        ArrayList<ArrayList<String>> listCitas = new ArrayList<>();

        ArrayList<String> citasAgendadas = new ArrayList<>();
        ArrayList<String> citasRealizadas = new ArrayList<>();
        ArrayList<String> referidos = new ArrayList<>();

        int cantidadC1A = 0;
        int cantidadC2A = 0;
        int cantidadC1R = 0;
        int cantidadC2R = 0;
        int cantidadC1REAGR = 0;
        int cantidadC2REAGR = 0;
        int cantidadREF = 0;

        int numero1rasEntrevistasAgendadas = 0;
        int numero2dasEntrevistasAgendadas = 0;
        int numero1rasEntrevistasRealizadasSinReagendadas = 0;
        int numero2dasEntrevistasRealizadasSinReagendadas = 0;
        int totalReferidos = 0;
        int totalObservacionesPersonales = 0;

        ArrayList<BarEntry> entries1ra = new ArrayList<>();
        ArrayList<BarEntry> entries2da = new ArrayList<>();

        String fechaInicioSemana = DateUtils.getDateStringyymmdd(getCurrentCalendario().getFechaInicioSemana());
        String fechaFinSemana = DateUtils.getDateStringyymmdd(getCurrentCalendario().getFechaFinSemana());

        Map<String, Integer> mapNumeroEntrevistasAgendadas = carteraCitaReunionController.obtenerNumeroEntrevistasAgendadas(fechaInicioSemana, fechaFinSemana);
        //Map<String, Integer> mapNumeroEntrevistasReagendadasRealizadas = carteraCitaReunionController.obtenerNumeroEntrevistasReagendadasRealizadas(fechaInicioSemana, fechaFinSemana);
        Map<String, Integer> mapNumeroEntrevistasRealizadas = carteraCitaReunionController.obtenerNumeroEntrevistasRealizadas(fechaInicioSemana, fechaFinSemana);

        //Map<String, Integer> mapNumeroProspectosFuenteADN = prospectoController.obtenerCantidadProspectoPorCodigoFuente(Constantes.codFuente_adn, fechaInicioSemana, fechaFinSemana); // Se cambio por Referido
        Map<String, Integer> mapNumeroReferidos = prospectoController.obtenerCantidadReferidos(fechaInicioSemana, fechaFinSemana);
        totalObservacionesPersonales = prospectoController.obtenerTotalProspectoPorCodigoFuente(Constantes.codFuente_op, fechaInicioSemana, fechaFinSemana);

        while (numeroDiasSemana <= 7) {
            lstDiaSemana.add(obtenerDiaSemana(currentDateTime));

            String strFecha = DateUtils.getDateStringyymmdd(currentDateTime);
            String key1raEnt = String.valueOf(Constantes.codEtapa1ra) + "#" + strFecha;
            String key2daEnt = String.valueOf(Constantes.codEtapa2da) + "#" + strFecha;

            cantidadC1A = StringUtils.isNullReturnZero(mapNumeroEntrevistasAgendadas.get(key1raEnt));
            numero1rasEntrevistasAgendadas = numero1rasEntrevistasAgendadas + cantidadC1A;

            cantidadC1R = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadas.get(key1raEnt));
            numero1rasEntrevistasRealizadasSinReagendadas = numero1rasEntrevistasRealizadasSinReagendadas + cantidadC1R + cantidadC1REAGR;

            citasRealizadas.add(getTextoCitas(cantidadC1R));
            citasAgendadas.add(getTextoCitas(cantidadC1A+cantidadC1R));

            cantidadC2A = StringUtils.isNullReturnZero(mapNumeroEntrevistasAgendadas.get(key2daEnt));
            numero2dasEntrevistasAgendadas = numero2dasEntrevistasAgendadas + cantidadC2A;

            cantidadC2R = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadas.get(key2daEnt));
            numero2dasEntrevistasRealizadasSinReagendadas = numero2dasEntrevistasRealizadasSinReagendadas + cantidadC2R + cantidadC2REAGR;

            citasRealizadas.add(getTextoCitas(cantidadC2R));
            citasAgendadas.add(getTextoCitas(cantidadC2A+cantidadC2R));

            cantidadREF = StringUtils.isNullReturnZero(mapNumeroReferidos.get(strFecha));
            totalReferidos = totalReferidos + cantidadREF;
            referidos.add(getTextoCitas(cantidadREF));
            referidos.add("-");

            Log.d("PSH", "entry 1ra + numeroDiasSemanaDesdeCero: " + numeroDiasSemanaDesdeCero + " cantidadC1A: " + cantidadC1A + " cantidadC1R: " + cantidadC1R + " cantidadC1REAGR: " + cantidadC1REAGR );
            Log.d("PSH", "entry 2da cantidadC2A: " + cantidadC2A + " cantidadC2R: " + cantidadC2R + " cantidadC2REAGR: " + cantidadC2REAGR );

            // datos del grafico
            entries1ra.add(new BarEntry(new float[]{cantidadC1R, cantidadC1A}, numeroDiasSemanaDesdeCero));
            entries2da.add(new BarEntry(new float[]{cantidadC2R, cantidadC2A}, numeroDiasSemanaDesdeCero));

            currentDateTime = currentDateTime.plusDays(1);
            numeroDiasSemana++;
            numeroDiasSemanaDesdeCero++;

        }

        //totalObservacionesPersonales = prospectoController.obtenerCantidadProspectoPorCodigoFuente(Constantes.codFuente_op, DateUtils.getDateStringyymmdd(getCurrentCalendario().getFechaInicioSemana()), DateUtils.getDateStringyymmdd(getCurrentCalendario().getFechaFinSemana()));

        //prospectoController.obtenerTodosProspectos();

        listCitas.add(citasAgendadas);
        listCitas.add(citasRealizadas);
        listCitas.add(referidos);

        return new ProgramacionCalendarioBean(lstDiaSemana, listCitas, numero1rasEntrevistasAgendadas, numero2dasEntrevistasAgendadas,
                numero1rasEntrevistasRealizadasSinReagendadas, numero2dasEntrevistasRealizadasSinReagendadas, totalReferidos, totalObservacionesPersonales,
                entries1ra, entries2da); //

    }

    public ArrayList<ProgramacionSemanalDetalleBean> obtenerEntrevistasAgendadas(CalendarioProcesadoBean calendarioActual) {

        Log.d("PSH", "");
        Log.d("PSH", "#### obtenerEntrevistasAgendadas ");

        CarteraCitaReunionController carteraCitaReunionController = new CarteraCitaReunionController();

        String fechaInicioSemana = DateUtils.getDateStringyymmdd(calendarioActual.getFechaInicioSemana());
        String fechaFinSemana = DateUtils.getDateStringyymmdd(calendarioActual.getFechaFinSemana());


        ArrayList<ProgramacionSemanalDetalleBean> lstEntrevistasAgendadas = carteraCitaReunionController.obtenerCitasAgendas_ProgramacionSemanalDetalle(fechaInicioSemana, fechaFinSemana);



        return lstEntrevistasAgendadas;


    }

    /*
    public List<CitaBean> obtenerEntrevistasAgendadas(CalendarioProcesadoBean calendarioActual) {

        Log.d("PSH", "");
        Log.d("PSH", "#### obtenerEntrevistasAgendadas ");

        CarteraCitaReunionController citaReunionController = new CarteraCitaReunionController();

        List<DiaSemanaBean> lstDiaSemana = new ArrayList<>();

        short numeroDiasSemana = 1;
        short numeroDiasSemanaDesdeCero = 0;
        String primeraEntevista = "1";
        String segundaEntevista = "2";
        DateTime currentDateTime = calendarioActual.getFechaInicioSemana();

        List<CitaBean> lstTodasCitas = new ArrayList<>();

        // modificar con between
        while (numeroDiasSemana <= 7) {
            lstDiaSemana.add(obtenerDiaSemana(currentDateTime));
            List<CitaBean> lstCitasPrimeraEntrevista = citaReunionController.obtenerEntrevistasAgendadas(primeraEntevista, DateUtils.getDateStringddmmyyy(currentDateTime));
            List<CitaBean> lstCitasSegundaEntrevista = citaReunionController.obtenerEntrevistasAgendadas(segundaEntevista, DateUtils.getDateStringddmmyyy(currentDateTime));

            lstTodasCitas.addAll(lstCitasPrimeraEntrevista);
            lstTodasCitas.addAll(lstCitasSegundaEntrevista);

            currentDateTime = currentDateTime.plusDays(1);
            numeroDiasSemana++;
            numeroDiasSemanaDesdeCero++;

        }

        return lstTodasCitas;


    }*/

    public List<DiaSemanaBean> obtenerDiasSemanaActual() {

        List<DiaSemanaBean> lstDiaSemana = new ArrayList<>();
        int diaSemana = getCurrentCalendario().getFechaInicioSemana().getDayOfMonth();
        int diaFin = getCurrentCalendario().getFechaFinSemana().getDayOfMonth();

        int numeroDiasSemana = 1;
        DateTime currentDateTime = getCurrentCalendario().getFechaInicioSemana();


        ContextCompat.getColor(getContext(), R.color.gris_claro_texto5);

        while (numeroDiasSemana <= 7) {
            lstDiaSemana.add(obtenerDiaSemana(currentDateTime));

            currentDateTime = currentDateTime.plusDays(1);
            numeroDiasSemana++;
        }

        return lstDiaSemana;

    }

    public DiaSemanaBean obtenerDiaSemana(DateTime currentDateTime) {
        int currentDayOfMonth = currentDateTime.getDayOfMonth();
        int diaColor;
        if (DateUtils.dateIsBefore(currentDateTime, getFechaHoy())) {
            diaColor = ContextCompat.getColor(getContext(), R.color.gris_claro_texto5); // @color/gris_claro_texto5
        } else if (DateUtils.dateIsEqual(currentDateTime, getFechaHoy())) {
            diaColor = ContextCompat.getColor(getContext(), R.color.pacifico_celeste); // @color/pacifico_celeste
        } else {
            diaColor = ContextCompat.getColor(getContext(), R.color.gris_claro_texto4); // @color/gris_claro_texto4
        }
        String nombreDiaSemana = StringUtils.cambiarPrimeraLetraMayuscula(DateUtils.getDayShortNameOfWeekSpanish(currentDateTime));
        Log.d("PSF", "diaMes: " + currentDayOfMonth + " colorDia: " + diaColor + " nombreDiaSemana: " + DateUtils.getDayShortNameOfWeekSpanish(currentDateTime));

        switch (nombreDiaSemana) {

            case ("Tue"):
                nombreDiaSemana = "Martes";
                break;
            case ("Wed"):
                nombreDiaSemana = "Miercoles";
                break;
            case ("Thu"):
                nombreDiaSemana = "Jueves";
                break;
            case ("Fri"):
                nombreDiaSemana = "Viernes";
                break;
            case ("Sat"):
                nombreDiaSemana = "Sabado";
                break;
            case ("Sun"):
                nombreDiaSemana = "Domingo";
                break;
            case ("Mon"):
                nombreDiaSemana = "Lunes";
                break;
        }


        return new DiaSemanaBean(currentDayOfMonth, diaColor, nombreDiaSemana);
    }

    public String obtenerTitulo() {
        CalendarioProcesadoBean calendarioProcesadoBean = getCurrentCalendario();

        if (calendarioProcesadoBean == null) {
            return "";
        }

        String str_al = getResources().getString(R.string.fecha_al);
        String str_de = getResources().getString(R.string.fecha_de);
        String str_del = getResources().getString(R.string.fecha_del);

        DecimalFormat df = new DecimalFormat("00");
        String titulo = "";

        //Log.d("PSF", "obtenerSemanaCalendarioActual generarTitulo: " + calendarioProcesadoBean.getIdCalendario());

        StringBuffer sbTitulo = new StringBuffer("(");
        //CalendarioProcesadoBean calendarioBusqueda
        Log.d("PSF", "iy: " + calendarioProcesadoBean.getFechaInicioSemana().getYear() + " fy: " + calendarioProcesadoBean.getFechaFinSemana().getYear());
        Log.d("PSF", "imy: " + calendarioProcesadoBean.getFechaInicioSemana().getMonthOfYear() + " ify: " + calendarioProcesadoBean.getFechaFinSemana().getMonthOfYear());
        String mesInicio = DateUtils.getMonthNameSpanish(calendarioProcesadoBean.getFechaInicioSemana());
        String mesFin = DateUtils.getMonthNameSpanish(calendarioProcesadoBean.getFechaFinSemana());

        if (calendarioProcesadoBean.getFechaInicioSemana().getYear() != calendarioProcesadoBean.getFechaFinSemana().getYear()) {
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaInicioSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_de).append(" ");
            sbTitulo.append(mesInicio);
            sbTitulo.append(" ").append(str_del).append(" ");
            sbTitulo.append(calendarioProcesadoBean.getFechaInicioSemana().getYear());
            sbTitulo.append(" ").append(str_al).append(" ");
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaFinSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_de).append(" ");
            sbTitulo.append(mesFin);
            sbTitulo.append(" ").append(str_del).append(" ");
            sbTitulo.append(calendarioProcesadoBean.getFechaFinSemana().getYear());
        } else if (calendarioProcesadoBean.getFechaInicioSemana().getMonthOfYear() == calendarioProcesadoBean.getFechaFinSemana().getMonthOfYear()) {
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaInicioSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_al).append(" ");
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaFinSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_de).append(" ");
            sbTitulo.append(mesFin);
            sbTitulo.append(", ");
            sbTitulo.append(calendarioProcesadoBean.getFechaFinSemana().getYear());
        } else {
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaInicioSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_de).append(" ");
            sbTitulo.append(mesInicio);
            sbTitulo.append(" ").append(str_al).append(" ");
            sbTitulo.append(df.format(calendarioProcesadoBean.getFechaFinSemana().getDayOfMonth()));
            sbTitulo.append(" ").append(str_de).append(" ");
            sbTitulo.append(mesFin);
            sbTitulo.append(", ");
            sbTitulo.append(calendarioProcesadoBean.getFechaFinSemana().getYear());
        }
        sbTitulo.append(")");
        Log.d("PSF", "titulo psem2: " + sbTitulo.toString());

        return sbTitulo.toString();
    }

}
