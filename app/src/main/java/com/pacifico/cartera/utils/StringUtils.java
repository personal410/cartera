package com.pacifico.cartera.utils;

/**
 * Created by dsb on 13/06/2016.
 */
public class StringUtils{
    public static Integer isNullReturnZero(Integer cantidad) {
        if (cantidad==null) {
            return 0;
        }
        return cantidad;
    }
    public static String cambiarPrimeraLetraMayuscula(String texto) {
        if (texto != null && texto.length() > 1) {
            String mayuscula = texto.charAt(0) + "";
            mayuscula = mayuscula.toUpperCase();
            texto = texto.replaceFirst(texto.charAt(0) + "", mayuscula);
        }
        return texto;
    }

    public static String obtenerTextoBooleano(int codigo) {
        String respuesta = "";

        if (codigo == Constantes.codBooleanSi) {
            respuesta = Constantes.desBooleanSi;

        } else if (codigo == Constantes.codBooleanNo) {
            respuesta = Constantes.desBooleanNo;
        }

        return respuesta;

    }

    public static String obtenerTextoBooleano(String codigo) {
        String respuesta = "";

        if (codigo == null) {
            return respuesta;
        }

        if (codigo.equals(Constantes.codBooleanTrue)) {
            respuesta = Constantes.desBooleanSi;

        } else if (codigo.equals(Constantes.codBooleanFalse)) {
            respuesta = Constantes.desBooleanNo;
        }

        return respuesta;

    }

}
