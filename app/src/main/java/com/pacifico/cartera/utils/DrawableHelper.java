package com.pacifico.cartera.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

import java.util.HashMap;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.DrawableTexto;

/**
 * Created by dsb on 05/07/2016.
 */
public class DrawableHelper {

    private Context context;
    private Resources resources;

    public DrawableHelper(Context context, Resources resources) {
        this.setContext(context);
        this.setResources(resources);
    }

    /*
    public String obtenerTextoBoolean(String strBoolean) {
        Resources resources = getResources();
        String booleanSi = resources.getString(R.string.booleanSi);
        String booleanNo = resources.getString(R.string.booleanNo);
        String booleanTrue = resources.getString(R.string.booleanTrue);
        String booleanFalse = resources.getString(R.string.booleanFalse);

        String result = "-";

        if (strBoolean.trim().equals(booleanTrue)) {
            result = booleanSi;
        }
        if (strBoolean.trim().equals(booleanFalse)) {
            result = booleanNo;
        }
        return result;

    }*/


    public Map<Integer, Drawable> obtenerMapaDrawableEtapa() {
        Map<Integer, Drawable> mapDrawable = new HashMap<Integer, Drawable>();

        // etapas
        Drawable drwCirculoPlomo = LayoutUtils.getDrawable(getContext(), R.drawable.img_circulo_plomo);
        Drawable drwCirculoVerde = LayoutUtils.getDrawable(getContext(), R.drawable.img_circulo_verde);
        Drawable drwCirculoFucsia = LayoutUtils.getDrawable(getContext(), R.drawable.img_circulo_fucsia);
        Drawable drwCirculoPurpura = LayoutUtils.getDrawable(getContext(), R.drawable.img_circulo_purpura);
        Drawable drwCirculoCeleste = LayoutUtils.getDrawable(getContext(), R.drawable.img_circulo_celeste);

        mapDrawable.put(Constantes.codEtapa1ra, drwCirculoVerde);
        mapDrawable.put(Constantes.codEtapa2da, drwCirculoFucsia);
        mapDrawable.put(Constantes.codEtapaNuevo, drwCirculoCeleste);
        mapDrawable.put(Constantes.codEtapaAdicional, drwCirculoPurpura);

        return mapDrawable;
    }

    public Map<Integer, Drawable> obtenerMapaDrawableEstado() {
        Map<Integer, Drawable> mapDrawable = new HashMap<Integer, Drawable>();

        // estados
        Drawable drwIcnAgendada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_calendar);
        Drawable drwIcnRealizada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_green_check);
        Drawable drwIcnRecLlamada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_blue_call);
        //Drawable drwIcnRecLlamada = LayoutUtils.getDrawable(getContext(), R.drawable.llamada_celeste);
        Drawable drwIcnReAgendada = LayoutUtils.getDrawable(getContext(), R.drawable.schedule1);
        Drawable drwIcnRealizadaCerrada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_ic_done_all_blue);
        Drawable drwIcnNoInteresado = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_ic_cancel);
        Drawable drwIcnPorContactar = LayoutUtils.getDrawable(getContext(), R.drawable.ic_alert_blue);
        Drawable drwIcnNuevo = LayoutUtils.getDrawable(getContext(), R.drawable.ic_alert_orange);

        mapDrawable.put(Constantes.codEstadoPorContactar, drwIcnPorContactar);
        mapDrawable.put(Constantes.codEstadoAgendada, drwIcnAgendada);
        mapDrawable.put(Constantes.codEstadoRealizada, drwIcnRealizada);
        mapDrawable.put(Constantes.codEstadoReagendada, drwIcnReAgendada);
        mapDrawable.put(Constantes.codIcnRecDeLlamado, drwIcnRecLlamada);
        mapDrawable.put(Constantes.codIcnRealizadaCerrada, drwIcnRealizadaCerrada);
        mapDrawable.put(Constantes.codIcnNoInteresado, drwIcnNoInteresado);
        mapDrawable.put(Constantes.codIcnNuevo, drwIcnNuevo);


        return mapDrawable;
    }

    // key codigoEstado*100 + codigoResultado
    public Map<Integer, DrawableTexto> obtenerMapaDrawableEstadoReporte() {
        Map<Integer, DrawableTexto> mapDrawable = new HashMap<Integer, DrawableTexto>();

        // etapas
        Drawable drwIcnAgendada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_calendar);
        Drawable drwIcnRealizada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_green_check);
        Drawable drwIcnRecLlamada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_blue_call);
        Drawable drwIcnReAgendada = LayoutUtils.getDrawable(getContext(), R.drawable.schedule1);
        Drawable drwIcnRealizadaCerrada = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_ic_done_all_black_24dp);
        Drawable drwIcnNoInteresado = LayoutUtils.getDrawable(getContext(), R.drawable.ic_action_ic_cancel);
        Drawable drwIcnPorContactar = LayoutUtils.getDrawable(getContext(), R.drawable.ic_alert_blue);

        Resources resources = getResources();
        String estadoReportePorContactar = resources.getString(R.string.estadoReportePorContactar);
        String estadoReporteNoInteresado = resources.getString(R.string.estadoReporteNoInteresado);
        String estadoReporteVolverALlamar = resources.getString(R.string.estadoReporteVolverALlamar);
        String estadoReporteAgendada = resources.getString(R.string.estadoReporteAgendada);
        String estadoReporteReAgendada = resources.getString(R.string.estadoReporteReAgendada);
        String estadoReporteCliente = resources.getString(R.string.estadoReporteCliente);

        mapDrawable.put(Constantes.codEstadoPorContactar * 100 + Constantes.codResultado_pendiente, new DrawableTexto(drwIcnPorContactar, estadoReportePorContactar));
        mapDrawable.put(Constantes.codEstadoPorContactar * 100 + Constantes.codResultado_noInteresado, new DrawableTexto(drwIcnNoInteresado, estadoReporteNoInteresado));
        mapDrawable.put(Constantes.codEstadoPorContactar * 100 + Constantes.codResultado_volverALlamar, new DrawableTexto(drwIcnRecLlamada, estadoReporteVolverALlamar));

        mapDrawable.put(Constantes.codEstadoAgendada * 100 + Constantes.codResultado_pendiente, new DrawableTexto(drwIcnAgendada, estadoReporteAgendada));
        mapDrawable.put(Constantes.codEstadoAgendada * 100 + Constantes.codResultado_noInteresado, new DrawableTexto(drwIcnNoInteresado, estadoReporteNoInteresado));
        mapDrawable.put(Constantes.codEstadoAgendada * 100 + Constantes.codResultado_reagendado, new DrawableTexto(drwIcnReAgendada, estadoReporteReAgendada));
        mapDrawable.put(Constantes.codEstadoAgendada * 100 + Constantes.codResultado_porContactar, new DrawableTexto(drwIcnPorContactar, estadoReportePorContactar));

        mapDrawable.put(Constantes.codEstadoRealizada * 100 + Constantes.codResultado_cierreVenta, new DrawableTexto(drwIcnRealizadaCerrada, estadoReporteCliente));
        mapDrawable.put(Constantes.codEstadoRealizada * 100 + Constantes.codResultado_noInteresado, new DrawableTexto(drwIcnNoInteresado, estadoReporteNoInteresado));

        mapDrawable.put(Constantes.codEstadoReagendada * 100 + Constantes.codResultado_pendiente, new DrawableTexto(drwIcnReAgendada, estadoReporteReAgendada));

        return mapDrawable;
    }


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }
}
