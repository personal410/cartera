package com.pacifico.cartera.utils;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.CalendarioBean;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pacifico.cartera.Model.Bean.Auxiliar.CalendarioProcesadoBean;


/**
 * Created by dsb on 13/06/2016.
 */
public class CalendarioHelper {

    private ArrayList<CalendarioProcesadoBean> listCalendario;
    private Map<String, CalendarioProcesadoBean> mapCalendarioMes;
    private Map<Integer, CalendarioProcesadoBean> mapCalendarioById;

    //private Integer indiceSemanaActual;
    //(private Integer indiceSemanaFechaActual;
    private CalendarioProcesadoBean calendarioHoy;
    private CalendarioProcesadoBean currentCalendario;
    private DateTime fechaHoy;
    private Resources resources;
    private Context context;

    public CalendarioHelper(Context context, Resources resources) {
        this.setContext(context);
        this.setResources(resources);
    }

    public void init() {
        this.listCalendario = obtenerTodos();
        obtenerMapaMesCalendario();
        fechaHoy = DateUtils.getCurrentDate();
        setCalendarioHoy(obtenerSemanaCalendarioActual());
        currentCalendario = getCalendarioHoy();

    }

    public ArrayList<CalendarioProcesadoBean> obtenerTodos() {

        //Log.d("PC", "Calendario Controlller obtener todos");
        //Log.d("PC", " ");

        ArrayList<CalendarioProcesadoBean> listCalendario = new ArrayList<>();

        CalendarioBean bean = null;
        CalendarioProcesadoBean procesadoBean = null;

        Iterator<Entity> beans;

        beans = CalendarioBean.tableHelper.getEntities(
                null,
                null).iterator();

        while (beans.hasNext()) {
            bean = (CalendarioBean) beans.next();
            procesadoBean = new CalendarioProcesadoBean(bean);
            listCalendario.add(procesadoBean);
            //imprimirCalendario(procesadoBean);
        }

        return listCalendario;
    }

    public boolean modificarCalendarioAcual(int incremento) {
        int idCalendarioActual = currentCalendario.getIdCalendario();
        CalendarioProcesadoBean nuevoCalendario = incrementarIdCalendarioActual(incremento);
        if(nuevoCalendario!=null){
            currentCalendario = nuevoCalendario;
            return true;
        }else{
            return false;
        }

    }

    public CalendarioProcesadoBean incrementarIdCalendarioActual(int incremento) {
        CalendarioProcesadoBean nuevoCalendario = null;
        int idCalendarioActual = currentCalendario.getIdCalendario();
        int nuevoIdCalendario = idCalendarioActual + incremento;
        Log.d("PSF", "incrementarIdCalendarioActual " + "idCalendarioActual: " + idCalendarioActual + " nuevoIdCalendario: " + nuevoIdCalendario + " incremento: " + incremento);

        if (nuevoIdCalendario > 0) {
            nuevoCalendario = mapCalendarioById.get(nuevoIdCalendario);
        }

        return nuevoCalendario;
    }

    public CalendarioProcesadoBean obtenerSemanaCalendarioActual() {
        Log.d("PSF", "obtenerSemanaCalendarioActual fechaHoy: " + fechaHoy);

        int anioActual = fechaHoy.getYear();
        int mesAnioActual = fechaHoy.getMonthOfYear();

        String keyCalendarioMes = anioActual + "-" + mesAnioActual;
        CalendarioProcesadoBean calendarioMesBean = mapCalendarioMes.get(keyCalendarioMes);
        CalendarioProcesadoBean calendarioBusqueda = null;

        if(calendarioMesBean!=null) {
            int idCalendario = calendarioMesBean.getIdCalendario();
            boolean continua = true;
            int contador = 0;
            while (continua) {
                calendarioBusqueda = mapCalendarioById.get(idCalendario);
                Log.d("PSF", "obtenerSemanaCalendarioActual idCalendario: " + idCalendario);
                if (calendarioBusqueda == null) {
                    break;
                }

            /*
            if (calendarioBusqueda.getFechaInicioSemana().getYear() != anioActual || calendarioBusqueda.getFechaInicioSemana().getMonthOfYear() != mesAnioActual) {
                Log.d("PSF", "getAnio: " + calendarioBusqueda.getFechaInicioSemana().getYear() + " calendarioBusqueda.getMesAnio: " + calendarioBusqueda.getFechaInicioSemana().getMonthOfYear() );
                continua = false;
            } else
            */
                if (DateUtils.dateInsideInterval(calendarioBusqueda.getFechaInicioSemana(), calendarioBusqueda.getFechaFinSemana(), fechaHoy)) {
                    Log.d("PSF", "idCalendarioFechaHoy: " + calendarioBusqueda.getIdCalendario());
                    continua = false;
                }
                if (contador == 5) {
                    continua = false;
                }

                contador++;
                idCalendario++;
            }
        }
        return calendarioBusqueda;

    }

    public void obtenerMapaMesCalendario() {
        mapCalendarioMes = new HashMap<String, CalendarioProcesadoBean>();
        mapCalendarioById = new HashMap<Integer, CalendarioProcesadoBean>();
        if(listCalendario==null){
            return;
        }
        for (CalendarioProcesadoBean calendarioProcesadoBean : listCalendario) {

            //imprimirCalendario(calendarioProcesadoBean);
            // generarTitulo(calendarioProcesadoBean);
            mapCalendarioById.put(calendarioProcesadoBean.getIdCalendario(), calendarioProcesadoBean);
            // obtener la primera semana del mnes
            if (calendarioProcesadoBean.getSemanaMes() == 1) {
              //  Log.d("CH", "anio: " + calendarioProcesadoBean.getAnio() + " mesAnio: " + calendarioProcesadoBean.getMesAnio());
                mapCalendarioMes.put(calendarioProcesadoBean.getAnio() + "-" + calendarioProcesadoBean.getMesAnio(), calendarioProcesadoBean);
            }

        }

    }

    /*
    public ArrayList<CalendarioProcesadoBean> cargarListaCalendario() {


        ArrayList<CalendarioProcesadoBean> listCalendario = new ArrayList<>();
        //(int idCalendario, int anio, int semanaAnio, int mesAnio, DateTime fechaInicioSemana, org.joda.time.DateTime
        //fechaFinSemana, int codigoSucursal, int codigoCanal)

        listCalendario.add(new CalendarioProcesadoBean(1, 2016, 1, 1, 1, new DateTime("2015-12-29"), new DateTime("2016-01-04"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(2, 2016, 2, 1, 2, new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(3, 2016, 3, 1, 3, new DateTime("2016-01-12"), new DateTime("2016-01-18"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(4, 2016, 4, 1, 4, new DateTime("2016-01-19"), new DateTime("2016-01-25"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(5, 2016, 5, 1, 5, new DateTime("2016-01-26"), new DateTime("2016-02-01"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(6, 2016, 6, 2, 1, new DateTime("2016-02-02"), new DateTime("2016-02-08"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(7, 2016, 7, 2, 2, new DateTime("2016-02-09"), new DateTime("2016-02-15"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(8, 2016, 8, 2, 3, new DateTime("2016-02-16"), new DateTime("2016-02-22"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(9, 2016, 9, 2, 4, new DateTime("2016-02-23"), new DateTime("2016-02-29"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(10, 2016, 10, 3, 1, new DateTime("2016-03-01"), new DateTime("2016-03-07"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(11, 2016, 11, 3, 2, new DateTime("2016-03-08"), new DateTime("2016-03-14"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(12, 2016, 12, 3, 3, new DateTime("2016-03-15"), new DateTime("2016-03-21"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(13, 2016, 13, 3, 4, new DateTime("2016-03-22"), new DateTime("2016-03-28"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(14, 2016, 14, 4, 1, new DateTime("2016-03-29"), new DateTime("2016-04-04"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(15, 2016, 15, 4, 2, new DateTime("2016-04-05"), new DateTime("2016-04-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(16, 2016, 16, 4, 3, new DateTime("2016-04-12"), new DateTime("2016-04-18"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(17, 2016, 17, 4, 4, new DateTime("2016-04-19"), new DateTime("2016-04-25"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(18, 2016, 18, 4, 5, new DateTime("2016-04-26"), new DateTime("2016-05-02"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(19, 2016, 19, 5, 1, new DateTime("2016-05-03"), new DateTime("2016-05-09"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(20, 2016, 20, 5, 2, new DateTime("2016-05-10"), new DateTime("2016-05-16"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(21, 2016, 21, 5, 3, new DateTime("2016-05-17"), new DateTime("2016-05-23"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(22, 2016, 22, 5, 4, new DateTime("2016-05-24"), new DateTime("2016-05-30"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(23, 2016, 23, 6, 1, new DateTime("2016-05-31"), new DateTime("2016-06-06"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(24, 2016, 24, 6, 2, new DateTime("2016-06-07"), new DateTime("2016-06-13"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(25, 2016, 25, 6, 3, new DateTime("2016-06-14"), new DateTime("2016-06-20"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(26, 2016, 26, 6, 4, new DateTime("2016-06-21"), new DateTime("2016-06-27"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(27, 2016, 27, 7, 1, new DateTime("2016-06-28"), new DateTime("2016-07-04"), 0, 1));


        /*
        listCalendario.add(new CalendarioProcesadoBean(28, 2016, 28, 1, new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(29, 2016, 29, 1, new DateTime("2015-12-29"), new DateTime("2016-01-04"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(30, 2016, 30, 1, new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(31, 2016, 31, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(32, 2016, 32, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(33, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(34, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(35, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(36, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(37, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(38, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(39, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(40, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(41, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(42, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(43, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(44, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(45, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(46, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(47, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(48, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(49, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(50, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(51, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));
        listCalendario.add(new CalendarioProcesadoBean(52, 2016, 2, 1,  new DateTime("2016-01-05"), new DateTime("2016-01-11"), 0, 1));


        return listCalendario;
    }*/

    public void imprimirCalendario(CalendarioProcesadoBean bean) {
        StringBuffer sb = new StringBuffer();
        sb.append(" ic: ").append(bean.getIdCalendario());
        sb.append(" an: ").append(bean.getAnio());
        sb.append(" sa: ").append(bean.getSemanaAnio());
        sb.append(" ma: ").append(bean.getMesAnio());
        sb.append(" sm: ").append(bean.getSemanaMes());
        sb.append(" fi: ").append(bean.getFechaInicioSemana());
        sb.append(" ff: ").append(bean.getFechaFinSemana());
        sb.append(" cs: ").append(bean.getCodigoSucursal());
        sb.append(" cc: ").append(bean.getCodigoCanal());

        Log.d("PC", "PPCT: " + sb.toString());
    }

    public ArrayList<CalendarioProcesadoBean> getListCalendario() {
        return listCalendario;
    }

    public void setListCalendario(ArrayList<CalendarioProcesadoBean> listCalendario) {
        this.listCalendario = listCalendario;
    }

    public Map<String, CalendarioProcesadoBean> getMapCalendarioMes() {
        return mapCalendarioMes;
    }

    public void setMapCalendarioMes(Map<String, CalendarioProcesadoBean> mapCalendarioMes) {
        this.mapCalendarioMes = mapCalendarioMes;
    }

    public Map<Integer, CalendarioProcesadoBean> getMapCalendarioById() {
        return mapCalendarioById;
    }

    public void setMapCalendarioById(Map<Integer, CalendarioProcesadoBean> mapCalendarioById) {
        this.mapCalendarioById = mapCalendarioById;
    }

    public CalendarioProcesadoBean getCalendarioHoy() {
        return calendarioHoy;
    }

    public void setCalendarioHoy(CalendarioProcesadoBean calendarioHoy) {
        this.calendarioHoy = calendarioHoy;
    }

    public CalendarioProcesadoBean getCurrentCalendario() {
        return currentCalendario;
    }

    public void setCurrentCalendario(CalendarioProcesadoBean currentCalendario) {
        this.currentCalendario = currentCalendario;
    }

    public DateTime getFechaHoy() {
        return fechaHoy;
    }

    public void setFechaHoy(DateTime fechaHoy) {
        this.fechaHoy = fechaHoy;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
