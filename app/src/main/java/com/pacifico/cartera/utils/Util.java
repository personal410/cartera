package com.pacifico.cartera.utils;

import android.content.Context;

import com.google.gson.Gson;

import org.reflections.util.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by DSB on 07/08/2015.
 */
public class Util{
    public static String obtenerFechaActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar ahora = Calendar.getInstance();
        return simpleDateFormat.format(ahora.getTime());
    }
    public static <T> T getObjecToFile(Context context, String nameFile, Class<T> classOfT) {
        String json = getStringToAssets(context, nameFile);
        if (!Utils.isEmpty(nameFile)) {
            Gson gson = new Gson();
            return gson.fromJson(json, classOfT);
        } else {
            return null;
        }
    }
    public static String getStringToAssets(Context context, String nameFile) {
        StringBuilder buf = new StringBuilder();
        InputStream is = null;
        BufferedReader in = null;
        String json = null;
        try {
            is = context.getAssets().open("json/" + nameFile);

            in = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String str = null;
            while ((str = in.readLine()) != null) {
                buf.append(str);
            }
            in.close();
            json = buf.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String strNULL(Object object) {
        return object == null ? "" : object.toString();
    }


        public static String capitalizedString(String string){

            if(string != null && string.length() > 0){
                String[] arrString = string.split(" ");
                String strFinal = "";
                for(int i = 0; i < arrString.length; i++){
                    String tempString = arrString[i].trim();

                    if (tempString.length() > 1)
                        strFinal = strFinal + String.format("%s%s ", tempString.substring(0, 1), tempString.substring(1).toLowerCase());
                    else if (tempString.length() == 1)
                        strFinal = strFinal + tempString;
                }
                return strFinal.trim();
            }else{
                return "";
            }
        }

}