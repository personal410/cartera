package com.pacifico.cartera.utils.chart;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

/**
 * Created by dsb on 20/04/2016.
 */
public class IntegerValueFormatter implements ValueFormatter {


    public IntegerValueFormatter() {
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return String.valueOf((int)value);
    }
}
