package com.pacifico.cartera.utils;

public final class Constantes{
    public static final String URLBase = "https://desvconecta.pacificovida.net";
    public static final String MensajeErrorCampoVacio = "Por favor registra los datos para este campo";
    public static final String SemillaEncriptacion = "pacifico2016";


    public static final int monedaDefecto = 1; // soles
    public static final String simboloMonedaSoles = "S/"; // soles
    public static final String simboloMonedaDolares = "$"; // dolares

    public static final short campoOrdenNombreAsc = 1;
    public static final short campoOrdenNombreDesc = 2;
    public static final short campoOrdenOtro = 3;

    public static final String codTablaSexo = "1";
    public static final String codTablaEstadoCivil = "2";
    public static final String codTablaRangoEdad = "3";
    public static final String codTablaRangoIngresos = "4";
    public static final String codTablaTipoDocumento= "5";
    public static final String codTablaNacionalidad = "6";
    public static final String codTablaFuenteDelContacto = "7";
    public static final String codTablaEntornoDelReferente = "8";
    public static final String codTablaEtapaProspecto = "9";
    public static final String codTablaEstadoProspecto = "10";
    public static final String codTablaEstadoCita = "11";
    public static final String codTablaResultadoCita = "12";
    public static final String codTablaMoneda = "13";
    public static final String codTablaTipoReunion = "14";
    public static final String codTablaFrecuenciaPago = "15";
    public static final String codTablaTipoCobranza = "16";
    public static final String codTablaTipoFamiliar = "17";
    public static final String codTablaTipoReferido = "18";
    public static final String codTablaTipoEmpleo = "19";

    public static final String codTablaFlagHijo = "99";

    public static final String desReferenciador = "Ref. ";
    public static final String fechaFinalLimite = "3000-12-31";

    // Fragment
    public static final Integer fragmentTusContactos = 1;
    public static final Integer fragmentIndicadores = 2;
    public static final Integer fragmentProspecto = 3;
    public static final Integer fragmentEditarProspecto = 4;
    public static final Integer fragmentAjustes=5;

    // Estado

    public static final int MonedaSoles = 0;
    public static final int MonedaDolares = 1;

    public static final Integer codEstadoPorContactar = 1;
    public static final Integer codEstadoAgendada = 2;
    public static final Integer codEstadoRealizada = 3;
    public static final Integer codEstadoReagendada = 4;

    public static final Integer codIcnRecDeLlamado = -4; // NO EXISTE ESTADO solo para icono
    public static final Integer codIcnRealizadaCerrada = -5; // NO EXISTE ESTADO solo para icono
    public static final Integer codIcnNoInteresado = -6; // NO EXISTE ESTADO solo para icono
    public static final Integer codIcnNuevo = -7;


    // sexo

    public static final short codSexo_Masculino = 1;
    public static final String codStrSexo_Masculino = "M";
    public static final String desSexo_Masculino = "Masculino";

    public static final String codStrSexo_Femenino = "F";
    public static final short codSexo_Femenino = 0;
    public static final String desSexo_Femenino = "Femenino";

    // estado civil

    public static final short codEstadoCivil_Soltero = 1;
    public static final short codEstadoCivil_Casado = 2;
    public static final short codEstadoCivil_Viudo = 3;
    public static final short codEstadoCivil_Divorciado = 4;
    public static final short codEstadoCivil_Conviviente = 5;

    // Rango edad

    public static final short codRangoEdad_MenorA25 = 1;
    public static final short codRangoEdad_MayorA24MenorIgualA30 = 2;
    public static final short codRangoEdad_MayorA30MenorIgualA40 = 3;
    public static final short codRangoEdad_MayorA40 = 4;

    // Ingresos

    public static final short codIngresosSinDefinir = 0;
    public static final String desIngresosSinDefinir = "-";
    public static final short codIngresosNivel1 = 1;
    public static final short codIngresosNivel2 = 2;
    public static final short codIngresosNivel3 = 3;
    public static final short codIngresosNivel4 = 4;

    // Booleano

    public static final short codBooleanNo = 0;
    public static final String codBooleanFalse = "False";
    public static final String desBooleanNo = "No";

    public static final short codBooleanSi = 1;
    public static final String codBooleanTrue = "True";
    public static final String desBooleanSi = "Si";

    // Tiene Hijos
    public static final short codFlagTieneHijos = 1;
    //public static final String desFlagTieneHijos = "Si tiene hijos";
    public static final short codFlagNoTieneHijos = 0;
    //public static final String desFlagNoTieneHijos = "No tiene hijos";


    // Etapa

    public static final Integer codEtapaNuevo = 1;
    public static final Integer codEtapa1ra = 2;
    public static final Integer codEtapa2da = 3;
    public static final Integer codEtapaAdicional = 4;

    // Tabla id 10 - Estado del Prospecto
    public static final int EstadoProspecto_Nuevo = 1;
    public static final int EstadoProspecto_EnProceso = 2;
    public static final int EstadoProspecto_CierreVenta = 3;
    public static final int EstadoProspecto_NoInteresado = 4;

    // resultados

    public static final Integer codResultado_pendiente = 0;
    public static final Integer codResultado_cierreVenta = 1;
    public static final Integer codResultado_siguienteEtapa = 2;
    public static final Integer codResultado_noInteresado = 3;
    public static final Integer codResultado_porContactar = 4;
    public static final Integer codResultado_reagendado = 5;
    public static final Integer codResultado_volverALlamar = 6;

    public static final String codResultado_recLlamado = "Rec. de llamado";
    public static final String codResultado_reagendada = "Reagendada";

    // fuentes
    public static final String codFuente_p200 = "1";
    public static final String codFuente_adn = "2";
    public static final String codFuente_centro_influencia = "3";
    public static final String codFuente_campanha = "4";
    public static final String codFuente_op = "5";
    public static final String codFuente_referidos = "6";

    // tipoFamiliar

    public static final Integer codTipoFamiliar_conyuge = 1;
    public static final Integer codTipoFamiliar_hijos = 2;

    // tus contactos caso tipo

    public static final Integer codTusContactosRealizadoNoInteresado = 1;
    public static final Integer codTusContactosAgendadoOPorContactar= 2;
    public static final Integer codTusContactosReagendado= 3;
    public static final Integer codTusContactosRecordatorioLlamada= 3;

    // indicadores

    public static final Short indicadores_primergrafico = 1;
    public static final Short indicadores_segundografico = 2;
    public static final Short indicadores_tercergrafico = 3;

    // pruebas
    public static final Boolean booleanAgregarRecordatorioLlamadaTusContactos = false;
    public static final Boolean booleanSincronizarDesdeServicio = true;

    public static final int constante_si= 1;

    // Tabla id 3
    public static final int RangoEdad_menor25= 1;
    public static final int RangoEdad_entre25_30= 2;
    public static final int RangoEdad_entre31_40= 3;
    public static final int RangoEdad_mayor41= 4;
}