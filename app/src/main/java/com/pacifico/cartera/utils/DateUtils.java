package com.pacifico.cartera.utils;

import android.util.Log;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by dsb on 15/05/2016.
 */
public class DateUtils {

    public static Date createDateFromString(String strDate) {
        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = format.parse(strDate);
            System.out.println(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static String getDateStringddmmyyy(DateTime dateTime) {

        // Log.d("DU", "getDateStringddmmyyy: " + dateTime.toString());
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy");
        // System.out.println(dtfOut.print(dateTime));

        return dtfOut.print(dateTime);
    }

    public static String getDateStringyymmdd(DateTime dateTime) {

        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");

        return dtfOut.print(dateTime);
    }

    public static DateTime createDateTimeFromString(String strDate) {
        if (isNullOrEmpty(strDate)) {
            return null;
        }
        DateTime dateTime = null;
        DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd");
        try {
            dateTime = format.parseDateTime(strDate);
            //     System.out.println(dateTime);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dateTime;
    }

    public static DateTime createDateTimeFromString_ddmmyy(String strDate) {

        if (isNullOrEmpty(strDate)) {
            return null;
        }

        DateTime dateTime = null;
        DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/yyyy");
        try {
            dateTime = format.parseDateTime(strDate);
            //     System.out.println(dateTime);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dateTime;
    }

    // dd/mm/yyyy to yyyy-MM-dd
    public static String changeDateFormat(String strDate) {

        if (isNullOrEmpty(strDate)) {
            return "";
        }

        Log.d("DU", "changeDateFormat: " + strDate);

// Format for input
        DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
// Parsing the date
        DateTime dateTime = null;
        try {
            dateTime = dtf.parseDateTime(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
// Format for output
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");
// Printing the date
        System.out.println(dtfOut.print(dateTime));

        return dtfOut.print(dateTime);

    }

    // https://www.adictosaltrabajo.com/tutoriales/introduccion-joda-time/
    // http://www.joda.org/joda-time/userguide.html
    // http://joda-time.sourceforge.net/apidocs/org/joda/time/format/DateTimeFormat.html

    public static int getDayOfWeek(DateTime dt) {
        return dt.getDayOfWeek();
    }

    public static int getDayOfMonth(DateTime dt) {
        return dt.getDayOfMonth();
    }


    // 1 lunes
    // Lun
    public static String getDayShortNameOfWeekSpanish(DateTime dt) {
        //DateTime.Property pDoW = dt.dayOfWeek();
        //return pDoW.getAsShortText(new Locale("es_ES"));
        return obtenerSemanaCortoEspanol(getDayOfWeek(dt));
    }

    // Lunes
    public static String getDayOfWeekNameSpanish(DateTime dt) {
        //DateTime.Property pDoW = dt.dayOfWeek();
        //return pDoW.getAsText(new Locale("es_ES"));
        return obtenerSemanaEspanol(getDayOfWeek(dt));
    }

    // Enero
    public static String getMonthNameSpanish(DateTime dt) {
        //return dt.monthOfYear().getAsText(new Locale("es_ES"));
        return obtenerMesEspanol(getMonthOfYear(dt));
    }

    // Ene
    public static String getMonthShortNameSpanish(DateTime dt) {
        //return dt.monthOfYear().getAsShortText(new Locale("es_ES"));
        return obtenerMesCortoEspanol(getMonthOfYear(dt));
    }


    public static int getWeekOfYear(DateTime dt) {
        return dt.getWeekOfWeekyear();
    }

    public static int getMonthOfYear(DateTime dt) {
        return dt.getMonthOfYear();
    }

    public static String getMonthName(DateTime dt) {
        return dt.monthOfYear().getAsText();
    }

    public static DateTime convertToJodaTime(Date date) {
        return new DateTime(date);
    }

    public static Date convertFromJodaTime(DateTime dt) {
        return dt.toDate();
    }

    public static boolean dateInsideInterval(DateTime dtBegin, DateTime dtEnd, DateTime date) {
        if (dtBegin == null || dtEnd == null || date == null) {
            return false;
        }
        DateTime dtBeginAtMidnight = dtBegin.withTimeAtStartOfDay();
        DateTime dtEndAtMidnight = dtEnd.withTimeAtStartOfDay();
        //DateTime dateAtMidnight = DateTime.now().withTimeAtStartOfDay();
        DateTime dateAtMidnight = date.withTimeAtStartOfDay();

        if ((dateAtMidnight.isAfter(dtBeginAtMidnight) || dateAtMidnight.isEqual(dtBeginAtMidnight)) && (dateAtMidnight.isBefore(dtEndAtMidnight) || dateAtMidnight.isEqual(dtEndAtMidnight))) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean dateIsBeforeOrEqual(DateTime dtBegin, DateTime date) {
        if (dtBegin == null || date == null) {
            return false;
        }
        DateTime dtBeginAtMidnight = dtBegin.withTimeAtStartOfDay();
        DateTime dateAtMidnight = date.withTimeAtStartOfDay();
        return (dateAtMidnight.isAfter(dtBeginAtMidnight) || dateAtMidnight.isEqual(dtBeginAtMidnight));
    }

    public static boolean dateIsBefore(DateTime dtBegin, DateTime date) {
        if (dtBegin == null || date == null) {
            return false;
        }
        DateTime dtBeginAtMidnight = dtBegin.withTimeAtStartOfDay();
        DateTime dateAtMidnight = date.withTimeAtStartOfDay();
        return (dateAtMidnight.isAfter(dtBeginAtMidnight));
    }

    public static DateTime convertStringToDateTime2(String strDate) {
        Log.d("DU", "convertStringToDateTime: " + strDate);
        if (isNullOrEmpty(strDate)) {
            return null;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
        DateTime dt = null;
        try {
            dt = formatter.parseDateTime(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dt;
    }

    // colocar try catch
    public static DateTime convertStringToDateTime(String strDate) {
        Log.d("DU", "convertStringToDateTime: " + strDate);
        if (isNullOrEmpty(strDate)) {
            return null;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTime dt = null;
        try {
            dt = formatter.parseDateTime(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dt;
    }

    public static DateTime convertStringToDate(String strDate) {
        Log.d("DU", "convertStringToDateTime: " + strDate);

        if (isNullOrEmpty(strDate)) {
            return null;
        }

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime dt = null;
        try {
            dt = formatter.parseDateTime(strDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dt;
    }

    public static boolean isNullOrEmpty(String str) {
        if (str == null || str.isEmpty()) {
            return true;
        }
        return false;
    }

    public static boolean dateIsEqual(String strDate1, String strDate2) {
        Log.d("DU", "dateIsEqual strDate1: " + strDate1 + " strDate2: " + strDate2);

        if (isNullOrEmpty(strDate1) || isNullOrEmpty(strDate2)) {
            return false;
        }
        DateTime dtFecha1 = DateUtils.convertStringToDateTime(strDate1);
        DateTime dtFecha2 = DateUtils.convertStringToDateTime(strDate2);

        DateTime dtFecha1AtMidnight = dtFecha1.withTimeAtStartOfDay();
        DateTime dtFecha2AtMidnight = dtFecha2.withTimeAtStartOfDay();
        return (dtFecha1AtMidnight.isEqual(dtFecha2AtMidnight));
    }

    public static boolean dateIsEqual(DateTime dtBegin, DateTime date) {
        if (dtBegin == null || date == null) {
            return false;
        }
        DateTime dtBeginAtMidnight = dtBegin.withTimeAtStartOfDay();
        DateTime dateAtMidnight = date.withTimeAtStartOfDay();
        return (dateAtMidnight.isEqual(dtBeginAtMidnight));
    }

    public static DateTime getCurrentDate() {
        return new DateTime();
    }

    public static String getCurrentDateString() {
        return getDateStringyymmdd(getCurrentDate());
    }

    public static boolean currentdateInsideInterval(DateTime dtBegin, DateTime dtEnd) {
        return dateInsideInterval(dtBegin, dtEnd, new DateTime());
    }

    // DateTimeFormatter f = DateTimeFormat.forPattern("E, d MMM yyyy HH:mm:ss").withLocale(Locale.US);


    // lun 6, ene
    public static String getDateFormat01(DateTime dt) {
        if (dt == null) {
            return "";
        }
        return getDayShortNameOfWeekSpanish(dt) + " " + getDayOfMonth(dt) + ", " + getMonthShortNameSpanish(dt);
    }

    // Lun 6, Ene
    public static String getDateFormat01Upper(DateTime dt) {
        if (dt == null) {
            return "";
        }
        return getDayShortNameOfWeekSpanish(dt) + " " + getDayOfMonth(dt) + ", " + getMonthShortNameSpanish(dt);
    }

    // Lun 6, Ene
    public static String getDateTimeFormat01Upper(DateTime dt) {
        return StringUtils.cambiarPrimeraLetraMayuscula(getDayShortNameOfWeekSpanish(dt)) + " " + getDayOfMonth(dt) + ", " + StringUtils.cambiarPrimeraLetraMayuscula(getMonthShortNameSpanish(dt));
    }

    // Lunes 6, Enero
    public static String getDateFormat02(DateTime dt) {
        return getDayOfWeekNameSpanish(dt) + " " + getDayOfMonth(dt) + ", " + getMonthNameSpanish(dt);
    }

    // Lun 6, Ene 11:00 a.m.
    public static String getDateFormat03(DateTime dt) {
        return getDateFormat01(dt) + "" + dt.toString("hh:mm") + " " + getamorpm(dt);
    }

    public static String getDateFormat12(DateTime dt) {
        if (dt == null) {
            return "";
        }
        return getDayOfMonth(dt) + " " + getMonthShortNameSpanish(dt) + " " + dt.getYear();
    }

    public static String getDateFormat15(String fecha) {
        Log.d("DU", "getDateFormat03 fecha: " + fecha);
        String fechaFormato = "";
        if (fecha != null) {
            fechaFormato = getDateFormat03Datetime(convertStringToDateTime(fecha));
        }
        return fechaFormato;
    }

    public static String getDateFormat03(String fecha) {
        Log.d("DU", "getDateFormat03 fecha: " + fecha);
        String fechaFormato = "";
        if (fecha != null) {
            fechaFormato = getDateFormat03Datetime(convertStringToDateTime(fecha));
        }
        return fechaFormato;
    }

    public static String getDateFormat04(String fecha) {
        Log.d("DU", "getDateFormat03 fecha: " + fecha);
        String fechaFormato = "";
        if (fecha != null) {
            fechaFormato = getDateFormat03Datetime(convertStringToDateTime2(fecha));
        }
        return fechaFormato;
    }


    public static String getDateFormat03(String fecha, String hora) {
        Log.d("DU", "getDateFormat03 fecha: " + fecha + " hora: " + hora);
        String fechaHora = "";
        if (fecha != null && hora != null) {
            fechaHora = fecha + " " + hora;
            fechaHora = getDateFormat03Datetime(convertStringToDateTime(fechaHora));
        }
        return fechaHora;
    }


    public static String getDateFormat03Datetime(DateTime dt) {
        if (dt == null) {
            return "";
        }
        return getDateFormat01(dt) + " " + dt.toString("hh:mm") + " " + getamorpm(dt);
    }

    public static String getamorpm(DateTime dt) {
        //twelveHourClockFormat = new DateTimeFormatterBuilder().appendPattern("hh:mm aa").toFormatter();
        if (dt == null) {
            return "";
        }
        String amorpm = dt.toString("a");

        String newamorpm = "";
        if (amorpm.equals("a.m.")) {
            newamorpm = "AM";
        } else {
            newamorpm = "PM";
        }
        Log.d("DU", "getamorpm: " + amorpm + " newamorpm: " + newamorpm);
        return newamorpm;
    }

    // 03 Feb 2016
    public static String getDateFormat04(DateTime dt) {
        return DateTimeFormat.forPattern("dd MMM YYYY").withLocale(new Locale("es_ES")).print(dt);
    }

    // 10 de Enero, 1977
    public static String getDateFormat05(DateTime dt) {
        return DateTimeFormat.forPattern("dd 'de' MMM ',' YYYY").withLocale(new Locale("es_ES")).print(dt);
    }

    // 9 Ene, 2016 9:30 AM
    public static String getDateFormat06(DateTime dt) {
        String date = DateTimeFormat.forPattern("d MMM ',' YYYY hh:mm").withLocale(new Locale("es_ES")).print(dt);
        return date;
    }

    // Mar, 8 Ene 11:00 AM
    public static String getDateFormat07(DateTime dt) {
        String date = DateTimeFormat.forPattern("E ,' d MMM YYYY hh:mm").withLocale(new Locale("es_ES")).print(dt);
        return date;
    }

    // 10 de Enero, 1977
    public static String getDateFormat20(String str_de, DateTime fecha) {

        //String str_de = getResources().getString(R.string.fecha_de);

        DecimalFormat df = new DecimalFormat("00");

        if (fecha == null) {
            return "";
        }

        StringBuffer sbTitulo = new StringBuffer("");
        sbTitulo.append(df.format(fecha.getDayOfMonth()));
        sbTitulo.append(" ").append(str_de).append(" ");
        sbTitulo.append(DateUtils.getMonthNameSpanish(fecha));
        sbTitulo.append(", ");
        sbTitulo.append(fecha.getYear());

        return sbTitulo.toString();
    }

    // 10 de Enero, 1977
    public static String getDateFormat21(DateTime fecha) {


        DecimalFormat df = new DecimalFormat("00");

        if (fecha == null) {
            return "";
        }

        StringBuffer sbTitulo = new StringBuffer("");
        sbTitulo.append(df.format(fecha.getDayOfMonth()));
        sbTitulo.append(" ").append(DateUtils.getMonthShortNameSpanish(fecha));
        sbTitulo.append(", ").append(fecha.getYear());
        sbTitulo.append(" ").append(fecha.toString("hh:mm"));
        sbTitulo.append(" ").append(getamorpm(fecha));

        Log.d("DU", "getDateFormat21: " + sbTitulo.toString());

        return sbTitulo.toString();
    }

    public static String obtenerSemanaCortoEspanol(int diaSemana) {

        String nombreSemana = "";
        switch (diaSemana) {
            case 1:
                nombreSemana = "Lun";
                break;
            case 2:
                nombreSemana = "Mar";
                break;
            case 3:
                nombreSemana = "Mié";
                break;
            case 4:
                nombreSemana = "Jue";
                break;
            case 5:
                nombreSemana = "Vie";
                break;
            case 6:
                nombreSemana = "Sáb";
                break;
            case 7:
                nombreSemana = "Dom";
                break;
        }
        return nombreSemana;
    }

    public static Integer calcularEdad(DateTime fechaNacimiento) {
        LocalDate fechaNacimientoLocal = new LocalDate(fechaNacimiento);
        LocalDate now = new LocalDate();
        Years age = Years.yearsBetween(fechaNacimientoLocal, now);
        return new Integer(age.getYears());
    }

    public static String obtenerSemanaEspanol(int diaSemana) {

        String nombreSemana = "";
        switch (diaSemana) {
            case 1:
                nombreSemana = "Lunes";
                break;
            case 2:
                nombreSemana = "Martes";
                break;
            case 3:
                nombreSemana = "Miércoles";
                break;
            case 4:
                nombreSemana = "Jueves";
                break;
            case 5:
                nombreSemana = "Viernes";
                break;
            case 6:
                nombreSemana = "Sábado";
                break;
            case 7:
                nombreSemana = "Domingo";
                break;
        }
        return nombreSemana;
    }

    public static String obtenerMesEspanol(int mes) {

        String nombreMes = "";
        switch (mes) {
            case 1:
                nombreMes = "Enero";
                break;
            case 2:
                nombreMes = "Febrero";
                break;
            case 3:
                nombreMes = "Marzo";
                break;
            case 4:
                nombreMes = "Abril";
                break;
            case 5:
                nombreMes = "Mayo";
                break;
            case 6:
                nombreMes = "Junio";
                break;
            case 7:
                nombreMes = "Julio";
                break;
            case 8:
                nombreMes = "Agosto";
                break;
            case 9:
                nombreMes = "Setiembre";
                break;
            case 10:
                nombreMes = "Octubre";
                break;
            case 11:
                nombreMes = "Noviembre";
                break;
            case 12:
                nombreMes = "Diciembre";
                break;
        }
        return nombreMes;
    }

    public static String obtenerMesCortoEspanol(int mes) {

        String nombreMes = "";
        switch (mes) {
            case 1:
                nombreMes = "Ene";
                break;
            case 2:
                nombreMes = "Feb";
                break;
            case 3:
                nombreMes = "Mar";
                break;
            case 4:
                nombreMes = "Abr";
                break;
            case 5:
                nombreMes = "May";
                break;
            case 6:
                nombreMes = "Jun";
                break;
            case 7:
                nombreMes = "Jul";
                break;
            case 8:
                nombreMes = "Ago";
                break;
            case 9:
                nombreMes = "Sep";
                break;
            case 10:
                nombreMes = "Oct";
                break;
            case 11:
                nombreMes = "Nov";
                break;
            case 12:
                nombreMes = "Dic";
                break;
        }
        return nombreMes;
    }


}
