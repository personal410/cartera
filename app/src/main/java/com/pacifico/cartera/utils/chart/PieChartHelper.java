package com.pacifico.cartera.utils.chart;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;
import java.util.List;

import com.pacifico.cartera.Model.Bean.Auxiliar.IndiceDatoBean;

/**
 * Created by dsb on 20/04/2016.
 */
public class PieChartHelper {

    private PieChart mChart;

    private Typeface tf;

    private Context ctx;

    public PieChartHelper(Context ctx){
        this.ctx = ctx;
    }

    public void create(PieChart chart){
        mChart = chart;
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110); // 110

        mChart.setHoleRadius(62f); // 58f

        mChart.setDrawCenterText(true);

        mChart.setTouchEnabled(false);

        mChart.setRotationAngle(270);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(false);

        mChart.getLegend().setEnabled(false);

        mChart.setDescription("");
    }

    public void setTf(Typeface tf) {
        this.tf = tf;
    }

    //public void animate(){
    //    mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
    //}

    public void setData(String titulo, List<IndiceDatoBean> lstGraficoDatos, int[] colors){
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> labels = new ArrayList<>();
        float valor = 0f;

        for(IndiceDatoBean graficoDatoBean :lstGraficoDatos){
            valor = (float)graficoDatoBean.getValor();
            entries.add(new Entry(valor, graficoDatoBean.getIndice()));
            labels.add("");
        }

        PieDataSet dataSet = new PieDataSet(entries, titulo);
        dataSet.setColors(colors, ctx);
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        PieData data = new PieData(labels, dataSet);

        data.setValueFormatter(new IntegerValueFormatter());

        data.setValueTextSize(18f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(tf);
        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    public void setSpannableText(String[] textos, int[] colores, float[] tamanosTexto, int[] typeFace){
        mChart.setCenterText(generateCenterSpannableText(textos, colores, tamanosTexto, typeFace));
    }

    private SpannableString generateCenterSpannableText(){
        SpannableString s = new SpannableString("MPAndroidChart developed by Philipp Jahoda");
         s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        return s;
    }

    private SpannableString generateCenterSpannableText(String[] textos, int[] colores, float[] tamanosTexto, int[] typeFace) {

        // validar que objetos no sean nulos y tengan el mismo tamanho
        String spanTexto = "";

        for(int i=0; i<textos.length; i++){
            if(i>0) {
                spanTexto = spanTexto + "\n";
            }
            spanTexto = spanTexto + textos[i];
        }
        Log.d("PieChartHelper", "spanTexto: " + spanTexto);

        SpannableString s = new SpannableString(spanTexto);

        int indiceTamanoTexto = 0;
        int indiceTexto = 0;

        for(int i=0; i<colores.length; i++) {
            if(i>0) {
                indiceTexto = indiceTexto + 1;
            }
            indiceTamanoTexto = textos[i].length() + indiceTexto;
            Log.d("PieChartHelper", "indiceTexto " + indiceTexto);
            Log.d("PieChartHelper", "indiceTamanoTexto [" + i + "] = " + indiceTamanoTexto);
            Log.d("PieChartHelper", "colores [" + i + "] = " + colores[i]);
            s.setSpan(new RelativeSizeSpan(tamanosTexto[i]), indiceTexto, indiceTamanoTexto, 0);
            s.setSpan(new StyleSpan(typeFace[i]), indiceTexto, indiceTamanoTexto, 0);
            s.setSpan(new ForegroundColorSpan(colores[i]), indiceTexto, indiceTamanoTexto, 0);
            indiceTexto = indiceTamanoTexto;
        }

        return s;
    }


}
