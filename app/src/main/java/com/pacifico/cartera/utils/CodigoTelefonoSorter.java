package com.pacifico.cartera.utils;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.Comparator;

public class CodigoTelefonoSorter implements Comparator<TablaTablasBean>{
    @Override
    public int compare(TablaTablasBean first, TablaTablasBean second){
        return first.getValorCadena().compareToIgnoreCase(second.getValorCadena());
    }
}