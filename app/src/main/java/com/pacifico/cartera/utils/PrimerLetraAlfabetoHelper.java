package com.pacifico.cartera.utils;

/**
 * Created by dsb on 11/05/2016.
 */
public class PrimerLetraAlfabetoHelper {
    private String[] arrayAbecedario = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
    private String[] arrayAbecedarioDesc = {"Z","Y","X","W","V","U","T","S","R","Q","P","O","N","M","L","K","J","I","H","G","F","E","D","C","B","A"};

    private boolean ordenAsc = true;
    private int indiceArray = 0;
    private int numeroCoincidencias = 0;
    private int tamanoAbecedario = 0;
    private String primeraLetraUpper = "";
    public PrimerLetraAlfabetoHelper(boolean ordenAsc) {
        this.ordenAsc = ordenAsc;
        indiceArray = 0;
        numeroCoincidencias = 0;
        tamanoAbecedario = 0;
        primeraLetraUpper = "";
        tamanoAbecedario = getTamanoAbecedario();
    }

    private int getTamanoAbecedario(){
        if(ordenAsc){
            return arrayAbecedario.length;
        }else{
            return arrayAbecedarioDesc.length;
        }
    }

    private String getPrimerLetra(String nombre){
        if(nombre!=null && nombre.length()>1){
            return nombre.substring(0,1);
        }else{
            return "";
        }

    }

    private String getLetraAbcedarioPorIndiceActual(){
        if(ordenAsc){
            //Log.d("PLH", "getLetraAbcedarioPorIndiceActual asc: " + arrayAbecedario[indiceArray]);
            return arrayAbecedario[indiceArray];
        }else{
            //Log.d("PLH", "getLetraAbcedarioPorIndiceActual desc: " + arrayAbecedarioDesc[indiceArray]);
        return arrayAbecedarioDesc[indiceArray];
    }

}

    public String getPrimeraLetraUnaVez(String valor){
        primeraLetraUpper = getPrimerLetra(valor).toUpperCase();
        //Log.d("PLH", "Primera letra: " + primeraLetraUpper + " valor: " + valor);

        while(indiceArray<tamanoAbecedario){

            //Log.d("PLH", "indiceArray: " + indiceArray + " numeroCoincidencias: " +numeroCoincidencias);

            if(primeraLetraUpper.equals(getLetraAbcedarioPorIndiceActual())){
                numeroCoincidencias++;
                if(numeroCoincidencias==1){
                    return primeraLetraUpper;
                }
                break;
            }else {
                numeroCoincidencias = 0;
            }
            indiceArray++;
        }

        // para caracteres que no corresponden alfabeto
        if(indiceArray==tamanoAbecedario && numeroCoincidencias==0){
            indiceArray = 0;
        }

        return "";
    }


}
