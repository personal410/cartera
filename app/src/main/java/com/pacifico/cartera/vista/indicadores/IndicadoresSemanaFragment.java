package com.pacifico.cartera.vista.indicadores;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraCitaReunionController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraProspectoController;
import com.pacifico.cartera.Model.Bean.Auxiliar.EstiloGraficoCircular;
import com.pacifico.cartera.Model.Bean.Auxiliar.IndiceDatoBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.SemanaPreviaDatoBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;
import com.pacifico.cartera.utils.IndicadoresSemanaHelper;
import com.pacifico.cartera.utils.StringUtils;
import com.pacifico.cartera.utils.chart.PieChartHelper;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

/**
 * Created by dsb on 22/04/2016.
 */
public class IndicadoresSemanaFragment extends Fragment {

    private PieChartHelper pch1rasEntrevistas;
    private PieChartHelper pch2dasEntrevistas;
    private PieChartHelper pchCierres;

    private IndicadoresSemanaHelper helper;

    private TextViewCustomFont tvTituloPrincipal;

    private TextViewCustomFont tv1rasNoRealizadasDescripcion;
    private TextViewCustomFont tv1rasNoRealizadasValor;
    private TextViewCustomFont tv1rasRealizadasDescripcion;
    private TextViewCustomFont tv1rasRealizadasValor;
    private TextViewCustomFont tvTotal1rasAgendadasDescripcion;
    private TextViewCustomFont tvTotal1rasAgendadasValor;

    private TextViewCustomFont tv2dasNoRealizadasDescripcion;
    private TextViewCustomFont tv2dasNoRealizadasValor;
    private TextViewCustomFont tv2dasRealizadasDescripcion;
    private TextViewCustomFont tv2dasRealizadasValor;
    private TextViewCustomFont tvTotal2dasAgendadasDescripcion;
    private TextViewCustomFont tvTotal2dasAgendadasValor;

    private TextViewCustomFont tvFamiliasDesprotegidasDescripcion;
    private TextViewCustomFont tvFamiliasDesprotegidasValor;
    private TextViewCustomFont tvFamiliasProtegidasDescripcion;
    private TextViewCustomFont tvFamiliasProtegidasValor;
    private TextViewCustomFont tvTotal2dasRealizadasDescripcion;
    private TextViewCustomFont tvTotal2dasRealizadasValor;

    private TextViewCustomFont tvPromediosReferidosADNValor;
    private TextViewCustomFont tvPromediosReferidosADNDescripcion;
    private TextViewCustomFont tvPromediosReferidosSemanaValor;
    private TextViewCustomFont tvPromediosReferidosSemanaDescripcion;

    private Resources viewResources;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.idcd_sepr_frgm, container, false);

        Context ctx = getActivity().getApplicationContext();
        setViewResources(getResources());
        getResourceItems(v);
        //actualizarDatos();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        actualizarDatos();
    }

    public void getResourceItems(View v) {

        String typeFaceFocobd = "foco_std_bd-webfont.ttf";
        Typeface typefaceFocBd = Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd);
        tvTituloPrincipal = (TextViewCustomFont) v.findViewById(R.id.tvTitle24);

        tv1rasNoRealizadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tv1rasNoRealizadasDescripcion);
        tv1rasNoRealizadasValor = (TextViewCustomFont) v.findViewById(R.id.tv1rasNoRealizadasValor);
        tv1rasRealizadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tv1rasRealizadasDescripcion);
        tv1rasRealizadasValor = (TextViewCustomFont) v.findViewById(R.id.tv1rasRealizadasValor);
        tvTotal1rasAgendadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tvTotal1rasAgendadasDescripcion);
        tvTotal1rasAgendadasValor = (TextViewCustomFont) v.findViewById(R.id.tvTotal1rasAgendadasValor);

        tv2dasNoRealizadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tv2dasNoRealizadasDescripcion);
        tv2dasNoRealizadasValor = (TextViewCustomFont) v.findViewById(R.id.tv2dasNoRealizadasValor);
        tv2dasRealizadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tv2dasRealizadasDescripcion);
        tv2dasRealizadasValor = (TextViewCustomFont) v.findViewById(R.id.tv2dasRealizadasValor);
        tvTotal2dasAgendadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tvTotal2dasAgendadasDescripcion);
        tvTotal2dasAgendadasValor = (TextViewCustomFont) v.findViewById(R.id.tvTotal2dasAgendadasValor);

        tvFamiliasDesprotegidasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tvFamiliasDesprotegidasDescripcion);
        tvFamiliasDesprotegidasValor = (TextViewCustomFont) v.findViewById(R.id.tvFamiliasDesprotegidasValor);
        tvFamiliasProtegidasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tvFamiliasProtegidasDescripcion);
        tvFamiliasProtegidasValor = (TextViewCustomFont) v.findViewById(R.id.tvFamiliasProtegidasValor);
        tvTotal2dasRealizadasDescripcion = (TextViewCustomFont) v.findViewById(R.id.tvTotal2dasRealizadasDescripcion);
        tvTotal2dasRealizadasValor = (TextViewCustomFont) v.findViewById(R.id.tvTotal2dasRealizadasValor);

        setTvPromediosReferidosADNValor((TextViewCustomFont) v.findViewById(R.id.tvPromediosReferidosADNValor));
        setTvPromediosReferidosADNDescripcion((TextViewCustomFont) v.findViewById(R.id.tvPromediosReferidosADNDescripcion));
        setTvPromediosReferidosSemanaValor((TextViewCustomFont) v.findViewById(R.id.tvPromediosReferidosSemanaValor));
        setTvPromediosReferidosSemanaDescripcion((TextViewCustomFont) v.findViewById(R.id.tvPromediosReferidosSemanaDescripcion));

        // 1rasEntrevistas
        pch1rasEntrevistas = (new PieChartHelper(getContext()));
        pch1rasEntrevistas.create((PieChart) v.findViewById(R.id.pieChart1rasEntrevistas));
        pch1rasEntrevistas.setTf(typefaceFocBd);

        // 2dasEntrevistas
        pch2dasEntrevistas = (new PieChartHelper(getContext()));
        pch2dasEntrevistas.create((PieChart) v.findViewById(R.id.pieChart2dasEntrevistas));
        pch2dasEntrevistas.setTf(typefaceFocBd);

        // cierres
        pchCierres = (new PieChartHelper(getContext()));
        pchCierres.create((PieChart) v.findViewById(R.id.pieChartCierre));
        pchCierres.setTf(typefaceFocBd);

    }

    public void actualizarDatos() {
        helper = new IndicadoresSemanaHelper(getActivity().getApplicationContext(), getViewResources());
        helper.init();

        DateTime fechaInicio = helper.getCalendarioInicio().getFechaInicioSemana();
        DateTime fechaFin = helper.getCalendarioFin().getFechaFinSemana();

        String strFechaInicio = DateUtils.getDateStringyymmdd(fechaInicio);
        String strFechaFin = DateUtils.getDateStringyymmdd(fechaFin);

        CarteraProspectoController carteraProspectoController = new CarteraProspectoController();
        CarteraCitaReunionController carteraCitaReunionController = new CarteraCitaReunionController();

        Map<Integer, Integer> mapNumeroEntrevistasAgendadas = carteraCitaReunionController.obtenerTotalEntrevistasAgendadas(strFechaInicio, strFechaFin);
        //Map<Integer, Integer> mapNumeroEntrevistasReagendadasRealizadas = carteraCitaReunionController.obtenerTotalEntrevistasReagendadasRealizadas(strFechaInicio, strFechaFin); // TODO: REVISAR LO DE REAGENDADAS
        Map<Integer, Integer> mapNumeroEntrevistasRealizadasSinReagendadas = carteraCitaReunionController.obtenerTotalEntrevistasRealizadasSinReagendadas(strFechaInicio, strFechaFin);

        Integer totalPrimNoRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasAgendadas.get(Constantes.codEtapa1ra));
        Integer totalPrimRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa1ra));

        Integer totalSegNoRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasAgendadas.get(Constantes.codEtapa2da));
        Integer totalSegRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa2da));
        Integer totalCierres = carteraCitaReunionController.obtenerTotalCierres(strFechaInicio, strFechaFin);
        //if (mapNumeroEntrevistasAgendadas.size() == 2 && mapNumeroEntrevistasReagendadasRealizadas.size() == 2 && mapNumeroEntrevistasRealizadasSinReagendadas.size() == 2) {

        //totalPrimRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa1ra)) + StringUtils.isNullReturnZero(mapNumeroEntrevistasReagendadasRealizadas.get(Constantes.codEtapa1ra)); //TODO: REVSIAR LO DE REAGENDADAS
        //totalPrimRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa1ra));


        //totalSegRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa2da)) + StringUtils.isNullReturnZero(mapNumeroEntrevistasReagendadasRealizadas.get(Constantes.codEtapa2da));
        //totalSegRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasRealizadasSinReagendadas.get(Constantes.codEtapa2da));
        //totalSegNoRealizadas = StringUtils.isNullReturnZero(mapNumeroEntrevistasAgendadas.get(Constantes.codEtapa2da));
        //}

        //totalCierres = carteraCitaReunionController.obtenerTotalCierres(strFechaInicio, strFechaFin);

        //int referidosPromedioADN = carteraProspectoController.obtenerReferidosPromedioPorADN();
        int referidosPromedioADN = carteraProspectoController.obtenerPromedioReferidoPorADN(strFechaInicio,strFechaFin);
        Log.d("SEMP", " referidosPromedioADN:" + referidosPromedioADN);

        int idCalendarioHoy = helper.getCurrentCalendario().getIdCalendario();
        int promedioRefer = carteraProspectoController.obtenerPromedioReferidos(idCalendarioHoy, strFechaInicio, strFechaFin);

        Resources resources = getViewResources();

        String str_Efectividad = resources.getString(R.string.Efectividad);
        String str_Primeras = resources.getString(R.string.Primeras);
        String str_Segundas = resources.getString(R.string.Segundas);
        String str_DeCierre = resources.getString(R.string.DeCierre);

        getTvTituloPrincipal().setText(helper.obtenerTitulo());
        actualizarEtiquetas(getViewResources());
        actualizarPrimerasEntrevistas(totalPrimNoRealizadas, totalPrimRealizadas, str_Efectividad, str_Primeras, obtenerEstiloGraficos(Constantes.indicadores_primergrafico));
        actualizarSegundasEntrevistas(totalSegNoRealizadas, totalSegRealizadas, str_Efectividad, str_Segundas, obtenerEstiloGraficos(Constantes.indicadores_segundografico));

        int totalSegRealizadasCierres = carteraCitaReunionController.obtenerTotalSegRealizadasCierres(strFechaInicio,strFechaFin);
        actualizarCierres(totalSegRealizadasCierres, totalCierres, str_Efectividad, str_DeCierre, obtenerEstiloGraficos(Constantes.indicadores_tercergrafico));

        actualizarResumenenes(referidosPromedioADN, promedioRefer);

    }

    public EstiloGraficoCircular obtenerEstiloGraficos(short tipoGraficoIndicador) {

        int[] colores_verde = new int[]{R.color.grafico_verde_claro, R.color.pacifico_verde};
        int[] colores_fucsia = new int[]{R.color.grafico_fucsia_claro, R.color.pacifico_fucsia};
        int[] colores_celeste = new int[]{R.color.grafico_celeste_claro, R.color.pacifico_celeste};

        int colorVerdeClaro = ContextCompat.getColor(getContext(), R.color.pacifico_verde);
        int colorCelesteClaro = ContextCompat.getColor(getContext(), R.color.pacifico_celeste);
        int colorFucsiaClaro = ContextCompat.getColor(getContext(), R.color.pacifico_fucsia);

        int colorGrisClaro = R.color.color_gris_claro;

        int[] colores_texto_verde = new int[]{colorVerdeClaro, colorGrisClaro, colorGrisClaro};
        int[] colores_texto_fucsia = new int[]{colorFucsiaClaro, colorGrisClaro, colorGrisClaro};
        int[] colores_texto_celeste = new int[]{colorCelesteClaro, colorGrisClaro, colorGrisClaro};

        float[] tamanoTexto = new float[]{3.0f, 1.4f, 1.4f};
        int[] typeFace = new int[]{Typeface.BOLD, Typeface.NORMAL, Typeface.NORMAL};

        //(int[] colores, int[] colores_texto, float[] tamnoTexto, int[] typeFace)

        if (tipoGraficoIndicador == 1) {
            return new EstiloGraficoCircular(colores_verde, colores_texto_verde, tamanoTexto, typeFace);
        } else if (tipoGraficoIndicador == 2) {
            return new EstiloGraficoCircular(colores_fucsia,colores_texto_fucsia , tamanoTexto, typeFace);
        } else if (tipoGraficoIndicador == 3) {
            return new EstiloGraficoCircular(colores_celeste,colores_texto_celeste , tamanoTexto, typeFace);
        } else {
            return null;
        }
    }

    public void actualizarPrimerasEntrevistas(Integer primerasNoRealizadas, Integer primerasRealizadas, String texto1, String texto2, EstiloGraficoCircular estiloGraficoCircular) {
        if (estiloGraficoCircular == null) {
            return;
        }
        int[] colores_verde = new int[]{R.color.grafico_verde_claro, R.color.pacifico_verde};
        if(primerasNoRealizadas == 0){
            colores_verde[0] = R.color.pacifico_verde;
        }
        int colorVerdeClaro = ContextCompat.getColor(getContext(), R.color.pacifico_verde);
        int colorGrisClaro = R.color.color_gris_claro;
        float[] tamanoTexto = new float[]{3.0f, 1.4f, 1.4f};
        int[] typeFace = new int[]{Typeface.BOLD, Typeface.NORMAL, Typeface.NORMAL};
        int[] colores_texto_verde = new int[]{colorVerdeClaro, colorGrisClaro, colorGrisClaro};
        EstiloGraficoCircular estiloGraficoCircularTemp = new EstiloGraficoCircular(colores_verde, colores_texto_verde, tamanoTexto, typeFace);

        Integer primerasAgendadas = primerasNoRealizadas + primerasRealizadas;

        // actulizar leyenda
        getTv1rasRealizadasValor().setText(String.valueOf(primerasRealizadas));
        getTv1rasNoRealizadasValor().setText(String.valueOf(primerasNoRealizadas));
        getTvTotal1rasAgendadasValor().setText(String.valueOf(primerasAgendadas));

        // actualizar grafico
        SemanaPreviaDatoBean semanaPrevia1rasEntrevistas = obtenerSemanaPreviaDatos(primerasNoRealizadas, primerasRealizadas, primerasAgendadas, texto1, texto2);
        actualizarPieChart(pch1rasEntrevistas, semanaPrevia1rasEntrevistas, estiloGraficoCircularTemp);

    }


    public void actualizarSegundasEntrevistas(Integer segundasNoRealizadas, Integer segundasRealizadas, String texto1, String texto2, EstiloGraficoCircular estiloGraficoCircular) {
        if (estiloGraficoCircular == null) {
            return;
        }
        int[] colores_fucsia = new int[]{R.color.grafico_fucsia_claro, R.color.pacifico_fucsia};
        if(segundasNoRealizadas == 0){
            colores_fucsia[0] = R.color.pacifico_fucsia;
        }
        int colorFucsiaClaro = ContextCompat.getColor(getContext(), R.color.pacifico_fucsia);
        int colorGrisClaro = R.color.color_gris_claro;
        float[] tamanoTexto = new float[]{3.0f, 1.4f, 1.4f};
        int[] typeFace = new int[]{Typeface.BOLD, Typeface.NORMAL, Typeface.NORMAL};
        int[] colores_texto_fucsia = new int[]{colorFucsiaClaro, colorGrisClaro, colorGrisClaro};
        EstiloGraficoCircular estiloGraficoCircularTemp = new EstiloGraficoCircular(colores_fucsia, colores_texto_fucsia , tamanoTexto, typeFace);

        Integer totalSegundasAgendadas = segundasNoRealizadas + segundasRealizadas;

        // actulizar leyenda
        getTvTotal2dasAgendadasValor().setText(String.valueOf(totalSegundasAgendadas));
        getTv2dasRealizadasValor().setText(String.valueOf(segundasRealizadas));
        getTv2dasNoRealizadasValor().setText(String.valueOf(segundasNoRealizadas));

        // actualizar grafico
        SemanaPreviaDatoBean semanaPrevia2dasEntrevistas = obtenerSemanaPreviaDatos(segundasNoRealizadas, segundasRealizadas, totalSegundasAgendadas, texto1, texto2);
        actualizarPieChart(pch2dasEntrevistas, semanaPrevia2dasEntrevistas, estiloGraficoCircularTemp);
    }

    public void actualizarCierres(Integer totalSegundasRealizadasCierres, Integer totalCierres, String texto1, String texto2, EstiloGraficoCircular estiloGraficoCircular) {
        if (estiloGraficoCircular == null) {
            return;
        }
        Integer totalNoCierres = totalSegundasRealizadasCierres - totalCierres;

        int[] colores_celeste = new int[]{R.color.grafico_celeste_claro, R.color.pacifico_celeste};
        if(totalNoCierres == 0){
            colores_celeste[0] = R.color.pacifico_celeste;
        }
        int colorCelesteClaro = ContextCompat.getColor(getContext(), R.color.pacifico_celeste);
        int colorGrisClaro = R.color.color_gris_claro;
        float[] tamanoTexto = new float[]{3.0f, 1.4f, 1.4f};
        int[] typeFace = new int[]{Typeface.BOLD, Typeface.NORMAL, Typeface.NORMAL};
        int[] colores_texto_celeste = new int[]{colorCelesteClaro, colorGrisClaro, colorGrisClaro};
        EstiloGraficoCircular estiloGraficoCircularTemp = new EstiloGraficoCircular(colores_celeste, colores_texto_celeste , tamanoTexto, typeFace);


        // actulizar leyenda
        getTvTotal2dasRealizadasValor().setText(String.valueOf(totalSegundasRealizadasCierres));
        getTvFamiliasProtegidasValor().setText(String.valueOf(totalCierres));
        getTvFamiliasDesprotegidasValor().setText(String.valueOf(totalNoCierres));

        // actualizar grafico
        SemanaPreviaDatoBean semanaPreviaCierres = obtenerSemanaPreviaDatos(totalNoCierres, totalCierres, totalSegundasRealizadasCierres,texto1, texto2);
        actualizarPieChart(pchCierres, semanaPreviaCierres, estiloGraficoCircularTemp);

    }

    public void actualizarResumenenes(int promediosReferidosADN, int promediosReferidosSemana) {
        getTvPromediosReferidosADNValor().setText(String.valueOf(promediosReferidosADN));
        getTvPromediosReferidosSemanaValor().setText(String.valueOf(promediosReferidosSemana));
    }


    /*

    public void actualizarLeyenda(Long primerasAgendadas, Long primerasRealizadas, Long segundasAgendadas, Long segundasRealizadas, Long segundasRealizadasCierre, Long cierres, Long promediosReferidosADN, Long promediosReferidosSemana) {
        Resources resources = getViewResources();

        String str_Efectividad = resources.getString(R.string.Efectividad);
        String str_Primeras = resources.getString(R.string.Primeras);
        String str_Segundas = resources.getString(R.string.Segundas);
        String str_DeCierre = resources.getString(R.string.DeCierre);

        // primeras entrevistas
        getTvTotal1rasAgendadasValor().setText(String.valueOf(primerasAgendadas));
        getTv1rasRealizadasValor().setText(String.valueOf(primerasRealizadas));

        // segundas entrevistas
        getTvTotal2dasAgendadasValor().setText(String.valueOf(segundasAgendadas));
        getTv2dasRealizadasValor().setText(String.valueOf(segundasRealizadas));

        // cierres
        getTvTotal2dasRealizadasValor().setText(String.valueOf(segundasRealizadasCierre));
        getTvFamiliasDesprotegidasDescripcion().setText(String.valueOf(cierres));
        getTvFamiliasProtegidasDescripcion().setText(String.valueOf(cierres));

        getTvPromediosReferidosADNValor().setText(String.valueOf(promediosReferidosADN));
        getTvPromediosReferidosSemanaValor().setText(String.valueOf(promediosReferidosSemana));

    }

    public void actualizarGraficos(Long primerasAgendadas, Long primerasRealizadas, Long segundasAgendadas, Long segundasRealizadas, Long segundasRealizadasCierre, Long cierres) {
        Resources resources = getViewResources();

        String str_Efectividad = resources.getString(R.string.Efectividad);
        String str_Primeras = resources.getString(R.string.Primeras);
        String str_Segundas = resources.getString(R.string.Segundas);
        String str_DeCierre = resources.getString(R.string.DeCierre);

        int[] colores_verde = new int[]{R.color.grafico_verde_claro, R.color.pacifico_verde};
        int[] colores_fucsia = new int[]{R.color.grafico_fucsia_claro, R.color.pacifico_fucsia};
        int[] colores_celeste = new int[]{R.color.grafico_celeste_claro, R.color.pacifico_celeste};

        int colorVerdeClaro = ContextCompat.getColor(getContext(), R.color.pacifico_verde);
        int colorCelesteClaro = ContextCompat.getColor(getContext(), R.color.pacifico_celeste);
        int colorFucsiaClaro = ContextCompat.getColor(getContext(), R.color.pacifico_fucsia);

        int colorGrisClaro = R.color.color_gris_claro;

        int[] colores_texto_verde = new int[]{colorVerdeClaro, colorGrisClaro, colorGrisClaro};
        int[] colores_texto_fucsia = new int[]{colorFucsiaClaro, colorGrisClaro, colorGrisClaro};
        int[] colores_texto_celeste = new int[]{colorCelesteClaro, colorGrisClaro, colorGrisClaro};

        float[] tamnoTexto = new float[]{3.0f, 1.4f, 1.4f};
        int[] typeFace = new int[]{Typeface.BOLD, Typeface.NORMAL, Typeface.NORMAL};


        // primeras entrevistas
        EstiloGraficoCircular estilo1rasEntrevistas = new EstiloGraficoCircular(colores_verde, colores_texto_verde, tamnoTexto, typeFace);
        SemanaPreviaDatoBean semanaPrevia1rasEntrevistas =  obtenerSemanaPreviaDatos(primerasAgendadas, primerasRealizadas, str_Efectividad, str_Primeras);
        actualizarPieChart(getPch1rasEntrevistas(), semanaPrevia1rasEntrevistas, estilo1rasEntrevistas);

        // segundas entrevistas
        EstiloGraficoCircular estiloe2dasEntrevistas = new EstiloGraficoCircular(colores_fucsia, colores_texto_fucsia, tamnoTexto, typeFace);
        SemanaPreviaDatoBean semanaPrevia2dasEntrevistas =  obtenerSemanaPreviaDatos(segundasAgendadas, segundasRealizadas, str_Efectividad, str_Segundas);
        actualizarPieChart(getPch2dasEntrevistas(), semanaPrevia2dasEntrevistas, estiloe2dasEntrevistas);

        // cierres
        EstiloGraficoCircular estiloeCierres = new EstiloGraficoCircular(colores_celeste, colores_texto_celeste, tamnoTexto, typeFace);
        SemanaPreviaDatoBean semanaPreviaCierres =  obtenerSemanaPreviaDatos(segundasRealizadasCierre, cierres, str_Efectividad, str_DeCierre);
        actualizarPieChart(getPchCierres(), semanaPreviaCierres, estiloeCierres);

    }*/

    public void actualizarEtiquetas(Resources resources) {
        String str_1rasNoRealizadas = resources.getString(R.string.des1rasNoRealizadas);
        String str_1rasRealizadas = resources.getString(R.string.des1rasRealizadas);
        String str_Total1rasAgendadas = resources.getString(R.string.desTotal1rasAgendadas);
        String str_2dasNoRealizadas = resources.getString(R.string.des2dasNoRealizadas);
        String str_2dasRealizadas = resources.getString(R.string.des2dasRealizadas);
        String str_Total2dasAgendadas = resources.getString(R.string.desTotal2dasAgendadas);
        String str_FamiliasDesprotegidas = resources.getString(R.string.desFamiliasDesprotegidas);
        String str_FamiliasProtegidas = resources.getString(R.string.desFamiliasProtegidas);
        String str_Total2dasRealizadas = resources.getString(R.string.desTotal2dasRealizadas);
        String str_PromediosReferidosADN = resources.getString(R.string.desPromediosReferidosADN);
        String str_PromediosReferidosSemana = resources.getString(R.string.desPromediosReferidosSemana);

        getTv1rasNoRealizadasDescripcion().setText(str_1rasNoRealizadas);
        getTv1rasRealizadasDescripcion().setText(str_1rasRealizadas);
        getTvTotal1rasAgendadasDescripcion().setText(str_Total1rasAgendadas);

        getTv2dasNoRealizadasDescripcion().setText(str_2dasNoRealizadas);
        getTv2dasRealizadasDescripcion().setText(str_2dasRealizadas);
        getTvTotal2dasAgendadasDescripcion().setText(str_Total2dasAgendadas);

        getTvFamiliasDesprotegidasDescripcion().setText(str_FamiliasDesprotegidas);
        getTvFamiliasProtegidasDescripcion().setText(str_FamiliasProtegidas);
        getTvTotal2dasRealizadasDescripcion().setText(str_Total2dasRealizadas);

        getTvPromediosReferidosADNDescripcion().setText(str_PromediosReferidosADN);
        getTvPromediosReferidosSemanaDescripcion().setText(str_PromediosReferidosSemana);

    }

    public SemanaPreviaDatoBean obtenerSemanaPreviaDatos(Integer primerDato, Integer segundoDato, Integer total,String primerTexto, String segundoTexto) {

        Integer proporcion = 0;
        if (total > 0 && segundoDato > 0) {
            proporcion = segundoDato * 100 / total;
        }
        String proporcionTexto = String.valueOf(proporcion) + "%";

        String[] textos = new String[]{proporcionTexto, primerTexto, segundoTexto};
        SemanaPreviaDatoBean datos = new SemanaPreviaDatoBean(obtenerListGraficoDatos(Long.valueOf(primerDato), Long.valueOf(segundoDato)), textos);
        return datos;
    }

    public List<IndiceDatoBean> obtenerListGraficoDatos(Long primerDato, Long segundoDato) {
        // 1eras entrevistas
        List<IndiceDatoBean> lstGraficoDatos = new ArrayList<>();
        if(primerDato > 0) {
            lstGraficoDatos.add(new IndiceDatoBean(0, "", primerDato));
        }
        if(segundoDato > 0){
            lstGraficoDatos.add(new IndiceDatoBean(lstGraficoDatos.size(), "", segundoDato));
        }
        return lstGraficoDatos;
    }

    public void actualizarPieChart(PieChartHelper pieChartHelper, SemanaPreviaDatoBean datoBean, EstiloGraficoCircular estiloGraficoCircular) {
        pieChartHelper.setData("", datoBean.getLstGraficoDatos(), estiloGraficoCircular.getColores());
        pieChartHelper.setSpannableText(datoBean.getSpannableTexto(), estiloGraficoCircular.getColores_texto(), estiloGraficoCircular.getTamnoTexto(), estiloGraficoCircular.getTypeFace());
    }


    public TextViewCustomFont getTvTituloPrincipal() {
        return tvTituloPrincipal;
    }

    public void setTvTituloPrincipal(TextViewCustomFont tvTituloPrincipal) {
        this.tvTituloPrincipal = tvTituloPrincipal;
    }

    public Resources getViewResources() {
        return viewResources;
    }

    public void setViewResources(Resources viewResources) {
        this.viewResources = viewResources;
    }

    public TextViewCustomFont getTvPromediosReferidosADNValor() {
        return tvPromediosReferidosADNValor;
    }

    public void setTvPromediosReferidosADNValor(TextViewCustomFont tvPromediosReferidosADNValor) {
        this.tvPromediosReferidosADNValor = tvPromediosReferidosADNValor;
    }

    public TextViewCustomFont getTvPromediosReferidosSemanaValor() {
        return tvPromediosReferidosSemanaValor;
    }

    public void setTvPromediosReferidosSemanaValor(TextViewCustomFont tvPromediosReferidosSemanaValor) {
        this.tvPromediosReferidosSemanaValor = tvPromediosReferidosSemanaValor;
    }

    public TextViewCustomFont getTv1rasNoRealizadasDescripcion() {
        return tv1rasNoRealizadasDescripcion;
    }

    public void setTv1rasNoRealizadasDescripcion(TextViewCustomFont tv1rasNoRealizadasDescripcion) {
        this.tv1rasNoRealizadasDescripcion = tv1rasNoRealizadasDescripcion;
    }

    public TextViewCustomFont getTv1rasNoRealizadasValor() {
        return tv1rasNoRealizadasValor;
    }

    public void setTv1rasNoRealizadasValor(TextViewCustomFont tv1rasNoRealizadasValor) {
        this.tv1rasNoRealizadasValor = tv1rasNoRealizadasValor;
    }

    public TextViewCustomFont getTv1rasRealizadasDescripcion() {
        return tv1rasRealizadasDescripcion;
    }

    public void setTv1rasRealizadasDescripcion(TextViewCustomFont tv1rasRealizadasDescripcion) {
        this.tv1rasRealizadasDescripcion = tv1rasRealizadasDescripcion;
    }

    public TextViewCustomFont getTv1rasRealizadasValor() {
        return tv1rasRealizadasValor;
    }

    public void setTv1rasRealizadasValor(TextViewCustomFont tv1rasRealizadasValor) {
        this.tv1rasRealizadasValor = tv1rasRealizadasValor;
    }

    public TextViewCustomFont getTvTotal1rasAgendadasDescripcion() {
        return tvTotal1rasAgendadasDescripcion;
    }

    public void setTvTotal1rasAgendadasDescripcion(TextViewCustomFont tvTotal1rasAgendadasDescripcion) {
        this.tvTotal1rasAgendadasDescripcion = tvTotal1rasAgendadasDescripcion;
    }

    public TextViewCustomFont getTvTotal1rasAgendadasValor() {
        return tvTotal1rasAgendadasValor;
    }

    public void setTvTotal1rasAgendadasValor(TextViewCustomFont tvTotal1rasAgendadasValor) {
        this.tvTotal1rasAgendadasValor = tvTotal1rasAgendadasValor;
    }

    public TextViewCustomFont getTv2dasNoRealizadasDescripcion() {
        return tv2dasNoRealizadasDescripcion;
    }

    public void setTv2dasNoRealizadasDescripcion(TextViewCustomFont tv2dasNoRealizadasDescripcion) {
        this.tv2dasNoRealizadasDescripcion = tv2dasNoRealizadasDescripcion;
    }

    public TextViewCustomFont getTv2dasNoRealizadasValor() {
        return tv2dasNoRealizadasValor;
    }

    public void setTv2dasNoRealizadasValor(TextViewCustomFont tv2dasNoRealizadasValor) {
        this.tv2dasNoRealizadasValor = tv2dasNoRealizadasValor;
    }

    public TextViewCustomFont getTv2dasRealizadasDescripcion() {
        return tv2dasRealizadasDescripcion;
    }

    public void setTv2dasRealizadasDescripcion(TextViewCustomFont tv2dasRealizadasDescripcion) {
        this.tv2dasRealizadasDescripcion = tv2dasRealizadasDescripcion;
    }

    public TextViewCustomFont getTv2dasRealizadasValor() {
        return tv2dasRealizadasValor;
    }

    public void setTv2dasRealizadasValor(TextViewCustomFont tv2dasRealizadasValor) {
        this.tv2dasRealizadasValor = tv2dasRealizadasValor;
    }

    public TextViewCustomFont getTvTotal2dasAgendadasDescripcion() {
        return tvTotal2dasAgendadasDescripcion;
    }

    public void setTvTotal2dasAgendadasDescripcion(TextViewCustomFont tvTotal2dasAgendadasDescripcion) {
        this.tvTotal2dasAgendadasDescripcion = tvTotal2dasAgendadasDescripcion;
    }

    public TextViewCustomFont getTvTotal2dasAgendadasValor() {
        return tvTotal2dasAgendadasValor;
    }

    public void setTvTotal2dasAgendadasValor(TextViewCustomFont tvTotal2dasAgendadasValor) {
        this.tvTotal2dasAgendadasValor = tvTotal2dasAgendadasValor;
    }

    public TextViewCustomFont getTvFamiliasDesprotegidasDescripcion() {
        return tvFamiliasDesprotegidasDescripcion;
    }

    public void setTvFamiliasDesprotegidasDescripcion(TextViewCustomFont tvFamiliasDesprotegidasDescripcion) {
        this.tvFamiliasDesprotegidasDescripcion = tvFamiliasDesprotegidasDescripcion;
    }

    public TextViewCustomFont getTvFamiliasDesprotegidasValor() {
        return tvFamiliasDesprotegidasValor;
    }

    public void setTvFamiliasDesprotegidasValor(TextViewCustomFont tvFamiliasDesprotegidasValor) {
        this.tvFamiliasDesprotegidasValor = tvFamiliasDesprotegidasValor;
    }

    public TextViewCustomFont getTvFamiliasProtegidasDescripcion() {
        return tvFamiliasProtegidasDescripcion;
    }

    public void setTvFamiliasProtegidasDescripcion(TextViewCustomFont tvFamiliasProtegidasDescripcion) {
        this.tvFamiliasProtegidasDescripcion = tvFamiliasProtegidasDescripcion;
    }

    public TextViewCustomFont getTvFamiliasProtegidasValor() {
        return tvFamiliasProtegidasValor;
    }

    public void setTvFamiliasProtegidasValor(TextViewCustomFont tvFamiliasProtegidasValor) {
        this.tvFamiliasProtegidasValor = tvFamiliasProtegidasValor;
    }

    public TextViewCustomFont getTvTotal2dasRealizadasDescripcion() {
        return tvTotal2dasRealizadasDescripcion;
    }

    public void setTvTotal2dasRealizadasDescripcion(TextViewCustomFont tvTotal2dasRealizadasDescripcion) {
        this.tvTotal2dasRealizadasDescripcion = tvTotal2dasRealizadasDescripcion;
    }

    public TextViewCustomFont getTvTotal2dasRealizadasValor() {
        return tvTotal2dasRealizadasValor;
    }

    public void setTvTotal2dasRealizadasValor(TextViewCustomFont tvTotal2dasRealizadasValor) {
        this.tvTotal2dasRealizadasValor = tvTotal2dasRealizadasValor;
    }

    public TextViewCustomFont getTvPromediosReferidosADNDescripcion() {
        return tvPromediosReferidosADNDescripcion;
    }

    public void setTvPromediosReferidosADNDescripcion(TextViewCustomFont tvPromediosReferidosADNDescripcion) {
        this.tvPromediosReferidosADNDescripcion = tvPromediosReferidosADNDescripcion;
    }

    public TextViewCustomFont getTvPromediosReferidosSemanaDescripcion() {
        return tvPromediosReferidosSemanaDescripcion;
    }

    public void setTvPromediosReferidosSemanaDescripcion(TextViewCustomFont tvPromediosReferidosSemanaDescripcion) {
        this.tvPromediosReferidosSemanaDescripcion = tvPromediosReferidosSemanaDescripcion;
    }
}

