package com.pacifico.cartera.vista.controles;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.pacifico.cartera.R;

/**
 * Created by vctrls3477 on 25/07/16.
 */
public class CustomButton extends Button{
    public CustomButton(Context context, AttributeSet attrs){
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CustomButton, 0, 0);
        try {
            String typeFaceName = typedArray.getString(R.styleable.CustomButton_typeFace4);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }
    }
}
