package com.pacifico.cartera.vista;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.pacifico.cartera.R;


public class DialogGeneral extends DialogFragment {


    IDialogSiNo mListener;
    String mTitulo;

    private TextView tvTitulo,texto1;
    private String Texto1;
    private String mEtiquetaNO;

    public DialogGeneral() {

    }

    public void setListener(IDialogSiNo mListener) {
        this.mListener = mListener;
    }

    public void setDatos(String titulo, String Texto, String etiquetaNo) {
        this.mTitulo = titulo;
        this.Texto1 = Texto;
        this.mEtiquetaNO = etiquetaNo;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_general, null);

        this.tvTitulo = (TextView) view.findViewById(R.id.tvTitulo);
        this.texto1 = (TextView) view.findViewById(R.id.texto1);
        this.tvTitulo.setText(mTitulo);
        this.texto1.setText(Texto1);
        this.texto1.setMovementMethod(new ScrollingMovementMethod());


        builder.setView(view)
                .setPositiveButton("", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_si();
                    }
                })
                .setNegativeButton(mEtiquetaNO, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mListener != null)
                            mListener.respuesta_no();
                    }
                });
        return builder.create();
    }

}

