package com.pacifico.cartera.vista;

public interface IDialogSiNo {
    void respuesta_si();
    void respuesta_no();
}
