package com.pacifico.cartera.vista.controles;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.RadioButton;

import com.pacifico.cartera.R;

/**
 * Created by dsb on 26/05/2016.
 */
public class RadioButtonCustomFont extends RadioButton{
    public RadioButtonCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RadioButtomCustomFont, 0, 0);
        try {
            String typeFaceName = typedArray.getString(R.styleable.RadioButtomCustomFont_typeFace3);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            typedArray.recycle();
        }
    }
}