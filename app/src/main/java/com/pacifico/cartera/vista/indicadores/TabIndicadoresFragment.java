package com.pacifico.cartera.vista.indicadores;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.IndicadoresAdapter;
import com.pacifico.cartera.vista.controles.setFuente;

public class TabIndicadoresFragment extends Fragment {
    public static TabLayout tabLayout;
    public static ViewPager viewPager;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view =  inflater.inflate(R.layout.idcd_main_frgm, null);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        IndicadoresAdapter adapter = new IndicadoresAdapter(getActivity().getSupportFragmentManager());
        adapter.addFrag(new ProgramacionSemanalFragment());
        adapter.addFrag(new ComposicionCarteraFragment());
        adapter.addFrag(new IndicadoresSemanaFragment());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        RelativeLayout rel1 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt1 = (TextView)rel1.findViewById(R.id.txt1);
        txt1.setText("PROGRAMACIÓN SEMANAL");
        setFuente.setFuenteRg(getActivity(),txt1);
        txt1.setTextColor(getResources().getColor(R.color.Blanco70));
        ImageView imgView1 = (ImageView)rel1.findViewById(R.id.img1);
        imgView1.setVisibility(View.GONE);
        RelativeLayout rel2 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt2 = (TextView)rel2.findViewById(R.id.txt1);
        txt2.setText("COMPOSICIÓN DE TU CARTERA");
        setFuente.setFuenteRg(getActivity(),txt2);
        txt2.setTextColor(getResources().getColor(R.color.Blanco70));
        ImageView imgView2 = (ImageView)rel2.findViewById(R.id.img1);
        imgView2.setVisibility(View.GONE);
        imgView2.setColorFilter(Color.rgb(170, 170, 170));
        RelativeLayout rel3 = (RelativeLayout)LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt3 = (TextView)rel3.findViewById(R.id.txt1);
        txt3.setText("INDICADORES SEMANAS PREVIAS");
        setFuente.setFuenteRg(getActivity(),txt3);
        txt3.setTextColor(getResources().getColor(R.color.Blanco70));
        ImageView imgView3 = (ImageView)rel3.findViewById(R.id.img1);
        imgView3.setColorFilter(Color.rgb(170, 170, 170));
        imgView3.setVisibility(View.GONE);

        tabLayout.getTabAt(0).setCustomView(rel1);
        tabLayout.getTabAt(1).setCustomView(rel2);
        tabLayout.getTabAt(2).setCustomView(rel3);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout relSelected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relSelected.findViewById(R.id.txt1);

                setFuente.setFuenteRg(getActivity(),txt1);
                txt1.setTextColor(getResources().getColor(R.color.white));
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){
                RelativeLayout relUnselected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relUnselected.findViewById(R.id.txt1);
                setFuente.setFuenteRg(getActivity(),txt1);
                txt1.setTextColor(getResources().getColor(R.color.Blanco70));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });
        return view;
    }
}