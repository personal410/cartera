package com.pacifico.cartera.vista.controles;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.pacifico.cartera.R;

/**
 * Created by dsb on 13/05/2016.
 */
public class TextViewCustomFont extends TextView {
    private String typeFaceName;
    boolean resizeTextView;
    private final int mOriginalTextSize;
    private final int mMinTextSize;
    private final static int sMinSize = 20;
    public TextViewCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        mOriginalTextSize = (int) getTextSize();
        mMinTextSize = sMinSize;
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.TextViewCustomFont, 0, 0);
        try {
            typeFaceName = typedArray.getString(R.styleable.TextViewCustomFont_typeFace);
            resizeTextView = typedArray.getBoolean(R.styleable.TextViewCustomFont_resize, false);
            if(typeFaceName!=null && !typeFaceName.isEmpty()) {
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            typedArray.recycle();
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (resizeTextView){
            ViewUtil.resizeText(this, mOriginalTextSize, mMinTextSize);
        }
    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (resizeTextView){
            ViewUtil.resizeText(this, mOriginalTextSize, mMinTextSize);
        }
    }
}