package com.pacifico.cartera.vista;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

/**
 * Created by joel on 7/24/16.
 */
public interface IDialogSiNoValor {
    void respuesta_si(TablaTablasBean valor);
    void respuesta_no();
}
