package com.pacifico.cartera.vista.indicadores;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.util.ArrayList;
import java.util.List;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.ProgramacionSemanalAdapter;

import com.pacifico.cartera.Model.Bean.Auxiliar.DiaSemanaBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionCalendarioBean;
import com.pacifico.cartera.utils.ProgramacionSemanalHelper;
import com.pacifico.cartera.utils.chart.EmptyValueFormatter;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

/**
 * Created by Ratan on 7/29/2015.
 */
public class ProgramacionSemanalFragment extends Fragment{
    private BarChart barChart;
    private ProgramacionSemanalHelper helper;
    private TextViewCustomFont tvTituloPrincipal;

    private ListView lstLeyendaDatos;

    private TextViewCustomFont tvTxt1Dia1;
    private TextViewCustomFont tvTxt2Dia1;
    private TextViewCustomFont tvTxt1Dia2;
    private TextViewCustomFont tvTxt2Dia2;
    private TextViewCustomFont tvTxt1Dia3;
    private TextViewCustomFont tvTxt2Dia3;
    private TextViewCustomFont tvTxt1Dia4;
    private TextViewCustomFont tvTxt2Dia4;
    private TextViewCustomFont tvTxt1Dia5;
    private TextViewCustomFont tvTxt2Dia5;
    private TextViewCustomFont tvTxt1Dia6;
    private TextViewCustomFont tvTxt2Dia6;
    private TextViewCustomFont tvTxt1Dia7;
    private TextViewCustomFont tvTxt2Dia7;
    private TextViewCustomFont tv1rasAgendadas;
    private TextViewCustomFont tv1rasRealizadas;
    private TextViewCustomFont tv2dasAgendadas;
    private TextViewCustomFont tv2dasRealizadas;
    private TextViewCustomFont tvTotalReferidos;
    private TextViewCustomFont tvTotalObservaciones;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context ctx = getActivity().getApplicationContext();
        View view = inflater.inflate(R.layout.idcd_psem_frgm, container, false);
        getResourceItems(view);

        // fuente foco
        //String typeFaceFocobd = "foco_std_rg-webfont.ttf";
        //Typeface mTf;
        //mTf = Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd);

        // calendario
        helper = new ProgramacionSemanalHelper(ctx, getResources());
        helper.init();
        return view;
    }

    public void getResourceItems(View view){
        // tvDetalle
        TextView tvDetalle = (TextView) view.findViewById(R.id.tvDetalle);
        tvDetalle.setOnClickListener(tvDetalleListener);
        tvTituloPrincipal = (TextViewCustomFont) view.findViewById(R.id.tvTitle24);
        tvTxt1Dia1 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia1);
        tvTxt2Dia1 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia1);
        tvTxt1Dia2 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia2);
        tvTxt2Dia2 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia2);
        tvTxt1Dia3 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia3);
        tvTxt2Dia3 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia3);
        tvTxt1Dia4 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia4);
        tvTxt2Dia4 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia4);
        tvTxt1Dia5 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia5);
        tvTxt2Dia5 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia5);
        tvTxt1Dia6 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia6);
        tvTxt2Dia6 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia6);
        tvTxt1Dia7 = (TextViewCustomFont) view.findViewById(R.id.tvTxt1Dia7);
        tvTxt2Dia7 = (TextViewCustomFont) view.findViewById(R.id.tvTxt2Dia7);

        tv1rasAgendadas = (TextViewCustomFont) view.findViewById(R.id.tvTotal1rasAgendadasValor);
        tv1rasRealizadas = (TextViewCustomFont) view.findViewById(R.id.tv1rasRealizadasValor);
        tv2dasAgendadas = (TextViewCustomFont) view.findViewById(R.id.tvTotal2dasAgendadasValor);
        tv2dasRealizadas = (TextViewCustomFont) view.findViewById(R.id.tv2dasRealizadasValor);
        tvTotalReferidos = (TextViewCustomFont) view.findViewById(R.id.tvTotalReferidos);
        tvTotalObservaciones = (TextViewCustomFont) view.findViewById(R.id.tvTotalObservaciones);

        // brackets anterior y posterior
        LinearLayout llAnterior = (LinearLayout) view.findViewById(R.id.llAnterior);
        llAnterior.setOnClickListener(llAnteriorListener);

        LinearLayout llPosterior = (LinearLayout) view.findViewById(R.id.llPosterior);
        llPosterior.setOnClickListener(llPosteriorListener);

        // barcChart
        barChart = (BarChart) view.findViewById(R.id.barChart);
        lstLeyendaDatos = (ListView) view.findViewById(R.id.lstDatosLeyenda);
    }
    public void actualizarPantalla(){
        tvTituloPrincipal.setText(helper.obtenerTitulo());
        ProgramacionCalendarioBean programacion = helper.obtenerProgramacionCalendario();

        List<DiaSemanaBean> lstDiaSemana = programacion.getLstDiaSemanaBean(); //helper.obtenerDiasSemanaActual();

        // actualizar textos dias semana
        int diaSemana = 1;
        for(DiaSemanaBean diaSemanaBean : lstDiaSemana){
           // Log.d("PSF", "diaSemana: " + diaSemana + " getDiaMes: " + diaSemanaBean.getDiaMes() + " getNombreDiaSemana: " + diaSemanaBean.getNombreDiaSemana());
            TextViewCustomFont textView1 = getTextViewSemana(10+diaSemana);
            TextViewCustomFont textView2 = getTextViewSemana(20+diaSemana);
            if(textView1!=null && textView2!=null ){
                updateTextViewSemana(textView1, textView2, diaSemanaBean);
            }
            diaSemana++;
        }

        // actualizar leyenda
        ProgramacionSemanalAdapter adpLeyendaDatos = new ProgramacionSemanalAdapter(getActivity(), programacion.getListCitas());
        lstLeyendaDatos.setAdapter(adpLeyendaDatos);
        setListViewHeightBasedOnItems(lstLeyendaDatos);
        adpLeyendaDatos.notifyDataSetChanged();

        // actualizar resumen
        tv1rasAgendadas.setText(String.valueOf(programacion.getNumero1rasEntrevistasAgendadas() + programacion.getNumero1rasEntrevistasRealizadas()));
        tv1rasRealizadas.setText(String.valueOf(programacion.getNumero1rasEntrevistasRealizadas()));
        tv2dasAgendadas.setText(String.valueOf(programacion.getNumero2dasEntrevistasAgendadas() + programacion.getNumero2dasEntrevistasRealizadas()));
        tv2dasRealizadas.setText(String.valueOf(programacion.getNumero2dasEntrevistasRealizadas()));
        tvTotalReferidos.setText(String.valueOf(programacion.getTotalReferidos()));
        tvTotalObservaciones.setText(String.valueOf(programacion.getTotalObservacionesPersonales()));

        cargarBarchart(getContext(), programacion.getEntries1ra(), programacion.getEntries2da());
    }
    @Override
    public void onResume() {
        super.onResume();
        actualizarPantalla();
    }

    public void updateTextViewSemana(TextViewCustomFont textView1, TextViewCustomFont textView2, DiaSemanaBean diaSemanaBean){
        if(textView1==null){
            Log.d("PSF", "textView1 null");
        }

        textView2.setText(String.valueOf(diaSemanaBean.getDiaMes()));
        textView2.setTextColor(diaSemanaBean.getColorDia());

        textView1.setText(String.valueOf(diaSemanaBean.getNombreDiaSemana()));
        textView1.setTextColor(diaSemanaBean.getColorDia());
    }

    public void cargarBarchart(Context ctx, ArrayList<BarEntry> entries1ra, ArrayList<BarEntry> entries2da) {
        // grafico de barras
        int[] colores_verde = new int[]{R.color.pacifico_verde,R.color.grafico_verde_claro};
        int[] colores_fucsia = new int[]{R.color.pacifico_fucsia,R.color.grafico_fucsia_claro };
        //int[] colores_verde = new int[]{R.color.grafico_verde_claro,R.color.pacifico_verde};
        //int[] colores_fucsia = new int[]{R.color.grafico_fucsia_claro,R.color.pacifico_fucsia };


        String typeFaceFocobd = "foco_std_rg-webfont.ttf";
        Typeface mTf;
        mTf = Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd);

        // creating labels, no se muestran los labels
        ArrayList<String> labels = new ArrayList<String>();
        labels.add("Mar 7");
        labels.add("Miér 8");
        labels.add("Jue 9");
        labels.add("Vie 10");
        labels.add("Sáb 11");
        labels.add("Dom 12");
        labels.add("Lun 13");

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();  // combined all dataset into an arraylist

        BarDataSet dataset = new BarDataSet(entries1ra, "1ras Entrevistas");
        dataset.setColors(colores_verde, ctx);
        dataset.setBarSpacePercent(35f);

        BarDataSet dataset2 = new BarDataSet(entries2da, "2das Entrevistas");
        dataset2.setColors(colores_fucsia, ctx);
        dataset2.setBarSpacePercent(35f);

        dataSets.add(dataset);
        dataSets.add(dataset2);

        BarData data = new BarData(labels, dataSets);

        data.setValueFormatter(new EmptyValueFormatter());

        data.setGroupSpace(40f);


        barChart.getAxisRight().setDrawLabels(false);
        barChart.setBorderColor(R.color.colorAzul);


        //Variable sagregadas Wilfredo
        barChart.setScaleEnabled(false);
        barChart.getBaseline();
        barChart.setGridBackgroundColor(getResources().getColor(R.color.colorAzul));
        //

        barChart.setData(data); // set the data and listDiaSemana of lables into chart

        barChart.setDescription("");  // set the description

        XAxis xAxis = barChart.getXAxis();
        xAxis.setGridColor(getResources().getColor(R.color.Lines));
        //xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTf);
        // xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setAxisLineColor(R.color.pacifico_verde);

        xAxis.setDrawLabels(false);

        xAxis.setDrawAxisLine(false);

        xAxis.setDrawLimitLinesBehindData(false);

        YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setTypeface(mTf);
        leftAxis.setLabelCount(8, false);

        //leftAxis.setValueFormatter(new MyYAxisValueFormatter());
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(30f);
        leftAxis.setGridColor(getResources().getColor(R.color.Lines));
        leftAxis.setTextColor(ContextCompat.getColor(ctx, R.color.gris_claro_texto4));
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        leftAxis.setTextSize(14f);

        // Agregado Wilfredo
        leftAxis.setMaxWidth(24f);
        leftAxis.setMinWidth(24f);

        leftAxis.setTextColor(ContextCompat.getColor(ctx, R.color.gris_claro_texto4));
        leftAxis.setAxisLineColor(R.color.pacifico_verde);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setTypeface(mTf);
        rightAxis.setLabelCount(8, false);
        //    rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(10f);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        rightAxis.setAxisLineColor(R.color.pacifico_verde);
        leftAxis.setAxisMinValue(0f);
        rightAxis.setAxisMinValue(0f);
        rightAxis.setGridColor(ContextCompat.getColor(getActivity(),R.color.Lines));

        barChart.getLegend().setEnabled(false);
        barChart.notifyDataSetChanged(); // let the chart know it's data changed
        barChart.invalidate(); // refresh
        barChart.setTouchEnabled(false);
    }

    public boolean setListViewHeightBasedOnItems(ListView listView) {
        Log.d("PSF", "1");
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();
            //Log.d("PSF", "numberOfItems: " + numberOfItems);

            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            //Log.d("PSF", "totalItemsHeight: " + totalItemsHeight);

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            //Log.d("PSF", "totalDividersHeight: " + totalDividersHeight);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();

            //Log.d("PSF", "height: " + params.height);
            params.height = totalItemsHeight + totalDividersHeight + 2;

            //Log.d("PSF", "new height: " + params.height);

            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;
        } else {
            return false;
        }
    }

    private View.OnClickListener llAnteriorListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("IDCD_PS", "llAnteriorListener OnClickListener");
            if (helper.modificarCalendarioAcual(-1)) {
                actualizarPantalla();
            }
        }
    };

    private View.OnClickListener llPosteriorListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("IDCD_PS", "OnClickListener");
            if (helper.modificarCalendarioAcual(+1)) {
                actualizarPantalla();
            }
        }
    };

    private View.OnClickListener tvDetalleListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("IDCD_PS", "OnClickListener");
            ProgramacionSemanalDialog programacionSemanalDialog = new ProgramacionSemanalDialog();
            programacionSemanalDialog.setCalendarioActual(helper.getCurrentCalendario());
            programacionSemanalDialog.show(getActivity().getSupportFragmentManager(), "xx");
        }
    };

    public TextViewCustomFont getTextViewSemana(int numeroSemanaTexto){
        TextViewCustomFont textView = null;
        switch (numeroSemanaTexto) {
            case 11:  textView = tvTxt1Dia1;
                break;
            case 21:  textView = tvTxt2Dia1;
                break;
            case 12:  textView = tvTxt1Dia2;
                break;
            case 22:  textView = tvTxt2Dia2;
                break;
            case 13:  textView = tvTxt1Dia3;
                break;
            case 23:  textView = tvTxt2Dia3;
                break;
            case 14:  textView = tvTxt1Dia4;
                break;
            case 24:  textView = tvTxt2Dia4;
                break;
            case 15:  textView = tvTxt1Dia5;
                break;
            case 25:  textView = tvTxt2Dia5;
                break;
            case 16:  textView = tvTxt1Dia6;
                break;
            case 26:  textView = tvTxt2Dia6;
                break;
            case 17:  textView = tvTxt1Dia7;
                break;
            case 27:  textView = tvTxt2Dia7;
                break;
        }
        return textView;
    }
}