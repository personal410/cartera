package com.pacifico.cartera.vista;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.pacifico.cartera.R;

/**
 * Created by dsb on 14/05/2016.
 */
public class TransactionFragment extends Fragment {

    private android.support.v7.app.ActionBar mActionBar;
    private android.support.v4.app.FragmentTransaction mFragmentTransaction;
    private android.support.v4.app.FragmentManager mFragmentManager;


    public android.support.v7.app.ActionBar getmActionBar() {
        return mActionBar;
    }

    public void loadFragment(final Fragment fragment) {

        // create a transaction for transition here
        final FragmentTransaction transaction = getmFragmentManager().beginTransaction();

        // put the fragment in place
        transaction.replace(R.id.containerView, fragment);

        // this is the part that will cause a fragment to be added to backstack,
        // this way we can return to it at any time using this tag
        transaction.addToBackStack(fragment.getClass().getName());

        transaction.commit();
    }

    public void setmActionBar(android.support.v7.app.ActionBar mActionBar) {
        this.mActionBar = mActionBar;
    }

    public FragmentTransaction getmFragmentTransaction() {
        return mFragmentTransaction;
    }

    public void setmFragmentTransaction(FragmentTransaction mFragmentTransaction) {
        this.mFragmentTransaction = mFragmentTransaction;
    }

    public FragmentManager getmFragmentManager() {
        return mFragmentManager;
    }

    public void setmFragmentManager(FragmentManager mFragmentManager) {
        this.mFragmentManager = mFragmentManager;
    }
}
