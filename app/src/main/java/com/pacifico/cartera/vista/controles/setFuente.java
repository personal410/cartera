package com.pacifico.cartera.vista.controles;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class setFuente {

    public static void setFuenteRg(Context context, TextView rb){

        String font_path = "foco_std_rg-webfont.ttf";

        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);

        rb.setTypeface(TF);


    }


}
