package com.pacifico.cartera.vista.indicadores;

import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.github.mikephil.charting.charts.PieChart;

import java.util.ArrayList;
import java.util.List;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.IndicadoresLeyendaAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraProspectoController;
import com.pacifico.cartera.Model.Bean.Auxiliar.IndiceDatoBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.chart.PieChartHelper;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

/**
 * Created by dsb on 22/04/2016.
 */
public class ComposicionCarteraFragment extends Fragment {
    private PieChartHelper pchGrupoEtareo;
    private PieChartHelper pchConOSinHijos;
    private PieChartHelper pchRangoIngresos;
    private TextViewCustomFont tvTituloPrincipal;
    private ListView lvEdades;
    private ListView lvIngresos;
    private ListView lvHijos;
    private Resources viewResources;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.idcd_ccar_frgm, container, false);
        viewResources = getResources();
        String typeFaceFocobd = "foco_std_bd-webfont.ttf";

        tvTituloPrincipal = (TextViewCustomFont) view.findViewById(R.id.tvTituloPrincipal);
        lvEdades = (ListView) view.findViewById(R.id.lvEdades);
        lvIngresos = (ListView) view.findViewById(R.id.lvIngresos);
        lvHijos = (ListView) view.findViewById(R.id.lvHijos);

        // grupo etareo
        pchGrupoEtareo = (new PieChartHelper(getContext()));
        pchGrupoEtareo.create((PieChart) view.findViewById(R.id.pieChartGrupoEtareo));
        pchGrupoEtareo.setTf(Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd));

        // rango de ingresos
        pchRangoIngresos = (new PieChartHelper(getContext()));
        pchRangoIngresos.create((PieChart) view.findViewById(R.id.pieChartRangoIngresos));
        pchRangoIngresos.setTf(Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd));

        // con o sin hijos
        pchConOSinHijos = (new PieChartHelper(getContext()));
        pchConOSinHijos.create((PieChart) view.findViewById(R.id.pieChartConOSinHijos));
        pchConOSinHijos.setTf(Typeface.createFromAsset(getActivity().getAssets(), typeFaceFocobd));
        //actualizarPantalla();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        actualizarPantalla();
    }

    private void actualizarPantalla() {
        CarteraProspectoController controller = new CarteraProspectoController();
        int codTablaFlagHijo = Integer.valueOf(Constantes.codTablaFlagHijo);

        Resources resources = getResources();
        String desFlagTieneHijos = resources.getString(R.string.desFlagTieneHijos);
        String desFlagNoTieneHijos = resources.getString(R.string.desFlagNoTieneHijos);

        Long totalProspectos = controller.obtenerNumeroProspectosComposicionCartera(/*fechaHoy, fechaFinalLimite*/);
        IndiceDatoBean tieneHijosDato = new IndiceDatoBean(codTablaFlagHijo, desFlagTieneHijos, Long.valueOf(Constantes.codFlagTieneHijos));
        IndiceDatoBean noTieneHijosDato = new IndiceDatoBean(codTablaFlagHijo, desFlagNoTieneHijos, Long.valueOf(Constantes.codFlagNoTieneHijos));

        ArrayList<ArrayList<IndiceDatoBean>> listListDatos = controller.obtenerComposicionCarteraDatos( /* fechaHoy, fechaFinalLimite,*/ tieneHijosDato, noTieneHijosDato);

        if (listListDatos != null && listListDatos.size() == 3) {
            actualizarPantalla(totalProspectos, listListDatos.get(0), listListDatos.get(1), listListDatos.get(2));
        }
    }


    private void actualizarPantalla(Long totalProspectos, List<IndiceDatoBean> lstGrupoEtareo, List<IndiceDatoBean> lstRangoIngresos, List<IndiceDatoBean> lstHijos) {
        String str_totalDeProspectos = viewResources.getString(R.string.totalDeProspectos);
        String str_totalDeProspectosInicio = viewResources.getString(R.string.totalDeProspectosInicio);
        String str_totalDeProspectosFin = viewResources.getString(R.string.totalDeProspectosFin);

        String strTitulo = str_totalDeProspectosInicio + str_totalDeProspectos + " " + String.valueOf(totalProspectos) + str_totalDeProspectosFin;
        tvTituloPrincipal.setText(strTitulo);

        int[] colores = new int[]{R.color.pacifico_verde, R.color.pacifico_celeste, R.color.pacifico_anaranjado, R.color.pacifico_fucsia};

        Drawable drwIcnVerde = ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.grafico_rectangulo_verde);
        Drawable drwIcnCeleste = ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.grafico_rectangulo_celeste);
        Drawable drwIcnAnaranjado = ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.grafico_rectangulo_anaranjado);
        Drawable drwIcnFucsia = ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.grafico_rectangulo_fucsia);

        Drawable[] drwIcons = new Drawable[]{drwIcnVerde, drwIcnCeleste, drwIcnAnaranjado, drwIcnFucsia};

        ArrayList<IndiceDatoBean> lstGrupoEtareoGrafico = new ArrayList<>();
        for(IndiceDatoBean indiceDatoBean : lstGrupoEtareo){
            if(indiceDatoBean.getValor() > 0){
                lstGrupoEtareoGrafico.add(indiceDatoBean);
            }
        }
        int[] arrColoresGrupoEtareo = new int[lstGrupoEtareoGrafico.size()];
        for(int i = 0; i < lstGrupoEtareoGrafico.size(); i++){
            IndiceDatoBean indiceDatoBean = lstGrupoEtareoGrafico.get(i);
            arrColoresGrupoEtareo[i] = colores[indiceDatoBean.getIndice() - 1];
        }

        ArrayList<IndiceDatoBean> lstRangoIngresosGrafico = new ArrayList<>();
        for(IndiceDatoBean indiceDatoBean : lstRangoIngresos){
            if(indiceDatoBean.getValor() > 0){
                lstRangoIngresosGrafico.add(indiceDatoBean);
            }
        }
        int[] arrColoresRangoIngresos = new int[lstRangoIngresosGrafico.size()];
        for(int i = 0; i < lstRangoIngresosGrafico.size(); i++){
            IndiceDatoBean indiceDatoBean = lstRangoIngresosGrafico.get(i);
            arrColoresRangoIngresos[i] = colores[indiceDatoBean.getIndice() - 1];
        }

        ArrayList<IndiceDatoBean> lstHijosGrafico = new ArrayList<>();
        for(IndiceDatoBean indiceDatoBean : lstHijos){
            if(indiceDatoBean.getValor() > 0){
                lstHijosGrafico.add(indiceDatoBean);
            }
        }
        int[] arrColoresHijos = new int[lstHijosGrafico.size()];
        for(int i = 0; i < lstHijosGrafico.size(); i++){
            IndiceDatoBean indiceDatoBean = lstHijosGrafico.get(i);
            arrColoresHijos[i] = colores[indiceDatoBean.getIndice()];
        }

        pchGrupoEtareo.setData("", lstGrupoEtareoGrafico, arrColoresGrupoEtareo);
        pchRangoIngresos.setData("", lstRangoIngresosGrafico, arrColoresRangoIngresos);
        pchConOSinHijos.setData("", lstHijosGrafico, arrColoresHijos);
        actualizarLeyendaGrupoEtareo(lstGrupoEtareo, drwIcons);
        actualizarLeyendaRangoIngresos(lstRangoIngresos, drwIcons);
        actualizarLeyendaHijos(lstHijos, drwIcons);
    }
    private void actualizarLeyendaGrupoEtareo(List<IndiceDatoBean> lstDato, Drawable[] drwIcons) {
        if (lstDato == null) {
            return;
        }
        IndicadoresLeyendaAdapter adapter = new IndicadoresLeyendaAdapter(getActivity(), lstDato, R.layout.idcd_ccar_item_edad, drwIcons);
        lvEdades.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    private void actualizarLeyendaRangoIngresos(List<IndiceDatoBean> lstDato, Drawable[] drwIcons) {
        if (lstDato == null) {
            return;
        }
        IndicadoresLeyendaAdapter adapter = new IndicadoresLeyendaAdapter(getActivity(), lstDato, R.layout.idcd_ccar_item_ingresos, drwIcons);
        lvIngresos.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
    private void actualizarLeyendaHijos(List<IndiceDatoBean> lstDato, Drawable[] drwIcons) {
        if (lstDato == null) {
            return;
        }
        IndicadoresLeyendaAdapter adapter = new IndicadoresLeyendaAdapter(getActivity(), lstDato, R.layout.idcd_ccar_item_hijos, drwIcons);
        lvHijos.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
