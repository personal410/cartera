package com.pacifico.cartera.vista;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import com.pacifico.cartera.Activity.MainActivity;
import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.TusContactosAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraProspectoController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.TusContactosBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DrawableHelper;
import com.pacifico.cartera.utils.PrimerLetraAlfabetoHelper;
import com.pacifico.cartera.utils.StringUtils;
import com.pacifico.cartera.Fragment.prospecto.ProspectoFragment;

/**
 * Created by dsb on 02/05/2016.
 */
public class TusContactosFragment extends TransactionFragment{
    private LinearLayout mItemProspecto;
    private LinearLayout mItemEtapa;
    private LinearLayout mItemEstado;
    private LinearLayout mItemUltimoEstado;
    private LinearLayout mItemIngresos;
    private LinearLayout mItemHijos;
    private ImageView mImageProspecto;
    private ImageView mImageEtapa;
    private ImageView mImageEstado;
    private ImageView mImageUltimoEstado;
    private ImageView mImageIngresos;
    private ImageView mImageHijos;
    private boolean mAscendingOrder[] = {false, true, true, true, true, true};
    private TusContactosAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tuco_frgm, null);

        Context ctx = getActivity().getApplicationContext();

        mItemProspecto = (LinearLayout) v.findViewById(R.id.layProspecto);
        mItemEtapa = (LinearLayout) v.findViewById(R.id.layEtapa);
        mItemEstado = (LinearLayout) v.findViewById(R.id.layEstado);
        mItemUltimoEstado = (LinearLayout) v.findViewById(R.id.layUltimoEstado);
        mItemIngresos = (LinearLayout) v.findViewById(R.id.layIngresos);
        mItemHijos = (LinearLayout) v.findViewById(R.id.layHijos);

        mImageProspecto = (ImageView) v.findViewById(R.id.ivProspectoSort);
        mImageEtapa = (ImageView) v.findViewById(R.id.ivEtapaSort);
        mImageEstado = (ImageView) v.findViewById(R.id.ivEstadoSort);
        mImageUltimoEstado = (ImageView) v.findViewById(R.id.ivUltimoEstadoSort);
        mImageIngresos = (ImageView) v.findViewById(R.id.ivIngresosSort);
        mImageHijos = (ImageView) v.findViewById(R.id.ivHijosSort);

        mItemProspecto.setOnClickListener(mProspectoListener);
        mItemEtapa.setOnClickListener(mEtapaListener);
        mItemEstado.setOnClickListener(mEstadoListener);
        mItemUltimoEstado.setOnClickListener(mUltimoEstadoListener);
        mItemIngresos.setOnClickListener(mIngresosListener);
        mItemHijos.setOnClickListener(mHijosListener);

        mAdapter = new TusContactosAdapter(getActivity(), llenarListaTusContactos(obtenerTusContactos()));
        //mAdapter = new TusContactosAdapter(getActivity(), null);

        ListView lvDatos = (ListView) v.findViewById(R.id.lvTusContactos);
        lvDatos.setAdapter(mAdapter);

        mAdapter.ordenarPorProspectoAsc();
        //mAdapter.notifyDataSetChanged();
        mImageProspecto.setImageResource(R.drawable.ic_ordenar_up);

        // Make visible the arrow next to the name and make the others invisible
        mImageProspecto.setVisibility(View.VISIBLE);
        mImageEtapa.setVisibility(View.GONE);
        //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
        mImageEstado.setVisibility(View.GONE);
        mImageUltimoEstado.setVisibility(View.GONE);
        mImageIngresos.setVisibility(View.GONE);
        mImageHijos.setVisibility(View.GONE);

        Log.d("TCF", "show action bar");
        ((MainActivity) getActivity()).getSupportActionBar().show();

        lvDatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                TusContactosBean tusContactosBean = (TusContactosBean) parent.getAdapter().getItem(position);
                if (tusContactosBean != null) {
                    //Se cambio a dispositivo -  System.out.println("tusContactosBean prospecto: " + tusContactosBean.getCodProspecto());
                    System.out.println("tusContactosBean prospecto: " + tusContactosBean.getIdProspectoDispositivo());
                }

                //Se cambio a dispositivo -  Log.d("TCF", " detalle prospecto: " + tusContactosBean.getCodProspecto() + "es: " + tusContactosBean.getDesEstado() + "es2: " + tusContactosBean.getDesEstado2());
                //Log.d("TCF", " detalle prospecto: " + tusContactosBean.getIdProspectoDispositivo() + "es: " + tusContactosBean.getDesEstado() + "es2: " + tusContactosBean.getDesEstado2());
                Log.d("TCF", " detalle prospecto: " + tusContactosBean.getIdProspectoDispositivo() + "es: " + tusContactosBean.getDesEstado() + "es2: " + tusContactosBean.getDesResultado());

                ProspectoFragment prospecto = new ProspectoFragment();
                prospecto.setIdProspectoDispositivo(tusContactosBean.getIdProspectoDispositivo());

                String etapaResultado = "";

                if (tusContactosBean.getCodEtapa() == Constantes.codEtapaNuevo)
                    etapaResultado = tusContactosBean.getDesEtapa();
                else
                    etapaResultado = tusContactosBean.getDesEtapa() + "  "  + tusContactosBean.getDesEstado();

                ((MainActivity) getActivity()).setIdProspectoDispositivo(tusContactosBean.getIdProspectoDispositivo());
                ((MainActivity) getActivity()).setEtapaResultado(etapaResultado);
                ((MainActivity) getActivity()).changeFragment(prospecto, Constantes.fragmentProspecto);
            }
        });

        return v;
    }

    /*@Override
    public void onResume()
    {
        super.onResume();
        actualizarDatos();

    }

    public void actualizarDatos(){
        mAdapter.setListaTusContactos(llenarListaTusContactos(obtenerTusContactos()));
        mAdapter.notifyDataSetChanged();
    }*/

    private View.OnClickListener mProspectoListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("TusContactos", " prospecto mAscendingOrder[1]: " + mAscendingOrder[1]);

            if (mAscendingOrder[0]) {
                Log.d("TusContactos", " prospecto ascendentemente ");
                mAscendingOrder[0] = false;
                mAdapter.ordenarPorProspectoAsc();
                mImageProspecto.setImageResource(R.drawable.ic_ordenar_up);
            } else {
                Log.d("TusContactos", " prospecto descendentemente ");
                mAscendingOrder[0] = true;
                mAdapter.ordenarPorPropectoDesc();
                mImageProspecto.setImageResource(R.drawable.ic_ordenar_down);
            }
            // Make visible the arrow next to the name and make the others invisible
            mImageProspecto.setVisibility(View.VISIBLE);
            mImageEtapa.setVisibility(View.GONE);
            //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
            mImageEstado.setVisibility(View.GONE);
            mImageUltimoEstado.setVisibility(View.GONE);
            mImageIngresos.setVisibility(View.GONE);
            mImageHijos.setVisibility(View.GONE);

            mAscendingOrder[1] = true;
            mAscendingOrder[2] = true;
            mAscendingOrder[3] = true;
            mAscendingOrder[4] = true;
        }
    };
    private View.OnClickListener mEtapaListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(mAscendingOrder[1]){
                mAscendingOrder[1] = false;
                mAdapter.ordenarPorEtapaAsc();
                mImageEtapa.setImageResource(R.drawable.ic_ordenar_up);
            }else{
                mAscendingOrder[1] = true;
                mAdapter.ordenarPorEtapaDesc();
                mImageEtapa.setImageResource(R.drawable.ic_ordenar_down);
            }
            mImageProspecto.setVisibility(View.GONE);
            mImageEtapa.setVisibility(View.VISIBLE);
            mImageEstado.setVisibility(View.GONE);
            mImageUltimoEstado.setVisibility(View.GONE);
            mImageIngresos.setVisibility(View.GONE);
            mImageHijos.setVisibility(View.GONE);

            mAscendingOrder[0] = true;
            mAscendingOrder[2] = true;
            mAscendingOrder[3] = true;
            mAscendingOrder[4] = true;
        }
    };
    private View.OnClickListener mEstadoListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(mAscendingOrder[2]){
                mAscendingOrder[2] = false;
                mAdapter.ordenarPorEstadoAsc();
                mImageEstado.setImageResource(R.drawable.ic_ordenar_up);
            } else {
                mAscendingOrder[2] = true;
                mAdapter.ordenarPorEstadoDesc();
                mImageEstado.setImageResource(R.drawable.ic_ordenar_down);
            }
            // Make visible the arrow next to the name and make the others invisible
            mImageProspecto.setVisibility(View.GONE);
            mImageEtapa.setVisibility(View.GONE);
            //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
            mImageEstado.setVisibility(View.VISIBLE);
            mImageUltimoEstado.setVisibility(View.GONE);
            mImageIngresos.setVisibility(View.GONE);
            mImageHijos.setVisibility(View.GONE);

            mAscendingOrder[0] = true;
            mAscendingOrder[1] = true;
            mAscendingOrder[3] = true;
            mAscendingOrder[4] = true;

        }
    };

    private View.OnClickListener mUltimoEstadoListener = new View.OnClickListener() {
        public void onClick(View v) {

            if (mAscendingOrder[3]) {
                mAscendingOrder[3] = false;
                mAdapter.ordenarPorFecUltEstadoAsc();
                mImageUltimoEstado.setImageResource(R.drawable.ic_ordenar_up);
            } else {
                mAscendingOrder[3] = true;
                mAdapter.ordenarPorFecUltEstadoDesc();
                mImageUltimoEstado.setImageResource(R.drawable.ic_ordenar_down);
            }
            // Make visible the arrow next to the name and make the others invisible
            mImageProspecto.setVisibility(View.GONE);
            mImageEtapa.setVisibility(View.GONE);
            //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
            mImageEstado.setVisibility(View.GONE);
            mImageUltimoEstado.setVisibility(View.VISIBLE);
            mImageIngresos.setVisibility(View.GONE);
            mImageHijos.setVisibility(View.GONE);

            mAscendingOrder[0] = true;
            mAscendingOrder[1] = true;
            mAscendingOrder[2] = true;
            mAscendingOrder[4] = true;
        }
    };

    private View.OnClickListener mIngresosListener = new View.OnClickListener() {
        public void onClick(View v) {

            if (mAscendingOrder[4]) {
                mAscendingOrder[4] = false;
                mAdapter.ordenarPorIngresosAsc();
                mImageIngresos.setImageResource(R.drawable.ic_ordenar_up);
            } else {
                mAscendingOrder[4] = true;
                mAdapter.ordenarPorIngresosDesc();
                mImageIngresos.setImageResource(R.drawable.ic_ordenar_down);
            }
            // Make visible the arrow next to the name and make the others invisible
            mImageProspecto.setVisibility(View.GONE);
            mImageEtapa.setVisibility(View.GONE);
            //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
            mImageEstado.setVisibility(View.GONE);
            mImageUltimoEstado.setVisibility(View.GONE);
            mImageIngresos.setVisibility(View.VISIBLE);
            mImageHijos.setVisibility(View.GONE);

            mAscendingOrder[0] = true;
            mAscendingOrder[1] = true;
            mAscendingOrder[2] = true;
            mAscendingOrder[3] = true;
        }
    };

    private View.OnClickListener mHijosListener = new View.OnClickListener() {
        public void onClick(View v) {

            if (mAscendingOrder[5]) {
                mAscendingOrder[5] = false;
                mAdapter.ordenarPorTieneHijosAsc();
                mImageHijos.setImageResource(R.drawable.ic_ordenar_up);
            } else {
                mAscendingOrder[5] = true;
                mAdapter.ordenarPorTieneHijosDesc();
                mImageHijos.setImageResource(R.drawable.ic_ordenar_down);
            }
            // Make visible the arrow next to the name and make the others invisible
            mImageProspecto.setVisibility(View.GONE);
            mImageEtapa.setVisibility(View.GONE);
            //mImageEtapa.setLayoutParams(new LayoutParams(0,LayoutParams.WRAP_CONTENT));
            mImageEstado.setVisibility(View.GONE);
            mImageUltimoEstado.setVisibility(View.GONE);
            mImageIngresos.setVisibility(View.GONE);
            mImageHijos.setVisibility(View.VISIBLE);

            mAscendingOrder[0] = true;
            mAscendingOrder[1] = true;
            mAscendingOrder[2] = true;
            mAscendingOrder[3] = true;
            mAscendingOrder[4] = true;
        }
    };

    private ArrayList<TusContactosBean> obtenerTusContactos() {
 //       CarteraProspectoController prospectoController = new CarteraProspectoController();

        /*ArrayList<TusContactosBean> lstContactos = new ArrayList<>();

        ArrayList<TusContactosBean> lstContactosReagendados = prospectoController.obtenerTusContactosReagendados();
        ArrayList<TusContactosBean> lstContactosAgendadosOPorContactar = prospectoController.obtenerTusContactosAgendadosOPorContactar();
        ArrayList<TusContactosBean> lstContactosEstadoRealizadoNoInteresado = prospectoController.obtenerTusContactosEstadoRealizadoNoInteresado();
        lstContactos.addAll(lstContactosReagendados);
        lstContactos.addAll(lstContactosAgendadosOPorContactar);
        if (Constantes.booleanAgregarRecordatorioLlamadaTusContactos) {
            ArrayList<TusContactosBean> lstContactosRecordatorioLLamada = prospectoController.obtenerTusContactosRecordatorioLLamada();
            lstContactos.addAll(lstContactosRecordatorioLLamada);
        }
        lstContactos.addAll(lstContactosEstadoRealizadoNoInteresado);
        */

        ArrayList<TusContactosBean> lstContactos = CarteraProspectoController.obtenerTusContactos();

        return lstContactos;
    }



    // procesar lista. poner iconos, orden alfabetico, formato de fecha
    private ArrayList<TusContactosBean> llenarListaTusContactos(ArrayList<TusContactosBean> listaTusContactos) {

        // ordenar por nombre asc

        Comparator<TusContactosBean> comparator = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object1.getNomProspecto().compareToIgnoreCase(object2.getNomProspecto());
            }
        };
        Collections.sort(listaTusContactos, comparator);

        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaRangoDeIngresos = tablasController.obtenerMapaTablas(Constantes.codTablaRangoIngresos);
        Map<Integer, TablaTablasBean> mapTablaEtapaProspecto = tablasController.obtenerMapaTablas(Constantes.codTablaEtapaProspecto);
        Map<Integer, TablaTablasBean> mapTablaEstadoCita = tablasController.obtenerMapaTablas(Constantes.codTablaEstadoCita);
        Map<Integer, TablaTablasBean> mapTablaResultadoCita = tablasController.obtenerMapaTablas(Constantes.codTablaResultadoCita);
        DrawableHelper drawableHelper = new DrawableHelper(getContext(), getResources());
        Map<Integer, Drawable> mapDrawableEtapa = drawableHelper.obtenerMapaDrawableEtapa();
        Map<Integer, Drawable> mapDrawableEstado = drawableHelper.obtenerMapaDrawableEstado();

        PrimerLetraAlfabetoHelper hlpAlfabetoAsc = new PrimerLetraAlfabetoHelper(true);

        String primeraLetraUnaVez = "";

        String codResultado_recLlamado = Constantes.codResultado_recLlamado;
        String codResultado_reagendada = Constantes.codResultado_reagendada;

        Drawable drwIcnAgendada = mapDrawableEstado.get(Constantes.codEstadoAgendada);
        Drawable drwIcnReAgendada = mapDrawableEstado.get(Constantes.codEstadoReagendada);
        Drawable drwIcnRealizada = mapDrawableEstado.get(Constantes.codEstadoRealizada);
        Drawable drwIcnRealizadaCerrada = mapDrawableEstado.get(Constantes.codIcnRealizadaCerrada);
        Drawable drwIcnRecDeLlamado = mapDrawableEstado.get(Constantes.codIcnRecDeLlamado);
        Drawable drwIcnNoInteresado = mapDrawableEstado.get(Constantes.codIcnNoInteresado);
        Drawable drwIcnNuevo = mapDrawableEstado.get(Constantes.codIcnNuevo);

        for (TusContactosBean tusContactos : listaTusContactos) {

            primeraLetraUnaVez = hlpAlfabetoAsc.getPrimeraLetraUnaVez(tusContactos.getNomProspecto());
            tusContactos.setPrimeraLetraAsc(primeraLetraUnaVez);


            // etapa
            if (tusContactos.getCodEtapa() == null) {
                tusContactos.setDesEtapa("");
            } else {
                tusContactos.setDesEtapa(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEtapaProspecto, tusContactos.getCodEtapa()));
                tusContactos.setDrwEtapa(mapDrawableEtapa.get(tusContactos.getCodEtapa()));
            }

            // estado
            if (tusContactos.getCodEstado() == null || tusContactos.getCodResultado() == null) {
                tusContactos.setDesEstado("");
                //tusContactos.setDesEstado2("");
                tusContactos.setDesResultado("");
            } else {

                String etapaProspecto = tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEtapaProspecto, tusContactos.getCodEtapa());
                String estadoCita = tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEstadoCita, tusContactos.getCodEstado());
                String resultadoCita = tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaResultadoCita, tusContactos.getCodResultado());

                Drawable drwEstado = mapDrawableEstado.get(tusContactos.getCodEstado());

                //Se cambio a dispositivo - Log.d("TF", "llenarListaTusContactos ip: " + tusContactos.getCodProspecto() + " casoTipo: " + tusContactos.getCasoTipo());
                Log.d("TF", "llenarListaTusContactos ip: " + tusContactos.getIdProspectoDispositivo() + " casoTipo: " + tusContactos.getCasoTipo());

                if (tusContactos.getCodEtapa() == Constantes.codEtapaNuevo) {
                    tusContactos.setDesEstado(etapaProspecto);
                }
                else if(tusContactos.getEsReagendada() == Constantes.constante_si && tusContactos.getCodEstado() == Constantes.codEstadoAgendada)
                {
                    tusContactos.setDesEstado(getResources().getString(R.string.re_agendada));
                }
                else
                    tusContactos.setDesEstado(estadoCita);

                // Seteando Icono
                if (tusContactos.getTieneRecordatorio() == Constantes.constante_si && tusContactos.getCodResultado() == Constantes.codResultado_pendiente) { // TODO: Volver a llamar
                    tusContactos.setDrwEstado(drwIcnRecDeLlamado);
                }
                else if (tusContactos.getEsReagendada() == Constantes.constante_si && tusContactos.getCodResultado() == Constantes.codResultado_pendiente){
                    tusContactos.setDrwEstado(drwIcnReAgendada);
                }
                else if (tusContactos.getCodEtapa() == Constantes.codEtapaNuevo)
                {
                   tusContactos.setDrwEstado(drwIcnNuevo);
                }else if (tusContactos.getCodResultado() == Constantes.codResultado_pendiente)
                {
                    tusContactos.setDrwEstado(drwIcnAgendada);
                }else if (tusContactos.getCodResultado() == Constantes.codResultado_siguienteEtapa){
                    tusContactos.setDrwEstado(drwIcnRealizada);
                }else if (tusContactos.getCodResultado() == Constantes.codResultado_cierreVenta){
                    tusContactos.setDrwEstado(drwIcnRealizadaCerrada);
                }else if (tusContactos.getCodResultado() == Constantes.codResultado_noInteresado){
                    tusContactos.setDrwEstado(drwIcnNoInteresado);
                }else if (tusContactos.getCodResultado() == Constantes.codResultado_porContactar){
                    tusContactos.setDrwEstado(drwIcnRecDeLlamado);
                }

                if (tusContactos.getCodResultado() == Constantes.codResultado_noInteresado ||
                    tusContactos.getCodResultado() == Constantes.codResultado_cierreVenta /*||
                    tusContactos.getCodResultado() == Constantes.codResultado_cierreVenta*/)
                {
                    tusContactos.setDesResultado(resultadoCita);
                }else if ( tusContactos.getTieneRecordatorio() == Constantes.constante_si)
                {
                    tusContactos.setDesResultado(getResources().getString(R.string.rec_llamada));
                }
                else
                    tusContactos.setDesResultado("");

           }

            // ingresos
            // etapa
            if (tusContactos.getCodIngresos() == null) {
                tusContactos.setDesIngresos("-");
            } else {
                tusContactos.setDesIngresos(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaRangoDeIngresos, tusContactos.getCodIngresos()));
            }

            // hijos
            tusContactos.setDesTieneHijos(StringUtils.obtenerTextoBooleano(tusContactos.getCodTieneHijos()));
        }

        // orden descendentemente

        PrimerLetraAlfabetoHelper hlpAlfabetoDesc = new PrimerLetraAlfabetoHelper(false);

        Comparator<TusContactosBean> comparator2 = new Comparator<TusContactosBean>() {

            @Override
            public int compare(TusContactosBean object1, TusContactosBean object2) {
                return object2.getNomProspecto().compareToIgnoreCase(object1.getNomProspecto());
            }
        };
        Collections.sort(listaTusContactos, comparator2);

        for (TusContactosBean tusContactos : listaTusContactos) {
            primeraLetraUnaVez = hlpAlfabetoDesc.getPrimeraLetraUnaVez(tusContactos.getNomProspecto());
            tusContactos.setPrimeraLetraDesc(primeraLetraUnaVez);
        }

        return listaTusContactos;
    }
}