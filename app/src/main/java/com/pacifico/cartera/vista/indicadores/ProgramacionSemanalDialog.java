package com.pacifico.cartera.vista.indicadores;

import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;
import com.pacifico.cartera.Util.Util;

import java.util.ArrayList;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.ProgramacionSemanalDetalleAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.CalendarioProcesadoBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionSemanalDetalleBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DrawableHelper;
import com.pacifico.cartera.utils.ProgramacionSemanalHelper;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

/**
 * Created by dsb on 15/05/2016.
 */
public class ProgramacionSemanalDialog extends android.support.v4.app.DialogFragment {

    private CalendarioProcesadoBean calendarioActual;

    private TextViewCustomFont tvIrAAgenda;
    private TextViewCustomFont tvCerrar;
    private ListView lvDatos;

    public ProgramacionSemanalDialog() {
        super();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.idcd_psem_deta_dial, null);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        getResourceItems(view);

        ArrayList<ProgramacionSemanalDetalleBean> listaEntrevistasAgendadas = getListaEntrevistasAgendadas(calendarioActual);
        ProgramacionSemanalDetalleAdapter adapter = new ProgramacionSemanalDetalleAdapter(getActivity(), llenarEntrevistasAgendadas(listaEntrevistasAgendadas));

        lvDatos.setAdapter(adapter);
        lvDatos.setScrollContainer(true);
        adapter.notifyDataSetChanged();

     /*   setListViewHeightBasedOnItems(lvDatos);*/

        // ajustar dialog a full tamaño
        Rect rect = getWindowDimension();
        view.setMinimumWidth((int) (rect.width() - 22f));
        view.setMinimumHeight((int) (rect.height() - 2f));

        return view;
    }

    public void getResourceItems(View view) {
        lvDatos = (ListView) view.findViewById(R.id.lvDetalle);
        tvIrAAgenda = (TextViewCustomFont) view.findViewById(R.id.tvIrAAgenda);
        tvCerrar = (TextViewCustomFont) view.findViewById(R.id.tvCerrar);

        tvIrAAgenda.setOnClickListener(tvIrAAgendaListener);
        tvCerrar.setOnClickListener(tvCerrarListener);

    }

    private ArrayList<ProgramacionSemanalDetalleBean> getListaEntrevistasAgendadas(CalendarioProcesadoBean calendarioActual) {

        ProgramacionSemanalHelper helper;
        helper = new ProgramacionSemanalHelper(getContext(), getResources());
        helper.init();
        ArrayList<ProgramacionSemanalDetalleBean> lstCitasAgendadas = helper.obtenerEntrevistasAgendadas(calendarioActual);

        return lstCitasAgendadas;
    }

    private ArrayList<ProgramacionSemanalDetalleBean> llenarEntrevistasAgendadas(ArrayList<ProgramacionSemanalDetalleBean> lstEntrevistasAgendadas) {

        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaFuenteDelContacto = tablasController.obtenerMapaTablas(Constantes.codTablaFuenteDelContacto);
        Map<Integer, TablaTablasBean> mapTablaEtapaProspecto = tablasController.obtenerMapaTablas(Constantes.codTablaEtapaProspecto);
        Map<Integer, TablaTablasBean> mapTablaEstadoCita = tablasController.obtenerMapaTablas(Constantes.codTablaEstadoCita);
        DrawableHelper drawableHelper = new DrawableHelper(getContext(), getResources());
        Map<Integer, Drawable> mapDrawableEtapa = drawableHelper.obtenerMapaDrawableEtapa();
        Map<Integer, Drawable> mapDrawableEstado = drawableHelper.obtenerMapaDrawableEstado();

        ArrayList<ProgramacionSemanalDetalleBean> lstProgramacion = new ArrayList<>();

        for (ProgramacionSemanalDetalleBean entrevista : lstEntrevistasAgendadas) {

            //entrevista.setNomProspecto(Constantes.desReferenciador + entrevista.getNomProspecto());
            if (entrevista.getNomReferenciador() == null) entrevista.setNomReferenciador("");
            entrevista.setNomReferenciador(Constantes.desReferenciador + entrevista.getNomReferenciador());

            // etapa
            if (entrevista.getCodEtapa() == null) {
                entrevista.setDesEtapa("");
            } else {
                entrevista.setDesEtapa(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEtapaProspecto, entrevista.getCodEtapa()));
                entrevista.setDrwEtapa(mapDrawableEtapa.get(entrevista.getCodEtapa()));
            }

            // estado
            if (entrevista.getCodEstado() == null || entrevista.getCodResultado() == null) {
                entrevista.setDesEstado("");
                entrevista.setDesEstado2("");
            } else {

                //            entrevista.setDesFecha(DateUtils.getDateFormat03(entrevista.getFecha()));

                // si tiene fecha reagendado
                // icono debe ser reagendado y se debe colocar la fecha
                //Log.d("PSH", " llenarEntrevistasAgendadas id: " + entrevista.getCodProspecto() + " fecreagendado: " + entrevista.getFecReagendado());
                Log.d("PSH", " llenarEntrevistasAgendadas id: " + entrevista.getIdProspectoDispositivo() + " fecreagendado: " + entrevista.getFecReagendado());
                entrevista.setDesEstado("");
                entrevista.setDesEstado2(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEstadoCita, entrevista.getCodEstado()));
                entrevista.setDrwEstado(mapDrawableEstado.get(entrevista.getCodEstado()));
            }

            // fuente
            if (entrevista.getCodFuente() == null) {
                entrevista.setDesFuente("");
            } else {
                entrevista.setDesFuente(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaFuenteDelContacto, entrevista.getCodFuente()));
            }

            lstProgramacion.add(entrevista);
        }

        return lstProgramacion;
    }

    private View.OnClickListener tvCerrarListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("IDCD_PS", "tvCerrarListener");
            getDialog().dismiss();
        }
    };

    private View.OnClickListener tvIrAAgendaListener = new View.OnClickListener() {
        public void onClick(View v) {
            Log.d("IDCD_PS", "tvIrAAgendaListener");
            try{
                Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.pacifico.agenda");
                if(launchIntent != null){
                    //  launchIntent.putExtra("tipo_operacion", 1);
                    //  launchIntent.putExtra("idProspecto", adnActivity.getProspectoBean().getIdProspecto());
                    startActivity(launchIntent);
                }
            }catch(Exception e){
                Util.mostrarAlertaConTitulo("Alerta", "No se pudo abrir agenda", ProgramacionSemanalDialog.this.getContext());
            }
        }
    };

    private Rect getWindowDimension() {
        //View rootView = inflater.inflate(R.layout.idcd_psem_deta_dial, container, false);
        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        //getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        // WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        // wmlp.gravity = (Gravity.FILL_HORIZONTAL | Gravity.FILL_VERTICAL);


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(getDialog().getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(lp);

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = getActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        //layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
        //layout.setMinimumHeight((int) (displayRectangle.height() * 0.9f));


// inflate and adjust layout
        //inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return displayRectangle;
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {

        Log.d("PSF", "1");
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();
            //  Log.d("PSF", "numberOfItems: " + numberOfItems);


            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }

            //  Log.d("PSF", "totalItemsHeight: " + totalItemsHeight);

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Log.d("PSF", "totalDividersHeight: " + totalDividersHeight);


            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();

            // Log.d("PSF", "height: " + params.height);
            params.height = totalItemsHeight + totalDividersHeight + 2;

            // Log.d("PSF", "new height: " + params.height);


            listView.setLayoutParams(params);
            listView.requestLayout();

            return true;

        } else {
            return false;
        }

    }


    public CalendarioProcesadoBean getCalendarioActual() {
        return calendarioActual;
    }

    public void setCalendarioActual(CalendarioProcesadoBean calendarioActual) {
        this.calendarioActual = calendarioActual;
    }
}
