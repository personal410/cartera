package com.pacifico.cartera.vista.controles;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.pacifico.cartera.R;

/**
 * Created by dsb on 16/05/2016.
 */
public class EditTextCustomFont extends EditText{
    private String customError;
    public EditTextCustomFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.configure();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.EditTextCustomFont, 0, 0);
        try{
            String typeFaceName = typedArray.getString(R.styleable.EditTextCustomFont_typeFace2);
            if(typeFaceName != null && !typeFaceName.isEmpty()){
                this.setTypeface(Typeface.createFromAsset(context.getAssets(), typeFaceName));
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            typedArray.recycle();
        }
    }
    public void configure(){
        this.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6) {
                    EditTextCustomFont.this.clearFocus();
                    InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(EditTextCustomFont.this.getWindowToken(), 0);
                }
                return false;
            }
        });
    }
    @Override
    public void setError(CharSequence error){
        if (error == null){
            customError = null;
            setCompoundDrawables(null, null, null, null);
        }else{
            customError = error.toString();
            Drawable errorIcon = getResources().getDrawable(R.drawable.ic_error);
            errorIcon.setBounds(new Rect(0, 0, errorIcon.getIntrinsicWidth(), errorIcon.getIntrinsicHeight()));
            setCompoundDrawables(null, null, errorIcon, null);
        }
    }
    @Override
    public CharSequence getError(){
        return customError;
    }
    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event){
        if(event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
            this.clearFocus();
        }
        return super.onKeyPreIme(keyCode, event);
    }
}