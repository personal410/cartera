package com.pacifico.cartera.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.CalendarioBean;
import com.pacifico.cartera.Model.Bean.MensajeSistemaBean;
import com.pacifico.cartera.Model.Bean.TablaIndiceBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;

public class TablasGeneralesController{
    public static void guardarTablaIndice(TablaIndiceBean tablaIndice) {
        TablaIndiceBean.tableHelper.insertEntity(tablaIndice);
    }
    public static void guardarListaTablaIndice(ArrayList<TablaIndiceBean> listaTablaIndiceBean){
        if (listaTablaIndiceBean != null && listaTablaIndiceBean.size()> 0)
            for(int i = 0; i < listaTablaIndiceBean.size(); i++)
                TablaIndiceBean.tableHelper.insertEntity(listaTablaIndiceBean.get(i));
    }
    public static void guardarTablaTablas(TablaTablasBean tablaTablas) {
        TablaTablasBean.tableHelper.insertEntity(tablaTablas);
    }
    public static void guardarListaTablaTablas(ArrayList<TablaTablasBean> listaTablaTablasBean){
        if (listaTablaTablasBean != null && listaTablaTablasBean.size()>0)
            for(int i =0; i<listaTablaTablasBean.size(); i++)
                TablaTablasBean.tableHelper.insertEntity(listaTablaTablasBean.get(i));
    }

    public static void actualizarTablaIndice(TablaIndiceBean tablaIndiceBean){
        String[] parametros = {Integer.toString(tablaIndiceBean.getIdTabla())};
        TablaIndiceBean.tableHelper.updateEntity(tablaIndiceBean, "IDTABLA = ?", parametros);
    }
    public static ArrayList<TablaTablasBean> obtenerTablaTablasPorIdTabla(int IdTabla){
        String[] parameters = new String[]{Integer.toString(IdTabla)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND FLAGACTIVO = 1", parameters);
        ArrayList<TablaTablasBean> arrTablaTablasFinal = new ArrayList<>();
        for (Entity entity : arrTablaTablas) {
            arrTablaTablasFinal.add((TablaTablasBean)entity);
        }
        return arrTablaTablasFinal;
    }
    public static void guardarCalendario(CalendarioBean calendarioBean) {
        CalendarioBean.tableHelper.insertEntity(calendarioBean);
    }
    public static void guardarListaCalendario(ArrayList<CalendarioBean> listaCalendarioBean) {
        if(listaCalendarioBean != null)
            for(CalendarioBean calendarioBean:listaCalendarioBean){
                CalendarioBean.tableHelper.insertEntity(calendarioBean);
            }
    }
    public static void guardarMensajeSistema(MensajeSistemaBean mensajeSistemaBean) {
        MensajeSistemaBean.tableHelper.insertEntity(mensajeSistemaBean);
    }
    public static void guardarListaMensajesSistemas(ArrayList<MensajeSistemaBean> listaMensajeSistemaBean){
        if(listaMensajeSistemaBean != null)
            for(MensajeSistemaBean mensajeSistemaBean : listaMensajeSistemaBean){
                MensajeSistemaBean.tableHelper.insertEntity(mensajeSistemaBean);
            }
    }
    public static TablaTablasBean obtenerTablaTablasPorIdTablaCodigoCampo(int idTabla, int codigoCampo){
        String[] parametros = {Integer.toString(idTabla), Integer.toString(codigoCampo)};
        ArrayList<Entity> arrTablaTablas = TablaTablasBean.tableHelper.getEntities("IDTABLA = ? AND CODIGOCAMPO = ? AND FLAGACTIVO = 1", parametros);
        if(arrTablaTablas.size() > 0){
            return (TablaTablasBean)arrTablaTablas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarTablaTablas(TablaTablasBean tablaTablasBean){
        String[] parametros = {Integer.toString(tablaTablasBean.getIdTabla()), Integer.toString(tablaTablasBean.getCodigoCampo())};
        TablaTablasBean.tableHelper.updateEntity(tablaTablasBean, "IDTABLA = ? AND CODIGOCAMPO = ? AND FLAGACTIVO = 1", parametros);
    }

    public static TablaIndiceBean obtenerTablaIndicePorIdTabla(int idTabla){
        String[] parametros = new String[]{Integer.toString(idTabla)};
        ArrayList<Entity> arrTablaIndices = TablaIndiceBean.tableHelper.getEntities("IDTABLA = ?", parametros);
        if (arrTablaIndices.size() > 0){
            return  (TablaIndiceBean)arrTablaIndices.get(0);
        }else{
            return null;
        }
    }

    public static CalendarioBean obtenerCalendarioPorIdCalendario(int idCalendario){
        String[] parametros = {Integer.toString(idCalendario)};
        ArrayList<Entity> arrTablaTablas = CalendarioBean.tableHelper.getEntities("IDCALENDARIO = ?", parametros);
        if(arrTablaTablas.size() > 0){
            return (CalendarioBean)arrTablaTablas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarCalendario(CalendarioBean calendarioBean){
        String[] parametros = {Integer.toString(calendarioBean.getIdCalendario())};
        CalendarioBean.tableHelper.updateEntity(calendarioBean, "IDCALENDARIO = ?", parametros);
    }
    public static MensajeSistemaBean obtenerMensajeSistemaPorIdMensajeSistema(int idMensajeSistema){
        String[] parametros = {Integer.toString(idMensajeSistema)};
        ArrayList<Entity> arrMensajeSistemas = MensajeSistemaBean.tableHelper.getEntities("IDMENSAJESISTEMA = ?", parametros);
        if(arrMensajeSistemas.size() > 0){
            return (MensajeSistemaBean)arrMensajeSistemas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarMensajeSistema(MensajeSistemaBean mensajeSistemaBean){
        String[] parametros = {Integer.toString(mensajeSistemaBean.getIdMensajeSistema())};
        MensajeSistemaBean.tableHelper.updateEntity(mensajeSistemaBean, "IDMENSAJESISTEMA = ?", parametros);
    }
}