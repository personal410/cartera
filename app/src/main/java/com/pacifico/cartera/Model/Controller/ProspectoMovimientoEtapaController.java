package com.pacifico.cartera.Model.Controller;

import android.database.Cursor;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Persistence.DatabaseConstants;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;


public class ProspectoMovimientoEtapaController{
    public static void guardarListaProspectoMovimientoEtapa(ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean){
        if(listaProspectoMovimientoEtapaBean != null){
            for(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean : listaProspectoMovimientoEtapaBean){
                if(prospectoMovimientoEtapaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(prospectoMovimientoEtapaBean.getIdProspecto());
                    prospectoMovimientoEtapaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                ProspectoMovimientoEtapaBean.tableHelper.insertEntity(prospectoMovimientoEtapaBean);
            }
        }
    }

    public static int obtenerMaximoIdProspectoMovimientoDispositivo(){
        int idProspectoMovimientoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDMOVIMIENTODISPOSITIVO) FROM " + DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idProspectoMovimientoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idProspectoMovimientoDispositivoMax;
    }
    public static ArrayList<ProspectoMovimientoEtapaBean> obtenerProspectoMovimientoEtapaSinEnviar(){
        ArrayList<Entity> arrBeans = ProspectoMovimientoEtapaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ProspectoMovimientoEtapaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ProspectoMovimientoEtapaBean) entity);
        }
        return arrBeanFinal;
    }
    public static ProspectoMovimientoEtapaBean obtenerProspectoMovimientoEtapaPorIdMovimientoDispositivo(int idMovimientoDispositivo){
        String[] parametros = new String[]{Integer.toString(idMovimientoDispositivo)};
        ArrayList<Entity> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaBean.tableHelper.getEntities("IDMOVIMIENTODISPOSITIVO = ?", parametros);
        if(arrProspectoMovimientoEtapas.size() > 0){
            return (ProspectoMovimientoEtapaBean)arrProspectoMovimientoEtapas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarProspectoMovimientoEtapa(ProspectoMovimientoEtapaBean prospectoMovimientoEtapaBean){
        String[] parametros = new String[]{Integer.toString(prospectoMovimientoEtapaBean.getIdMovimientoDispositivo())};
        ProspectoMovimientoEtapaBean.tableHelper.updateEntity(prospectoMovimientoEtapaBean, "IDMOVIMIENTODISPOSITIVO = ?", parametros);
    }
}