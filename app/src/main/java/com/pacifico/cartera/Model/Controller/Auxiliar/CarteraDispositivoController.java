package com.pacifico.cartera.Model.Controller.Auxiliar;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.DispositivoBean;

import java.util.ArrayList;

/**
 * Created by xcode on 25/08/2016.
 */
public class CarteraDispositivoController {

    public static DispositivoBean obtenerDispositivo(){
        ArrayList<Entity> arrDispositivo = DispositivoBean.tableHelper.getEntities("", null);
        if(arrDispositivo.size() > 0){
            return (DispositivoBean) arrDispositivo.get(0);
        }else{
            return null;
        }
    }
}
