package com.pacifico.cartera.Model.Bean;

public class CitaProspectoBean {

    private String idCita = "";
    private String fechaCita = "";
    private String idProspecto = "";
    private String nombres = "";
    private String apellidoPaterno = "";
    private String apellidoMaterno = "";
    private String idReferenciador = "";

    private String horaIncioCita = "";
    private String horaFinCita = "";


    public CitaProspectoBean()
    {

    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getIProspecto() {
        return idProspecto;
    }

    public void setIdProspecto(String idProspecto) {
        this.idProspecto = idProspecto;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getIdReferenciador() {
        return idReferenciador;
    }

    public void setIdReferenciador(String idReferenciador) {
        this.idReferenciador = idReferenciador;
    }

    public String getFechaCita() {
        return fechaCita;
    }

    public void setFechaCita(String fechaCita) {
        this.fechaCita = fechaCita;
    }

    public String getHoraIncioCita() {
        return horaIncioCita;
    }

    public void setHoraIncioCita(String horaIncioCita) {
        this.horaIncioCita = horaIncioCita;
    }

    public String getHoraFinCita() {
        return horaFinCita;
    }

    public void setHoraFinCita(String horaFinCita) {
        this.horaFinCita = horaFinCita;
    }
}
