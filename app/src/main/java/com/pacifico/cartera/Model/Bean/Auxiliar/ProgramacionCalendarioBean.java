package com.pacifico.cartera.Model.Bean.Auxiliar;

import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsb on 23/06/2016.
 */
public class ProgramacionCalendarioBean {

    private List<DiaSemanaBean> lstDiaSemanaBean;
    private ArrayList<ArrayList<String>> listCitas;
    private long numero1rasEntrevistasAgendadas;
    private long numero2dasEntrevistasAgendadas;
    private long numero1rasEntrevistasRealizadas;
    private long numero2dasEntrevistasRealizadas;
    private long totalReferidos;
    private long totalObservacionesPersonales;
    private ArrayList<BarEntry> entries1ra;
    private ArrayList<BarEntry> entries2da;


    public ProgramacionCalendarioBean() {

    }

    public ProgramacionCalendarioBean(List<DiaSemanaBean> lstDiaSemanaBean, ArrayList<ArrayList<String>> listCitas,
                                      long numero1rasEntrevistasAgendadas, long numero2dasEntrevistasAgendadas,
                                      long numero1rasEntrevistasRealizadas, long numero2dasEntrevistasRealizadas,
                                      long totalReferidos, long totalObservacionesPersonales,
                                      ArrayList<BarEntry> entries1ra, ArrayList<BarEntry> entries2da) {

        this.lstDiaSemanaBean = lstDiaSemanaBean;
        this.listCitas = listCitas;
        this.numero1rasEntrevistasAgendadas = numero1rasEntrevistasAgendadas;
        this.numero2dasEntrevistasAgendadas = numero2dasEntrevistasAgendadas;
        this.numero1rasEntrevistasRealizadas = numero1rasEntrevistasRealizadas;
        this.numero2dasEntrevistasRealizadas = numero2dasEntrevistasRealizadas;
        this.totalReferidos = totalReferidos;
        this.totalObservacionesPersonales = totalObservacionesPersonales;
        this.entries1ra = entries1ra;
        this.entries2da = entries2da;

    }


    public List<DiaSemanaBean> getLstDiaSemanaBean() {
        return lstDiaSemanaBean;
    }

    public void setLstDiaSemanaBean(List<DiaSemanaBean> lstDiaSemanaBean) {
        this.lstDiaSemanaBean = lstDiaSemanaBean;
    }

    public ArrayList<ArrayList<String>> getListCitas() {
        return listCitas;
    }

    public void setListCitas(ArrayList<ArrayList<String>> listCitas) {
        this.listCitas = listCitas;
    }

    public long getNumero1rasEntrevistasAgendadas() {
        return numero1rasEntrevistasAgendadas;
    }

    public void setNumero1rasEntrevistasAgendadas(long numero1rasEntrevistasAgendadas) {
        this.numero1rasEntrevistasAgendadas = numero1rasEntrevistasAgendadas;
    }

    public long getNumero2dasEntrevistasAgendadas() {
        return numero2dasEntrevistasAgendadas;
    }

    public void setNumero2dasEntrevistasAgendadas(long numero2dasEntrevistasAgendadas) {
        this.numero2dasEntrevistasAgendadas = numero2dasEntrevistasAgendadas;
    }

    public long getNumero1rasEntrevistasRealizadas() {
        return numero1rasEntrevistasRealizadas;
    }

    public void setNumero1rasEntrevistasRealizadas(long numero1rasEntrevistasRealizadas) {
        this.numero1rasEntrevistasRealizadas = numero1rasEntrevistasRealizadas;
    }

    public long getNumero2dasEntrevistasRealizadas() {
        return numero2dasEntrevistasRealizadas;
    }

    public void setNumero2dasEntrevistasRealizadas(long numero2dasEntrevistasRealizadas) {
        this.numero2dasEntrevistasRealizadas = numero2dasEntrevistasRealizadas;
    }

    public long getTotalReferidos() {
        return totalReferidos;
    }

    public void setTotalReferidos(long totalReferidos) {
        this.totalReferidos = totalReferidos;
    }

    public long getTotalObservacionesPersonales() {
        return totalObservacionesPersonales;
    }

    public void setTotalObservacionesPersonales(long totalObservacionesPersonales) {
        this.totalObservacionesPersonales = totalObservacionesPersonales;
    }

    public ArrayList<BarEntry> getEntries2da() {
        return entries2da;
    }

    public void setEntries2da(ArrayList<BarEntry> entries2da) {
        this.entries2da = entries2da;
    }

    public ArrayList<BarEntry> getEntries1ra() {
        return entries1ra;
    }

    public void setEntries1ra(ArrayList<BarEntry> entries1ra) {
        this.entries1ra = entries1ra;
    }
}
