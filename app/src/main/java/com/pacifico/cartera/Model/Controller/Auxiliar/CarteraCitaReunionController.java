package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaProspectoBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProgramacionSemanalDetalleBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;

/**
 * Created by Joel on 13/06/2016.
 */
public class CarteraCitaReunionController {

    public Iterator<Entity> obtenerEntrevistas() {

        Log.d("CRC", "obtenerEntrevistas");
        Log.d("CRC", " ");

        CitaBean citaBean = null;

        Iterator<Entity> citas;

        citas = CitaBean.tableHelper.getEntities(
                CitaBean.CN_CODIGOETAPAPROSPECTO,
                null).iterator();


        while (citas.hasNext()) {
            citaBean = (CitaBean) citas.next();
            imprimirCita(citaBean);
        }

        return citas;
    }

    // Obtener entrevistas agendadas
 /*
    public List<CitaBean> obtenerEntrevistasAgendadas(String codigoEtapa, String strFecha) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerEntrevistasAgendadas codigoEtapa: " + codigoEtapa + " strFecha: " + strFecha);

        CitaBean citaBean = null;

        List lstCitas = new ArrayList<>();
        Iterator<Entity> citas;

//        citas = CitaBean.tableHelper.getEntities(
//                CitaBean.CN_CODIGOETAPA + " = ?  AND " +  CitaBean.CN_FECHACITA  + " = ? ",
//                new String[] { codigoEtapa, strFecha }).iterator();

        citas = CitaBean.tableHelper.getEntities(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        " ( " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoAgendada +
                        " OR " + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada +
                        " OR (" + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoReagendada +
                        " AND " + CitaBean.CN_CODIGORESULTADO + " = " + Constantes.codResultado_cierreVenta + ") " +
                        " ) AND " +
                        CitaBean.CN_FECHACITA + " = ? ",
                new String[]{codigoEtapa, strFecha}).iterator();

        while (citas.hasNext()) {
            citaBean = (CitaBean) citas.next();
            lstCitas.add(citaBean);
            imprimirCita(citaBean);
        }

        return lstCitas;
    }*/

    // Obtiene la cantidad de agendadas para la barra de programacion semanal
    public Map<String, Integer> obtenerNumeroEntrevistasAgendadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerNumeroEntrevistasAgendadas fi:" + strFechaInicio + " ff:" + strFechaFin);

        // CODIGOESTADO = 2 OR CODIGOESTADO = 3 OR CODIGOESTADO = 4
        // '2016-07-05' AND '2016-07-11'
        // CODIGOETAPA = @codigoEtapa

        /*
        11	1	Por Contactar
        11	2	Agendada
        11	3	Realizada
        11	4	Re-agendado
        */

        String sql = String.format(
               /* " " +
                        " SELECT COUNT(*), T1.FECHACITA,t1.CODIGOETAPAPROSPECTO  FROM " +
                        " (" +
                        " SELECT DISTINCT p.IDPROSPECTODISPOSITIVO, UC.FECHACITA, UC.CODIGOETAPAPROSPECTO" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, " +
                        "    c.IdProspectoDispositivo, c.CODIGOESTADO, c.CODIGORESULTADO, c.CODIGOETAPAPROSPECTO  " +
                        "    FROM tbl_cita c  " +
                        "    GROUP BY c.IdProspectoDispositivo) " +
                        "  UC" +
                        "    on UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO " +
                        "    WHERE (UC.CODIGOESTADO = %s OR UC.CODIGOESTADO = %s)" +
                        "    AND UC.FECHACITA BETWEEN '%s' AND '%s'" +
                        " ) t1" +
                        " WHERE t1.CODIGOETAPAPROSPECTO = %s or t1.CODIGOETAPAPROSPECTO = %s " +
                        " GROUP BY t1.FECHACITA, t1.CODIGOETAPAPROSPECTO" +
                        " ORDER BY t1.FECHACITA, t1.CODIGOETAPAPROSPECTO; "*/
                "SELECT \n" +
                        "\t\t\tCOUNT(*), \n" +
                        "\t\t\tT1.FECHACITA, \n" +
                        "\t\t\tT1.CODIGOETAPAPROSPECTO \n" +
                        "\tFROM \n" +
                        "\t( SELECT  \n" +
                        "\t\t\t\t\tC.FECHACITA, \n" +
                        "\t\t\t\t    C.CODIGOETAPAPROSPECTO,\n" +
                        "\t\t\t\t\tC.CODIGOESTADO,\n" +
                        "\t\t\t\t\tC.NUMEROENTREVISTA,\n" +
                        "\t\t\t\t\tC.IDPROSPECTODISPOSITIVO\n" +
                        "\t\tFROM tbl_cita C \n" +
                        "\t\t ) T1 \n" +
                        "\tWHERE \n" +
                        "\t\tT1.FECHACITA BETWEEN '%S' AND '%S' AND\n" +
                        "\t\t(\n" +
                        "\t\t\tT1.CODIGOESTADO = 2 OR\n" +
                        "\t\t\t(\n" +
                        "\t\t\t\tT1.CODIGOESTADO = 4  AND \n" +
                        "\t\t\t\t(\n" +
                        "\t\t\t\t\tEXISTS(SELECT 1 FROM TBL_CITA CR WHERE CR.NUMEROENTREVISTA = (T1.NUMEROENTREVISTA - 1)  AND CR.IDPROSPECTODISPOSITIVO = T1.IDPROSPECTODISPOSITIVO and CR.CODIGOESTADO <>  4)\n" +
                        "\t\t\t\t\tOR \n" +
                        "\t\t\t\t\tT1.NUMEROENTREVISTA = 1 -- SI ES LA PRIEMRA ENTREVISTA DEL PROSPECTO\n" +
                        "\t\t\t\t)\n" +
                        "\t\t\t)\n" +
                        "\t\t) AND\n" +
                        "\t\t(T1.CODIGOETAPAPROSPECTO = 2 or T1.CODIGOETAPAPROSPECTO = 3 ) \n" +
                        "\tGROUP BY T1.FECHACITA, T1.CODIGOETAPAPROSPECTO \n" +
                        "\tORDER BY T1.FECHACITA, T1.CODIGOETAPAPROSPECTO;",
                strFechaInicio, strFechaFin
        );

        Log.d("CRC", " obtenerNumeroEntrevistasAgendadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        String fechaCita = "";
        int codigoEtapa = 0;
        int cantidad = 0;
        String key = "";

        Map<String, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                fechaCita = cursor.getString(1);
                codigoEtapa = cursor.getInt(2);
                key = String.valueOf(codigoEtapa) + "#" + fechaCita;
                mapNumeroEntrevistas.put(key, cantidad);

            } while (cursor.moveToNext());


        }

        return mapNumeroEntrevistas;
    }

    public Map<Integer, Integer> obtenerTotalEntrevistasAgendadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerTotalEntrevistasAgendadas fi:" + strFechaInicio + " ff:" + strFechaFin);

        String sql = String.format(
                /*" " +
                        " SELECT COUNT(*),t1.CODIGOETAPA  FROM " +
                        " (" +
                        " SELECT DISTINCT p.IDPROSPECTO, UC.FECHACITA, P.CODIGOETAPA" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, " +
                        "    c.IdProspecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "    FROM tbl_cita c  " +
                        "    GROUP BY c.IdProspecto) " +
                        "  UC" +
                        "    on UC.IdProspecto = p.IDPROSPECTO " +
                        "    WHERE (UC.CODIGOESTADO = %s OR UC.CODIGOESTADO = %s)" +
                        "    AND UC.FECHACITA BETWEEN '%s' AND '%s'" +
                        " ) t1" +
                        " WHERE t1.CODIGOETAPA = %s or t1.CODIGOETAPA = %s " +
                        " GROUP BY t1.CODIGOETAPA " +
                        " ORDER BY t1.CODIGOETAPA; ",*/
                "SELECT \n" +
                        "\t\tCOUNT(*), T1.CODIGOETAPAPROSPECTO\n" +
                        "\tFROM\n" +
                        "\t( \n" +
                        "\t\tSELECT \n" +
                        "\t\t\tC.FECHACITA, \n" +
                        "\t\t\tC.CODIGOETAPAPROSPECTO,\n" +
                        "\t\t\tC.CODIGOESTADO,\n" +
                        "\t\t\tC.NUMEROENTREVISTA,\n" +
                        "\t\t\tC.IDPROSPECTODISPOSITIVO\n" +
                        "\t\t FROM tbl_cita C \n" +
                        "\t) T1 \n" +
                        "\tWHERE \n" +
                        "\t\tT1.FECHACITA BETWEEN  '%S' AND  '%S' AND\n" +
                        "\t\t(\n" +
                        "\t\t\tT1.CODIGOESTADO = 2 OR\n" +
                        "\t\t\t(\n" +
                        "\t\t\t\tT1.CODIGOESTADO = 4  AND \n" +
                        "\t\t\t\t(\n" +
                        "\t\t\t\t\tEXISTS(SELECT 1 FROM TBL_CITA CR WHERE CR.NUMEROENTREVISTA = (T1.NUMEROENTREVISTA - 1)  AND CR.IDPROSPECTODISPOSITIVO = T1.IDPROSPECTODISPOSITIVO and CR.CODIGOESTADO <>  4)\n" +
                        "\t\t\t\t\tOR \n" +
                        "\t\t\t\t\tT1.NUMEROENTREVISTA = 1 -- SI ES LA PRIEMRA ENTREVISTA DEL PROSPECTO\n" +
                        "\t\t\t\t)\n" +
                        "\t\t\t)\n" +
                        "\t\t) AND\n" +
                        "\t    (T1.CODIGOETAPAPROSPECTO = 2 or T1.CODIGOETAPAPROSPECTO = 3 ) \n" +
                        "\tGROUP BY T1.CODIGOETAPAPROSPECTO \n" +
                        "\tORDER BY T1.CODIGOETAPAPROSPECTO;",
                strFechaInicio, strFechaFin
        );

        Log.d("CRC", " obtenerTotalEntrevistasAgendadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int codigoEtapa = 0;
        int cantidad = 0;

        Map<Integer, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                codigoEtapa = cursor.getInt(1);
                mapNumeroEntrevistas.put(codigoEtapa, cantidad);

            } while (cursor.moveToNext());


        }

        return mapNumeroEntrevistas;
    }

    // TODO: Revisar Relacionado a Reagendados
    /*
    public Map<String, Integer> obtenerNumeroEntrevistasReagendadasRealizadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerNumeroEntrevistasReagendadasRealizadas fi:" + strFechaInicio + " ff:" + strFechaFin);



        String sql = String.format(
                " SELECT COUNT(*), REAG_REAL.FECHACITA, REAG_REAL.CODIGOETAPA FROM " +
                        " (" +
                        "  SELECT DISTINCT p.IDPROSPECTO, UC.FECHACITA, P.CODIGOETAPA, REAG.codigoetapaprospecto" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "    (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, c.IdProspecto, " +
                        "      c.CODIGOESTADO, c.CODIGORESULTADO" +
                        "     FROM tbl_cita c  " +
                        "     GROUP BY c.IdProspecto) " +
                        "   UC" +
                        "     on UC.IdProspecto = p.IDPROSPECTO" +
                        "   INNER JOIN" +
                        "     (" +
                        "        SELECT DISTINCT IDCITA, IdProspecto, codigoetapaprospecto, CODIGOESTADO  FROM TBL_CITA" +
                        "        WHERE CODIGOESTADO = %s" +
                        "    ) REAG " +
                        "   ON P.IDPROSPECTO = REAG.IdProspecto" +
                        "   WHERE  UC.CodigoEstado = %s " +
                        "           AND UC.FECHACITA BETWEEN '%s' AND '%s' " +
                        "   ) REAG_REAL " +
                        " GROUP BY REAG_REAL.FECHACITA, REAG_REAL.CODIGOETAPA" +
                        " ORDER BY REAG_REAL.FECHACITA, REAG_REAL.CODIGOETAPA;  ",
                Constantes.codEstadoReagendada,
                Constantes.codEstadoRealizada,
                strFechaInicio, strFechaFin);

        Log.d("CRC", " obtenerNumeroEntrevistasReagendadasRealizadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        String fechaCita = "";
        int codigoEtapa = 0;
        int cantidad = 0;
        String key = "";

        Map<String, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                fechaCita = cursor.getString(1);
                codigoEtapa = cursor.getInt(2);
                key = String.valueOf(codigoEtapa) + "#" + fechaCita;
                mapNumeroEntrevistas.put(key, cantidad);

            } while (cursor.moveToNext());


        }

        return mapNumeroEntrevistas;
    }*/

    // TODO: REVISAR LO DE REAGENDADAS
    /*
    public Map<Integer, Integer> obtenerTotalEntrevistasReagendadasRealizadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerTotalEntrevistasReagendadasRealizadas fi:" + strFechaInicio + " ff:" + strFechaFin);

        String sql = String.format(
                " SELECT COUNT(*), REAG_REAL.CODIGOETAPA FROM " +
                        " (" +
                        "  SELECT DISTINCT p.IDPROSPECTO, UC.FECHACITA, P.CODIGOETAPA, REAG.codigoetapaprospecto" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "    (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, c.IdProspecto, " +
                        "      c.CODIGOESTADO, c.CODIGORESULTADO" +
                        "     FROM tbl_cita c  " +
                        "     GROUP BY c.IdProspecto) " +
                        "   UC" +
                        "     on UC.IdProspecto = p.IDPROSPECTO" +
                        "   INNER JOIN" +
                        "     (" +
                        "        SELECT DISTINCT IDCITA, IdProspecto, codigoetapaprospecto, CODIGOESTADO  FROM TBL_CITA" +
                        "        WHERE CODIGOESTADO = %s" +
                        "    ) REAG " +
                        "   ON P.IDPROSPECTO = REAG.IdProspecto" +
                        "   WHERE  UC.CodigoEstado = %s " +
                        "           AND UC.FECHACITA BETWEEN '%s' AND '%s' " +
                        "   ) REAG_REAL " +
                        " GROUP BY REAG_REAL.CODIGOETAPA" +
                        " ORDER BY REAG_REAL.CODIGOETAPA;  ",
                Constantes.codEstadoReagendada,
                Constantes.codEstadoRealizada,
                strFechaInicio, strFechaFin);

        Log.d("CRC", " obtenerTotalEntrevistasReagendadasRealizadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int codigoEtapa = 0;
        int cantidad = 0;

        Map<Integer, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                codigoEtapa = cursor.getInt(1);
                mapNumeroEntrevistas.put(codigoEtapa, cantidad);

            } while (cursor.moveToNext());


        }

        return mapNumeroEntrevistas;
    }*/

    //Obtiene la cantidad de realizadas para la barra de programacion semanal
    public Map<String, Integer> obtenerNumeroEntrevistasRealizadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerNumeroEntrevistasRealizadas fi:" + strFechaInicio + " ff:" + strFechaFin);

        // CODIGOESTADO = 2 OR CODIGOESTADO = 3 OR CODIGOESTADO = 4
        // '2016-07-05' AND '2016-07-11'
        // CODIGOETAPA = @codigoEtapa

        /*
        11	1	Por Contactar
        11	2	Agendada
        11	3	Realizada
        11	4	Re-agendado
        */

        String sql = String.format(
                /*" SELECT COUNT(*), T1.FECHACITA,t1.CODIGOETAPA  FROM " +
                        " (" +
                        " SELECT DISTINCT p.IDPROSPECTO, UC.FECHACITA, P.CODIGOETAPA" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, " +
                        "    c.IdProspecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "    FROM tbl_cita c  " +
                        "    GROUP BY c.IdProspecto) " +
                        "  UC" +
                        "    on UC.IdProspecto = p.IDPROSPECTO " +
                        "    WHERE UC.CODIGOESTADO = %s " +
                        "    AND UC.FECHACITA BETWEEN '%s' AND '%s'" +
                        " ) t1" +
                        " WHERE t1.CODIGOETAPA = %s or t1.CODIGOETAPA = %s " +
                        " AND t1.IDPROSPECTO NOT IN ( %s )" +
                        " GROUP BY t1.FECHACITA, t1.CODIGOETAPA" +
                        " ORDER BY t1.FECHACITA, t1.CODIGOETAPA; "*/
                "SELECT \n" +
                        "\t\tCOUNT(*), \n" +
                        "\t\tT1.FECHACITA, \n" +
                        "\t\tCASE WHEN T1.CODIGOETAPAPROSPECTO =%S THEN %S ELSE T1.CODIGOETAPAPROSPECTO END AS CODIGOETAPA\n" +
                        "FROM \n" +
                        "\t( SELECT \n" +
                        "\t\t\tC.FECHACITA, \n" +
                        "\t\t\tC.CODIGOETAPAPROSPECTO ,\n" +
                        "\t\t\tC.CODIGOESTADO,\n" +
                        "\t\t\tC.CODIGORESULTADO\n" +
                        "\t\tFROM tbl_cita C \n" +
                        "\t\tWHERE \n" +
                        "\t\t\tC.FECHACITA BETWEEN '%S' AND '%S'  AND\n" +
                        "\t\t\tC.CODIGOESTADO = %S AND \n" +
                        "\t\t\t(C.CODIGOETAPAPROSPECTO = %S or c.CODIGOETAPAPROSPECTO= %S OR (C.CODIGOETAPAPROSPECTO = %S AND C.CODIGORESULTADO= %S)) \n" +
                        "\t) T1\n" +
                        "GROUP BY T1.FECHACITA, CODIGOETAPA\n" +
                        "ORDER BY T1.FECHACITA, CODIGOETAPA;;"
                ,
                Constantes.codEtapaAdicional,Constantes.codEtapa2da,
                strFechaInicio, strFechaFin,
                Constantes.codEstadoRealizada,
                Constantes.codEtapa1ra, Constantes.codEtapa2da, Constantes.codEtapaAdicional, Constantes.codResultado_cierreVenta
        );

        Log.d("CRC", " obtenerNumeroEntrevistasRealizadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        String fechaCita = "";
        int codigoEtapa = 0;
        int cantidad = 0;
        String key = "";

        Map<String, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                fechaCita = cursor.getString(1);
                codigoEtapa = cursor.getInt(2);
                key = String.valueOf(codigoEtapa) + "#" + fechaCita;
                mapNumeroEntrevistas.put(key, cantidad);
            } while (cursor.moveToNext());

        }

        return mapNumeroEntrevistas;
    }

    public Map<Integer, Integer> obtenerTotalEntrevistasRealizadasSinReagendadas(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerTotalEntrevistasRealizadasSinReagendadas fi:" + strFechaInicio + " ff:" + strFechaFin);

        String sql = String.format(
                /*" SELECT COUNT(*),t1.CODIGOETAPA  FROM " +
                        " (" +
                        " SELECT DISTINCT p.IDPROSPECTO, UC.FECHACITA, P.CODIGOETAPA" +
                        "  FROM tbl_prospecto p" +
                        "  INNER JOIN  " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, " +
                        "    c.IdProspecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "    FROM tbl_cita c  " +
                        "    GROUP BY c.IdProspecto) " +
                        "  UC" +
                        "    on UC.IdProspecto = p.IDPROSPECTO " +
                        "    WHERE UC.CODIGOESTADO = %s " +
                        "    AND UC.FECHACITA BETWEEN '%s' AND '%s'" +
                        " ) t1" +
                        " WHERE t1.CODIGOETAPA = %s or t1.CODIGOETAPA = %s " +
                        " AND t1.IDPROSPECTO NOT IN ( %s )" +
                        " GROUP BY t1.CODIGOETAPA" +
                        " ORDER BY t1.CODIGOETAPA; ",
                Constantes.codEstadoRealizada,
                strFechaInicio, strFechaFin,
                Constantes.codEtapa1ra, Constantes.codEtapa2da,
                queryProspectosReagendados(strFechaInicio, strFechaFin)*/
        "SELECT \n" +
                "\t\tCOUNT(*), \n" +
                "\t\tCASE WHEN T1.CODIGOETAPAPROSPECTO =4 THEN 3 ELSE T1.CODIGOETAPAPROSPECTO END AS CODIGOETAPA\n" +
                "\tFROM\n" +
                "\t( \n" +
                "\t\tSELECT \n" +
                "\t\t\tC.FECHACITA, \n" +
                "\t\t\tC.CODIGOETAPAPROSPECTO,\n" +
                "\t\t\tC.CODIGOESTADO,\n" +
                "\t\t\tC.CODIGORESULTADO\n" +
                "\t\t FROM tbl_cita C \n" +
                "\t\tWHERE \n" +
                "\t\t\tC.FECHACITA BETWEEN  '%S' AND  '%S' AND\n" +
                "\t\t\t(C.CODIGOESTADO = 3) AND\n" +
                "\t\t\t(C.CODIGOETAPAPROSPECTO = 2 or C.CODIGOETAPAPROSPECTO = 3  or (C.CODIGOETAPAPROSPECTO = 4 AND C.CODIGORESULTADO=1) )\n" +
                "\t\t) T1 \t\n" +
                "\tGROUP BY CODIGOETAPA \n" +
                "\tORDER BY CODIGOETAPA; ; ",
                strFechaInicio, strFechaFin
                );


        Log.d("CRC", " obtenerTotalEntrevistasRealizadasSinReagendadas sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int codigoEtapa = 0;
        int cantidad = 0;

        Map<Integer, Integer> mapNumeroEntrevistas = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                codigoEtapa = cursor.getInt(1);
                mapNumeroEntrevistas.put(codigoEtapa, cantidad);

            } while (cursor.moveToNext());


        }

        return mapNumeroEntrevistas;
    }

   /*public String queryProspectosReagendados(String strFechaInicio, String strFechaFin){
        String sql = String.format(
                "         SELECT DISTINCT p.IDPROSPECTO" +
                        "        FROM tbl_prospecto p  " +
                        "        INNER JOIN      " +
                        "        (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, c.IdProspecto, " +
                        "        c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "        FROM tbl_cita c       " +
                        "        GROUP BY c.IdProspecto)    UC     " +
                        "        on UC.IdProspecto = p.IDPROSPECTO  " +
                        "        INNER JOIN     (    " +
                        "        SELECT DISTINCT IDCITA, IdProspecto, codigoetapaprospecto, CODIGOESTADO  " +
                        "        FROM TBL_CITA        " +
                        "        WHERE CODIGOESTADO = 4    " +
                        "        ) REAG    " +
                        "        ON P.IDPROSPECTO = REAG.IdProspecto   WHERE  UC.CodigoEstado = 3 " +
                        "        AND UC.FECHACITA BETWEEN '2016-06-21' AND '2016-07-18'    ",
                Constantes.codEstadoReagendada,
                Constantes.codEstadoRealizada,
                strFechaInicio, strFechaFin);

        return sql.toString();
    }*/

    public Integer obtenerTotalCierres(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", "");
        Log.d("CRC", "#### obtenerTotalCierres fi:" + strFechaInicio + " ff:" + strFechaFin);

        // UC.CODIGORESULTADO = 1
        // AND (P.CODIGOETAPA = 2 or P.CODIGOETAPA = 3 or P.CODIGOETAPA = 4)
        // UC.FECHACITA BETWEEN '2016-07-05' AND '2016-07-31'

        String sql = String.format(
                /*" SELECT COUNT(*)   FROM (" +
                        "   SELECT DISTINCT p.IDPROSPECTO " +
                        "   FROM tbl_prospecto p" +
                        "   INNER JOIN  " +
                        "    (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.fechacita, c.IdProspecto, " +
                        "     c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "     FROM tbl_cita c  " +
                        "     GROUP BY c.IdProspecto) " +
                        "   UC" +
                        "     on UC.IdProspecto = p.IDPROSPECTO" +
                        "     WHERE UC.CODIGORESULTADO = %s" +
                        "     AND (P.CODIGOETAPA = %s or P.CODIGOETAPA = %s or P.CODIGOETAPA = %s)" +
                        "     AND UC.FECHACITA BETWEEN '%s' AND '%s' " +
                        " ) "*/
                /*" SELECT COUNT(*)   \n" +
                        " FROM \n" +
                        " (   \n" +
                        "\t\tSELECT DISTINCT \n" +
                        "\t\t\t\tp.IDPROSPECTODISPOSITIVO    \n" +
                        "\t\tFROM tbl_prospecto p   \n" +
                        "\t\tINNER JOIN      \n" +
                        "\t\t(\n" +
                        "\t\t\tSELECT \n" +
                        "\t\t\t\tmax(c.NUMEROENTREVISTA) , \n" +
                        "\t\t\t\tc.fechacita, \n" +
                        "\t\t\t\tc.IdProspectoDispositivo ,\n" +
                        "\t\t\t\tc.CODIGOESTADO,\n" +
                        "\t\t\t\tc.CODIGORESULTADO ,\n" +
                        "\t\t\t\tc.CODIGOETAPAPROSPECTO\n" +
                        "\t\t\tFROM tbl_cita c       \n" +
                        "\t\t\tGROUP BY c.IdProspectoDispositivo\n" +
                        "\t\t) UC  \n" +
                        "\t\ton UC.IdProspectoDispositivo = p.IdProspectoDispositivo     \n" +
                        "\t\tWHERE UC.CODIGORESULTADO = %S    \n" +
                        "\t\t\tAND \n" +
                        "\t\t\t(\n" +
                        "\t\t\t\tUC.CODIGOETAPAPROSPECTO = %s or UC.CODIGOETAPAPROSPECTO = %s or UC.CODIGOETAPAPROSPECTO = %s\n" +
                        "\t\t\t)     \t\t\t\n" +
                        "\t\t\tAND UC.FECHACITA BETWEEN '%s' AND '%s' \n" +
                        " ) "*/
                        "select count(1)\n" +
                                "\t\t\tfrom tbl_cita \n" +
                                "\t\t\twhere \n" +
                                "\t\t\t\t\tCODIGORESULTADO = 1 AND \n" + // Venta realizada
                                "\t\t\t\t\tFECHACITA BETWEEN '%S' AND '%S' ",
                strFechaInicio, strFechaFin
        );

        Log.d("CRC", " obtenerTotalCierres sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int cantidad = 0;

        if (cursor.moveToFirst()) {
            cantidad = cursor.getInt(0);
        }

        return cantidad;
    }

    /*public long obtenerNumeroEntrevistasAgendadas(Integer codigoEtapa, String strFecha) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerNumeroEntrevistasAgendadas codigoEtapa: " + codigoEtapa + " strFecha: " + strFecha);

        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        " ( " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoAgendada +
                        " OR " + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada +
                        " OR (" + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoReagendada +
                        " AND " + CitaBean.CN_CODIGORESULTADO + " = " + Constantes.codResultado_cierreVenta + ") " +
                        " ) AND " +
                        CitaBean.CN_FECHACITA + " = ? ",
                new String[]{String.valueOf(codigoEtapa), strFecha});


        return numeroCitas;
    }*/

    /*public long obtenerNumeroEntrevistasAgendadas(Integer codigoEtapa, String strFechaInicio, String strFechaFin) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerNumeroEntrevistasAgendadas codigoEtapa: " + codigoEtapa + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        " ( " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoAgendada +
                        " OR " + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada +
                        " OR (" + CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoReagendada +
                        " AND " + CitaBean.CN_CODIGORESULTADO + " = " + Constantes.codResultado_cierreVenta + ") " +
                        " ) AND " +
                        CitaBean.CN_FECHACITA + " BETWEEN ?  AND ? ",
                new String[]{String.valueOf(codigoEtapa), strFechaInicio, strFechaFin});


        return numeroCitas;
    }*/

    /*public Iterator<Entity> obtenerEntrevistasRealizadas(String codigoEtapa, String strFecha) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerEntrevistasRealizadas codigoEtapa: " + codigoEtapa + " strFecha: " + strFecha);

        CitaBean citaBean = null;

        Iterator<Entity> citas;

        citas = CitaBean.tableHelper.getEntities(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada + " AND " +
                        CitaBean.CN_FECHACITA + " = ? ",
                new String[]{codigoEtapa, strFecha}).iterator();


        while (citas.hasNext()) {
            citaBean = new CitaBean();
            citaBean = (CitaBean) citas.next();
            imprimirCita(citaBean);
        }

        return citas;
    }*/

    /*public long obtenerNumeroEntrevistasRealizadasSinReagendadas(Integer codigoEtapa, String strFecha) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerNumeroEntrevistasRealizadasSinReagendadas codigoEtapa: " + codigoEtapa + " strFecha: " + strFecha);

        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada + " AND " +
                        CitaBean.CN_FECHACITA + " = ? ",
                new String[]{String.valueOf(codigoEtapa), strFecha});

        return numeroCitas;
    }*/

    /*public long obtenerNumeroEntrevistasRealizadasSinReagendadas(Integer codigoEtapa, String strFechaInicio, String strFechaFin) {

        Log.d("CRC", " ");
        Log.d("CRC", "obtenerNumeroEntrevistasRealizadasSinReagendadas codigoEtapa: " + codigoEtapa + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_CODIGOETAPAPROSPECTO + " = ?  AND " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada + " AND " +
                        CitaBean.CN_FECHACITA + " BETWEEN ?  AND ? ",
                new String[]{String.valueOf(codigoEtapa), strFechaInicio, strFechaFin});

        return numeroCitas;
    } */


    /*
    public long obtenerNumeroCierres(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", " ");

        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                " ( " +
                        CitaBean.CN_CODIGOETAPAPROSPECTO + " = " + Constantes.codEtapa1ra + "  OR " +
                        CitaBean.CN_CODIGOETAPAPROSPECTO + " = " + Constantes.codEtapa2da + "  OR " +
                        CitaBean.CN_CODIGOETAPAPROSPECTO + " = " + Constantes.codEtapaAdicional + "  " +
                        " ) AND " +
                        CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada + " AND " +
                        CitaBean.CN_CODIGORESULTADO + " = " + Constantes.codResultado_cierreVenta + " AND " +
                        CitaBean.CN_FECHACITA + " BETWEEN ?  AND ? ",
                new String[]{strFechaInicio, strFechaFin});

        Log.d("CRC", "obtenerNumeroCierres numeroCitas: " + numeroCitas + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        return numeroCitas;
    }*/

    /*public long obtenerNumeroCierres1(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", " ");


        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_CODIGOESTADO + " = " + Constantes.codEstadoRealizada + " AND " +
                        CitaBean.CN_CODIGORESULTADO + " = " + Constantes.codResultado_cierreVenta + " AND " +
                        CitaBean.CN_FECHACITA + " BETWEEN ?  AND ? ",
                new String[]{strFechaInicio, strFechaFin});

        Log.d("CRC", "obtenerNumeroCierres1 numeroCitas: " + numeroCitas + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        return numeroCitas;
    }*/

    /*public long obtenerNumeroCierres2(String strFechaInicio, String strFechaFin) {

        Log.d("CRC", " ");


        long numeroCitas = 0L;

        numeroCitas = CitaBean.tableHelper.countRows(
                CitaBean.CN_FECHACITA + " BETWEEN ?  AND ? ",
                new String[]{strFechaInicio, strFechaFin});

        Log.d("CRC", "obtenerNumeroCierres2 numeroCitas: " + numeroCitas + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        return numeroCitas;
    }*/

    /*public static ArrayList<ArrayList<CitaProspectoBean>> getProspectosDelDia(String fechaCita) {

        Log.d("PSH", "");
        Log.d("PSH", "#### getProspectosDelDia ");

        ArrayList<CitaProspectoBean> listaAgendados = new ArrayList<>();
        ArrayList<CitaProspectoBean> listaContactados = new ArrayList<>();
        ArrayList<ArrayList<CitaProspectoBean>> lista = new ArrayList<>();


        //se usa el codigoestado = 1 "por contactar" y  codigoestado = 2 "agendado" de acuerdo a tabla de tablas
        fechaCita = "2016-26-05";


        String sql = String.format(
                "select DISTINCT IDCITADISPOSITIVO" +
                        " ,FECHACITA " +
                        " ,c.IDPROSPECTO " +
                        " ,NOMBRES " +
                        " ,APELLIDOPATERNO " +
                        " ,APELLIDOMATERNO " +
                        " ,CODIGOESTADO " +

                        " from tbl_cita c" +
                        " inner join tbl_prospecto p" +
                        " on c.IDPROSPECTO = p.idprospecto" +

                        " where " +
                        " FECHACITA = '%s'" +
                        " and codigoestado = %s" +
                        " or codigoestado = %s",
                fechaCita,
                Constantes.codEstadoPorContactar,
                Constantes.codEstadoAgendada
        );

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        CitaProspectoBean bean;
        Integer codigoEstado = null;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaProspectoBean();
                bean.setIdCita(cursor.getString(0));
                bean.setFechaCita(cursor.getString(1));
                bean.setIdProspecto(cursor.getString(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));

                codigoEstado = cursor.getInt(6);

                if (codigoEstado.equals(Constantes.codEstadoPorContactar)) {
                    listaAgendados.add(bean);
                } else if (codigoEstado.equals(Constantes.codEstadoAgendada)) {
                    listaContactados.add(bean);
                }

                imprimirCitaProspecto(bean);

            } while (cursor.moveToNext());

            lista.add(listaAgendados);
            lista.add(listaContactados);
        }

        return lista;
    }*/

    // faltan las reagendadas
    // Ver Detalle de la Programacion semanal
    public static ArrayList<ProgramacionSemanalDetalleBean> obtenerCitasAgendas_ProgramacionSemanalDetalle
    (String fechaInicio, String fechaFin) {
        ArrayList<ProgramacionSemanalDetalleBean> lista = new ArrayList<>();

        //  '2016-07-19' AND '2016-08-01'
        // (p.codigoetapa = 2 or p.codigoetapa = 3)
        // (uc.codigoestado = 2 or uc.codigoestado = 3)

        String sql = String.format(
                /*" select  DISTINCT  uc.IdCita  ,p.idprospecto  ,p.NOMBRES || ' ' || p.APELLIDOPATERNO  || ' ' || p.APELLIDOMATERNO as nomprospecto, " +
                " ref.idreferenciador  ,ref.nomreferenciador ,p.CODIGOFUENTE  ,p.CodigoEtapa  " +
                " ,uc.ULTFECHA, uc.Codigoestado, uc.codigoresultado, reag.fechareagendado  " +
                " from tbl_prospecto p" +
                " inner join " +
                "  (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idcita, c.fechacita, c.IdProspecto,       " +
                "        c.CODIGOESTADO, c.CODIGORESULTADO     " +
                "        FROM tbl_cita c       " +
                "        GROUP BY c.IdProspecto)    UC" +
                " on UC.IdProspecto = p.IDPROSPECTO   " +
                " LEFT JOIN      " +
                "     (SELECT idprospecto as idreferenciador, (NOMBRES || ' ' || APELLIDOMATERNO || ' ' || APELLIDOPATERNO) as nomreferenciador        " +
                "     FROM         tbl_prospecto) ref        " +
                " on p.idreferenciador = ref.idreferenciador " +
                " LEFT JOIN  ( " +
                "      SELECT DISTINCT IDCITA, IdProspecto, FECHACITA || ' ' || HoraInicio as FECHAreagendado" +
                "      FROM TBL_CITA " +
                "      WHERE CODIGOESTADO = %s AND CODIGORESULTADO = %s  " +
                "   ) REAG " +
                " ON P.IDPROSPECTO = REAG.IdProspecto" +
                " where uc.FECHACITA BETWEEN '%s' AND '%s'" +
                " and (p.codigoetapa = %s or p.codigoetapa = %s)" +
                " and (uc.codigoestado = %s or uc.codigoestado = %s);",*/
                "select  DISTINCT  \n" +
                        "\t\tuc.IdCita  ,\n" +
                        "\t\tuc.IdCitaDispositivo ,\n" +
                        "\t\tp.idprospectodispositivo  ,\n" +
                        "\t\tIFNULL(p.NOMBRES,'') || ' ' || IFNULL(p.APELLIDOPATERNO,'')  || ' ' || IFNULL(p.APELLIDOMATERNO,'') as nomprospecto,  \n" +
                        "\t\tref.idreferenciadordispositivo,\n" +
                        "\t\tIFNULL(ref.nomreferenciador ,''),\n" +
                        "\t\tp.CODIGOFUENTE  ,\n" +
                        "\t\tuc.CODIGOETAPAPROSPECTO   ,\n" +
                        "\t\tuc.fechahora, \n" +
                        "\t\tuc.Codigoestado, \n" +
                        "\t\tuc.codigoresultado\n" +
                        "\tfrom tbl_prospecto p \n" +
                        "\tinner join   \n" +
                        "\t(SELECT \n" +
                        "\t\t\tc.idcita, \n" +
                        "\t\t\tc.idcitadispositivo,\n" +
                        "\t\t\tc.fechacita || ' ' || c.HoraInicio as fechahora, \n" +
                        "\t\t\tc.IdProspectoDispositivo,               \n" +
                        "\t\t\tc.CODIGOESTADO, \n" +
                        "\t\t\tc.CODIGOETAPAPROSPECTO, \n" +
                        "\t\t\tc.CODIGORESULTADO, \n" +
                        "\t\t\tc.fechacita,\n" +
                        "\t\t\tC.NUMEROENTREVISTA\n" +
                        "\tFROM tbl_cita c               \n" +
                        "\t) UC \n" +
                        "\ton UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO    \n" +
                        "\tLEFT JOIN  \n" +
                        "\t(SELECT \n" +
                        "\t\t\tidprospectodispositivo as idreferenciadordispositivo, \n" +
                        "\t\t\t(IFNULL(NOMBRES,'') || ' ' || IFNULL(APELLIDOMATERNO,'') || ' ' || IFNULL(APELLIDOPATERNO,'')) as nomreferenciador             \n" +
                        "\t FROM    tbl_prospecto) ref         \n" +
                        "\t on p.idreferenciadordispositivo = ref.idreferenciadordispositivo \n" +
                        "\t where \n" +
                        "\t\tuc.FECHACITA BETWEEN '%S' AND '%S' \n" +
                        "\t\tand (\n" +
                        "\t\t\t\t\tuc.codigoestado = 2 or uc.codigoestado = 3\n" +
                        "\t\t\t\t\t OR\n" +
                        "\t\t\t\t\t(\t\n" +
                        "\t\t\t\t\t\tUC.CODIGOESTADO = 4 AND \n" +
                        "\t\t\t\t\t\t(\n" +
                        "\t\t\t\t\t\t\tEXISTS(SELECT 1 FROM TBL_CITA CR WHERE CR.NUMEROENTREVISTA = (UC.NUMEROENTREVISTA - 1)  AND CR.IDPROSPECTODISPOSITIVO = UC.IDPROSPECTODISPOSITIVO and CR.CODIGOESTADO <>  4)\n" +
                        "\t\t\t\t\t\t\tOR\n" +
                        "\t\t\t\t\t\t\tUC.NUMEROENTREVISTA = 1 -- SI ES LA PRIMERA ENTREVISTA DEL PROSPECTO\n" +
                        "\t\t\t\t\t\t)\n" +
                        "\t\t\t\t\t)\n" +
                        "\t\t\t\t)\t\n" +
                        "\torder by uc.fechacita Asc;",
                /*Constantes.codEstadoReagendada, Constantes.codResultado_reagendado,*/
                fechaInicio, fechaFin
        );

        Log.d("CRC", " sql: " + sql.toString());

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        ProgramacionSemanalDetalleBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new ProgramacionSemanalDetalleBean();
                //bean.setIdCita(cursor.getString(0));
                bean.setIdCitaDispositivo(cursor.getString(1));
                bean.setIdProspectoDispositivo(cursor.getString(2));
                //bean.setCodProspecto(cursor.getString(1));
                bean.setNomProspecto(cursor.getString(3));
                bean.setIdReferenciadorDispositivo(cursor.getString(4));
                bean.setNomReferenciador(cursor.getString(5));
                bean.setCodFuente(cursor.getInt(6));
                bean.setCodEtapa(cursor.getInt(7));
                bean.setFecha(cursor.getString(8));
                bean.setDesFecha(DateUtils.getDateFormat04(bean.getFecha()));
                bean.setCodEstado(cursor.getInt(9));
                bean.setCodResultado(cursor.getInt(10));
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }

    /*public static ArrayList<CitaProspectoBean> getProspectosAgendados(String fechaInicio, String fechaFin) {
        ArrayList<CitaProspectoBean> lista = new ArrayList<>();

        String sql = String.format(
                "select DISTINCT IDCITADISPOSITIVO" +
                        " ,FECHACITA " +
                        " ,c.IDPROSPECTO " +
                        " ,NOMBRES " +
                        " ,APELLIDOPATERNO " +
                        " ,APELLIDOMATERNO " +
                        " from tbl_cita c" +
                        " inner join tbl_prospecto p" +
                        " on c.IDPROSPECTO = p.idprospecto" +
                        " where " +
                        " FECHACITA = '%s'" +
                        " and codigoestado = %s",
                fechaInicio,
                Constantes.codEstadoAgendada
        );


        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        CitaProspectoBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaProspectoBean();
                bean.setIdCita(cursor.getString(0));
                bean.setFechaCita(cursor.getString(1));
                bean.setIdProspecto(cursor.getString(2));
                bean.setNombres(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setApellidoMaterno(cursor.getString(5));
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/

    public void imprimirCita(CitaBean cita) {
        StringBuffer sb = new StringBuffer();
        sb.append(" ci: ").append(cita.getIdCitaDispositivo()); // falta dato
        sb.append(" ip: ").append(cita.getIdProspecto()); // falta dato
        sb.append(" ne: ").append(cita.getNumeroEntrevista());
        sb.append(" ep: ").append(cita.getCodigoEtapaProspecto());
        sb.append(" ce: ").append(cita.getCodigoEstado());
        sb.append(" cr: ").append(cita.getCodigoResultado());
        sb.append(" fc: ").append(cita.getFechaCita());

        Log.d("CDH", "CITA: " + sb.toString());
    }

    public static void imprimirCitaProspecto(CitaProspectoBean citaProspectoBean) {
        StringBuffer sb = new StringBuffer();
        sb.append(" ci: ").append(citaProspectoBean.getIdCita());
        sb.append(" no: ").append(citaProspectoBean.getNombres());
        sb.append(" am: ").append(citaProspectoBean.getApellidoMaterno());
        sb.append(" ap: ").append(citaProspectoBean.getApellidoPaterno());
        sb.append(" ip: ").append(citaProspectoBean.getFechaCita());
        sb.append(" ne: ").append(citaProspectoBean.getIProspecto());
        sb.append(" ep: ").append(citaProspectoBean.getNombres());
        sb.append(" ce: ").append(citaProspectoBean.getApellidoPaterno());
        sb.append(" cr: ").append(citaProspectoBean.getApellidoMaterno());
        Log.d("CDH", "CitaProspecto: " + sb.toString());
    }

    public int obtenerTotalSegRealizadasCierres(String fechaInicio, String fechaFin){
        String sql = String.format("SELECT COUNT(1)\n" +
                        "\tFROM TBL_CITA c\n" +
                        "\tWHERE\n" +
                        "\t\tC.FECHACITA BETWEEN  '%S' AND  '%S' AND\n" +
                        "\t\t(C.CODIGOESTADO = 3) AND\n" +
                        "\t\t(C.CODIGOETAPAPROSPECTO = 3  or (C.CODIGOETAPAPROSPECTO =  2 AND C.CODIGORESULTADO=1) OR (C.CODIGOETAPAPROSPECTO = 4 AND C.CODIGORESULTADO=1))",
                fechaInicio, fechaFin
        );

        Log.d("CRC", " sql: " + sql.toString());

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int totalSegundaRealizadaCierre =0;
        if (cursor.moveToFirst()) {
                totalSegundaRealizadaCierre=cursor.getInt(0);
        }

        return totalSegundaRealizadaCierre;
    }

}
