package com.pacifico.cartera.Model.Bean.Auxiliar;

import android.graphics.drawable.Drawable;

import org.joda.time.DateTime;

/**
 * Created by dsb on 02/05/2016.
 */
public class TusContactosBean {

    private String primeraLetraAsc;
    private String primeraLetraDesc;
    // TODO: Ver si descomentar y mantener aparte
    //private Integer codProspecto;
    private Integer idProspectoDispositivo;
    private String nomProspecto;
    private Integer codEtapa;
    private Integer codEtapaCitaProspecto;
    private String desEtapa;
    private Drawable drwEtapa;
    private Integer codResultado;
    private Integer codEstado;
    private String desEstado;
    //private String desEstado2;
    private String desResultado;
    private Drawable drwEstado;

    private DateTime fecUltEstado;
    private String desFecUltEstado;
    private Integer codIngresos;
    private String desIngresos;
    private Integer codTieneHijos;
    private String desTieneHijos;

    private Integer casoTipo;
    private Integer tieneRecordatorio;
    private Integer esReagendada;

    private String fechaCreacionProspecto;
    /*
    // la fecha por ahora se envia
    // rango de ingresos por ahora se envia
    public TusContactosBean(String codProspecto, String nomProspecto, String codEtapa, String codEstado,
                            DateTime fecUltEstado, String desFecUltEstado, Short codIngresos, String codTieneHijos) {

        this.codProspecto = codProspecto;
        this.nomProspecto = nomProspecto;
        this.codEtapa = codEtapa;
        this.codEstado = codEstado;
        this.fecUltEstado = fecUltEstado;
        this.setDesFecUltEstado(desFecUltEstado);
        this.codIngresos = codIngresos;
        this.codTieneHijos = codTieneHijos;
    }

    public TusContactosBean(String primeraLetraAsc, String primeraLetraDesc, String codProspecto, String nomProspecto, String codEtapa, String desEtapa, Drawable drwEtapa,
                            String codEstado, String desEstado, String desEstado2, Drawable drwEstado, DateTime fecUltEstado, String desFecUltEstado,
                            Short codIngresos, String desIngresos, String codTieneHijos, String desTieneHijos) {

        this.primeraLetraAsc = primeraLetraAsc;
        this.primeraLetraDesc = primeraLetraDesc;
        this.codProspecto = codProspecto;
        this.nomProspecto = nomProspecto;

        this.codEtapa = codEtapa;
        this.desEtapa = desEtapa;
        this.drwEtapa = drwEtapa;

        this.codEstado = codEstado;
        this.desEstado = desEstado;
        this.desEstado2 = desEstado2;
        this.drwEstado = drwEstado;

        this.fecUltEstado = fecUltEstado;
        this.setDesFecUltEstado(desFecUltEstado);
        this.codIngresos = codIngresos;
        this.desIngresos = desIngresos;
        this.codTieneHijos = codTieneHijos;
        this.desTieneHijos = desTieneHijos;
    }*/

    /*public Integer getCodProspecto() {
        return codProspecto;
    }

    public void setCodProspecto(Integer codProspecto) {
        this.codProspecto = codProspecto;
    }*/
    public Integer getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(Integer idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    public String getNomProspecto() {
        return nomProspecto;
    }

    public void setNomProspecto(String nomProspecto) {
        this.nomProspecto = nomProspecto;
    }

    public Integer getCodEtapa() {
        return codEtapa;
    }

    public void setCodEtapa(Integer codEtapa) {
        this.codEtapa = codEtapa;
    }

    public String getDesEtapa() {
        return desEtapa;
    }

    public void setDesEtapa(String desEtapa) {
        this.desEtapa = desEtapa;
    }

    public Integer getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(Integer codEstado) {
        this.codEstado = codEstado;
    }

    public String getDesEstado() {
        return desEstado;
    }

    public void setDesEstado(String desEstado) {
        this.desEstado = desEstado;
    }

    public DateTime getFecUltEstado() {
        return fecUltEstado;
    }

    public void setFecUltEstado(DateTime fecUltEstado) {
        this.fecUltEstado = fecUltEstado;
    }

    public Integer getCodIngresos() {
        return codIngresos;
    }

    public void setCodIngresos(Integer codIngresos) {
        this.codIngresos = codIngresos;
    }

    public String getDesIngresos() {
        return desIngresos;
    }

    public void setDesIngresos(String desIngresos) {
        this.desIngresos = desIngresos;
    }

    public Integer getCodTieneHijos() {
        return codTieneHijos;
    }

    public void setCodTieneHijos(Integer codTieneHijos) {
        this.codTieneHijos = codTieneHijos;
    }

    public String getDesTieneHijos() {
        return desTieneHijos;
    }

    public void setDesTieneHijos(String desTieneHijos) {
        this.desTieneHijos = desTieneHijos;
    }

    public String getDesFecUltEstado() {
        return desFecUltEstado;
    }

    public void setDesFecUltEstado(String desFecUltEstado) {
        this.desFecUltEstado = desFecUltEstado;
    }

    public String getPrimeraLetraAsc() {
        return primeraLetraAsc;
    }

    public void setPrimeraLetraAsc(String primeraLetraAsc) {
        this.primeraLetraAsc = primeraLetraAsc;
    }

    public String getPrimeraLetraDesc() {
        return primeraLetraDesc;
    }

    public void setPrimeraLetraDesc(String primeraLetraDesc) {
        this.primeraLetraDesc = primeraLetraDesc;
    }

    //public String getDesEstados() {        return desEstado + desEstado2;    }
    public String getDesEstados() {
        return desEstado + desResultado;  }

    public String getDesResultado() {
        return desResultado;
    }

    public void setDesResultado(String desResultado) {
        this.desResultado = desResultado;
    }

    public Drawable getDrwEtapa() {
        return drwEtapa;
    }

    public void setDrwEtapa(Drawable drwEtapa) {
        this.drwEtapa = drwEtapa;
    }

    public Drawable getDrwEstado() {
        return drwEstado;
    }

    public void setDrwEstado(Drawable drwEstado) {
        this.drwEstado = drwEstado;
    }

    public Integer getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(Integer codResultado) {
        this.codResultado = codResultado;
    }

    public Integer getCasoTipo() {
        return casoTipo;
    }

    public void setCasoTipo(Integer casoTipo) {
        this.casoTipo = casoTipo;
    }


    // Agregados

    public Integer getCodEtapaCitaProspecto() {
        return codEtapaCitaProspecto;
    }

    public void setCodEtapaCitaProspecto(Integer codEtapaCitaProspecto) {
        this.codEtapaCitaProspecto = codEtapaCitaProspecto;
    }

    public Integer getTieneRecordatorio() {
        return tieneRecordatorio;
    }

    public void setTieneRecordatorio(Integer tieneRecordatorio) {
        this.tieneRecordatorio = tieneRecordatorio;
    }

    public Integer getEsReagendada() {
        return esReagendada;
    }

    public void setEsReagendada(Integer esReagendada) {
        this.esReagendada = esReagendada;
    }

    public String getFechaCreacionProspecto() {
        return fechaCreacionProspecto;
    }

    public void setFechaCreacionProspecto(String fechaCreacionProspecto) {
        this.fechaCreacionProspecto = fechaCreacionProspecto;
    }
}
