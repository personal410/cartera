package com.pacifico.cartera.Model.Bean.Auxiliar;

import org.joda.time.DateTime;

/**
 * Created by dsb on 15/05/2016.
 */
public class ReferidosBean {

    private Integer codProspecto;
    private String nomProspecto;
    private String telefono;
    private String flagTieneHijos;
    private String desTieneHijos;
    private Integer codEdad;
    private String desEdad;
    private Integer codIngresos;
    private String desIngresos;
    private DateTime fecContactado;
    private String fecContactadoTexto;
    private int codContactado;
    private int tipoEmpleo;
    private String desTipoEmpleo;

    public ReferidosBean(){

    }

    /*
    public ReferidosBean(Integer codProspecto, String nomProspecto, String telefono, Integer codTieneHijos, Integer codEdad, Integer codIngresos, Integer codContactado){
        this.codProspecto = codProspecto;
        this.nomProspecto = nomProspecto;
        this.telefono = telefono;
        this.codTieneHijos = codTieneHijos;
        this.codIngresos = codIngresos;
        this.codEdad = codEdad;
        this.codContactado = codContactado;
    }

    public ReferidosBean(Integer codProspecto, String nomProspecto, String telefono, Integer codTieneHijos, String desTieneHijos, Integer codEdad, String desEdad, Integer codIngresos, String desIngresos, Integer codContactado, String desContactado){
        this.codProspecto = codProspecto;
        this.nomProspecto = nomProspecto;
        this.telefono = telefono;
        this.codTieneHijos = codTieneHijos;
        this.codTieneHijos = codTieneHijos;
        this.codIngresos = codIngresos;
        this.desIngresos = desIngresos;
        this.codEdad = codEdad;
        this.desEdad = desEdad;
        this.codContactado = codContactado;
        this.desContactado = desContactado;
    }*/

    public Integer getCodProspecto() {
        return codProspecto;
    }

    public void setCodProspecto(Integer codProspecto) {
        this.codProspecto = codProspecto;
    }

    public String getNomProspecto() {
        return nomProspecto;
    }

    public void setNomProspecto(String nomProspecto) {
        this.nomProspecto = nomProspecto;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDesTieneHijos() {
        return desTieneHijos;
    }

    public void setDesTieneHijos(String desTieneHijos) {
        this.desTieneHijos = desTieneHijos;
    }

    public Integer getCodEdad() {
        return codEdad;
    }

    public void setCodEdad(Integer codEdad) {
        this.codEdad = codEdad;
    }

    public String getDesEdad() {
        return desEdad;
    }

    public void setDesEdad(String desEdad) {
        this.desEdad = desEdad;
    }

    public Integer getCodIngresos() {
        return codIngresos;
    }

    public void setCodIngresos(Integer codIngresos) {
        this.codIngresos = codIngresos;
    }

    public String getDesIngresos() {
        return desIngresos;
    }

    public void setDesIngresos(String desIngresos) {
        this.desIngresos = desIngresos;
    }

    public String getFlagTieneHijos() {
        return flagTieneHijos;
    }

    public void setFlagTieneHijos(String flagTieneHijos) {
        this.flagTieneHijos = flagTieneHijos;
    }

    public DateTime getFecContactado() {
        return fecContactado;
    }

    public void setFecContactado(DateTime fecContactado) {
        this.fecContactado = fecContactado;
    }

    public String getFecContactadoTexto() {
        return fecContactadoTexto;
    }

    public void setFecContactadoTexto(String fecContactadoTexto) {
        this.fecContactadoTexto = fecContactadoTexto;
    }

    public int getCodContactado() {
        return codContactado;
    }

    public void setCodContactado(int codContactado) {
        this.codContactado = codContactado;
    }

    public int getTipoEmpleo() {
        return tipoEmpleo;
    }

    public void setTipoEmpleo(int tipoEmpleo) {
        this.tipoEmpleo = tipoEmpleo;
    }

    public String getDesTipoEmpleo() {
        return desTipoEmpleo;
    }

    public void setDesTipoEmpleo(String desTipoEmpleo) {
        this.desTipoEmpleo = desTipoEmpleo;
    }
}
