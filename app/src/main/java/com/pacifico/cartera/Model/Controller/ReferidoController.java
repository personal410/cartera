package com.pacifico.cartera.Model.Controller;

import android.database.Cursor;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ReferidoBean;
import com.pacifico.cartera.Persistence.DatabaseConstants;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;

//import com.pacifico.cartera.Activity.ADNApplication;

/**
 * Created by vctrls3477 on 4/07/16.
 */
public class ReferidoController{
    public static void guardarReferido(ReferidoBean referidoBean){
        ReferidoBean.tableHelper.insertEntity(referidoBean);
    }
    public static void actualizarReferido(ReferidoBean referidoBean){
        String sentencia = "IDREFERIDODISPOSITIVO = ?";
        String[] parametros = new String[]{Integer.toString(referidoBean.getIdReferidoDispositivo())};
        ReferidoBean.tableHelper.updateEntity(referidoBean, sentencia, parametros);
    }

    public static int obtenerMaximoIdReferidoDispositivo(){
        int idReferidoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDREFERIDODISPOSITIVO) FROM " + DatabaseConstants.TBL_REFERIDO;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idReferidoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idReferidoDispositivoMax;
    }
    public static ArrayList<ReferidoBean> obtenerReferidoPorTipoReferido(int IdProspecto, int IdProspectoDispositivo, int codigoTipoReferido){
        String[] parametros = new String[2];
        String sentencia;
        if(IdProspecto == 0){
            parametros[0] = Integer.toString(IdProspectoDispositivo);
            sentencia = "IDPROSPECTODISPOSITIVO = ? AND CODIGOTIPOREFERIDO = ?";
        }else{
            parametros[0] = Integer.toString(IdProspecto);
            sentencia = "IDPROSPECTO = ? AND CODIGOTIPOREFERIDO = ?";
        }
        parametros[1] = Integer.toString(codigoTipoReferido);
        sentencia = sentencia + "";
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities(sentencia, parametros);
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        if(arrReferidos != null){
            for(Entity referido : arrReferidos){
                arrReferidosFinal.add((ReferidoBean)referido);
            }
        }
        return arrReferidosFinal;
    }
    public static void guardaListaReferido(ArrayList<ReferidoBean> listaReferidoBean){
        for(ReferidoBean referidoBean : listaReferidoBean){
            if(referidoBean.getIdProspectoDispositivo() == -1){
                ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(referidoBean.getIdProspecto());
                referidoBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
            }
            ReferidoBean.tableHelper.insertEntity(referidoBean);
        }
    }
    public static ArrayList<ReferidoBean> obtenerReferidosSinEnviar(){
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ReferidoBean> arrReferidosFinal = new ArrayList<>();
        for(Entity entity : arrReferidos){
            arrReferidosFinal.add((ReferidoBean) entity);
        }
        return arrReferidosFinal;
    }
    public static int obtenerIdReferidoDispositivoDisponible(){
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities("", null);
        return arrReferidos.size() + 1;
    }
    public static ReferidoBean  obtenerReferidoPorIdReferidoDispositivo(int idReferidoDispositivo){
        String sentencia = "IDREFERIDODISPOSITIVO = ?";
        String[] parametros = new String[]{Integer.toString(idReferidoDispositivo)};
        ArrayList<Entity> arrReferidos = ReferidoBean.tableHelper.getEntities(sentencia, parametros);
        return (ReferidoBean)arrReferidos.get(0);
    }
}