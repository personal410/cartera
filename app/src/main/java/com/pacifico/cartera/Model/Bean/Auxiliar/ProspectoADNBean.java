package com.pacifico.cartera.Model.Bean.Auxiliar;

/**
 * Created by dsb on 07/07/2016.
 */
public class ProspectoADNBean{
    private int idProspectoDispositivo;
    private int idMoneda;
    private int tipoCambio;
    private int idMonedaDescobertura;  // capital necesario en caso de fallecimientpo
    private int descobertura;
    private String descoberturaConTipoCambio;
    private int idMonedaCompromisoPago; // monto mensual a invertir
    private int compromisoPago;
    private String compromisoPagoConTipoCambio;
    private int idMonedaEfectivoYAhorros; // total activo realizable
    private int efectivoYAhorros;
    private String efectivoYAhorrosConTipoCambio;
    private int idMonedaGastosMensuales; // total gasto familiar mensual
    private int gastosMensuales;
    private String gastosMensualesConTipoCambio;
    private int idMonedaIngresosMensuales; // total ingreso familiar mensual
    private int ingresosMensuales;
    private String ingresosMensualesConTipoCambio;

    public int getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(int idMoneda) {
        this.idMoneda = idMoneda;
    }

    public int getIdMonedaDescobertura() {
        return idMonedaDescobertura;
    }

    public void setIdMonedaDescobertura(int idMonedaDescobertura) {
        this.idMonedaDescobertura = idMonedaDescobertura;
    }

    public int getDescobertura() {
        return descobertura;
    }

    public void setDescobertura(int descobertura) {
        this.descobertura = descobertura;
    }

    public int getIdMonedaCompromisoPago() {
        return idMonedaCompromisoPago;
    }

    public void setIdMonedaCompromisoPago(int idMonedaCompromisoPago) {
        this.idMonedaCompromisoPago = idMonedaCompromisoPago;
    }

    public int getCompromisoPago() {
        return compromisoPago;
    }

    public void setCompromisoPago(int compromisoPago) {
        this.compromisoPago = compromisoPago;
    }

    public int getIdMonedaEfectivoYAhorros() {
        return idMonedaEfectivoYAhorros;
    }

    public void setIdMonedaEfectivoYAhorros(int idMonedaEfectivoYAhorros) {
        this.idMonedaEfectivoYAhorros = idMonedaEfectivoYAhorros;
    }

    public int getEfectivoYAhorros() {
        return efectivoYAhorros;
    }

    public void setEfectivoYAhorros(int efectivoYAhorros) {
        this.efectivoYAhorros = efectivoYAhorros;
    }

    public int getGastosMensuales() {
        return gastosMensuales;
    }

    public void setGastosMensuales(int gastosMensuales) {
        this.gastosMensuales = gastosMensuales;
    }

    public int getIngresosMensuales() {
        return ingresosMensuales;
    }

    public void setIngresosMensuales(int ingresosMensuales) {
        this.ingresosMensuales = ingresosMensuales;
    }

    public int getIdMonedaIngresosMensuales() {
        return idMonedaIngresosMensuales;
    }

    public void setIdMonedaIngresosMensuales(int idMonedaIngresosMensuales) {
        this.idMonedaIngresosMensuales = idMonedaIngresosMensuales;
    }

    public int getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(int tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public String getEfectivoYAhorrosConTipoCambio() {
        return efectivoYAhorrosConTipoCambio;
    }

    public void setEfectivoYAhorrosConTipoCambio(String efectivoYAhorrosConTipoCambio) {
        this.efectivoYAhorrosConTipoCambio = efectivoYAhorrosConTipoCambio;
    }

    public String getDescoberturaConTipoCambio() {
        return descoberturaConTipoCambio;
    }

    public void setDescoberturaConTipoCambio(String descoberturaConTipoCambio) {
        this.descoberturaConTipoCambio = descoberturaConTipoCambio;
    }

    public String getCompromisoPagoConTipoCambio() {
        return compromisoPagoConTipoCambio;
    }

    public void setCompromisoPagoConTipoCambio(String compromisoPagoConTipoCambio) {
        this.compromisoPagoConTipoCambio = compromisoPagoConTipoCambio;
    }

    public int getIdMonedaGastosMensuales() {
        return idMonedaGastosMensuales;
    }

    public void setIdMonedaGastosMensuales(int idMonedaGastosMensuales) {
        this.idMonedaGastosMensuales = idMonedaGastosMensuales;
    }

    public String getGastosMensualesConTipoCambio() {
        return gastosMensualesConTipoCambio;
    }

    public void setGastosMensualesConTipoCambio(String gastosMensualesConTipoCambio) {
        this.gastosMensualesConTipoCambio = gastosMensualesConTipoCambio;
    }

    public String getIngresosMensualesConTipoCambio() {
        return ingresosMensualesConTipoCambio;
    }

    public void setIngresosMensualesConTipoCambio(String ingresosMensualesConTipoCambio) {
        this.ingresosMensualesConTipoCambio = ingresosMensualesConTipoCambio;
    }
}
