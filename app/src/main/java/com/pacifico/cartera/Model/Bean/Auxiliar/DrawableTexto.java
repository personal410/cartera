package com.pacifico.cartera.Model.Bean.Auxiliar;

import android.graphics.drawable.Drawable;

/**
 * Created by dsb on 05/07/2016.
 */
public class DrawableTexto {

    private Drawable drwImage;
    private String strTexto;
    private String strTexto2;


    public DrawableTexto(){

    }

    public DrawableTexto(Drawable drwImage, String strTexto){
        this.drwImage = drwImage;
        this.strTexto = strTexto;
    }

    public DrawableTexto(Drawable drwImage, String strTexto, String strTexto2){
        this.drwImage = drwImage;
        this.strTexto = strTexto;
        this.strTexto2 = strTexto2;
    }


    public Drawable getDrwImage() {
        return drwImage;
    }

    public void setDrwImage(Drawable drwImage) {
        this.drwImage = drwImage;
    }

    public String getStrTexto() {
        return strTexto;
    }

    public void setStrTexto(String strTexto) {
        this.strTexto = strTexto;
    }

    public String getStrTexto2() {
        return strTexto2;
    }

    public void setStrTexto2(String strTexto2) {
        this.strTexto2 = strTexto2;
    }
}
