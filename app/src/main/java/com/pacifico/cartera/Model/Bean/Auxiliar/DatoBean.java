package com.pacifico.cartera.Model.Bean.Auxiliar;

/**
 * Created by dsb on 18/07/2016.
 */
public class DatoBean {
    private String descripcion;
    private Long valor;

    public DatoBean(String _descripcion, Long _valor){
        this.descripcion = _descripcion;
        this.setValor(_valor);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }
}
