package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.ProspectoBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.IndiceDatoBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProspectoDetalleBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.TusContactosBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;

public class CarteraProspectoController {

    public Iterator<Entity> obtenerTodosProspectos() {

        Log.d("PC", "obtenerTodosProspectos");
        Log.d("PC", " ");

        ProspectoBean bean = null;

        Iterator<Entity> beans;

        beans = ProspectoBean.tableHelper.getEntities(
                null,
                null).iterator();

        while (beans.hasNext()) {
            bean = (ProspectoBean) beans.next();
            imprimirProspecto(bean);
        }

        return beans;
    }

    public long obtenerNumeroTodosProspectos() {

        Log.d("PC", "obtenerTodosProspectos");
        Log.d("PC", " ");

        ProspectoBean bean = null;

        long numeroProspectos = 0L;

        numeroProspectos = ProspectoBean.tableHelper.countRows(
                null,
                null);

        return numeroProspectos;
    }

    public long obtenerNumeroProspectosComposicionCartera(/*String strFechaInicio, String strFechaFin*/) {

        Log.d("PC", "obtenerNumeroProspectosComposicionCartera");
        Log.d("PC", " ");

        long numeroProspectos = 0L;
        // UC.FECHACITA BETWEEN '2016-07-05' AND '3000-07-11'
        // (P.codigoetapa = 2)   AND (UC.CODIGOESTADO = 2)

        String sql = String.format(
               /* " SELECT count(*) as cantidad" +
                        "  FROM " +
                        "  (" +
                        "      SELECT DISTINCT p.IDPROSPECTO, p.codigorangoingreso, uc.CODIGOESTADO" +
                        "      FROM tbl_prospecto p " +
                        "      INNER JOIN  " +
                        "          (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, " +
                        "           c.fechacita, C.HORAINICIO, c.IdProspecto, c.CODIGOESTADO, " +
                        "           c.CODIGORESULTADO " +
                        "            FROM tbl_cita c     GROUP BY c.IdProspecto)   UC " +
                        "      on UC.IdProspecto = p.IDPROSPECTO " +
                        "      WHERE UC.FECHACITA BETWEEN '%s' AND '%s' " +
                        "     AND (P.codigoetapa = %s)   AND (UC.CODIGOESTADO = %s)" +
                        "  ) PRAGPE"*/
                "SELECT \n" +
                        "\t\t\tcount(*) as cantidad  \n" +
                        "\tFROM  \n" +
                        "\t(      \n" +
                        "\t\tSELECT DISTINCT \n" +
                        "\t\t\t\tp.IDPROSPECTODISPOSITIVO, p.codigorangoingreso, uc.CODIGOESTADO , uc.CODIGOETAPAPROSPECTO\n" +
                        "\t\tFROM tbl_prospecto p       \n" +
                        "\t\tINNER JOIN            \n" +
                        "\t\t(SELECT \n" +
                        "\t\t\t\tmax(c.NUMEROENTREVISTA) as NUMEROENTREVISTA, \n" +
                        "\t\t\t\tc.fechacita, C.HORAINICIO, c.IdProspecto,c.IDPROSPECTODISPOSITIVO, c.CODIGOESTADO, \n" +
                        "\t\t\t\tc.CODIGORESULTADO , c.CODIGOETAPAPROSPECTO        \n" +
                        "\t\t  FROM tbl_cita c     \n" +
                        "\t\t  GROUP BY c.IdProspectoDispositivo)   UC       \n" +
                        "\t\t  on UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO       \n" +
                        "\t\t  WHERE\n" +
                        "\t\t\t\t(uc.CODIGOETAPAPROSPECTO = %s)   AND (UC.CODIGOESTADO = %s)  \n" +
                        "\t) PRAGPE",
                Constantes.codEtapa1ra, Constantes.codEstadoAgendada
        );


        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        Log.d("PC", "obtenerNumeroProspectosComposicionCartera sql:" + sql);

        if (cursor.moveToFirst()) {
            numeroProspectos = cursor.getLong(0);
        }
        return numeroProspectos;
    }

    /*public int obtenerReferidosPromedioPorADN() {

        Log.d("PC", "obtenerReferidosPromedioPorADN");
        Log.d("PC", " ");

        int promedio = 0;
        int totalReferidos = 0;
        int nrodADNS = 0;

        String sql = String.format(
                "  SELECT \n" +
                        "\t\tCOUNT(*) AS CANTIDAD, \n" +
                        "\t\tPPCT.IDREFERENCIADORDISPOSITIVO  \n" +
                        "   FROM  \n" +
                        "   (  \n" +
                        "\t\tSELECT DISTINCT \n" +
                        "\t\t\tIDPROSPECTODISPOSITIVO, \n" +
                        "\t\t\tIDREFERENCIADORDISPOSITIVO  \n" +
                        "\t\tFROM tbl_prospecto P  \n" +
                        "\t\tWHERE \n" +
                        "\t\t\tCODIGOFUENTE = %s  AND \n" +
                        "\t\t\t(IDREFERENCIADORDISPOSITIVO IS NOT NULL AND IDREFERENCIADORDISPOSITIVO > 0) \n" +
                        "\t) PPCT \n" +
                        "\tGROUP BY PPCT.IDREFERENCIADORDISPOSITIVO;"
                , Constantes.codFuente_adn);


        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        Log.d("CPC", " query obtenerReferidosPromedioPorADNsql: " + sql);


        String idReferenciador = "";

        if (cursor.moveToFirst()) {
            do {
                totalReferidos = totalReferidos + cursor.getInt(0);
                idReferenciador = cursor.getString(1);
                Log.d("CPC", " totalReferidos: " + totalReferidos + " idReferenciador: " + idReferenciador);
                nrodADNS++;
            } while (cursor.moveToNext());
        }

        if (nrodADNS > 0) {
            promedio = totalReferidos / nrodADNS;
        }

        Log.d("CPC", " nrodADNS: " + nrodADNS + " promedio: " + promedio);

        return promedio;
    }*/

    public int obtenerPromedioReferidoPorADN(String strFechaInicio, String strFechaFin){
        int promedio = 0;
        String sql = String.format(
                "select round(avg(cant),0) from\n" +
                "(\n" +
                "\tselect count(1) as cant\n" +
                "\tfrom tbl_referido\n" +
                "\tWHERE " +
                        "substr(fechacreaciondispositivo,0,11) between '%S' and '%S' \n" +
                "\tgroup by idProspectoDIspositivo\n" +
                ")",strFechaInicio,strFechaFin);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);
        if (cursor.moveToFirst()) {
                promedio = cursor.getInt(0);
        }

        return promedio;
    }

    public ArrayList<ArrayList<IndiceDatoBean>> obtenerComposicionCarteraDatos(/*String strFechaInicio, String strFechaFin,*/ IndiceDatoBean tieneHijosDatos, IndiceDatoBean noTieneHijosDatos) {

        if (/*strFechaInicio == null || strFechaFin == null ||*/ tieneHijosDatos == null || noTieneHijosDatos == null) {
            return null;
        }

        ArrayList<ArrayList<IndiceDatoBean>> listListDatos = new ArrayList<ArrayList<IndiceDatoBean>>();

        Log.d("CPC", "");
        //Log.d("CPC", "#### obtenerComposicionCarteraDatos fi:" + strFechaInicio + " ff:" + strFechaFin);

        listListDatos.add(obtenerComposicionCartera_Dato(queryComposicionCartera_RangoEdad(/*strFechaInicio, strFechaFin*/)));
        listListDatos.add(obtenerComposicionCartera_Dato(queryComposicionCartera_RangoIngresos(/*strFechaInicio, strFechaFin*/)));
        listListDatos.add(obtenerComposicionCartera_Dato(queryComposicionCartera_FlagHijos(/*strFechaInicio, strFechaFin,*/ tieneHijosDatos, noTieneHijosDatos)));

        return listListDatos;
    }

    public ArrayList<IndiceDatoBean> obtenerComposicionCartera_Dato(String sql) {
        ArrayList<IndiceDatoBean> listDato = new ArrayList<>();
        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        Integer idCampo = 0;
        String descripcion = "";
        Long cantidad = 0L;

        if (cursor.moveToFirst()) {
            do {
                descripcion = cursor.getString(1);
                cantidad = cursor.getLong(2);
                idCampo = cursor.getInt(3);
                listDato.add(new IndiceDatoBean(idCampo, descripcion, cantidad));

            } while (cursor.moveToNext());
        }
        return listDato;
    }

    public String queryComposicionCartera_RangoEdad(/*String strFechaInicio, String strFechaFin*/) {

        Log.d("CPC", "");
        Log.d("CPC", "#### queryComposicionCartera_RangoEdad fi:" /*+ strFechaInicio + " ff:" + strFechaFin*/);

        /*
        UC.FECHACITA BETWEEN '2016-07-05' AND '2016-07-11'
        AND (P.codigoetapa = 2) AND (UC.CODIGOESTADO = 2)
        TT.IDTABLA = 3
         */

        String sql = String.format(/*" SELECT DISTINCT TT.IDTABLA, TT.VALORCADENA, gr.cantidad, tt.codigocampo" +
                        " FROM TBL_TABLA_TABLAS tt" +
                        " LEFT JOIN" +
                        " (" +
                        "    SELECT count(*) as cantidad, PRAGPE.codigorangoedad as valor" +
                        "    FROM" +
                        "    (" +
                        "        SELECT DISTINCT p.IDPROSPECTO, p.codigorangoedad, uc.CODIGOESTADO" +
                        "        FROM tbl_prospecto p  " +
                        "        INNER JOIN   " +
                        "            (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, " +
                        "             c.fechacita, C.HORAINICIO, c.IdProspecto, c.CODIGOESTADO," +
                        "             c.CODIGORESULTADO   " +
                        "              FROM tbl_cita c     GROUP BY c.IdProspecto)   UC" +
                        "        on UC.IdProspecto = p.IDPROSPECTO " +
                        "        WHERE UC.FECHACITA BETWEEN '%s' AND '%s'" +
                        "        AND (P.codigoetapa = %s) AND (UC.CODIGOESTADO = %s)" +
                        "    ) PRAGPE" +
                        "    group BY PRAGPE.codigorangoedad" +
                        " ) gr" +
                        " on tt.codigocampo = GR.valor" +
                        " WHERE TT.IDTABLA = %s" +
                        " ORDER BY 4", "strFechaInicio", "strFechaFin",*/
                        "SELECT DISTINCT \n" +
                                "\t\t\tTT.IDTABLA, \n" +
                                "\t\t\tTT.VALORCADENA, \n" +
                                "\t\t\tIFNULL(gr.cantidad,0) AS CANTIDAD, \n" +
                                "\t\t\ttt.codigocampo \n" +
                                "\t FROM TBL_TABLA_TABLAS tt \n" +
                                "\t LEFT JOIN \n" +
                                "\t (   \n" +
                                "\t\t\tSELECT \n" +
                                "\t\t\t\tcount(*) as cantidad, \n" +
                                "\t\t\t\tPRAGPE.codigorangoedad as valor    \n" +
                                "\t\t\tFROM    \n" +
                                "\t\t\t(\n" +
                                "\t\t\t\tSELECT DISTINCT \n" +
                                "\t\t\t\t\t\tp.IDPROSPECTODISPOSITIVO, p.codigorangoedad, uc.CODIGOESTADO        \n" +
                                "\t\t\t\tFROM tbl_prospecto p        \n" +
                                "\t\t\t\tINNER JOIN               \n" +
                                "\t\t\t\t(\n" +
                                "\t\t\t\t\tSELECT \n" +
                                "\t\t\t\t\t    max(c.NUMEROENTREVISTA) ,              \n" +
                                "\t\t\t\t\t\tc.fechacita, \n" +
                                "\t\t\t\t\t\tC.HORAINICIO, \n" +
                                "\t\t\t\t\t\tc.IdProspectoDispositivo,\t \n" +
                                "\t\t\t\t\t\tc.CODIGOESTADO,             \n" +
                                "\t\t\t\t\t\tc.CODIGORESULTADO  ,\n" +
                                "\t\t\t\t\t\tc.CODIGOETAPAPROSPECTO\n" +
                                "\t\t\t\t\tFROM tbl_cita c     \n" +
                                "\t\t\t\t\tGROUP BY c.IdProspectoDispositivo\n" +
                                "\t\t\t\t)   UC        \n" +
                                "\t\t\t\ton UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO        \n" +
                                "\t\t\t\tWHERE \n" +
                                "\t\t\t\t\t(uc.CODIGOETAPAPROSPECTO = %s) \n" +
                                "\t\t\t\t\tAND (UC.CODIGOESTADO = %s)    \n" +
                                "\t\t\t) PRAGPE   \n" +
                                "\t\t\tgroup BY PRAGPE.codigorangoedad \n" +
                                "\t\t) gr \n" +
                                "\t\ton tt.codigocampo = GR.valor \n" +
                                "\t\tWHERE TT.IDTABLA = %s \n" +
                                "\t\tORDER BY 4\n",
                Constantes.codEtapa1ra, Constantes.codEstadoAgendada,
                Constantes.codTablaRangoEdad
        );

        Log.d("CPC", " queryComposicionCartera_RangoEdad sql: " + sql);

        return sql.toString();
    }

    public String queryComposicionCartera_RangoIngresos(/*String strFechaInicio, String strFechaFin*/) {

        Log.d("CPC", "");
        Log.d("CPC", "#### queryComposicionCartera_RangoIngresos fi:" /*+ strFechaInicio + " ff:" + strFechaFin*/);

        /*
        UC.FECHACITA BETWEEN '2016-07-05' AND '2016-07-11'
        AND (P.codigoetapa = 2) AND (UC.CODIGOESTADO = 2)
        TT.IDTABLA = 3
         */

        String sql = String.format(/*" SELECT DISTINCT TT.IDTABLA, TT.VALORCADENA, gr.cantidad, tt.codigocampo" +
                        " FROM TBL_TABLA_TABLAS tt" +
                        " LEFT JOIN" +
                        " (" +
                        "    SELECT count(*) as cantidad, PRAGPE.codigorangoingreso as valor" +
                        "    FROM " +
                        "    (" +
                        "        SELECT DISTINCT p.IDPROSPECTO, p.codigorangoingreso, uc.CODIGOESTADO" +
                        "        FROM tbl_prospecto p   " +
                        "        INNER JOIN   " +
                        "            (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, " +
                        "             c.fechacita, C.HORAINICIO, c.IdProspecto, c.CODIGOESTADO, " +
                        "             c.CODIGORESULTADO   " +
                        "              FROM tbl_cita c     GROUP BY c.IdProspecto)   UC  " +
                        "        on UC.IdProspecto = p.IDPROSPECTO  " +
                        "        WHERE Uc.FECHACITA BETWEEN '%s' AND '%s' " +
                        "       AND (P.codigoetapa = %s) AND (UC.CODIGOESTADO = %s)" +
                        "    ) PRAGPE" +
                        "    group BY PRAGPE.codigorangoingreso" +
                        " ) gr" +
                        " on tt.codigocampo = GR.valor" +
                        " WHERE TT.IDTABLA = %s" +
                        " ORDER BY 4", //strFechaInicio, strFechaFin,*/
                "SELECT DISTINCT \n" +
                        "\t\t\tTT.IDTABLA, \n" +
                        "\t\t\tTT.VALORCADENA, \n" +
                        "\t\t\tIFNULL(gr.cantidad,0) AS CANTIDAD, \n" +
                        "\t\t\ttt.codigocampo \n" +
                        "\t FROM TBL_TABLA_TABLAS tt \n" +
                        "\t LEFT JOIN \n" +
                        "\t (   \n" +
                        "\t\t\tSELECT \n" +
                        "\t\t\t\tcount(*) as cantidad, \n" +
                        "\t\t\t\tPRAGPE.codigorangoingreso as valor    \n" +
                        "\t\t\tFROM    \n" +
                        "\t\t\t(\n" +
                        "\t\t\t\tSELECT DISTINCT \n" +
                        "\t\t\t\t\t\tp.IDPROSPECTODISPOSITIVO, p.codigorangoingreso, uc.CODIGOESTADO        \n" +
                        "\t\t\t\tFROM tbl_prospecto p        \n" +
                        "\t\t\t\tINNER JOIN               \n" +
                        "\t\t\t\t(\n" +
                        "\t\t\t\t\tSELECT \n" +
                        "\t\t\t\t\t    max(c.NUMEROENTREVISTA) ,              \n" +
                        "\t\t\t\t\t\tc.fechacita, \n" +
                        "\t\t\t\t\t\tC.HORAINICIO, \n" +
                        "\t\t\t\t\t\tc.IdProspectoDispositivo,\t \n" +
                        "\t\t\t\t\t\tc.CODIGOESTADO,             \n" +
                        "\t\t\t\t\t\tc.CODIGORESULTADO  ,\n" +
                        "\t\t\t\t\t\tc.CODIGOETAPAPROSPECTO\n" +
                        "\t\t\t\t\tFROM tbl_cita c     \n" +
                        "\t\t\t\t\tGROUP BY c.IdProspectoDispositivo\n" +
                        "\t\t\t\t)   UC        \n" +
                        "\t\t\t\ton UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO        \n" +
                        "\t\t\t\tWHERE \n" +
                        "\t\t\t\t\t(uc.CODIGOETAPAPROSPECTO = %s) \n" +
                        "\t\t\t\t\tAND (UC.CODIGOESTADO = %s)    \n" +
                        "\t\t\t) PRAGPE   \n" +
                        "\t\t\tgroup BY PRAGPE.codigorangoingreso\n" +
                        "\t\t) gr \n" +
                        "\t\ton tt.codigocampo = GR.valor \n" +
                        "\t\tWHERE TT.IDTABLA = %s\n" +
                        "\t\tORDER BY 4",
                Constantes.codEtapa1ra, Constantes.codEstadoAgendada,
                Constantes.codTablaRangoIngresos
        );

        Log.d("CPC", " queryComposicionCartera_RangoIngresos sql: " + sql);

        return sql.toString();
    }

    public String queryComposicionCartera_FlagHijos(/*String strFechaInicio, String strFechaFin,*/ IndiceDatoBean tieneHijosDatos, IndiceDatoBean noTieneHijosDatos) {

        Log.d("CPC", "");
        Log.d("CPC", "#### queryComposicionCartera_FlagHijos fi:" /*+ strFechaInicio + " ff:" + strFechaFin*/);

        if (tieneHijosDatos == null || noTieneHijosDatos == null) {
            return null;
        }
        int idTabla = tieneHijosDatos.getIndice();

        /*
        -3 idTabla
        1 flagTrue
        Si tiene hijos

        -3 idTabla
        0 flagFalse
        No tiene hijos

        -3 idTabla

        UC.FECHACITA BETWEEN '2016-07-05' AND '2016-07-11'
        AND (P.codigoetapa = 2) AND (UC.CODIGOESTADO = 2)
        TT.IDTABLA = 3

         */

        String sql = String.format(/*" SELECT DISTINCT TT.IDTABLA, TT.VALORCADENA, gr.cantidad, tt.codigocampo" +
                        " FROM " +
                        "  (SELECT '%s' AS  IDTABLA, '%s' AS CODIGOCAMPO, '%s' as VALORCADENA" +
                        "    UNION " +
                        "    SELECT '%s' AS  IDTABLA, '%s' AS CODIGOCAMPO, '%s' as VALORCADENA" +
                        "   ) tt" +
                        " LEFT JOIN" +
                        " (" +
                        "     SELECT count(*) as cantidad, PRAGPE.flaghijo as valor" +
                        "     FROM " +
                        "     (" +
                        "         SELECT DISTINCT p.IDPROSPECTO, p.flaghijo, uc.CODIGOESTADO" +
                        "         FROM tbl_prospecto p  " +
                        "         INNER JOIN  " +
                        "             (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA," +
                        "              c.fechacita, C.HORAINICIO, c.IdProspecto, c.CODIGOESTADO, " +
                        "              c.CODIGORESULTADO  " +
                        "               FROM tbl_cita c     GROUP BY c.IdProspecto)   UC  " +
                        "         on UC.IdProspecto = p.IDPROSPECTO  " +
                        "         WHERE Uc.FECHACITA BETWEEN '%s' AND '%s' " +
                        "        AND (P.codigoetapa = %s)  AND (UC.CODIGOESTADO = %s) " +
                        "    ) PRAGPE " +
                        "    group BY PRAGPE.flaghijo " +
                        " ) gr " +
                        " on tt.codigocampo = GR.valor " +
                        " WHERE TT.IDTABLA = '%s'" +
                        " ORDER BY 4"*/
                "SELECT DISTINCT \n" +
                        "\t\t\tTT.IDTABLA, \n" +
                        "\t\t\tTT.VALORCADENA, \n" +
                        "\t\t\tgr.cantidad, \n" +
                        "\t\t\ttt.codigocampo \n" +
                        "FROM   \n" +
                        "(\n" +
                        "\t\tSELECT '%s' AS  IDTABLA, \n" +
                        "\t\t'%s' AS CODIGOCAMPO, \n" +
                        "\t\t'%s' as VALORCADENA    \n" +
                        "\t\tUNION     \n" +
                        "\t\tSELECT '%s' AS  IDTABLA, \n" +
                        "\t\t'%s' AS CODIGOCAMPO, \n" +
                        "\t\t'%s' as VALORCADENA   \n" +
                        ") tt LEFT JOIN \n" +
                        "(     \n" +
                        "\t\tSELECT count(*) as cantidad, \n" +
                        "\t\tPRAGPE.flaghijo as valor     \n" +
                        "\t\tFROM      \n" +
                        "\t\t(         \n" +
                        "\t\t\tSELECT DISTINCT \n" +
                        "\t\t\t\tp.IDPROSPECTODISPOSITIVO, p.flaghijo, uc.CODIGOESTADO         \n" +
                        "\t\t\tFROM tbl_prospecto p           \n" +
                        "\t\t\tINNER JOIN               \n" +
                        "\t\t\t(\n" +
                        "\t\t\t\tSELECT \n" +
                        "\t\t\t\t\t max(c.NUMEROENTREVISTA) ,              \n" +
                        "\t\t\t\t\t\tc.fechacita, \n" +
                        "\t\t\t\t\t\tC.HORAINICIO, \n" +
                        "\t\t\t\t\t\tc.IdProspectoDispositivo,\t \n" +
                        "\t\t\t\t\t\tc.CODIGOESTADO,             \n" +
                        "\t\t\t\t\t\tc.CODIGORESULTADO  ,\n" +
                        "\t\t\t\t\t\tc.CODIGOETAPAPROSPECTO              \n" +
                        "\t\t\t\tFROM tbl_cita c     \n" +
                        "\t\t\t\tGROUP BY c.IdProspectoDispositivo\n" +
                        "\t\t\t)   UC           \n" +
                        "\t\t\ton UC.IdProspectoDispositivo = p.IDPROSPECTODISPOSITIVO         \n" +
                        "\t\t\tWHERE      \n" +
                        "\t\t\t\t(uc.CODIGOETAPAPROSPECTO = '%s') \n" +
                        "\t\t\t\t\tAND (UC.CODIGOESTADO = '%s')    \n" +
                        "\t\t) PRAGPE     \n" +
                        "\t\tgroup BY PRAGPE.flaghijo  \n" +
                        "\t) gr  on tt.codigocampo = GR.valor  WHERE TT.IDTABLA = '%s' ORDER BY 4\n", idTabla, tieneHijosDatos.getValor(), tieneHijosDatos.getDescripcion(),
                idTabla, noTieneHijosDatos.getValor(), noTieneHijosDatos.getDescripcion(),
                //strFechaInicio, strFechaFin,
                Constantes.codEtapa1ra, Constantes.codEstadoAgendada,
                idTabla
        );

        Log.d("CPC", " queryComposicionCartera_FlagHijos sql: " + sql);

        return sql.toString();
    }

    public long obtenerCantidadProspectosRangoIngresos(short codigoRangoIngreso) {

        Log.d("PC", "obtenerCantidadProspectosRangoIngresos:  " + codigoRangoIngreso);
        Log.d("PC", " ");

        long cantidad = ProspectoBean.tableHelper.countRows(
                ProspectoBean.CN_CODIGORANGOINGRESO + " = ?  ",
                new String[]{String.valueOf(codigoRangoIngreso)});

        return cantidad;
    }

    public long obtenerCantidadProspectosGrupoEtareo(short codigoRangoEdad) {

        Log.d("PC", "obtenerCantidadProspectosGrupoEtareo:  " + codigoRangoEdad);
        Log.d("PC", " ");

        long cantidad = ProspectoBean.tableHelper.countRows(
                ProspectoBean.CN_CODIGORANGOEDAD + " = ?  ",
                new String[]{String.valueOf(codigoRangoEdad)});

        return cantidad;
    }

    public long obtenerCantidadProspectosHijos(short tieneHijos) {

        Log.d("PC", "obtenerCantidadProspectosHijos:  " + tieneHijos);
        Log.d("PC", " ");

        long cantidad = ProspectoBean.tableHelper.countRows(
                ProspectoBean.CN_FLAGHIJO + " = ?  ",
                new String[]{String.valueOf(tieneHijos)});

        return cantidad;

    }

    public Iterator<Entity> obtenerProspectoPorCodigoFuente(String codigoFuente, String strFecha) {

        Log.d("PC", "obtenerProspectoPorCodigoFuente codigoFuente: " + codigoFuente + " strFecha: " + strFecha);
        Log.d("PC", " ");

        ProspectoBean bean = null;

        Iterator<Entity> beans;

        beans = ProspectoBean.tableHelper.getEntities(
                ProspectoBean.CN_CODIGOFUENTE + " = ?  AND " +
                        ProspectoBean.CN_FECHACREACIONDISPOSITIVO + " = ? ",
                new String[]{codigoFuente, strFecha}).iterator();

        while (beans.hasNext()) {
            bean = (ProspectoBean) beans.next();
            imprimirProspecto(bean);
        }

        return beans;
    }

    /*
    public ArrayList<TusContactosBean> obtenerTusContactos() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();

        String sql =
                "SELECT DISTINCT p.idprospecto, (p.NOMBRES || ' ' || p.APELLIDOMATERNO || ' ' || p.APELLIDOPATERNO) as nombres, " +
                        "p.CODIGOETAPA, p.CODIGORANGOINGRESO, p.FLAGHIJO, cita.ultfecha, cita.CODIGOESTADO, cita.CODIGORESULTADO " +
                        "   from tbl_prospecto p, " +
                        "    (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "      FROM tbl_cita c  GROUP BY c.idprospecto) cita " +
                        "   where p.idprospecto = cita.idprospecto";

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                bean.setCodProspecto(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setCodEtapa(cursor.getInt(2));
                bean.setCodIngresos(cursor.getInt(3));
                bean.setCodTieneHijos(cursor.getInt(4));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(5)));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                bean.setCodEstado(cursor.getInt(6));
                bean.setCodResultado(cursor.getInt(7));
                imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/

    //--3 Si el eatdo es realizado, colocas estado cita y resultado
    //--5 si resultado no es interesado, mostrar etapa y resultado
    /*public ArrayList<TusContactosBean> obtenerTusContactosEstadoRealizadoNoInteresado() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();

        String sql = String.format(
                " SELECT distinct p.idprospecto, (p.NOMBRES || ' ' || p.APELLIDOMATERNO || ' ' || p.APELLIDOPATERNO) as nombres," +
                        " p.CodigoEtapa, p.CODIGORANGOINGRESO, p.FLAGHIJO, UC.ultfecha, UC.CODIGOESTADO, UC.CODIGORESULTADO " +
                        " from tbl_prospecto p " +
                        " INNER JOIN " +
                        "        (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, " +
                        " c.CODIGOESTADO, c.CODIGORESULTADO" +
                        "                FROM tbl_cita c  GROUP BY c.idprospecto) UC" +
                        " on UC.idprospecto = p.idprospecto" +
                        " WHERE (UC.CODIGOESTADO = %s OR UC.CODIGORESULTADO= %s)", Constantes.codEstadoRealizada, Constantes.codResultado_noInteresado);

        Log.d("CPC", "obtenerTusContactosEstadoRealizadoNoInteresado sql:" + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                //Se cambio a dispositivo - bean.setCodProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setCodEtapa(cursor.getInt(2));
                bean.setCodIngresos(cursor.getInt(3));
                bean.setCodTieneHijos(cursor.getInt(4));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(5)));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                bean.setCodEstado(cursor.getInt(6));
                bean.setCodResultado(cursor.getInt(7));
                bean.setCasoTipo(Constantes.codTusContactosRealizadoNoInteresado);
                imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/


    // Obtiene todos los contactos
    public static ArrayList<TusContactosBean> obtenerTusContactos() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();

        String sql = String.format(
                "SELECT distinct \n" +
                        "        p.idprospecto, \n" +
                        "        p.idprospectodispositivo,\n" +
                        "        (IFNULL(p.NOMBRES ,'')|| ' ' || IFNULL(p.APELLIDOPATERNO,'')  || ' ' || IFNULL(p.APELLIDOMATERNO,'')) as nombres, \n" +
                        "        p.CodigoEtapa, \n" +
                        "--        p.CodigoEstado,\n" +
                        "        -- TIENE RECORDATORIO DE LLAMADA INDEPENDIENTE SI ES NUEVO O EN SU CITA VIGENTE??\n" +
                        "\t\t(case when p.CodigoEtapa = 1 then\n" +
                        "            (case  when exists (select 1 from tbl_recordatorio_llamada rec1 where rec1.idprospectodispositivo = p.idprospectodispositivo AND UC.IDCITADISPOSITIVO is NULL) then 1 else 0 end)\n" +
                        "                    else                                     \n" +
                        "                        (case  when exists (select 1 from tbl_recordatorio_llamada rec2 where rec2.idcitadispositivo = UC.IDCITADISPOSITIVO) then 1 else 0 end )\n" +
                        "                    end\n" +
                        "            ) as TIENERECORDATORIO,\n" +
                        "        uc.CodigoEtapaProspecto,\n" +
                        "        p.CODIGORANGOINGRESO, \n" +
                        "        p.FLAGHIJO, \n" +
                        "        UC.CODIGOESTADO as CodigoEstadoCIta, \n" +
                        "        UC.CODIGORESULTADO as CodigoResultadoCita,\n" +
                        "        IFNULL(UC.FECHAULTIMOMOVIMIENTO,''),\n" +
                        "\t\tUC.ESREAGENDADA,\n" +
                        "        UC.IDCITADISPOSITIVO, p.FECHACREACIONDISPOSITIVO\n" +
                        "from \n" +
                        "tbl_prospecto p  \n" +
                        "LEFT JOIN    \n" +
                        "(\n" +
                        "    SELECT \n" +
                        "        max(c.NUMEROENTREVISTA) as NUMEROENTREVISTA,\n" +
                        "\t\tc.CODIGOESTADO,\n" +
                        "\t\tc.CODIGORESULTADO,\n" +
                        "        -- ver si es reagendada\n" +
                        "        (case when EXISTS(SELECT 1 FROM TBL_CITA CR WHERE CR.NUMEROENTREVISTA = (C.NUMEROENTREVISTA - 1)  AND CR.IDPROSPECTODISPOSITIVO = C.IDPROSPECTODISPOSITIVO and CR.CODIGOESTADO =  4) then 1 else 0 end) AS ESREAGENDADA,\n" +
                        "        c.IDPROSPECTODISPOSITIVO,\n" +
                        "        c.CODIGOETAPAPROSPECTO,\n" +
                        "        c.IDCITADISPOSITIVO,\n" +
                        "        (select max(fechamovimientoestadodispositivo) from tbl_cita_movimiento_estado where idcitadispositivo = c.idcitadispositivo) as FECHAULTIMOMOVIMIENTO\n" +
                        "    FROM tbl_cita c  \n" +
                        "    GROUP BY c.idprospectodispositivo\n" +
                        ") UC \n" +
                        "on \n" +
                        "UC.idprospectodispositivo = p.idprospectodispositivo   "
        );

        Log.d("CPC", "obtenerTusContactosAgendadosOPorContactar sql:" + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                //Se cambio a dispositivo - bean.setCodProspecto(cursor.getInt(0));
                //bean.setIdProspecto(cursor.getInt(0)); // TODO: Agregar Luego
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNomProspecto(cursor.getString(2));
                bean.setCodEtapa(cursor.getInt(3));
                bean.setTieneRecordatorio(cursor.getInt(4));
                bean.setCodEtapaCitaProspecto(cursor.getInt(5)); // EL codigo de etapa prospetco de la ultima cita
                bean.setCodIngresos(cursor.getInt(6));
                bean.setCodTieneHijos(cursor.getInt(7));
                bean.setCodEstado(cursor.getInt(8));
                bean.setCodResultado(cursor.getInt(9));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(10)));
                bean.setEsReagendada(cursor.getInt(11));
                bean.setFechaCreacionProspecto(DateUtils.getDateFormat12(DateUtils.convertStringToDateTime(cursor.getString(13).substring(0, 19))));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                //bean.setCasoTipo(Constantes.codTusContactosAgendadoOPorContactar);
                //imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }

    //--  Si el estado de la cita es Agendad o por contactar verificas si tiene un recordatorio de llamadaç
    //-- QUITAR REAGENDADOS
    //-- quitar  no interesado

    //--1 Si tiene un recordatorio de llamada, muestras es estado de la cita y abajo muestras Rec. De llamada
    /*
    public ArrayList<TusContactosBean> obtenerTusContactosRecordatorioLLamada() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();

        String sql = String.format(
                " SELECT distinct p.idprospecto, (p.NOMBRES || ' ' || p.APELLIDOMATERNO || ' ' || p.APELLIDOPATERNO) as nombres," +
                        " p.CodigoEtapa, p.CODIGORANGOINGRESO, p.FLAGHIJO, UC.ultfecha, UC.CODIGOESTADO, UC.CODIGORESULTADO " +
                        " from tbl_prospecto p " +
                        " INNER JOIN " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "         FROM tbl_cita c  GROUP BY c.idprospecto) UC" +
                        " on UC.idprospecto = p.idprospecto " +
                        " WHERE (UC.CODIGOESTADO = %s OR UC.CODIGOESTADO = %s) AND Uc.CodigoResultado!=%s " +
                        " AND p.idprospecto NOT IN ( " +
                        "         SELECT IDProspecto  FROM TBL_CITA " +
                        "         WHERE CODIGOESTADO = %s AND CodigoResultado = %s " +
                        "         ) " +
                        " AND P.idprospecto IN ( " +
                        "         SELECT IDPROSPECTO FROM TBL_RECORDATORIO" +
                        "         WHERE FLAGACTIVO = 1" +
                        " );", Constantes.codEstadoPorContactar, Constantes.codEstadoAgendada,
                Constantes.codResultado_noInteresado,
                Constantes.codEstadoAgendada, Constantes.codResultado_reagendado
        );

        Log.d("CPC", "obtenerTusContactosRecordatorioLLamada sql:" + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                //bean.setCodProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setCodEtapa(cursor.getInt(2));
                bean.setCodIngresos(cursor.getInt(3));
                bean.setCodTieneHijos(cursor.getInt(4));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(5)));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                bean.setCodEstado(cursor.getInt(6));
                bean.setCodResultado(cursor.getInt(7));
                bean.setCasoTipo(Constantes.codTusContactosRecordatorioLlamada);
                imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/

    // --2 Si no tiene recordatorio de llamada muestro el estado de la cita
    /*public ArrayList<TusContactosBean> obtenerTusContactosAgendadosOPorContactar() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();


        //                " AND P.idprospecto NOT IN ( " +
        //                "         SELECT IDPROSPECTO FROM TBL_RECORDATORIO " +
        //                "           WHERE FLAGACTIVO = 1 " +


        String sql = String.format(
                " SELECT distinct p.idprospecto, (p.NOMBRES || ' ' || p.APELLIDOMATERNO || ' ' || p.APELLIDOPATERNO) as nombres," +
                        " p.CodigoEtapa, p.CODIGORANGOINGRESO, p.FLAGHIJO, UC.ultfecha, UC.CODIGOESTADO, UC.CODIGORESULTADO " +
                        " from tbl_prospecto p " +
                        " INNER JOIN " +
                        "   (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "         FROM tbl_cita c  GROUP BY c.idprospecto) UC" +
                        " on UC.idprospecto = p.idprospecto " +
                        " WHERE (UC.CODIGOESTADO = %s OR UC.CODIGOESTADO = %s) AND Uc.CodigoResultado!=%s " +
                        " AND p.idprospecto NOT IN ( " +
                        "         SELECT idprospecto  FROM TBL_CITA " +
                        "         WHERE CODIGOESTADO = %s AND CodigoResultado = %s " +
                        "         ) " +
                        "; ", Constantes.codEstadoPorContactar, Constantes.codEstadoAgendada,
                Constantes.codResultado_noInteresado,
                Constantes.codEstadoAgendada, Constantes.codResultado_reagendado
        );

        Log.d("CPC", "obtenerTusContactosAgendadosOPorContactar sql:" + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                //Se cambio a dispositivo - bean.setCodProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setCodEtapa(cursor.getInt(2));
                bean.setCodIngresos(cursor.getInt(3));
                bean.setCodTieneHijos(cursor.getInt(4));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(5)));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                bean.setCodEstado(cursor.getInt(6));
                bean.setCodResultado(cursor.getInt(7));
                bean.setCasoTipo(Constantes.codTusContactosAgendadoOPorContactar);
                imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/

    // --4 Logica para mostrar la palabra re agendada
    // --  Obtienes la ultima cita de la etapa
    // --  Si està en estado agendada y resultado pendiente, se coloca re agenda

    /*
    public ArrayList<TusContactosBean> obtenerTusContactosReagendados() {
        ArrayList<TusContactosBean> lista = new ArrayList<>();

        //
        //CODIGOESTADO = 2
        //CodigoResultado = 5

        //UC.CodigoEstado=2
        //AND Uc.CodigoResultado=0

// 1	Por Contactar
//2	Agendada
//3	Realizada
//4	Re-agendado

//0	Pendiente
//1	Cierre de Venta
//2	Siguiente Etapa
//3	No interesado
//4	Por Contactar
//5	Re-agendada
// 6	Volver a llamar

        String sql = String.format(
                " SELECT distinct p.idprospecto, (p.NOMBRES || ' ' || p.APELLIDOMATERNO || ' ' || p.APELLIDOPATERNO) as nombres," +
                        " p.CodigoEtapa, p.CODIGORANGOINGRESO, p.FLAGHIJO, UC.ultfecha, UC.CODIGOESTADO, UC.CODIGORESULTADO " +
                        " from tbl_prospecto p " +
                        " INNER JOIN " +
                        "       (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, c.CODIGOESTADO, " +
                        "         c.CODIGORESULTADO  " +
                        "           FROM tbl_cita c  " +
                        "        GROUP BY c.idprospecto) UC" +
                        " on UC.idprospecto = p.idprospecto " +
                        " INNER JOIN " +
                        "       (SELECT IDCITA,idprospecto  FROM TBL_CITA" +
                        "          WHERE CODIGOESTADO = %s AND CodigoResultado = %s" +
                        "       ) REAG" +
                        " ON P.idprospecto = REAG.idprospecto" +
                        " WHERE  UC.CodigoEstado=%s AND Uc.CodigoResultado=%s; ",
                Constantes.codEstadoReagendada, Constantes.codResultado_reagendado,
                Constantes.codEstadoAgendada, Constantes.codResultado_pendiente
        );

        Log.d("CPC", "obtenerTusContactosReagendados sql:" + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        TusContactosBean bean;
        if (cursor.moveToFirst()) {
            do {
                bean = new TusContactosBean();
                // Se cambio a dispositivo - bean.setCodProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setCodEtapa(cursor.getInt(2));
                bean.setCodIngresos(cursor.getInt(3));
                bean.setCodTieneHijos(cursor.getInt(4));
                bean.setFecUltEstado(DateUtils.convertStringToDateTime(cursor.getString(5)));
                bean.setDesFecUltEstado(DateUtils.getDateFormat12(bean.getFecUltEstado()));
                bean.setCodEstado(cursor.getInt(6));
                bean.setCodResultado(cursor.getInt(7));
                bean.setCasoTipo(Constantes.codTusContactosReagendado);
                imprimirTusContactos(bean);
                lista.add(bean);
            } while (cursor.moveToNext());
        }

        return lista;
    }*/

    public ProspectoDetalleBean obtenerProspectoDetalle(int idProspecto) {
        ProspectoDetalleBean bean = new ProspectoDetalleBean();

        /*String sql = String.format(
                "SELECT DISTINCT p.idprospecto, p.NOMBRES, p.APELLIDOMATERNO, p.APELLIDOPATERNO," +
                        " referente.idreferente, referente.nomreferente, " +
                        " p.CodigoEtapa, c.CodigoEstado, c.CodigoResultado, " +
                        " p.EMPRESA, p.CARGO, " +
                        " p.CORREOELECTRONICO1, p.TELEFONOCELULAR, p.TELEFONOFIJO," +
                        " p.CODIGOTIPODOCUMENTO, p.NUMERODOCUMENTO, p.FECHANACIMIENTO, p.FLAGHIJO, p.FlagConyuge, " +
                        " p.CODIGOESTADOCIVIL, p.CODIGOSEXO, " +
                        " p.CODIGONACIONALIDAD,p.CONDICIONFUMADOR, p.nota " +
                        "   from tbl_prospecto p " +
                        "   INNER JOIN " +
                        "    (SELECT max(c.FECHACITA || ' ' || c.HoraInicio) as ULTFECHA, c.idprospecto, c.CODIGOESTADO, c.CODIGORESULTADO  " +
                        "      FROM tbl_cita c  GROUP BY c.idprospecto) c " +
                        "   on c.idprospecto = p.idprospecto " +
                        "   LEFT JOIN  " +
                        "    (SELECT idprospecto as idreferente, (NOMBRES || ' ' || APELLIDOMATERNO || ' ' || APELLIDOPATERNO) as nomreferente  " +
                        "      FROM tbl_prospecto) referente " +
                        "   on p.idreferenciador = referente.idreferente" +
                        "   where p.idprospecto = '%s' ", idProspecto);*/
        String sql = String.format(
                "SELECT DISTINCT \n" +
                        "\t\t\tp.idprospecto, p.IDPROSPECTODISPOSITIVO , p.NOMBRES, p.APELLIDOMATERNO, p.APELLIDOPATERNO, \n" +
                        "\t\t\tR.IDREFERENTE,R.IDREFERENTEDISPOSITIVO, R.nomreferente,  p.CodigoEtapa, c.CodigoEstado, \n" +
                        "\t\t\tc.CodigoResultado, C.CODIGOETAPAPROSPECTO,  p.EMPRESA, p.CARGO,  p.CORREOELECTRONICO1, \n" +
                        "\t\t\tp.TELEFONOCELULAR, p.TELEFONOFIJO, p.CODIGOTIPODOCUMENTO, \n" +
                        "\t\t\tp.NUMERODOCUMENTO, p.FECHANACIMIENTO, p.FLAGHIJO, p.FlagConyuge,  \n" +
                        "\t\t\tp.CODIGOESTADOCIVIL, p.CODIGOSEXO,  p.CODIGONACIONALIDAD,\n" +
                        "\t\t\tp.CONDICIONFUMADOR, p.nota, p.codigofuente    \n" +
                        "from tbl_prospecto p    \n" +
                        "LEFT JOIN    \n" +
                        " (\n" +
                        "\tSELECT\n" +
                        "\t\tmax(c.NUMEROENTREVISTA) as NUMEROENTREVISTA, c.IDPROSPECTODISPOSITIVO,\n" +
                        "\t\tc.CODIGOESTADO, c.CODIGORESULTADO, C.CODIGOETAPAPROSPECTO      \n" +
                        "\tFROM tbl_cita c  GROUP BY c.IDPROSPECTODISPOSITIVO\n" +
                        ") c    \n" +
                        "on c.IDPROSPECTODISPOSITIVO = p.IDPROSPECTODISPOSITIVO \n" +
                        "LEFT JOIN      \n" +
                        "(\n" +
                        "\tSELECT \n" +
                        "\t\tIDPROSPECTO as IDREFERENTE,IDPROSPECTODISPOSITIVO as IDREFERENTEDISPOSITIVO, (NOMBRES || ' ' || APELLIDOMATERNO || ' ' || APELLIDOPATERNO) as nomreferente        \n" +
                        "\tFROM tbl_prospecto\n" +
                        ") R    \n" +
                        "on p.IDREFERENCIADORDISPOSITIVO = R.IDREFERENTEDISPOSITIVO   \n" +
                        "where p.IDPROSPECTODISPOSITIVO = '%s' ", idProspecto);
        Log.d("CPC", "obtenerProspectoDetalle: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoDetalleBean();
                //bean.setIdProspecto(cursor.getInt(0));
                bean.setIdProspectoDispositivo(cursor.getInt(1));
                bean.setNombres(cursor.getString(2));
                bean.setApellidoMaterno(cursor.getString(3));
                bean.setApellidoPaterno(cursor.getString(4));
                bean.setIdReferenciador(cursor.getInt(5));
                bean.setIdReferenciadorDispositivo(cursor.getInt(6));
                bean.setNombreReferenciador(cursor.getString(7));
                bean.setCodigoEtapa(cursor.getInt(8));
                bean.setCodigoEstado(cursor.getInt(9));
                bean.setCodigoResultado(cursor.getInt(10));
                bean.setEtapaUltimaCita(cursor.getInt(11));
                bean.setEmpresa(cursor.getString(12));
                bean.setCargo(cursor.getString(13));
                bean.setCorreoElectronico1(cursor.getString(14));
                bean.setTelefonoCelular(cursor.getString(15));
                bean.setTelefonoFijo(cursor.getString(16));
                bean.setCodigoTipoDocumento(cursor.getInt(17));
                bean.setNumeroDocumento(cursor.getString(18));
                bean.setFechaNacimiento(DateUtils.convertStringToDate(cursor.getString(19)));
                bean.setFlagHijos(cursor.getInt(20));
                bean.setFlagConyuge(cursor.getInt(21));
                bean.setCodigoEstadoCivil(cursor.getInt(22));
                bean.setCodigoSexo(cursor.getInt(23));
                bean.setCodigoNacionalidad(cursor.getInt(24));
                bean.setCondicionFumador(cursor.getInt(25));
                bean.setNota(cursor.getString(26));
                bean.setCodigoFuente(cursor.getInt(27));


                Log.d("CPC", "obtenerProspectoDetalle idDispositivo: " + bean.getIdProspectoDispositivo());

                //imprimirTusContactos(bean);
            } while (cursor.moveToNext());
        }

        return bean;
    }


    public long obtenerCantidadProspectoPorCodigoFuente(String codigoFuente, String strFecha) {

        Log.d("PC", "obtenerProspectoPorCodigoFuente codigoFuente: " + codigoFuente + " strFecha: " + strFecha);

        long cantidad = ProspectoBean.tableHelper.countRows(
                ProspectoBean.CN_CODIGOFUENTE + " = ?  AND " +
                        ProspectoBean.CN_FECHACREACIONDISPOSITIVO + " = ? ",
                new String[]{codigoFuente, strFecha});
        Log.d("PC", "cantidad:  " + cantidad);

        return cantidad;
    }

    /*public Map<String, Integer> obtenerCantidadProspectoPorCodigoFuente(String codigoFuente, String strFechaInicio, String strFechaFin) {

        Log.d("CPC", "");
        Log.d("CPC", "#### obtenerCantidadProspectoPorCodigoFuente cf" + codigoFuente + " fi:" + strFechaInicio + " ff:" + strFechaFin);

        // '2016-07-05' AND '2016-07-11'
        // P.CODIGOFUENTE=2

        String sql = String.format(
                "  SELECT COUNT(*), NUMPORFUENTE.FECHACREACIONDISPOSITIVO " +
                        "  FROM " +
                        "  (" +
                        "     SELECT p.IDPROSPECTODISPOSITIVO, P.FECHACREACIONDISPOSITIVO, P.CODIGOFUENTE" +
                        "     FROM tbl_prospecto P" +
                        "     WHERE P.FECHACREACIONDISPOSITIVO" +
                        "     BETWEEN '%s' AND '%s'" +
                        "     AND P.CODIGOFUENTE=%s" +
                        "  ) NUMPORFUENTE" +
                        "  GROUP BY NUMPORFUENTE.FECHACREACIONDISPOSITIVO ",
                strFechaInicio, strFechaFin,
                codigoFuente
        );

        Log.d("CPC", " obtenerCantidadProspectoPorCodigoFuente sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        String fechaCita = "";
        Integer cantidad = 0;

        Map<String, Integer> mapNumeroProspectos = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                fechaCita = cursor.getString(1);
                mapNumeroProspectos.put(fechaCita, cantidad);

            } while (cursor.moveToNext());

        }

        return mapNumeroProspectos;
    }*/

    public Map<String, Integer> obtenerCantidadReferidos(String strFechaInicio, String strFechaFin) {

        Log.d("CPC", "");
        Log.d("CPC", "#### obtenerCantidadReferidos fi:" + strFechaInicio + " ff:" + strFechaFin);

        // '2016-07-05' AND '2016-07-11'
        // P.CODIGOFUENTE=2

        String sql = String.format(
                "  SELECT COUNT(*), SUBSTR(FECHACREACIONDISPOSITIVO,0,11)" +
                        " FROM TBL_REFERIDO" +
                        " WHERE FECHACREACIONDISPOSITIVO" +
                        " BETWEEN '%s' AND '%s'" +
                        " GROUP BY FECHACREACIONDISPOSITIVO",
                strFechaInicio, strFechaFin
        );

        Log.d("CPC", " obtenerCantidadProspectoPorCodigoFuente sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        String fechaCita = "";
        Integer cantidad = 0;

        Map<String, Integer> mapNumeroProspectos = new HashMap<>();

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);
                fechaCita = cursor.getString(1);
                mapNumeroProspectos.put(fechaCita, cantidad);

            } while (cursor.moveToNext());

        }

        return mapNumeroProspectos;
    }

    public int obtenerTotalProspectoPorCodigoFuente(String codigoFuente, String strFechaInicio, String strFechaFin) {

        Log.d("CPC", "");
        Log.d("CPC", "#### obtenerTotalProspectoPorCodigoFuente cf" + codigoFuente + " fi:" + strFechaInicio + " ff:" + strFechaFin);

        // '2016-07-05' AND '2016-07-11'
        // P.CODIGOFUENTE=2

        String sql = String.format(
                " SELECT COUNT(*)   FROM " +
                        " (" +
                        "  SELECT DISTINCT p.IDPROSPECTODISPOSITIVO" +
                        "  FROM tbl_prospecto P" +
                        "  WHERE P.FECHACREACIONDISPOSITIVO" +
                        "  BETWEEN '%s' AND '%s'" +
                        "  AND P.CODIGOFUENTE = %s" +
                        " ) NUMPORFUENTE;",
                strFechaInicio, strFechaFin,
                codigoFuente
        );

        Log.d("CPC", " obtenerTotalProspectoPorCodigoFuente sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int cantidad = 0;

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);

            } while (cursor.moveToNext());

        }

        return cantidad;
    }

    public int obtenerTotalReferidosEntreFechas( String strFechaInicio, String strFechaFin) {

        Log.d("CPC", "");
        Log.d("CPC", "#### obtenerTotalReferidosEntreFechas  fi:" + strFechaInicio + " ff:" + strFechaFin);

        // '2016-07-05' AND '2016-07-11'
        // P.CODIGOFUENTE=2

        String sql = String.format(
                " select count(1)\n" +
                        "from tbl_referido\n" +
                        "where fechacreaciondispositivo between '%S' and '%S';",
                strFechaInicio, strFechaFin
        );

        Log.d("CPC", " obtenerTotalProspectoPorCodigoFuente sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int cantidad = 0;

        if (cursor.moveToFirst()) {
            do {
                cantidad = cursor.getInt(0);

            } while (cursor.moveToNext());

        }

        return cantidad;
    }

    public int obtenerPromedioReferidos(int idCalendarioHoy, String strFechaInicio, String strFechaFin) {
        int idCalendarioPrimeraEntrevista = obtenerIdCalendarioPrimeraEntrevista();
        int totalReferidos4UltSemanas = obtenerTotalReferidosEntreFechas(strFechaInicio, strFechaFin);

        Log.d("SEMP", "obtenerPromedioReferidos idCalendarioPrimeraEntrevista:" + idCalendarioPrimeraEntrevista + " idCalendarioHoy: " + idCalendarioHoy);
        int numeroSemanasAsesor = idCalendarioHoy - idCalendarioPrimeraEntrevista; // hacer Query

        int promedioRefer = 0;

        if (numeroSemanasAsesor > 4) {
            numeroSemanasAsesor = 4;
        }

        if (numeroSemanasAsesor > 0) {
            promedioRefer = totalReferidos4UltSemanas / numeroSemanasAsesor;
        }

        Log.d("SEMP", "obtenerPromedioReferidos promedioRefer:" + promedioRefer + " numeroSemanasAsesor:" + numeroSemanasAsesor + " totalReferidos4UltSemanas:" + totalReferidos4UltSemanas);

        return promedioRefer;
    }

    public int obtenerIdCalendarioPrimeraEntrevista() {

        Log.d("CPC", "");
        Log.d("CPC", "#### obtenerIdCalendarioPrimeraEntrevista");

        // '2016-07-05' AND '2016-07-11'
        // P.CODIGOFUENTE=2

        String sql =
                "  SELECT IDCALENDARIO FROM TBL_CALENDARIO" +
                        "  WHERE (" +
                        "    SELECT min(c.FECHACITA || ' ' || c.HoraInicio) as primera_cita" +
                        "    FROM tbl_cita c)" +
                        "  BETWEEN FECHAINICIOSEMANA AND  FECHAFINSEMANA";

        Log.d("CPC", " obtenerIdCalendarioPrimeraEntrevista sql: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        int idCalendario = 0;

        if (cursor.moveToFirst()) {
            do {
                idCalendario = cursor.getInt(0);
            } while (cursor.moveToNext());
        }

        return idCalendario;
    }


    /*public long obtenerCantidadProspectoPorCodigoFuente(String codigoFuente, String strFechaInicio, String strFechaFin) {

        Log.d("PC", "obtenerProspectoPorCodigoFuente codigoFuente: " + codigoFuente + " strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        long cantidad = ProspectoBean.tableHelper.countRows(
                ProspectoBean.CN_CODIGOFUENTE + " = ?  AND " +
                        ProspectoBean.CN_FECHACREACIONDISPOSITIVO + " BETWEEN ?  AND ? ",
                new String[]{codigoFuente, strFechaInicio, strFechaFin});
        Log.d("PC", "cantidad:  " + cantidad);

        return cantidad;
    }*/

    /*public long obtenerCantidadReferidos(String strFechaInicio, String strFechaFin) {

        Log.d("PC", "obtenerProspectoPorCodigoFuente strFechaInicio: " + strFechaInicio + " strFechaFin: " + strFechaFin);

        long cantidad = ReferidoBean.tableHelper.countRows(
                        ReferidoBean.CN_FECHACREACIONDISPOSITIVO + " BETWEEN ?  AND ? ",
                new String[]{strFechaInicio, strFechaFin});
        Log.d("PC", "cantidad:  " + cantidad);

        return cantidad;
    }*/

    public void imprimirProspecto(ProspectoBean bean) {
        StringBuffer sb = new StringBuffer();
        sb.append(" ip: ").append(bean.getIdProspecto());
        sb.append(" no: ").append(bean.getNombres());
        sb.append(" ap: ").append(bean.getApellidoPaterno());
        sb.append(" am: ").append(bean.getApellidoMaterno());
        sb.append(" cs: ").append(bean.getCodigoSexo());
        sb.append(" ec: ").append(bean.getCodigoEstadoCivil());
        sb.append(" fn: ").append(bean.getFechaNacimiento());
        sb.append(" re: ").append(bean.getCodigoRangoEdad());
        sb.append(" ri: ").append(bean.getCodigoRangoIngreso());
        sb.append(" fh: ").append(bean.getFlagHijo());
        sb.append(" fc: ").append(bean.getFlagConyuge());
        sb.append(" ce: ").append(bean.getCodigoEtapa());
        sb.append(" cf: ").append(bean.getCodigoFuente());
        sb.append(" fc: ").append(bean.getFechaCreacionDispositivo());

        Log.d("PC", "PPCT: " + sb.toString());
    }

    public void imprimirTusContactos(TusContactosBean bean) {
        StringBuffer sb = new StringBuffer();
        //Se cambio a dispositivo - sb.append(" cp: ").append(bean.getCodProspecto());
        sb.append(" cp: ").append(bean.getIdProspectoDispositivo());
        sb.append(" np: ").append(bean.getNomProspecto());
        sb.append(" ce: ").append(bean.getCodEtapa());
        sb.append(" ci: ").append(bean.getCodIngresos());
        sb.append(" th: ").append(bean.getCodTieneHijos());
        sb.append(" fu: ").append(bean.getDesFecUltEstado());
        sb.append(" ce: ").append(bean.getCodEstado());
        sb.append(" cr: ").append(bean.getCodResultado());
        Log.d("PC", "TC: " + sb.toString());
    }

}
