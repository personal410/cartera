package com.pacifico.cartera.Model.Bean.Auxiliar;

import org.joda.time.DateTime;

/**
 * Created by dsb on 06/07/2016.
 */
public class ProspectoDetalleBean {

    // agregado el idProspectoDispositivo
    private int idProspectoDispositivo;

    //private int idProspecto;
    private String Nombres;
    private String ApellidoMaterno;
    private String ApellidoPaterno;
    private int CodigoEtapa;
    private String Etapa;
    private int EtapaUltimaCita;
    private int CodigoEstado;
    private String Estado;
    private int CodigoResultado;
    private int CodigoEstadoReporte;
    private String EtapaEstado;
    private int IdReferenciador;
    private int IdReferenciadorDispositivo;
    private String nombreReferenciador;
    private String Empresa;
    private String Cargo;
    private String CorreoElectronico1;
    private String TelefonoCelular;
    private String TelefonoFijo;

    // datos personales
    private int CodigoSexo;
    private String Sexo;
    private int CodigoEstadoCivil;
    private String EstadoCivil;
    private String FechaNacimientoTexto;
    private DateTime FechaNacimiento;
    private Integer edad;
    private int FlagHijos;
    private String Hijos;
    private int FlagConyuge;
    private String Conyuge;
    private int CodigoTipoDocumento;
    private String TipoDocumento;
    private String NumeroDocumento;
    private int CodigoNacionalidad;
    private String Nacionalidad;
    private int CondicionFumador;
    private String Fumador;
    private int CodigoFuente;

    //
    private String Nota;

    // agregado
    public int getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public int getCodigoEtapa() {
        return CodigoEtapa;
    }

    public void setCodigoEtapa(int codigoEtapa) {
        CodigoEtapa = codigoEtapa;
    }

    public int getCodigoEstado() {
        return CodigoEstado;
    }

    public void setCodigoEstado(int codigoEstado) {
        CodigoEstado = codigoEstado;
    }

    public int getCodigoResultado() {
        return CodigoResultado;
    }

    public void setCodigoResultado(int codigoResultado) {
        CodigoResultado = codigoResultado;
    }

    public int getCodigoEstadoReporte() {
        return CodigoEstadoReporte;
    }

    public void setCodigoEstadoReporte(int codigoEstadoReporte) {
        CodigoEstadoReporte = codigoEstadoReporte;
    }

    public String getEtapaEstado() {
        return EtapaEstado;
    }

    public void setEtapaEstado(String etapaEstado) {
        EtapaEstado = etapaEstado;
    }

    public int getIdReferenciador() {
        return IdReferenciador;
    }

    public void setIdReferenciador(int idReferenciador) {
        IdReferenciador = idReferenciador;
    }

    public int getIdReferenciadorDispositivo() {
        return IdReferenciadorDispositivo;
    }

    public void setIdReferenciadorDispositivo(int idReferenciadorDispositivo) {
        IdReferenciadorDispositivo = idReferenciadorDispositivo;
    }

    public String getNombreReferenciador() {
        return nombreReferenciador;
    }

    public void setNombreReferenciador(String nombreReferenciador) {
        this.nombreReferenciador = nombreReferenciador;
    }

    public String getEmpresa() {
        return Empresa;
    }

    public void setEmpresa(String empresa) {
        Empresa = empresa;
    }

    public String getCargo() {
        return Cargo;
    }

    public void setCargo(String cargo) {
        Cargo = cargo;
    }

    public String getCorreoElectronico1() {
        return CorreoElectronico1;
    }

    public void setCorreoElectronico1(String correoElectronico1) {
        CorreoElectronico1 = correoElectronico1;
    }

    public String getTelefonoCelular() {
        return TelefonoCelular;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        TelefonoCelular = telefonoCelular;
    }

    public String getTelefonoFijo() {
        return TelefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        TelefonoFijo = telefonoFijo;
    }

    public int getCodigoSexo() {
        return CodigoSexo;
    }

    public void setCodigoSexo(int codigoSexo) {
        CodigoSexo = codigoSexo;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String sexo) {
        Sexo = sexo;
    }

    public int getCodigoEstadoCivil() {
        return CodigoEstadoCivil;
    }

    public void setCodigoEstadoCivil(int codigoEstadoCivil) {
        CodigoEstadoCivil = codigoEstadoCivil;
    }

    public String getEstadoCivil() {
        return EstadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        EstadoCivil = estadoCivil;
    }

    public String getFechaNacimientoTexto() {
        return FechaNacimientoTexto;
    }

    public void setFechaNacimientoTexto(String fechaNacimientoTexto) {
        FechaNacimientoTexto = fechaNacimientoTexto;
    }

    public DateTime getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(DateTime fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public int getFlagHijos() {
        return FlagHijos;
    }

    public void setFlagHijos(int flagHijos) {
        FlagHijos = flagHijos;
    }

    public String getHijos() {
        return Hijos;
    }

    public void setHijos(String hijos) {
        Hijos = hijos;
    }

    public int getCodigoTipoDocumento() {
        return CodigoTipoDocumento;
    }

    public void setCodigoTipoDocumento(int codigoTipoDocumento) {
        CodigoTipoDocumento = codigoTipoDocumento;
    }

    public String getTipoDocumento() {
        return TipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        TipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return NumeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        NumeroDocumento = numeroDocumento;
    }

    public int getCodigoNacionalidad() {
        return CodigoNacionalidad;
    }

    public void setCodigoNacionalidad(int codigoNacionalidad) {
        CodigoNacionalidad = codigoNacionalidad;
    }

    public String getNacionalidad() {
        return Nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        Nacionalidad = nacionalidad;
    }

    public int getCondicionFumador() {
        return CondicionFumador;
    }

    public void setCondicionFumador(int condicionFumador) {
        CondicionFumador = condicionFumador;
    }

    public String getFumador() {
        return Fumador;
    }

    public void setFumador(String fumador) {
        Fumador = fumador;
    }

    public String getNota() {
        return Nota;
    }

    public void setNota(String nota) {
        Nota = nota;
    }

    public String getEtapa() {
        return Etapa;
    }

    public void setEtapa(String etapa) {
        Etapa = etapa;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public int getFlagConyuge() {
        return FlagConyuge;
    }

    public void setFlagConyuge(int flagConyuge) {
        FlagConyuge = flagConyuge;
    }

    public String getConyuge() {
        return Conyuge;
    }

    public void setConyuge(String conyuge) {
        Conyuge = conyuge;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return ApellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        ApellidoPaterno = apellidoPaterno;
    }

    public int getEtapaUltimaCita() {
        return EtapaUltimaCita;
    }

    public void setEtapaUltimaCita(int etapaUltimaCita) {
        EtapaUltimaCita = etapaUltimaCita;
    }

    /*public int getIdProspecto() {
        return idProspecto;
    }

    public void setIdProspecto(int idProspecto) {
        this.idProspecto = idProspecto;
    }*/

    public int getCodigoFuente() {
        return CodigoFuente;
    }

    public void setCodigoFuente(int codigoFuente) {
        CodigoFuente = codigoFuente;
    }
}
