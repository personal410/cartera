package com.pacifico.cartera.Model.Bean.Auxiliar;

import android.graphics.drawable.Drawable;

import org.joda.time.DateTime;

/**
 * Created by dsb on 15/05/2016.
 */
public class ProgramacionSemanalDetalleBean {

    //private String idCita;
    private String idCitaDispositivo; // Agregado
    private String idProspectoDispositivo;
    //private String codProspecto;
    private String nomProspecto;
    private String idReferenciadorDispositivo;
    private String idReferenciador;
    private String nomReferenciador;
    private Integer codFuente;
    private String desFuente;
    private Integer codEtapa;
    private String desEtapa;
    private Drawable drwEtapa;
    private String fecha;
    private String desFecha;
    private Integer codEstado;
    private Integer codResultado;
    private String desEstado;
    private String desEstado2;
    private DateTime fecReagendado;
    private Drawable drwEstado;

    public ProgramacionSemanalDetalleBean(){

    }

    /*public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }*/

    public String getIdCitaDispositivo() {
        return idCitaDispositivo;
    }

    public void setIdCitaDispositivo(String idCitaDispositivo) {
        this.idCitaDispositivo = idCitaDispositivo;
    }

    public String getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(String idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    /*public String getCodProspecto() {
        return codProspecto;
    }

    public void setCodProspecto(String codProspecto) {
        this.codProspecto = codProspecto;
    }*/

    public String getNomProspecto() {
        return nomProspecto;
    }

    public void setNomProspecto(String nomProspecto) {
        this.nomProspecto = nomProspecto;
    }

    public String getIdReferenciadorDispositivo() {
        return idReferenciadorDispositivo;
    }

    public void setIdReferenciadorDispositivo(String idReferenciadorDispositivo) {
        this.idReferenciadorDispositivo = idReferenciadorDispositivo;
    }

    public String getIdReferenciador() {
        return idReferenciador;
    }

    public void setIdReferenciador(String idReferenciador) {
        this.idReferenciador = idReferenciador;
    }

    public String getNomReferenciador() {
        return nomReferenciador;
    }

    public void setNomReferenciador(String nomReferenciador) {
        this.nomReferenciador = nomReferenciador;
    }

    public Integer getCodFuente() {
        return codFuente;
    }

    public void setCodFuente(Integer codFuente) {
        this.codFuente = codFuente;
    }

    public String getDesFuente() {
        return desFuente;
    }

    public void setDesFuente(String desFuente) {
        this.desFuente = desFuente;
    }

    public Integer getCodEtapa() {
        return codEtapa;
    }

    public void setCodEtapa(Integer codEtapa) {
        this.codEtapa = codEtapa;
    }

    public String getDesEtapa() {
        return desEtapa;
    }

    public void setDesEtapa(String desEtapa) {
        this.desEtapa = desEtapa;
    }

    public Drawable getDrwEtapa() {
        return drwEtapa;
    }

    public void setDrwEtapa(Drawable drwEtapa) {
        this.drwEtapa = drwEtapa;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDesFecha() {
        return desFecha;
    }

    public void setDesFecha(String desFecha) {
        this.desFecha = desFecha;
    }

    public Integer getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(Integer codEstado) {
        this.codEstado = codEstado;
    }

    public Integer getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(Integer codResultado) {
        this.codResultado = codResultado;
    }

    public String getDesEstado() {
        return desEstado;
    }

    public void setDesEstado(String desEstado) {
        this.desEstado = desEstado;
    }

    public String getDesEstado2() {
        return desEstado2;
    }

    public void setDesEstado2(String desEstado2) {
        this.desEstado2 = desEstado2;
    }

    public DateTime getFecReagendado() {
        return fecReagendado;
    }

    public void setFecReagendado(DateTime fecReagendado) {
        this.fecReagendado = fecReagendado;
    }

    public Drawable getDrwEstado() {
        return drwEstado;
    }

    public void setDrwEstado(Drawable drwEstado) {
        this.drwEstado = drwEstado;
    }

}

