package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProspectoADNBean;

/**
 * Created by dsb on 06/07/2016.
 */
public class CarteraAdnController {

    public CarteraAdnController(){}

    public ProspectoADNBean obtenerProspectoAdnResumen(int idProspectoDispositivo) {
        ProspectoADNBean bean = new ProspectoADNBean();

        String sql = String.format(
                " SELECT ADN.TIPOCAMBIO, ADN.MONEDATOTALACTIVOREALIZABLE, ADN.TOTALACTIVOREALIZABLE, " +
               " ADN.MONEDATOTALINGRESOFAMILIARMENSUAL, ADN.TOTALINGRESOFAMILIARMENSUAL, " +
                        " ADN.MONEDATOTALGASTOFAMILIARMENSUAL, ADN.TOTALGASTOFAMILIARMENSUAL, " +
                        " ADN.MONEDACAPITALNECESARIOFALLECIMIENTO, ADN.CAPITALNECESARIOFALLECIMIENTO, " +
                        " ADN.MONEDAMONTOMENSUALINVERTIR, ADN.MONTOMENSUALINVERTIR" +
                        " FROM TBL_ADN ADN" +
                        " WHERE ADN.IDPROSPECTODISPOSITIVO = '%s' " , idProspectoDispositivo);

        Log.d("CAC", "obtenerProspectoAdnResumen: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                bean = new ProspectoADNBean();
                //bean.setIdProspecto(idProspecto);
                bean.setIdProspectoDispositivo(idProspectoDispositivo);
                bean.setTipoCambio(cursor.getInt(0));
                bean.setIdMonedaEfectivoYAhorros(cursor.getInt(1));
                bean.setEfectivoYAhorros(cursor.getInt(2));
                bean.setIdMonedaIngresosMensuales(cursor.getInt(3));
                bean.setIngresosMensuales(cursor.getInt(4));
                bean.setIdMonedaGastosMensuales(cursor.getInt(5));
                bean.setGastosMensuales(cursor.getInt(6));
                bean.setIdMonedaDescobertura(cursor.getInt(7));
                bean.setDescobertura(cursor.getInt(8));
                bean.setIdMonedaCompromisoPago(cursor.getInt(9));
                bean.setCompromisoPago(cursor.getInt(10));
               //imprimirTusContactos(bean);
            } while (cursor.moveToNext());
        }
        return bean;
    }
}