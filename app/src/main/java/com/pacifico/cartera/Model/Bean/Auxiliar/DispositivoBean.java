package com.pacifico.cartera.Model.Bean.Auxiliar;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.cartera.Persistence.DatabaseConstants;
import com.pacifico.cartera.Persistence.OrganizateEntityFactory;

/**
 * Created by xcode on 25/08/2016.
 */
public class DispositivoBean extends Entity {

    private int numeroSerie;
    private int idIntermedario;



    public int getIdIntermedario() {
        return idIntermedario;
    }

    public void setIdIntermedario(int idIntermedario) {
        this.idIntermedario = idIntermedario;
    }

    public int getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(int numeroSerie) {
        this.numeroSerie = numeroSerie;
    }


    @Override
    public Object getColumnValue(int column) {
        switch (column) {
            case CO_NUMEROSERIE:
                return numeroSerie;
            case CO_IDINTERMEDIARIO:
                return idIntermedario;
        }
        return null;
    }
    @Override
    public void setColumnValue(int column, Object object) {
        switch(column){
            case CO_NUMEROSERIE:
                numeroSerie = (int) object;
                break;
            case CO_IDINTERMEDIARIO:
                idIntermedario = (int) object;
                break;

        }
    }
    public static final String CN_NUMEROSERIE = "NUMEROSERIE";
    public static final String CT_NUMEROSERIE = TableHelper.INT_DATATYPE_NAME;
    public static final int CO_NUMEROSERIE = 1;

    public static final String CN_IDINTERMEDIARIO = "IDINTERMEDIARIO";
    public static final String CT_IDINTERMEDIARIO = TableHelper.VARCHAR_DATATYPE_NAME;
    public static final int CO_IDINTERMEDIARIO = 2;

    public static final TableHelper tableHelper = new TableHelper(DatabaseConstants.TBL_INTERMEDIARIO, new OrganizateEntityFactory())
            .addColumn(CN_NUMEROSERIE, CT_NUMEROSERIE, CO_NUMEROSERIE)
            .addColumn(CN_IDINTERMEDIARIO, CT_IDINTERMEDIARIO, CO_IDINTERMEDIARIO);

}
