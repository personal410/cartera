package com.pacifico.cartera.Model.Controller;

import android.database.Cursor;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Persistence.DatabaseConstants;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;

/**
 * Created by Joel on 13/06/2016.
 */
public class FamiliarController{
    public static void guardarFamiliar(FamiliarBean familiarBean){
        FamiliarBean.tableHelper.insertEntity(familiarBean);
    }
    public static void guardarListaFamiliar(ArrayList<FamiliarBean> listaFamiliarBean){
        if(listaFamiliarBean != null){
            for(FamiliarBean familiarBean : listaFamiliarBean){
                if(familiarBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(familiarBean.getIdProspecto());
                    familiarBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                FamiliarBean.tableHelper.insertEntity(familiarBean);
            }
        }
    }
    public static int obtenerMaximoIdFamiliarDispositivo(){
        int idFamiliarDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDFAMILIARDISPOSITIVO) FROM " + DatabaseConstants.TBL_FAMILIAR;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idFamiliarDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idFamiliarDispositivoMax;
    }

    public static ArrayList<FamiliarBean> obtenerFamiliarPorIdProspectoPorTipoFamiliar(int idProspectoDispositivo, int codigoTipoFamiliar){
        String[] parametros = {Integer.toString(idProspectoDispositivo), Integer.toString(codigoTipoFamiliar)};
        String sentencia = "IDPROSPECTODISPOSITIVO = ? AND CODIGOTIPOFAMILIAR = ? AND FLAGACTIVO = 1";
        parametros[1] = Integer.toString(codigoTipoFamiliar);
        ArrayList<Entity> arrFamiliares = FamiliarBean.tableHelper.getEntities(sentencia, parametros);
        ArrayList<FamiliarBean> arrFamiliaresFinal = new ArrayList<>();
        if(arrFamiliares != null){
            for(Entity familiarBean : arrFamiliares){
                arrFamiliaresFinal.add((FamiliarBean) familiarBean);
            }
        }
        return arrFamiliaresFinal;
    }
    public static void actualizarFamiliar(FamiliarBean familiarBean){
        String[] parameters = new String[]{Integer.toString(familiarBean.getIdFamiliarDispositivo())};
        FamiliarBean.tableHelper.updateEntity(familiarBean, "IDFAMILIARDISPOSITIVO = ? ", parameters);
    }
    public static ArrayList<FamiliarBean> obtenerFamiliaresSinEviar(){
        ArrayList<Entity> arrFamiliares = FamiliarBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<FamiliarBean> arrFamiliaresFinal = new ArrayList<>();
        for(Entity entity : arrFamiliares){
            arrFamiliaresFinal.add((FamiliarBean)entity);
        }
        return arrFamiliaresFinal;
    }
    public static FamiliarBean obtenerFamiliarPorIdFamiliar(int idFamiliarDispositivo){
        String[] parametros = {Integer.toString(idFamiliarDispositivo)};
        ArrayList<Entity> arrFamiliares = FamiliarBean.tableHelper.getEntities("IDFAMILIARDISPOSITIVO = ?", parametros);
        return (FamiliarBean)arrFamiliares.get(0);
    }
}