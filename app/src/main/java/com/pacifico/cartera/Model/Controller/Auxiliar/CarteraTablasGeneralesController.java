package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.util.Log;

import com.dsbmobile.dsbframework.controller.persistence.Entity;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class CarteraTablasGeneralesController {

    public Map<Integer, TablaTablasBean> obtenerMapaTablas(String idTabla) {

        Log.d("TGC", " obtenerMapaTablas idTabla: " + idTabla);
        Log.d("TGC", " ");

        Map<Integer, TablaTablasBean> mapTablaTablas = new HashMap<Integer, TablaTablasBean>();

        Iterator<Entity> beans = obtenerTablaTablas(idTabla);

        TablaTablasBean bean = null;
        while (beans.hasNext()) {
            bean = (TablaTablasBean) beans.next();
            mapTablaTablas.put(bean.getCodigoCampo(), bean);
           // imprimirTablaTablas(bean);
        }

        return mapTablaTablas;
    }

    public List<TablaTablasBean> obtenerTablasAsList(String idTabla) {

        List<TablaTablasBean> listTablaTablas = new ArrayList<>();

        Iterator<Entity> beans = obtenerTablaTablas(idTabla);
        TablaTablasBean bean = null;
        while (beans.hasNext()) {
            bean = (TablaTablasBean) beans.next();
            listTablaTablas.add(bean);
        }

        return listTablaTablas;
    }

    public List<String> obtenerTablasAsListValues(String idTabla) {

        List<String> listValores = new ArrayList<>();

        Iterator<Entity> beans = obtenerTablaTablas(idTabla);
        TablaTablasBean bean = null;
        while (beans.hasNext()) {
            bean = (TablaTablasBean) beans.next();
            listValores.add(bean.getValorCadena());
        }

        return listValores;
    }

    public TablaTablasBean obtenerTablaByCodigoCampo(Map<Integer, TablaTablasBean> mapTablaTablas, Integer codigoCampo) {
        if (mapTablaTablas == null) {
            return null;
        }
        return mapTablaTablas.get(codigoCampo);
    }

    public String obtenerTablaValorCadenaByCodigoCampo(Map<Integer, TablaTablasBean> mapTablaTablas, Integer codigoCampo) {

        //Log.d("TGC", "obtenerTablaTablasValor codigoCampo: " + codigoCampo);
        //Log.d("TGC", " ");

        if (mapTablaTablas == null) {
            return "";
        }
        TablaTablasBean tablaTablasBean = obtenerTablaByCodigoCampo(mapTablaTablas, codigoCampo);
        //imprimirTablaTablas(tablaTablasBean);

        //Log.d("TGC", "obtenerTablaTablasValor codigoCampo: " + codigoCampo + " valorCadena: " +  tablaTablasBean.getValorCadena());

        if(tablaTablasBean==null){
            return "";
        }
        return tablaTablasBean.getValorCadena();
    }

    public Iterator<Entity> obtenerTablaTablas(String idTabla) {

        Log.d("TGC", "obtenerTablaTablasValor idTabla: " + idTabla);
        Log.d("TGC", " ");

        TablaTablasBean bean = null;

        Iterator<Entity> beans;

        beans = TablaTablasBean.tableHelper.getEntities(
                TablaTablasBean.CN_IDTABLA + " = ?  ",
                new String[]{idTabla}).iterator();

        return beans;
    }

    public Iterator<Entity> obtenerTablaTablas() {

        Log.d("TGC", "obtenerTablaTablas");
        Log.d("TGC", " ");

        TablaTablasBean bean = null;

        Iterator<Entity> beans;

        beans = TablaTablasBean.tableHelper.getEntities(
                TablaTablasBean.CN_IDTABLA, null
        ).iterator();

        while (beans.hasNext()) {
            bean = (TablaTablasBean) beans.next();
            imprimirTablaTablas(bean);
        }

        return beans;
    }

    public Iterator<Entity> obtenerTablaTablasBean(String idTabla, String codigoCampo) {

        Log.d("PC", "obtenerTablaTablasValor idTabla: " + idTabla + " codigoCampo: " + codigoCampo);
        Log.d("PC", " ");

        TablaTablasBean bean = null;

        Iterator<Entity> beans;

        beans = TablaTablasBean.tableHelper.getEntities(
                TablaTablasBean.CN_IDTABLA + " = ?  AND " +
                        TablaTablasBean.CN_CODIGOCAMPO + " = ? ",
                new String[]{idTabla, codigoCampo}).iterator();

        /*
        while (beans.hasNext()) {
            bean = (TablaTablasBean) beans.next();
        }*/

        return beans;
    }

    public static void imprimirTablaTablas(TablaTablasBean tablaTablasBean) {
        StringBuffer sb = new StringBuffer();
        sb.append(" it: ").append(tablaTablasBean.getIdTabla());
        sb.append(" cc: ").append(tablaTablasBean.getCodigoCampo());
        sb.append(" vc: ").append(tablaTablasBean.getValorCadena());
        sb.append(" vn: ").append(tablaTablasBean.getValorNumerico());
        sb.append(" fa: ").append(tablaTablasBean.getFlagActivo());
        Log.d("TGC", "TablaTablasBean: " + sb.toString());
    }



}