package com.pacifico.cartera.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.IntermediarioBean;

import java.util.ArrayList;

/**
 * Created by vctrls3477 on 6/07/16.
 */
public class IntermediarioController{
    public static void guardarIntermediario(IntermediarioBean intermediarioBean){
        IntermediarioBean intermediario = obtenerIntermediario();
        if(intermediario == null){
            IntermediarioBean.tableHelper.insertEntity(intermediarioBean);
        }else{
            IntermediarioBean.tableHelper.updateEntity(intermediarioBean, "", null);
        }
    }
    public static IntermediarioBean obtenerIntermediario(){
        ArrayList<Entity> arrIntermediarios = IntermediarioBean.tableHelper.getEntities("", null);
        if(arrIntermediarios.size() > 0){
            return (IntermediarioBean)arrIntermediarios.get(0);
        }else{
            return null;
        }
    }
}