package com.pacifico.cartera.Model.Bean.Auxiliar;

/**
 * Created by dsb on 29/06/2016.
 */
public class EstiloGraficoCircular {
    private int[] colores;
    private int[] colores_texto;
    private float[] tamnoTexto;
    private int[] typeFace;

    public EstiloGraficoCircular(int[] colores, int[] colores_texto, float[] tamnoTexto, int[] typeFace){
        this.colores = colores;
        this.colores_texto = colores_texto;
        this.tamnoTexto = tamnoTexto;
        this.typeFace = typeFace;
    }


    public int[] getColores() {
        return colores;
    }

    public void setColores(int[] colores) {
        this.colores = colores;
    }

    public int[] getColores_texto() {
        return colores_texto;
    }

    public void setColores_texto(int[] colores_texto) {
        this.colores_texto = colores_texto;
    }

    public float[] getTamnoTexto() {
        return tamnoTexto;
    }

    public void setTamnoTexto(float[] tamnoTexto) {
        this.tamnoTexto = tamnoTexto;
    }

    public int[] getTypeFace() {
        return typeFace;
    }

    public void setTypeFace(int[] typeFace) {
        this.typeFace = typeFace;
    }
}
