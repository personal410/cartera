package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.FamiliarCarteraBean;
import com.pacifico.cartera.utils.DateUtils;

/**
 * Created by Joel on 13/06/2016.
 */

public class CarteraFamiliarController {

    public ArrayList<FamiliarCarteraBean> obtenerProspectoFamiliar(int idProspectoDispositivo) {

        ArrayList<FamiliarCarteraBean> familiaLista = new ArrayList<>();
        FamiliarCarteraBean bean = new FamiliarCarteraBean();

        String sql = String.format(
                " SELECT DISTINCT IDFAMILIAR, NOMBRES,APELLIDOPATERNO,APELLIDOMATERNO,FECHANACIMIENTO, EDAD, OCUPACION, CODIGOTIPOFAMILIAR,FLAGACTIVO " +
                        " FROM TBL_FAMILIAR" +
                        " WHERE idprospectodispositivo = '%s' " +
                        " ORDER BY FECHANACIMIENTO", idProspectoDispositivo);

        Log.d("CAC", "obtenerProspectoFamiliar: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                bean = new FamiliarCarteraBean();
                bean.setIdFamiliar(cursor.getInt(0));
                bean.setNombres(cursor.getString(1));
                bean.setApellidoPaterno(cursor.getString(2));
                bean.setApellidoMaterno(cursor.getString(3));
                bean.setFechaNacimientoDateTime(DateUtils.convertStringToDate(cursor.getString(4)));
                bean.setEdad(cursor.getInt(5));
                bean.setOcupacion(cursor.getString(6));
                bean.setCodigoTipoFamiliar(cursor.getInt(7));
                bean.setFlagActivo(cursor.getInt(8));
                familiaLista.add(bean);

            } while (cursor.moveToNext());
        }

        return familiaLista;
    }

}
