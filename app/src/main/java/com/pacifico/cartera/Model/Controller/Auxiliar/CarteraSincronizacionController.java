package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.content.Context;
import android.util.Log;

import com.pacifico.cartera.Model.Bean.AdnBean;
import com.pacifico.cartera.Model.Bean.CalendarioBean;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.cartera.Model.Bean.DispositivoBean;
import com.pacifico.cartera.Model.Bean.EntidadBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.MensajeSistemaBean;
import com.pacifico.cartera.Model.Bean.ParametroBean;
import com.pacifico.cartera.Model.Bean.ReferidoBean;
import com.pacifico.cartera.Model.Bean.TablaIndiceBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;
import com.pacifico.cartera.Model.Controller.ADNController;
import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.DispositivoController;
import com.pacifico.cartera.Model.Controller.EntidadController;
import com.pacifico.cartera.Model.Controller.FamiliarController;
import com.pacifico.cartera.Model.Controller.ParametroController;
import com.pacifico.cartera.Model.Controller.ProspectoController;
import com.pacifico.cartera.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.cartera.Model.Controller.ReferidoController;
import com.pacifico.cartera.Model.Controller.TablasGeneralesController;
import com.pacifico.cartera.Network.Response.GetData.GetDataResponse;
import com.pacifico.cartera.Util.DataMapperResponse;

import java.util.ArrayList;

import com.pacifico.cartera.utils.Util;

/**
 * Created by dsb on 05/07/2016.
 */
public class CarteraSincronizacionController {

    public GetDataResponse getStaticData(Context context) {
        GetDataResponse inicialResponse = Util.getObjecToFile(context,
                "data.json", GetDataResponse.class);
        return inicialResponse;
    }

    public void procesarInitialResponse(GetDataResponse dataResponse, String marca, String modelo, String so, String mac, String numeroSerie) {

        Log.d("SC", "procesarInitialResponse");

        ArrayList<ParametroBean> listaParametrosBean = DataMapperResponse.transformListParametro(dataResponse.getParametro());
        ParametroController.guardarListaParametros(listaParametrosBean);
        ArrayList<TablaIndiceBean> listaTablaIndicesBean = DataMapperResponse.transformListTablaIndice(dataResponse.getTablaIndice());
        TablasGeneralesController.guardarListaTablaIndice(listaTablaIndicesBean);
        ArrayList<TablaTablasBean> listaTablaTablasBean = DataMapperResponse.transformListTablaTablas(dataResponse.getTablaTablas());
        TablasGeneralesController.guardarListaTablaTablas(listaTablaTablasBean);
        ArrayList<CalendarioBean> listaCalendarioBean = DataMapperResponse.transformListCalendario(dataResponse.getCalendario());
        TablasGeneralesController.guardarListaCalendario(listaCalendarioBean);
        ArrayList<MensajeSistemaBean> listaMensajeSistemaBean = DataMapperResponse.transformListMensajeSistema(dataResponse.getMensajeSistema());
        TablasGeneralesController.guardarListaMensajesSistemas(listaMensajeSistemaBean);
        ArrayList<EntidadBean> listaEntidadBean = DataMapperResponse.transformListEntidad(dataResponse.getEntidad());
        EntidadController.guardaListaEntidad(listaEntidadBean);
        //IntermediarioResponse intermediarioResponse = dataResponse.getConsolidadoIntermediario().get(0);
        //IntermediarioBean intermediarioBean = DataMapperResponse.transform(intermediarioResponse);
        //IntermediarioController.guardarIntermediario(intermediarioBean);
//        ArrayList<ReunionInternaBean> listaReunionInternaBean = DataMapperResponse.transformListReunion(dataResponse.getReunion());
//        CitaReunionController.guardarListaReuniones(listaReunionInternaBean);
        ArrayList<ProspectoBean> listaProspectosBean = DataMapperResponse.transformListProspecto(dataResponse.getProspecto());
        ProspectoController.guardarListaProspectos(listaProspectosBean);
        ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean = DataMapperResponse.transformListProspectoMovimientoEtapa(dataResponse.getProspectoMovimientoEtapa());
        ProspectoMovimientoEtapaController.guardarListaProspectoMovimientoEtapa(listaProspectoMovimientoEtapaBean);
        ArrayList<AdnBean> listaAdnsBean = DataMapperResponse.transformListAdn(dataResponse.getADN());
        ADNController.guardarListaADNs(listaAdnsBean);
        ArrayList<FamiliarBean> listaFamiliaresBean = DataMapperResponse.transformListFamiliar(dataResponse.getFamiliar());
        FamiliarController.guardarListaFamiliar(listaFamiliaresBean);
        ArrayList<CitaBean> listaCitaBean = DataMapperResponse.transformListCita(dataResponse.getCita());
        CitaReunionController.guardarListaCitas(listaCitaBean);
        ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean = DataMapperResponse.transformListCitaMovimientoEstado(dataResponse.getCitaMovimientoEstado());
        CitaReunionController.guardarListaCitaMovimientoEstado(listaCitaMovimientoEstadoBean);
        ArrayList<ReferidoBean> listaReferidosBean = DataMapperResponse.transformListReferido(dataResponse.getReferido());
        ReferidoController.guardaListaReferido(listaReferidosBean);
        DispositivoBean dispositivoBean = new DispositivoBean();
        dispositivoBean.setIdDispositivo(1);
        //dispositivoBean.setIdIntermediario(intermediarioBean.getCodigoIntermediario());
        dispositivoBean.setMarca(marca);
        dispositivoBean.setModelo(modelo);
        dispositivoBean.setSistemaOperativo(so);
        dispositivoBean.setMAC(mac);
        dispositivoBean.setNumeroSerie(numeroSerie);
        dispositivoBean.setFechaCreacion(Util.obtenerFechaActual());
        dispositivoBean.setFechaUltimaSincronizacion(Util.obtenerFechaActual());
        dispositivoBean.setFlagNuevaMigracionCartera(0);
        DispositivoController.guardarDispositivo(dispositivoBean);
    }

}
