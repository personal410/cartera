package com.pacifico.cartera.Model.Controller;

import android.database.Cursor;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.cartera.Model.Bean.ReunionInternaBean;
import com.pacifico.cartera.Model.Bean.TablaIdentificadorBean;
import com.pacifico.cartera.Persistence.DatabaseConstants;

import java.util.ArrayList;

//import com.pacifico.cartera.Activity.ADNApplication;
import com.pacifico.cartera.Activity.CarteraApplication;

public class CitaReunionController{

    public static int obtenerMaximoIdRecordatorioLlamadaDispositivo(){
        int idRecordatorioLlamadaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDRECORDATORIOLLAMADADISPOSITIVO) FROM " + DatabaseConstants.TBL_RECORDATORIO_LLAMADA;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idRecordatorioLlamadaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idRecordatorioLlamadaDispositivoMax;
    }

    public static void guardarListaRecordatorioLlamadas(ArrayList<RecordatorioLlamadaBean> listaRecordatorioLLamadaBean)
    {
        if (listaRecordatorioLLamadaBean != null && listaRecordatorioLLamadaBean.size()>0)
            for (RecordatorioLlamadaBean recordatorioLlamadaBean : listaRecordatorioLLamadaBean){
                if(recordatorioLlamadaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(recordatorioLlamadaBean.getIdProspecto());
                    recordatorioLlamadaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                if(recordatorioLlamadaBean.getIdCitaDispositivo() == -1 && recordatorioLlamadaBean.getIdCita() != -1){ // No siempre hay una cita relacionada al recordatorio
                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCita(recordatorioLlamadaBean.getIdCita());
                    recordatorioLlamadaBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                }

                RecordatorioLlamadaBean.tableHelper.insertEntity(recordatorioLlamadaBean);
            }
    }

    public static void guardarReunion(ReunionInternaBean reunionInternaBean) {
        ReunionInternaBean.tableHelper.insertEntity(reunionInternaBean);
    }
    public static void guardarListaReuniones(ArrayList<ReunionInternaBean> listaReunionInternaBean) {
        if(listaReunionInternaBean != null){
            for(ReunionInternaBean reunionInternaBean : listaReunionInternaBean){
                ReunionInternaBean.tableHelper.insertEntity(reunionInternaBean);
            }
        }
    }

    public static void guardarCita(CitaBean citaBean, boolean esNuevo) {
        if(esNuevo){
            TablaIdentificadorBean citaTablaIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_CITA);
            int lastid = citaTablaIdentificadorBean.getIdentity() + 1;
            citaBean.setIdCitaDispositivo(lastid);
            long row = CitaBean.tableHelper.insertEntity(citaBean);
            if(row != -1){
                citaTablaIdentificadorBean.setIdentity(lastid);
                TablaIdentificadorController.actualizarTablaIdentificador(citaTablaIdentificadorBean);
            }
        }else{
            if (citaBean.getIdCita() != -1) {
                String[] parameters = new String[]{Integer.toString(citaBean.getIdCita())};
                CitaBean.tableHelper.updateEntity(citaBean, CitaBean.CN_IDCITA + " = ?", parameters);
            }else{
                String[] parameters = new String[]{Integer.toString(citaBean.getIdCitaDispositivo())};
                CitaBean.tableHelper.updateEntity(citaBean, CitaBean.CN_IDCITADISPOSITIVO + " = ?", parameters);
            }
        }
    }

    public static void guardarCitaMovimientoEstado(CitaMovimientoEstadoBean citaMovimientoEstadoBean) {
        TablaIdentificadorBean citaMovimientoTablaIdentificadorBean = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);
        int lastid = citaMovimientoTablaIdentificadorBean.getIdentity() + 1;
        citaMovimientoEstadoBean.setIdMovimientoDispositivo(lastid);
        long row = CitaMovimientoEstadoBean.tableHelper.insertEntity(citaMovimientoEstadoBean);
        if (row != -1){
            citaMovimientoTablaIdentificadorBean.setIdentity(lastid);
            TablaIdentificadorController.actualizarTablaIdentificador(citaMovimientoTablaIdentificadorBean);
        }
    }

    public static CitaBean obteniendoUltimaCitaProspecto(ProspectoBean prospectoBean){
        String idUsado = CitaBean.CN_IDPROSPECTODISPOSITIVO;
        int valorIdUsado = prospectoBean.getIdProspectoDispositivo();
        if (prospectoBean.getIdProspecto() != -1) {
            idUsado = CitaBean.CN_IDPROSPECTO;
            valorIdUsado = prospectoBean.getIdProspecto();
        }
        String sql = "SELECT * FROM " + DatabaseConstants.TBL_CITA + " WHERE " + idUsado + " = " + valorIdUsado +
                " AND CODIGOESTADO = 2 AND CODIGORESULTADO = 0 ORDER BY " + CitaBean.CN_NUMEROENTREVISTA + " DESC LIMIT 1";
        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);
        CitaBean bean = null;
        if(cursor.moveToFirst()){
            do {
                bean = new CitaBean();
                bean.setIdCita(cursor.getInt(0));
                bean.setIdProspecto(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(2));
                bean.setIdCitaDispositivo(cursor.getInt(3));
                bean.setNumeroEntrevista(cursor.getInt(4));
                bean.setCodigoEstado(cursor.getInt(5));
                bean.setCodigoResultado(cursor.getInt(6));
                bean.setFechaCita(cursor.getString(7));
                bean.setHoraInicio(cursor.getString(8));
                bean.setHoraFin(cursor.getString(9));
                bean.setUbicacion(cursor.getString(10));
                bean.setReferenciaUbicacion(cursor.getString(11));
                bean.setFlagInvitadoGU(cursor.getInt(12));
                bean.setFlagInvitadoGA(cursor.getInt(13));
                bean.setAlertaMinutosAntes(cursor.getInt(14));
                bean.setCantidadVI(cursor.getInt(15));
                bean.setPrimaTargetVI(cursor.getDouble(16));
                bean.setCantidadAP(cursor.getInt(17));
                bean.setPrimaTargetAP(cursor.getDouble(18));
                bean.setCodigoIntermediarioCreacion(cursor.getInt(19));
                bean.setCodigoIntermediarioModificacion(cursor.getInt(20));
                bean.setCodigoEtapaProspecto(cursor.getInt(21));
                bean.setCodigoMotivoReagendado(cursor.getInt(22));
                bean.setFechaCreacionDispositivo(cursor.getString(23));
                bean.setFechaModificacionDispositivo(cursor.getString(24));
                bean.setFlagEnviado(cursor.getInt(25));
            } while(cursor.moveToNext());
        }
        return bean;
    }

    public static CitaBean obtenerCitaPorIdCita(int idCita){
        String[] parametros = {Integer.toString(idCita)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("IDCITA = ?", parametros);
        if(arrCitas.size() > 0){
            return (CitaBean)arrCitas.get(0);
        }else{
            return null;
        }
    }

    public static int obtenerMaximoIdCitaDispositivo(){
        int idCitaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDCITADISPOSITIVO) FROM " + DatabaseConstants.TBL_CITA;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idCitaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idCitaDispositivoMax;
    }
    public static int obtenerMaximoIdReunionInternaDispositivo(){
        int idReunionInternaDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDREUNIONINTERNADISPOSITIVO) FROM " + DatabaseConstants.TBL_REUNION_INTERNA;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idReunionInternaDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idReunionInternaDispositivoMax;
    }


    public static int obtenerMaximoIdCitaMovimientoDispositivo(){
        int idCitaMovimientoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDMOVIMIENTODISPOSITIVO) FROM " + DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idCitaMovimientoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idCitaMovimientoDispositivoMax;
    }

    public static void guardarListaCitas(ArrayList<CitaBean> listaCitaBean) {
        if(listaCitaBean != null){
            for (CitaBean citaBean : listaCitaBean){
                if(citaBean.getIdProspectoDispositivo() == -1){
                    ProspectoBean prospectoBean = ProspectoController.obtenerProspectoPorIdProspecto(citaBean.getIdProspecto());
                    citaBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                }
                CitaBean.tableHelper.insertEntity(citaBean);
            }

        }
    }
    public static void guardarListaCitaMovimientoEstado(ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean) {
        if (listaCitaMovimientoEstadoBean != null && listaCitaMovimientoEstadoBean.size()>0)
            for(CitaMovimientoEstadoBean citaMovimientoEstadoBean : listaCitaMovimientoEstadoBean){
                if(citaMovimientoEstadoBean.getIdCitaDispositivo() == -1){
                    CitaBean citaBean = CitaReunionController.obtenerCitaPorIdCita(citaMovimientoEstadoBean.getIdCita());
                    citaMovimientoEstadoBean.setIdCitaDispositivo(citaBean.getIdCitaDispositivo());
                }
                CitaMovimientoEstadoBean.tableHelper.insertEntity(citaMovimientoEstadoBean);
            }
    }
    public static  ArrayList<ProspectoMovimientoEtapaBean> obtenerProspectoMovimientoEtapa(){
        ArrayList<Entity> arrBeans = ProspectoMovimientoEtapaBean.tableHelper.getEntities("FlagEnviado = 0", null);
        ArrayList<ProspectoMovimientoEtapaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ProspectoMovimientoEtapaBean) entity);
        }
        return arrBeanFinal;
    }
    public static CitaBean ObteniendoUltimaCitaProspecto(ProspectoBean prospectoBean){
        String sql = "";
        String idUsado = CitaBean.CN_IDPROSPECTODISPOSITIVO;
        int valorIdUsado = prospectoBean.getIdProspectoDispositivo();
        if (prospectoBean.getIdProspecto() != -1) {
            idUsado = CitaBean.CN_IDPROSPECTO;
            valorIdUsado = prospectoBean.getIdProspecto();
        }
        sql = String.format(
                "select *" +
                        " from " + DatabaseConstants.TBL_CITA +
                        " where" +
                        " " + idUsado + " = %s" +
                        " order by " + CitaBean.CN_NUMEROENTREVISTA + " desc" +
                        " LIMIT 1",
                valorIdUsado//,
        );
        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        CitaBean bean= null;
        if (cursor.moveToFirst()) {
            do {
                bean = new CitaBean();
                bean.setIdCita(cursor.getInt(0));
                bean.setIdProspecto(cursor.getInt(1));
                bean.setIdProspectoDispositivo(cursor.getInt(2));
                bean.setIdCitaDispositivo(cursor.getInt(3));
                bean.setNumeroEntrevista(cursor.getInt(4));
                bean.setCodigoEstado(cursor.getInt(5));
                bean.setCodigoResultado(cursor.getInt(6));
                bean.setFechaCita(cursor.getString(7));
                bean.setHoraInicio(cursor.getString(8));
                bean.setHoraFin(cursor.getString(9));
                bean.setUbicacion(cursor.getString(10));
                bean.setReferenciaUbicacion(cursor.getString(11));
                bean.setFlagInvitadoGU(cursor.getInt(12));
                bean.setFlagInvitadoGA(cursor.getInt(13));
                bean.setAlertaMinutosAntes(cursor.getInt(14));
                bean.setCodigoIntermediarioCreacion(cursor.getInt(15));
                bean.setCodigoIntermediarioModificacion(cursor.getInt(16));
                bean.setCodigoEtapaProspecto(cursor.getInt(17));
                bean.setFechaCreacionDispositivo(cursor.getString(18));
                bean.setFechaModificacionDispositivo(cursor.getString(19));
                bean.setFlagEnviado(cursor.getInt(20));
            } while(cursor.moveToNext());
        }
        return bean;
    }
    public static ArrayList<CitaBean> obtenerCitasSinEnviar() {
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<CitaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaBean)entity);
        }
        return arrBeanFinal;
    }
    public static  ArrayList<ReunionInternaBean> obtenerReunionInternasSinEnviar(){
        ArrayList<Entity> arrBeans = ReunionInternaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ReunionInternaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((ReunionInternaBean)entity);
        }
        return arrBeanFinal;
    }
    public static  ArrayList<CitaMovimientoEstadoBean> obtenerCitasMovimientoEstadoSinEnviar() {
        ArrayList<Entity> arrBeans = CitaMovimientoEstadoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<CitaMovimientoEstadoBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((CitaMovimientoEstadoBean) entity);
        }
        return arrBeanFinal;
    }
    public static  ArrayList<RecordatorioLlamadaBean> obtenerRecordatorioLlamadasSinEnviar() {
        ArrayList<Entity> arrBeans = RecordatorioLlamadaBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<RecordatorioLlamadaBean> arrBeanFinal = new ArrayList<>();
        for(Entity entity : arrBeans){
            arrBeanFinal.add((RecordatorioLlamadaBean) entity);
        }
        return arrBeanFinal;
    }
    public static ReunionInternaBean obtenerReunionInterPorIdReunionInternaDispositivo(int idReunionInternaDispositivo){
        ArrayList<Entity> arrReunionInternas = ReunionInternaBean.tableHelper.getEntities("IDREUNIONINTERNADISPOSITIVO = ?", new String[]{Integer.toString(idReunionInternaDispositivo)});
        if(arrReunionInternas.size() > 0){
            return (ReunionInternaBean)arrReunionInternas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarReunionIntera(ReunionInternaBean reunionInternaBean){
        String[] parameters = new String[]{Integer.toString(reunionInternaBean.getIdReunionInternaDispositivo())};
        ReunionInternaBean.tableHelper.updateEntity(reunionInternaBean, "IDREUNIONINTERNADISPOSITIVO = ?", parameters);
    }
    public static CitaBean obtenerCitaPorIdCitaDispositivo(int idCitaDispositivo){
        String[] parametros = {Integer.toString(idCitaDispositivo)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("IDCITADISPOSITIVO = ?", parametros);
        if(arrCitas.size() > 0){
            return (CitaBean)arrCitas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarCita(CitaBean citaBean){
        String[] parametros = {Integer.toString(citaBean.getIdCitaDispositivo())};
        CitaBean.tableHelper.updateEntity(citaBean, "IDCITADISPOSITIVO = ?", parametros);
    }
    public static CitaMovimientoEstadoBean obtenerCitaMovimientoEstadoPorIdMovimientoDispositivo(int idMovimientoDispositivo){
        String[] parametros = {Integer.toString(idMovimientoDispositivo)};
        ArrayList<Entity> arrCitaMovimientoEstados = CitaMovimientoEstadoBean.tableHelper.getEntities("IDMOVIMIENTODISPOSITIVO = ?", parametros);
        if(arrCitaMovimientoEstados.size() > 0){
            return (CitaMovimientoEstadoBean)arrCitaMovimientoEstados.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarCitaMovimientoEstado(CitaMovimientoEstadoBean citaMovimientoEstadoBean){
        String[] parametros = {Integer.toString(citaMovimientoEstadoBean.getIdMovimientoDispositivo())};
        CitaMovimientoEstadoBean.tableHelper.updateEntity(citaMovimientoEstadoBean, "IDMOVIMIENTODISPOSITIVO = ?", parametros);
    }
    public static RecordatorioLlamadaBean obtenerRecordatorioLlamadaPorIdRecordatorioLlamadaDispositivo(int idRecordatorioLlamadaDispositivo){
        String[] parametros = {Integer.toString(idRecordatorioLlamadaDispositivo)};
        ArrayList<Entity> arrRecordatorioLlamadas = RecordatorioLlamadaBean.tableHelper.getEntities("IDRECORDATORIOLLAMADADISPOSITIVO = ?", parametros);
        if(arrRecordatorioLlamadas.size() > 0){
            return (RecordatorioLlamadaBean)arrRecordatorioLlamadas.get(0);
        }else{
            return null;
        }
    }
    public static void actualizarRecordatorioLlamada(RecordatorioLlamadaBean recordatorioLlamadaBean){
        String[] parametros = {Integer.toString(recordatorioLlamadaBean.getIdRecordatorioLlamadaDispositivo())};
        RecordatorioLlamadaBean.tableHelper.updateEntity(recordatorioLlamadaBean, "IDRECORDATORIOLLAMADADISPOSITIVO = ?", parametros);
    }
    public static boolean prospectoTieneCita(int idProspectoDispositivo){
        String[] parametros = {Integer.toString(idProspectoDispositivo)};
        ArrayList<Entity> arrCitas = CitaBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = ?", parametros);
        return arrCitas.size() > 0;
    }
    public static boolean hayAlgunaCita(){
        ArrayList<Entity> arrBeans = CitaBean.tableHelper.getEntities("", null);
        return arrBeans.size() > 0;
    }
}