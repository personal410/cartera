package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.HistorialBean;
import com.pacifico.cartera.utils.DateUtils;

/**
 * Created by dsb on 08/07/2016.
 */
public class CarteraHistorialController {

    public ArrayList<HistorialBean> obtenerProspectoHistorial(int idProspectodispositivo) {

        ArrayList<HistorialBean> historicoLista = new ArrayList<>();
        HistorialBean bean = new HistorialBean();

        /*String sql = String.format(
                " SELECT DISTINCT IDCITA, FECHACITA || ' ' || HORAINICIO AS FECHA, CODIGOETAPAPROSPECTO, " +
                        " CODIGOESTADO, CODIGORESULTADO  FROM TBL_CITA" +
                        " WHERE idprospectodispositivo = %S;", idProspectodispositivo);*/
        String sql = String.format(
                " select \n" +
                        "\tCM.IDCITA,\n" +
                        "\tCM.IDCITADISPOSITIVO,\n" +
                        //"\tCM.FECHAMOVIMIENTOESTADODISPOSITIVO,\n" +
                        "\tC.FECHACITA || ' ' || C.HORAINICIO AS FECHA,\n" +
                        "\tC.CODIGOETAPAPROSPECTO,\n" +
                        "\tCM.CODIGOESTADO,\n" +
                        "\tCM.CODIGORESULTADO\n" +
                        "from tbl_cita_movimiento_estado cm\n" +
                        "inner join \n" +
                        "tbl_cita  c \n" +
                        "on c.IDCITADISPOSITIVO = cm.IDCITADISPOSITIVO\n" +
                        "where IDPROSPECTODISPOSITIVO = '%S'", idProspectodispositivo);

        Log.d("CAC", "obtenerProspectoHistorial: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                bean = new HistorialBean();
                //bean.setIdCita(cursor.getInt(0));
                bean.setIdCitaDispositivo(cursor.getInt(1));
                bean.setFechaEvento(DateUtils.convertStringToDateTime2(cursor.getString(2)));
                bean.setDesFechaEvento(DateUtils.getDateFormat21(bean.getFechaEvento()));
                bean.setCodEtapa(cursor.getInt(3));
                bean.setCodEstado(cursor.getInt(4));
                bean.setCodResultado(cursor.getInt(5));
                historicoLista.add(bean);

                //   Log.d("CAC", "obtenerProspectoHistorial idProspecto: " + idProspecto + " fecEvento: " + bean.getDesFechaEvento());

                //imprimirTusContactos(bean);
            } while (cursor.moveToNext());
        }

        return historicoLista;
    }
}
