package com.pacifico.cartera.Model.Bean.Auxiliar;

/**
 * Created by dsb on 21/04/2016.
 */
public class IndiceDatoBean extends DatoBean {

    private int indice;

    public IndiceDatoBean(int _indice, String _descripcion, Long _valor){
        super(_descripcion, _valor);
        this.indice = _indice;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }
}
