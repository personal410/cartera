package com.pacifico.cartera.Model.Bean.Auxiliar;

import com.pacifico.cartera.Model.Bean.FamiliarBean;

import org.joda.time.DateTime;

/**
 * Created by dsb on 07/07/2016.
 */
public class FamiliarCarteraBean extends FamiliarBean {

    private DateTime fechaNacimientoDateTime;
    private String fechaNacimientoTexto;


    public DateTime getFechaNacimientoDateTime() {
        return fechaNacimientoDateTime;
    }

    public void setFechaNacimientoDateTime(DateTime fechaNacimientoDateTime) {
        this.fechaNacimientoDateTime = fechaNacimientoDateTime;
    }

    public String getFechaNacimientoTexto() {
        return fechaNacimientoTexto;
    }

    public void setFechaNacimientoTexto(String fechaNacimientoTexto) {
        this.fechaNacimientoTexto = fechaNacimientoTexto;
    }

}
