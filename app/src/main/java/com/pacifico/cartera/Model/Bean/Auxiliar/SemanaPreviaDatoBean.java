package com.pacifico.cartera.Model.Bean.Auxiliar;

import java.util.List;

/**
 * Created by dsb on 29/06/2016.
 */
public class SemanaPreviaDatoBean {

    private List<IndiceDatoBean> lstGraficoDatos;
    private String[] spannableTexto;

    public SemanaPreviaDatoBean(List<IndiceDatoBean> lstGraficoDatos, String[] spannableTexto){
        this.setLstGraficoDatos(lstGraficoDatos);
        this.spannableTexto = spannableTexto;
    }

    public String[] getSpannableTexto() {
        return spannableTexto;
    }

    public void setSpannableTexto(String[] spannableTexto) {
        this.spannableTexto = spannableTexto;
    }

    public List<IndiceDatoBean> getLstGraficoDatos() {
        return lstGraficoDatos;
    }

    public void setLstGraficoDatos(List<IndiceDatoBean> lstGraficoDatos) {
        this.lstGraficoDatos = lstGraficoDatos;
    }
}
