package com.pacifico.cartera.Model.Controller.Auxiliar;

import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;
import com.pacifico.cartera.Model.Bean.Auxiliar.ReferidosBean;

public class CarteraReferidoController {

   /* public ArrayList<ReferidosBean> obtenerProspectoReferidos(int idProspecto) {

        ArrayList<ReferidosBean> referidoLista = new ArrayList<>();
        ReferidosBean bean = new ReferidosBean();

        String sql = String.format(
                " SELECT DISTINCT REF.IDREFERIDO, (IFNULL(REF.NOMBRES,'') || ' ' || IFNULL(REF.APELLIDOMATERNO,'') || ' ' || IFNULL(REF.APELLIDOPATERNO,'')) as NOMBRE, " +
                        " TELEFONO, FLAGHIJO, CODIGORANGOINGRESO, CODIGORANGOEDAD, FECHACREACIONDISPOSITIVO  " +
                        " FROM TBL_REFERIDO REF " +
                        " WHERE REF.idprospectodispositivo = '%s' " +
                        " ORDER BY NOMBRE", idProspecto);

        Log.d("CAC", "obtenerProspectoReferidos: " + sql);

        Cursor cursor = CarteraApplication.getDB().rawQuery(sql, null);

        if (cursor.moveToFirst()) {
            do {
                bean = new ReferidosBean();
                bean.setCodProspecto(cursor.getInt(0));
                bean.setNomProspecto(cursor.getString(1));
                bean.setTelefono(cursor.getString(2));
                bean.setFlagTieneHijos(cursor.getString(3));
                bean.setCodIngresos(cursor.getInt(4));
                bean.setCodEdad(cursor.getInt(5));
                bean.setFecContactado(DateUtils.convertStringToDate(cursor.getString(6)));
                referidoLista.add(bean);

                //imprimirTusContactos(bean);
            } while (cursor.moveToNext());
        }

        return referidoLista;
    }*/

    public ArrayList<ReferidosBean> obtenerProspectoReferidos (int idDispositivoProspecto){

        ArrayList<ReferidosBean> referidoLista = new ArrayList<>();

        ReferidosBean beanRefe = new ReferidosBean();

        String sql = String.format(/*"  SELECT DISTINCT \n" +
                        " REF.IDREFERIDODISPOSITIVO, (IFNULL(REF.NOMBRES,'') || ' ' || IFNULL(REF.APELLIDOMATERNO,'') || ' ' || IFNULL(REF.APELLIDOPATERNO,'')) as NOMBRE, \n" +
                        " REF.TELEFONO, REF.FLAGHIJO,REF.CODIGORANGOEDAD, TBL.VALORCADENA AS TIPOEMPLEO, \n" +
                        " 0 AS CONTACTADO\n" +
                        " FROM TBL_REFERIDO REF  LEFT JOIN TBL_TABLA_TABLAS TBL ON REF.CODIGOTIPOEMPLEO = TBL.CODIGOCAMPO AND TBL.IDTABLA =19 WHERE REF.idprospectodispositivo = %s\n" +
                        " AND (REF.TELEFONO is NULL or length(ref.telefono) < 6)\n" +
                        " UNION\n" +
                        "SELECT  PROS.IDPROSPECTODISPOSITIVO,\n" +
                        "(IFNULL(PROS.NOMBRES,'') || ' ' || IFNULL(PROS.APELLIDOMATERNO,'') || ' ' || IFNULL(PROS.APELLIDOPATERNO,''))AS NOMBRE,\n" +
                        "PROS.TELEFONOCELULAR,PROS.FLAGHIJO,PROS.CODIGORANGOEDAD, ' ' AS TIPOEMPLEO , \n" +
                        "CASE WHEN PROS.CODIGOESTADO = 1 THEN 0 ELSE 1 END AS  CONTACTADO FROM TBL_PROSPECTO PROS \n" +
                        "WHERE PROS.IDREFERENCIADORDISPOSITIVO = %s\n" +
                        "ORDER BY NOMBRE"*/
                        " SELECT\n" +
                                "\tRE.IDREFERIDODISPOSITIVO, \n" +
                                "\tCASE WHEN PROS.IDPROSPECTODISPOSITIVO IS NULL THEN  (IFNULL(RE.NOMBRES,'') || ' ' || IFNULL(RE.APELLIDOMATERNO,'') || ' ' || IFNULL(RE.APELLIDOPATERNO,'')) \n" +
                                "\t\t\tELSE (IFNULL(PROS.NOMBRES,'') || ' ' || IFNULL(PROS.APELLIDOMATERNO,'') || ' ' || IFNULL(PROS.APELLIDOPATERNO,'')) END AS NOMBRE,\n" +
                                "\tCASE WHEN PROS.IDPROSPECTODISPOSITIVO IS NULL THEN   RE.TELEFONO\n" +
                                "\t\t\tELSE PROS.TELEFONOCELULAR END AS TELEFONO,\t\t\t\n" +
                                "\tCASE WHEN PROS.IDPROSPECTODISPOSITIVO IS NULL THEN   RE.FLAGHIJO\n" +
                                "\t\t\tELSE PROS.FLAGHIJO END AS FLAGHIJO,\t\t\t\n" +
                                "\tCASE WHEN PROS.IDPROSPECTODISPOSITIVO IS NULL THEN   RE.CODIGORANGOEDAD\n" +
                                "\t\t\tELSE PROS.CODIGORANGOEDAD END AS CODIGORANGOEDAD,\n" +
                                "\tRE.CODIGOTIPOEMPLEO,\n" +
                                "\tCASE WHEN PROS.IDPROSPECTODISPOSITIVO IS NULL OR PROS.CODIGOESTADO = 1 THEN 0 ELSE 1 END AS  CONTACTADO\n" +
                                "FROM\n" +
                                "\tTBL_REFERIDO RE LEFT JOIN TBL_PROSPECTO PROS\n" +
                                "ON RE.IDREFERIDODISPOSITIVO = PROS.ADICIONALNUMERICO1\n" +
                                " WHERE\n" +
                                "\tRE.idprospectodispositivo = %S\n" +
                                "ORDER BY NOMBRE",
                                idDispositivoProspecto);
        Log.d("CAC", "obtenerProspectoReferidos: " + sql);
        Cursor cursor = CarteraApplication.getDB().rawQuery(sql,null);
        if(cursor.moveToFirst()){

            do{
                beanRefe = new ReferidosBean();
                beanRefe.setCodProspecto(cursor.getInt(0));
                beanRefe.setNomProspecto(cursor.getString(1));
                beanRefe.setTelefono(cursor.getString(2));
                beanRefe.setFlagTieneHijos(cursor.getString(3));
                beanRefe.setCodEdad(cursor.getInt(4));
                beanRefe.setTipoEmpleo(cursor.getInt(5));
                beanRefe.setCodContactado(cursor.getInt(6));
                referidoLista.add(beanRefe);
            }while(cursor.moveToNext());
        }

        return referidoLista;
    }




}
