package com.pacifico.cartera.Model.Controller;

import android.database.Cursor;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Persistence.DatabaseConstants;

import java.util.ArrayList;

import com.pacifico.cartera.Activity.CarteraApplication;


/**
 * Created by vctrls3477 on 6/06/16.
 */
public class ProspectoController{
    public static void actualizarProspecto(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspectoDispositivo())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTODISPOSITIVO = ?", parameters);
    }
    public static void guardarProspecto(ProspectoBean prospectoBean){
        ProspectoBean.tableHelper.insertEntity(prospectoBean);
    }

    public static void actualizarProspectoSinProspectoDispositivo(ProspectoBean prospectoBean){
        String[] parameters = new String[]{Integer.toString(prospectoBean.getIdProspecto())};
        ProspectoBean.tableHelper.updateEntity(prospectoBean, "IDPROSPECTO = ?", parameters);
    }

    public static void guardarListaProspectos(ArrayList<ProspectoBean> listaProspectoBean) {

        ArrayList<ProspectoBean> prospectosConReferente = new ArrayList<ProspectoBean>();

        if(listaProspectoBean != null){
            for(ProspectoBean prospectoBean : listaProspectoBean){
                if (prospectoBean.getIdReferenciador() != -1)
                    prospectosConReferente.add(prospectoBean);

                guardarProspecto(prospectoBean);
            }
        }

        // ACTUALIZAR LOS ID REFERIDOS (se inserta aparte por que es la misma entidad)
        for(ProspectoBean prospectoBean : prospectosConReferente){
            ProspectoBean prospectoReferente = obtenerProspectoPorIdProspecto(prospectoBean.getIdReferenciador());
            prospectoBean.setIdReferenciadorDispositivo(prospectoReferente.getIdProspectoDispositivo());

            actualizarProspecto(prospectoBean);
        }
    }

    public static ProspectoBean obtenerProspectoPorIdProspecto(int idProspecto){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTO = ?", new String[]{Integer.toString(idProspecto)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }

    public static int obtenerMaximoIdProspectoDispositivo(){
        int idProspectoDispositivoMax = 0;
        String sentencia = "SELECT MAX(IDPROSPECTODISPOSITIVO) FROM " + DatabaseConstants.TBL_PROSPECTO;
        Cursor cursor = CarteraApplication.getDB().rawQuery(sentencia, null);
        if(cursor.moveToFirst()){
            idProspectoDispositivoMax = cursor.getInt(0);
        }
        cursor.close();
        return idProspectoDispositivoMax;
    }

    public static ArrayList<ProspectoBean> obtenerProspectoSinProspectoDispositivo(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = -1", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }

    public static ArrayList<ProspectoBean> getProspectsBySearchText(String searchText){
        ArrayList<Entity> arrContactoProspectos = ProspectoBean.tableHelper.getEntities("", new String[]{});
        ArrayList<ProspectoBean> arrProspectoBean = new ArrayList<>();
        for(Entity entity: arrContactoProspectos){
            ProspectoBean prospectoBean = (ProspectoBean)entity;
            if(searchText.length() == 0){
                arrProspectoBean.add(prospectoBean);
            }else{
                String nombreCompleto = prospectoBean.getNombreCompleto();
                if(nombreCompleto.toLowerCase().contains(searchText)){
                    arrProspectoBean.add(prospectoBean);
                }
            }
        }
        return arrProspectoBean;
    }
    public static ArrayList<ProspectoBean> obtenerProspectosSinEviar(){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("FlagEnviado <> 2", null);
        ArrayList<ProspectoBean> arrProspectosFinal = new ArrayList<>();
        for(Entity entity : arrProspectos){
            arrProspectosFinal.add((ProspectoBean)entity);
        }
        return arrProspectosFinal;
    }

    //Se cambio a dispositivo -
    /*public static ProspectoBean obtenerProspectoPorIdProspecto(int idProspecto){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTO = ?", new String[]{Integer.toString(idProspecto)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }*/

    public static ProspectoBean obtenerProspectoPorIdProspectoDispositivo(int idProspectoDispositivo){
        ArrayList<Entity> arrProspectos = ProspectoBean.tableHelper.getEntities("IDPROSPECTODISPOSITIVO = ?", new String[]{Integer.toString(idProspectoDispositivo)});
        if(arrProspectos.size() > 0){
            return (ProspectoBean)arrProspectos.get(0);
        }else{
            return null;
        }
    }
}