package com.pacifico.cartera.Model.Bean.Auxiliar;

import com.pacifico.cartera.Model.Bean.CalendarioBean;

import org.joda.time.DateTime;

import com.pacifico.cartera.utils.DateUtils;

/**
 * Created by dsb on 09/06/2016.
 */
public class CalendarioProcesadoBean {

    private int idCalendario;
    private int anio;
    private int semanaAnio;
    private int mesAnio;
    private int semanaMes;
    private DateTime fechaInicioSemana;
    private DateTime fechaFinSemana;
    private int codigoSucursal;
    private int codigoCanal;

    public CalendarioProcesadoBean(CalendarioBean calendarioBean) {
        this.idCalendario = calendarioBean.getIdCalendario();
        this.anio = calendarioBean.getAnio();
        this.semanaAnio = calendarioBean.getSemanaAno();
        this.mesAnio = calendarioBean.getMesAno();
        this.semanaMes = calendarioBean.getSemanaMes();

        if (calendarioBean.getFechaInicioSemana() != null) {
            this.fechaInicioSemana = DateUtils.createDateTimeFromString(calendarioBean.getFechaInicioSemana());
        }
        if (calendarioBean.getFechaFinSemana() != null) {
            this.fechaFinSemana = DateUtils.createDateTimeFromString(calendarioBean.getFechaFinSemana());
        }
        this.codigoSucursal = calendarioBean.getCodigoSucursal();

        this.codigoCanal = calendarioBean.getCodigoCanal();
    }

    public CalendarioProcesadoBean(int idCalendario, int anio, int semanaAnio, int mesAnio, int semanaMes, DateTime fechaInicioSemana, DateTime fechaFinSemana, int codigoSucursal, int codigoCanal) {
        this.idCalendario = idCalendario;
        this.anio = anio;
        this.semanaAnio = semanaAnio;
        this.mesAnio = mesAnio;
        this.semanaMes = semanaMes;
        this.fechaInicioSemana = fechaInicioSemana;
        this.fechaFinSemana = fechaFinSemana;
        this.codigoSucursal = codigoSucursal;
        this.codigoCanal = codigoCanal;
    }


    public int getIdCalendario() {
        return idCalendario;
    }

    public void setIdCalendario(int idCalendario) {
        this.idCalendario = idCalendario;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getSemanaAnio() {
        return semanaAnio;
    }

    public void setSemanaAnio(int semanaAnio) {
        this.semanaAnio = semanaAnio;
    }

    public int getMesAnio() {
        return mesAnio;
    }

    public void setMesAnio(int mesAnio) {
        this.mesAnio = mesAnio;
    }

    public int getSemanaMes() {
        return semanaMes;
    }

    public void setSemanaMes(int semanaMes) {
        this.semanaMes = semanaMes;
    }

    public DateTime getFechaInicioSemana() {
        return fechaInicioSemana;
    }

    public void setFechaInicioSemana(DateTime fechaInicioSemana) {
        this.fechaInicioSemana = fechaInicioSemana;
    }

    public DateTime getFechaFinSemana() {
        return fechaFinSemana;
    }

    public void setFechaFinSemana(DateTime fechaFinSemana) {
        this.fechaFinSemana = fechaFinSemana;
    }

    public int getCodigoSucursal() {
        return codigoSucursal;
    }

    public void setCodigoSucursal(int codigoSucursal) {
        this.codigoSucursal = codigoSucursal;
    }

    public int getCodigoCanal() {
        return codigoCanal;
    }

    public void setCodigoCanal(int codigoCanal) {
        this.codigoCanal = codigoCanal;
    }
}
