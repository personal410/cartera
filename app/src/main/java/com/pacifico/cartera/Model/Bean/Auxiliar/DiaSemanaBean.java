package com.pacifico.cartera.Model.Bean.Auxiliar;

/**
 * Created by dsb on 13/06/2016.
 */
public class DiaSemanaBean {
    private int diaMes;
    private int colorDia;
    private String nombreDiaSemana;

    public DiaSemanaBean(int diaMes, int colorDia, String nombreDiaSemana) {
        this.diaMes = diaMes;
        this.colorDia = colorDia;
        this.nombreDiaSemana = nombreDiaSemana;
    }

    public int getDiaMes() {
        return diaMes;
    }

    public void setDiaMes(int diaMes) {
        this.diaMes = diaMes;
    }

    public int getColorDia() {
        return colorDia;
    }

    public void setColorDia(int colorDia) {
        this.colorDia = colorDia;
    }

    public String getNombreDiaSemana() {
        return nombreDiaSemana;
    }

    public void setNombreDiaSemana(String nombreDiaSemana) {
        this.nombreDiaSemana = nombreDiaSemana;
    }
}
