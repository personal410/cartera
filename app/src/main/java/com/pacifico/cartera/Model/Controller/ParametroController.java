package com.pacifico.cartera.Model.Controller;

import com.dsbmobile.dsbframework.controller.persistence.Entity;
import com.pacifico.cartera.Model.Bean.ParametroBean;

import java.util.ArrayList;

/**
 * Created by victorsalazar on 7/06/16.
 */
public class ParametroController{
    public static void guardarParametro(ParametroBean parametroBean){
        ParametroBean.tableHelper.insertEntity(parametroBean);
    }
    public static void actualizarParametro(ParametroBean parametroBean){
        ParametroBean.tableHelper.updateEntity(parametroBean, "IDPARAMETRO", new String[]{Integer.toString(parametroBean.getIdParametro())});
    }
    public static ParametroBean obtenerParametroBeanPorIdParametro(int idParametro){
        String[] parametros = new String[]{Integer.toString(idParametro)};
        ArrayList<Entity> arrParametros = ParametroBean.tableHelper.getEntities("IDPARAMETRO = ?", parametros);
        if(arrParametros.size() > 0){
            return (ParametroBean)arrParametros.get(0);
        }else{
            return null;
        }
    }
    public static void guardarListaParametros(ArrayList<ParametroBean> listaParametroBean) {
        if(listaParametroBean != null){
            for(int i = 0; i < listaParametroBean.size(); i++){
                ParametroBean parametroBean = listaParametroBean.get(i);
                int idParametro = parametroBean.getIdParametro();
                if(obtenerParametroBeanPorIdParametro(idParametro) == null){
                    guardarParametro(parametroBean);
                }
            }
        }
    }
}