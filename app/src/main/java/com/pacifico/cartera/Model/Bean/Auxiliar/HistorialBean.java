package com.pacifico.cartera.Model.Bean.Auxiliar;

import android.graphics.drawable.Drawable;

import org.joda.time.DateTime;

/**
 * Created by dsb on 15/05/2016.
 */
public class HistorialBean {

    //private int idCita;
    private int idCitaDispositivo;
    private int codEtapa;
    private String desEtapa;
    private Drawable drwEtapa;
    private DateTime fechaEvento;
    private String desFechaEvento;

    private int codEstado;
    private int codResultado;
    private String desEstado;
    private String desEstado2;
    private Drawable drwEstado;


    /*public int getIdCita() {
        return idCita;
    }

    public void setIdCita(int idCita) {
        this.idCita = idCita;
    }*/

    public int getIdCitaDispositivo() {
        return idCitaDispositivo;
    }

    public void setIdCitaDispositivo(int idCitaDispositivo) {
        this.idCitaDispositivo = idCitaDispositivo;
    }

    public int getCodEtapa() {
        return codEtapa;
    }

    public void setCodEtapa(int codEtapa) {
        this.codEtapa = codEtapa;
    }

    public String getDesEtapa() {
        return desEtapa;
    }

    public void setDesEtapa(String desEtapa) {
        this.desEtapa = desEtapa;
    }

    public Drawable getDrwEtapa() {
        return drwEtapa;
    }

    public void setDrwEtapa(Drawable drwEtapa) {
        this.drwEtapa = drwEtapa;
    }

    public DateTime getFechaEvento() {
        return fechaEvento;
    }

    public void setFechaEvento(DateTime fechaEvento) {
        this.fechaEvento = fechaEvento;
    }

    public String getDesFechaEvento() {
        return desFechaEvento;
    }

    public void setDesFechaEvento(String desFechaEvento) {
        this.desFechaEvento = desFechaEvento;
    }

    public int getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(int codEstado) {
        this.codEstado = codEstado;
    }

    public int getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(int codResultado) {
        this.codResultado = codResultado;
    }

    public String getDesEstado() {
        return desEstado;
    }

    public void setDesEstado(String desEstado) {
        this.desEstado = desEstado;
    }

    public String getDesEstado2() {
        return desEstado2;
    }

    public void setDesEstado2(String desEstado2) {
        this.desEstado2 = desEstado2;
    }

    public Drawable getDrwEstado() {
        return drwEstado;
    }

    public void setDrwEstado(Drawable drwEstado) {
        this.drwEstado = drwEstado;
    }
}


