package com.pacifico.cartera.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.pacifico.cartera.Model.Bean.AdnBean;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.ParametroBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.cartera.Model.Bean.ReferidoBean;
import com.pacifico.cartera.Model.Bean.ReunionInternaBean;
import com.pacifico.cartera.Model.Controller.ADNController;
import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.FamiliarController;
import com.pacifico.cartera.Model.Controller.ParametroController;
import com.pacifico.cartera.Model.Controller.ProspectoController;
import com.pacifico.cartera.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.cartera.Model.Controller.ReferidoController;
import com.pacifico.cartera.Network.RespuestaSincronizacionListener;
import com.pacifico.cartera.Network.SincronizacionController;

import com.pacifico.cartera.Fragment.Ajustes.AjustesFragment;
import com.pacifico.cartera.R;
import com.pacifico.cartera.Util.Util;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.vista.TusContactosFragment;
import com.pacifico.cartera.vista.indicadores.TabIndicadoresFragment;
import com.pacifico.cartera.Fragment.prospecto.ProspectoFragment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    TextView tvTitulo;
    private ProgressDialog progressDialog;
    private int idProspectoDispositivo;
    private int idFragment;
    private String etapaResultado;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_acti);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, new TabIndicadoresFragment()).commit();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                if(menuItem.getItemId() == R.id.nav_item_indicadores ){
                    FragmentTransaction xfragmentTransaction = mFragmentManager.beginTransaction();
                    xfragmentTransaction.replace(R.id.containerView, new TabIndicadoresFragment()).commit();
                    tvTitulo.setText("Indicadores de tu gestión");
                    getSupportActionBar().show();
                }
                if (menuItem.getItemId() == R.id.nav_item_contactos ) {
                    FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
                    TusContactosFragment tusContactosFragment = new TusContactosFragment();
                    tusContactosFragment.setmActionBar(getSupportActionBar());
                    tusContactosFragment.setmFragmentManager(mFragmentManager);
                    tusContactosFragment.setmFragmentTransaction(mFragmentTransaction);
                    fragmentTransaction.replace(R.id.containerView, tusContactosFragment).commit();
                    tvTitulo.setText("Tus contactos");
                    getSupportActionBar().show();
                }
                if (menuItem.getItemId() == R.id.nav_item_ajustes ){
                    Ajustes();
                    tvTitulo.setText("Ajustes");
                    idFragment = 5;
                }
                if(menuItem.getItemId() == R.id.nav_item_salir)
                    finishAffinity();
                return false;
            }

        });

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        tvTitulo = (TextView) toolbar.findViewById(R.id.tvTitulo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }
    public void changeFragment(Fragment fragment, int idFragment){
        Log.i("MA", "changeFragment ip: " + " if: " + idFragment + " this.ip: " + this.idProspectoDispositivo);
        this.idFragment = idFragment;
        if(fragment != null && !fragment.isAdded()){
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.containerView, fragment, fragment.getClass().toString());
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            fragmentTransaction.show(fragment);
        }
    }
    @Override
    protected void onResume(){
        super.onResume();
        verificarTiempoSinSincronizar();
    }


    @Override
    public void onBackPressed() {
        if (getIdFragment() == Constantes.fragmentTusContactos) {
            Log.i("MainActivity", "on back pressed no hacer nada");
        } else if (getIdFragment() == Constantes.fragmentEditarProspecto) {
           /* Log.i("MainActivity", "on back pressed FRAGMENT PROSPECTO: idProspecto: " + getIdProspecto());
            ProspectoFragment prospecto = new ProspectoFragment();
            prospecto.setidprospecto(getIdProspecto());
            changeFragment(prospecto, Constantes.fragmentProspecto);*/
        }
        if (getIdFragment() == Constantes.fragmentEditarProspecto) {
            // Se cambio a dispositivo - Log.i("MainActivity", "on back pressed FRAGMENT fragmentEditarProspecto: idProspecto: " + getIdProspecto());
            Log.i("MainActivity", "on back pressed FRAGMENT fragmentEditarProspecto: idProspecto: " + getIdProspectoDispositivo());
            ProspectoFragment prospecto = new ProspectoFragment();
            //Se cambio a dispositivo - prospecto.setidprospecto(getIdProspecto());
            prospecto.setIdProspectoDispositivo(getIdProspectoDispositivo());
            changeFragment(prospecto, Constantes.fragmentProspecto);
        } else if (getIdFragment() == Constantes.fragmentProspecto) {
            //Se cambio a dispositivo - Log.i("MainActivity", "on back pressed FRAGMENT PROSPECTO: idProspecto: " + getIdProspecto());
            Log.i("MainActivity", "on back pressed FRAGMENT PROSPECTO: idProspectoDispositivo: " + getIdProspectoDispositivo());
            changeFragment(new TusContactosFragment(), Constantes.fragmentProspecto);
        } else if (mFragmentManager.getBackStackEntryCount() > 0) {
            Log.i("MainActivity", "popping backstack");

        } else {
            Log.i("MainActivity", "nothing on backstack, calling super");
            //super.onBackPressed();
        }
        if(Constantes.fragmentAjustes == getIdFragment()){
            moveTaskToBack(false);
        }
    }

    public void Ajustes(){
        AjustesFragment ajustesFragment = new AjustesFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.containerView,ajustesFragment).addToBackStack(null).commit();
        getSupportActionBar().show();
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        View view = getCurrentFocus();
        if (view != null && (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) && view instanceof EditText && !view.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            view.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + view.getLeft() - scrcoords[0];
            float y = ev.getRawY() + view.getTop() - scrcoords[1];
            if (x < view.getLeft() || x > view.getRight() || y < view.getTop() || y > view.getBottom()){
                ((InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((this.getWindow().getDecorView().getApplicationWindowToken()), 0);
                view.clearFocus();
            }
        }
        return super.dispatchTouchEvent(ev);
    }
    public void sincro(View view){
        sincronizar();
    }
    private void sincronizar(){
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                }else if(codigo == 1){
                    mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                }else{
                    if(codigo == 4 || codigo == 6){
                        Intent inicioSesionIntent = new Intent(MainActivity.this, InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                        startActivity(inicioSesionIntent);
                    }else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                 }
                }
        });
        int resultado = sincronizacionController.sincronizar(this);
        if(resultado == 0){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
        }else if(resultado == 2){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }

    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(this)
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               /* dialog.cancel();*/
                verificarTiempoSinSincronizar();
            }
        }).show();
    }

    public void verificarTiempoSinSincronizar(){
        double tiempo = Util.expiroTiempoSinSincronizacion();
        if (tiempo > 0){
            int cantObjectosSinEnviar = 0;
            ArrayList<ReunionInternaBean> arrReunionInternas = CitaReunionController.obtenerReunionInternasSinEnviar();
            cantObjectosSinEnviar += arrReunionInternas.size();
            ArrayList<ProspectoBean> arrProspectos = ProspectoController.obtenerProspectosSinEviar();
            cantObjectosSinEnviar += arrProspectos.size();
            ArrayList<ProspectoMovimientoEtapaBean> arrProspectoMovimientoEtapas = ProspectoMovimientoEtapaController.obtenerProspectoMovimientoEtapaSinEnviar();
            cantObjectosSinEnviar += arrProspectoMovimientoEtapas.size();
            final ArrayList<AdnBean> arrAdns = ADNController.obtenerADNSinEviar();
            cantObjectosSinEnviar += arrAdns.size();
            ArrayList<FamiliarBean> arrFamiliares = FamiliarController.obtenerFamiliaresSinEviar();
            cantObjectosSinEnviar += arrFamiliares.size();
            ArrayList<CitaBean> arrCitas = CitaReunionController.obtenerCitasSinEnviar();
            cantObjectosSinEnviar += arrCitas.size();
            ArrayList<CitaMovimientoEstadoBean> arrCitaMovimientoEstados = CitaReunionController.obtenerCitasMovimientoEstadoSinEnviar();
            cantObjectosSinEnviar += arrCitaMovimientoEstados.size();
            ArrayList<ReferidoBean> arrReferidos = ReferidoController.obtenerReferidosSinEnviar();
            cantObjectosSinEnviar += arrReferidos.size();
            ArrayList<RecordatorioLlamadaBean> arrRecordatorioLlamadas = CitaReunionController.obtenerRecordatorioLlamadasSinEnviar();
            cantObjectosSinEnviar += arrRecordatorioLlamadas.size();
            if (cantObjectosSinEnviar > 0){
                ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(16);
                double tiempoSinSincronizacionExtra = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.MILLISECOND, (int)(tiempoSinSincronizacionExtra - tiempo));
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM 'a las' hh:mm a");
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog
                        .setTitle("Alerta")
                        .setMessage("Por favor, realiza la sincronización antes del " + simpleDateFormat.format(calendar.getTime())+". Después de esta hora se bloquearán las aplicaciones.")
                        .setPositiveButton("Sincronizar Ahora", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sincronizar();
                            }
                        }).setCancelable(false);
                if (tiempo < tiempoSinSincronizacionExtra){
                    alertDialog.setNegativeButton("Sincronizar mas tarde", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
                alertDialog.show();
            }
        }
    }


    public int getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(int idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    public int getIdFragment() {
        return idFragment;
    }

    public void setIdFragment(int idFragment) {
        this.idFragment = idFragment;
    }

    public String getEtapaResultado() {
        return etapaResultado;
    }

    public void setEtapaResultado(String etapaResultado) {
        this.etapaResultado = etapaResultado;
    }
}