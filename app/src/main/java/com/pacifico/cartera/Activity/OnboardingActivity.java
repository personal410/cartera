package com.pacifico.cartera.Activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.pacifico.cartera.Model.Bean.AdnBean;
import com.pacifico.cartera.Model.Bean.CalendarioBean;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.cartera.Model.Bean.DispositivoBean;
import com.pacifico.cartera.Model.Bean.EntidadBean;
import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.IntermediarioBean;
import com.pacifico.cartera.Model.Bean.MensajeSistemaBean;
import com.pacifico.cartera.Model.Bean.ParametroBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.cartera.Model.Bean.ReferidoBean;
import com.pacifico.cartera.Model.Bean.ReunionInternaBean;
import com.pacifico.cartera.Model.Bean.TablaIdentificadorBean;
import com.pacifico.cartera.Model.Bean.TablaIndiceBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;
import com.pacifico.cartera.Model.Controller.ADNController;
import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.DispositivoController;
import com.pacifico.cartera.Model.Controller.EntidadController;
import com.pacifico.cartera.Model.Controller.FamiliarController;
import com.pacifico.cartera.Model.Controller.IntermediarioController;
import com.pacifico.cartera.Model.Controller.ParametroController;
import com.pacifico.cartera.Model.Controller.ProspectoController;
import com.pacifico.cartera.Model.Controller.ProspectoMovimientoEtapaController;
import com.pacifico.cartera.Model.Controller.ReferidoController;
import com.pacifico.cartera.Model.Controller.TablaIdentificadorController;
import com.pacifico.cartera.Model.Controller.TablasGeneralesController;
import com.pacifico.cartera.Network.SincronizacionController;
import com.pacifico.cartera.Persistence.DatabaseConstants;
import com.pacifico.cartera.Util.DataMapperResponse;
import com.pacifico.cartera.Util.Util;
import com.pacifico.cartera.utils.Constantes;
import com.scottyab.aescrypt.AESCrypt;

import java.net.NetworkInterface;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.pacifico.cartera.Fragment.IndicadorPaginaFragment;
import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.OnboardingPagerAdapter;
import com.squareup.okhttp.OkHttpClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class OnboardingActivity extends AppCompatActivity {
    private IndicadorPaginaFragment indicadorPaginaFragment;
    private ViewPager pagADN;
    private String login, token, numeroSerie, marca, modelo, so, codigoIntermediario, mac;
    boolean puedeAvanzar = false;
    boolean irPrincipal = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);
        pagADN = (ViewPager)findViewById(R.id.pagContenedor);
        OnboardingPagerAdapter onboardingPagerAdapter = new OnboardingPagerAdapter(getSupportFragmentManager());
        pagADN.setAdapter(onboardingPagerAdapter);
        indicadorPaginaFragment = (IndicadorPaginaFragment)getSupportFragmentManager().findFragmentById(R.id.fragIndicadorPagina);
        pagADN.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){}
            @Override
            public void onPageSelected(int position){
                indicadorPaginaFragment.actualizarIndicadorPagina(position);
            }
            @Override
            public void onPageScrollStateChanged(int state){}
        });
        if(IntermediarioController.obtenerIntermediario() == null){
            Bundle extras = getIntent().getExtras();
            login = extras.getString("login");
            token = extras.getString("token");
            numeroSerie = extras.getString("numeroSerie");
            codigoIntermediario = extras.getString("codigoIntermediario");
            marca = Build.MANUFACTURER;
            modelo = Build.MODEL;
            String version = Build.VERSION.RELEASE;
            so = "Android" + version;
            mac = getWifiMacAddress().replace(":", "");
            obtenerInformacion();
        }else{
            puedeAvanzar = true;
        }
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            irPrincipal = extras.getBoolean("irPrincipal", true);
        }
    }
    public void actualizarPaginaConDelta(int delta){
        int pagina = pagADN.getCurrentItem() + delta;
        pagADN.setCurrentItem(pagina);
    }
    @Override
    public void onBackPressed() {
        if(pagADN.getCurrentItem() == 0){
            super.onBackPressed();
        }else{
            pagADN.setCurrentItem(pagADN.getCurrentItem() - 1);
        }
    }
    public void empezar(View v){
        if(puedeAvanzar){
            if(irPrincipal){
                Intent buscarProspectoIntent = new Intent(this, MainActivity.class);
                startActivity(buscarProspectoIntent);
            }else{
                finish();
            }
        }else{
            Util.mostrarAlertaConTitulo("Alerta", "No ha hecho la sincronizacion", this);
        }
    }
    public void obtenerInformacion(){
        OkHttpClient okHttpClient = new OkHttpClient();
        try{
            SincronizacionController sincronizacionController = new SincronizacionController();
            okHttpClient.setSslSocketFactory(sincronizacionController.getSSLSocketFactory(this));
        }catch(Exception e){}
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Constantes.URLBase).addConverterFactory(GsonConverterFactory.create()).client(okHttpClient).build();
        com.pacifico.cartera.Network.RestMethods restMethods = retrofit.create(com.pacifico.cartera.Network.RestMethods.class);
        Call<com.pacifico.cartera.Network.Response.GetData.GetDataResponse> respuesta = restMethods.GetData(login, codigoIntermediario, token, marca, modelo, so, mac, numeroSerie);
        respuesta.enqueue(new Callback<com.pacifico.cartera.Network.Response.GetData.GetDataResponse>() {
            @Override
            public void onResponse(Response<com.pacifico.cartera.Network.Response.GetData.GetDataResponse> response, Retrofit retrofit){
                Log.i("TAG", "response iniciarSesion");
                com.pacifico.cartera.Network.Response.GetData.GetDataResponse dataResponse = response.body();
                if(dataResponse == null){
                    Util.mostrarAlertaConTitulo("Alerta", "Hubo un error en la conexión, consulte con el administrador", OnboardingActivity.this);
                }else{
                    int codigoResultado = Integer.parseInt(dataResponse.getResultCode());
                    if(codigoResultado == 1){
                        com.pacifico.cartera.Network.Response.Comun.ProcesoSincronizacionResponse procesoSincronizacionResponse = dataResponse.getProcesoSicronizacion();
                        if(procesoSincronizacionResponse.getFlagExito().equals("true")){
                            ArrayList<ParametroBean> listaParametrosBean = DataMapperResponse.transformListParametro(dataResponse.getParametro());
                            ParametroController.guardarListaParametros(listaParametrosBean);
                            ArrayList<TablaIndiceBean> listaTablaIndicesBean = DataMapperResponse.transformListTablaIndice(dataResponse.getTablaIndice());
                            TablasGeneralesController.guardarListaTablaIndice(listaTablaIndicesBean);
                            ArrayList<TablaTablasBean> listaTablaTablasBean = DataMapperResponse.transformListTablaTablas(dataResponse.getTablaTablas());
                            TablasGeneralesController.guardarListaTablaTablas(listaTablaTablasBean);
                            ArrayList<CalendarioBean> listaCalendarioBean = DataMapperResponse.transformListCalendario(dataResponse.getCalendario());
                            TablasGeneralesController.guardarListaCalendario(listaCalendarioBean);
                            ArrayList<MensajeSistemaBean> listaMensajeSistemaBean = DataMapperResponse.transformListMensajeSistema(dataResponse.getMensajeSistema());
                            TablasGeneralesController.guardarListaMensajesSistemas(listaMensajeSistemaBean);
                            ArrayList<EntidadBean> listaEntidadBean = DataMapperResponse.transformListEntidad(dataResponse.getEntidad());
                            EntidadController.guardaListaEntidad(listaEntidadBean);
                            com.pacifico.cartera.Network.Response.Comun.IntermediarioResponse intermediarioResponse = dataResponse.getConsolidadoIntermediario().get(0);
                            IntermediarioBean intermediarioBean = DataMapperResponse.transform(intermediarioResponse);
                            try{
                                intermediarioBean.setLogin(AESCrypt.encrypt(Constantes.SemillaEncriptacion, login));
                                intermediarioBean.setToken(AESCrypt.encrypt(Constantes.SemillaEncriptacion, token));
                            }catch(GeneralSecurityException e){
                                e.printStackTrace();
                            }
                            IntermediarioController.guardarIntermediario(intermediarioBean);
                            ArrayList<ReunionInternaBean> listaReunionInternaBean = DataMapperResponse.transformListReunion(dataResponse.getReunion());
                            CitaReunionController.guardarListaReuniones(listaReunionInternaBean);
                            ArrayList<ProspectoBean> listaProspectosBean = DataMapperResponse.transformListProspecto(dataResponse.getProspecto());
                            ProspectoController.guardarListaProspectos(listaProspectosBean);
                            ArrayList<ProspectoMovimientoEtapaBean> listaProspectoMovimientoEtapaBean = DataMapperResponse.transformListProspectoMovimientoEtapa(dataResponse.getProspectoMovimientoEtapa());
                            ProspectoMovimientoEtapaController.guardarListaProspectoMovimientoEtapa(listaProspectoMovimientoEtapaBean);
                            ArrayList<AdnBean> listaAdnsBean = DataMapperResponse.transformListAdn(dataResponse.getADN());
                            ADNController.guardarListaADNs(listaAdnsBean);
                            ArrayList<FamiliarBean> listaFamiliaresBean = DataMapperResponse.transformListFamiliar(dataResponse.getFamiliar());
                            FamiliarController.guardarListaFamiliar(listaFamiliaresBean);
                            ArrayList<CitaBean> listaCitaBean = DataMapperResponse.transformListCita(dataResponse.getCita());
                            CitaReunionController.guardarListaCitas(listaCitaBean);
                            ArrayList<CitaMovimientoEstadoBean> listaCitaMovimientoEstadoBean = DataMapperResponse.transformListCitaMovimientoEstado(dataResponse.getCitaMovimientoEstado());
                            CitaReunionController.guardarListaCitaMovimientoEstado(listaCitaMovimientoEstadoBean);
                            ArrayList<ReferidoBean> listaReferidosBean = DataMapperResponse.transformListReferido(dataResponse.getReferido());
                            ReferidoController.guardaListaReferido(listaReferidosBean);
                            ArrayList<RecordatorioLlamadaBean> listaRecordatorioLlamadoBean = DataMapperResponse.transformListRecordatorioLlamadaBean(dataResponse.getRecordatorioLlamada());
                            CitaReunionController.guardarListaRecordatorioLlamadas(listaRecordatorioLlamadoBean);
                            DispositivoBean dispositivoBean = new DispositivoBean();
                            dispositivoBean.setIdDispositivo(1);
                            dispositivoBean.setIdIntermediario(intermediarioBean.getCodigoIntermediario());
                            dispositivoBean.setMarca(marca);
                            dispositivoBean.setModelo(modelo);
                            dispositivoBean.setSistemaOperativo(so);
                            dispositivoBean.setMAC(mac);
                            dispositivoBean.setNumeroSerie(numeroSerie);
                            dispositivoBean.setFechaCreacion(Util.obtenerFechaActual());
                            dispositivoBean.setFechaUltimaSincronizacion(procesoSincronizacionResponse.getFechaSincronizacion());
                            dispositivoBean.setFlagNuevaMigracionCartera(0);
                            DispositivoController.guardarDispositivo(dispositivoBean);

                            TablaIdentificadorBean citaIdentificadorBean = new TablaIdentificadorBean();
                            citaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA);
                            citaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdCitaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(citaIdentificadorBean);

                            TablaIdentificadorBean citaMovimientoIdentificadorBean = new TablaIdentificadorBean();
                            citaMovimientoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_CITA_MOVIMIENTO_ESTADO);
                            citaMovimientoIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdCitaMovimientoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(citaMovimientoIdentificadorBean);

                            TablaIdentificadorBean familiarIdentificadorBean = new TablaIdentificadorBean();
                            familiarIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_FAMILIAR);
                            familiarIdentificadorBean.setIdentity(FamiliarController.obtenerMaximoIdFamiliarDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(familiarIdentificadorBean);

                            TablaIdentificadorBean referidoIdentificadorBean = new TablaIdentificadorBean();
                            referidoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REFERIDO);
                            referidoIdentificadorBean.setIdentity(ReferidoController.obtenerMaximoIdReferidoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(referidoIdentificadorBean);

                            TablaIdentificadorBean reunionInternaIdentificadorBean = new TablaIdentificadorBean();
                            reunionInternaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_REUNION_INTERNA);
                            reunionInternaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdReunionInternaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(reunionInternaIdentificadorBean);

                            TablaIdentificadorBean prospectoMovimientoIdentificadorBean = new TablaIdentificadorBean();
                            prospectoMovimientoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO_MOVIMIENTO_ETAPA);
                            prospectoMovimientoIdentificadorBean.setIdentity(ProspectoMovimientoEtapaController.obtenerMaximoIdProspectoMovimientoDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(prospectoMovimientoIdentificadorBean);

                            TablaIdentificadorBean recordatorioLlamadaIdentificadorBean = new TablaIdentificadorBean();
                            recordatorioLlamadaIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_RECORDATORIO_LLAMADA);
                            recordatorioLlamadaIdentificadorBean.setIdentity(CitaReunionController.obtenerMaximoIdRecordatorioLlamadaDispositivo());
                            TablaIdentificadorController.actualizarTablaIdentificador(recordatorioLlamadaIdentificadorBean);

                            ArrayList<ProspectoBean> arrProspectosSinProspectoDispositivo = ProspectoController.obtenerProspectoSinProspectoDispositivo();
                            int idProspectoDispositivo = ProspectoController.obtenerMaximoIdProspectoDispositivo();
                            for(ProspectoBean prospectoBean : arrProspectosSinProspectoDispositivo){
                                idProspectoDispositivo++;
                                prospectoBean.setIdProspectoDispositivo(idProspectoDispositivo);
                                prospectoBean.setFlagEnviado(0);
                                prospectoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                                ProspectoController.actualizarProspectoSinProspectoDispositivo(prospectoBean);
                            }

                            TablaIdentificadorBean prospectoIdentificadorBean = new TablaIdentificadorBean();
                            prospectoIdentificadorBean.setnombreTabla(DatabaseConstants.TBL_PROSPECTO);
                            prospectoIdentificadorBean.setIdentity(idProspectoDispositivo);
                            TablaIdentificadorController.actualizarTablaIdentificador(prospectoIdentificadorBean);
                            puedeAvanzar = true;
                            Log.i("TAG", "sincronizacion completa");
                        }else{
                            Util.mostrarAlertaConTitulo("Alerta", "Hubo un problema en la sincronización", OnboardingActivity.this);
                        }
                    }else{
                        Util.mostrarAlertaConTitulo("Error", dataResponse.getResultMessage(), OnboardingActivity.this);
                    }
                }
            }
            @Override
            public void onFailure(Throwable t){
                AlertDialog.Builder builder = new AlertDialog.Builder(OnboardingActivity.this);
                builder.setTitle("Hubo un error en la conexion. Se reintentara la sincronización");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        obtenerInformacion();
                    }
                });
                builder.create().show();
            }
        });
    }
    public static String getWifiMacAddress(){
        try{
            String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for(NetworkInterface intf : interfaces) {
                if(!intf.getName().equalsIgnoreCase(interfaceName)){
                    continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if(mac == null){
                    return "";
                }
                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length()>0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        }catch(Exception ex){}
        return "";
    }
}