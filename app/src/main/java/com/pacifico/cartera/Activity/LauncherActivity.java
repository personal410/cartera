package com.pacifico.cartera.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.Timer;
import java.util.TimerTask;

import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.IntermediarioController;
import com.pacifico.cartera.R;

public class LauncherActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){
                if(IntermediarioController.obtenerIntermediario() == null){
                    startActivity(new Intent(LauncherActivity.this, InicioSesionActivity.class));
                }else{
                    if(CitaReunionController.hayAlgunaCita()){
                        startActivity(new Intent(LauncherActivity.this, MainActivity.class));
                    }else{
                        startActivity(new Intent(LauncherActivity.this, OnboardingActivity.class));
                    }
                }
            }
        }, 1000);
    }
}