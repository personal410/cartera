package com.pacifico.cartera.Activity;

import android.content.Context;

import com.dsbmobile.dsbframework.DSBApplication;

import com.pacifico.cartera.Model.Controller.AjustesController;
import com.pacifico.cartera.Model.Controller.TablaIdentificadorController;
import com.pacifico.cartera.Persistence.DatabaseConstants;
import com.pacifico.cartera.Persistence.OrganizateDatabaseAdapter;


/**
 * Created by DSBMobile-AAE14-001 on 13/05/2016.
 */
public class CarteraApplication extends DSBApplication{
    @Override
    public void onCreate(){
        super.onCreate();
        Context context_compartido = null;
        try {
            context_compartido = this.createPackageContext(
                    "com.pacifico.agenda",
                    Context.CONTEXT_INCLUDE_CODE);
            if(context_compartido == null){
                return;
            }
        } catch (Exception e) {
            String error = e.getMessage();
            System.out.println("DB error : " + error);
            return;
        }


        OrganizateDatabaseAdapter.open(context_compartido, /* DatabaseConstants.DATABASE_PATH + "/" + DatabaseConstants.DATABASE_DIRECTORY
                        + "/" +*/ DatabaseConstants.DATABASE_NAME,
                DatabaseConstants.DATABASE_VERSION);
        TablaIdentificadorController.inicializarTabla();
        AjustesController.inicializarTabla();
    }
}
