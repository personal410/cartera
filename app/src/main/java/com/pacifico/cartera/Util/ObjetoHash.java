package com.pacifico.cartera.Util;

import com.pacifico.cartera.Network.Request.SetData.ADNRequest;
import com.pacifico.cartera.Network.Request.SetData.CitaMovimientoEstadoRequest;
import com.pacifico.cartera.Network.Request.SetData.CitaRequest;
import com.pacifico.cartera.Network.Request.SetData.FamiliarRequest;
import com.pacifico.cartera.Network.Request.SetData.ProspectoMovimientoEtapaRequest;
import com.pacifico.cartera.Network.Request.SetData.ProspectoRequest;
import com.pacifico.cartera.Network.Request.SetData.RecordatorioLLamadaRequest;
import com.pacifico.cartera.Network.Request.SetData.ReferidoRequest;
import com.pacifico.cartera.Network.Request.SetData.ReunionInternaRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DSB on 01/07/2016.
 */
public class ObjetoHash {
    private List<ReunionInternaRequest> Reunion;
    private List<ProspectoRequest> Prospecto;
    private List<ProspectoMovimientoEtapaRequest> ProspectoMovimientoEtapa;
    private List<ADNRequest> ADN;
    private List<FamiliarRequest> Familiar;
    private List<CitaRequest> Cita;
    private List<CitaMovimientoEstadoRequest> CitaMovimientoEtapa;
    private List<ReferidoRequest> Referido;
    private List<RecordatorioLLamadaRequest> RecordatorioLlamada;
    public ObjetoHash(){
        Reunion = new ArrayList<>();
        Prospecto = new ArrayList<>();
        ProspectoMovimientoEtapa = new ArrayList<>();
        ADN = new ArrayList<>();
        Familiar = new ArrayList<>();
        Cita = new ArrayList<>();
        CitaMovimientoEtapa = new ArrayList<>();
        Referido = new ArrayList<>();
        RecordatorioLlamada = new ArrayList<>();
    }
    public void setReunion(List<ReunionInternaRequest> reunion) {
        Reunion = reunion;
    }
    public void setProspecto(List<ProspectoRequest> prospecto) {
        Prospecto = prospecto;
    }
    public void setProspectoMovimientoEtapa(List<ProspectoMovimientoEtapaRequest> prospectoMovimientoEtapa) {
        ProspectoMovimientoEtapa = prospectoMovimientoEtapa;
    }
    public void setADN(List<ADNRequest> ADN) {
        this.ADN = ADN;
    }
    public void setFamiliar(List<FamiliarRequest> familiar) {
        this.Familiar = familiar;
    }
    public void setCita(List<CitaRequest> cita) {
        this.Cita = cita;
    }
    public void setCitaMovimientoEtapa(List<CitaMovimientoEstadoRequest> citaMovimientoEtapa) {
        this.CitaMovimientoEtapa = citaMovimientoEtapa;
    }
    public void setReferido(List<ReferidoRequest> referido) {
        this.Referido = referido;
    }
    public void setRecordatorioLlamada(List<RecordatorioLLamadaRequest> recordatorioLlamada) {
        this.RecordatorioLlamada = recordatorioLlamada;
    }
}