package com.pacifico.cartera.Util;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.EditText;
import android.widget.TextView;

public  class Fuente {
    public static void setFuenteBd(Context context,TextView txt){
        String font_path = "foco_std_bd_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        txt.setTypeface(TF);
    }
    public static void setFuenteRg(Context context, TextView rb){
        String font_path = "foco_std_rg_webfont.ttf";
        Typeface TF = Typeface.createFromAsset(context.getAssets(), font_path);
        rb.setTypeface(TF);
    }
    public static void setFuenteRgEdit(Context context, EditText rb){
        String font_path = "foco_std_rg_webfont.ttf";
        Typeface tf = Typeface.createFromAsset(context.getAssets(), font_path);
        rb.setTypeface(tf);
    }
    public static Typeface obtenerFuenteRegular(Context context){
        return Typeface.createFromAsset(context.getAssets(), "foco_std_rg_webfont.ttf");
    }

}