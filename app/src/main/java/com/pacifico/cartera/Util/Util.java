package com.pacifico.cartera.Util;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.pacifico.cartera.Model.Bean.DispositivoBean;
import com.pacifico.cartera.Model.Bean.ParametroBean;
import com.pacifico.cartera.Model.Controller.DispositivoController;
import com.pacifico.cartera.Model.Controller.ParametroController;
import com.pacifico.cartera.utils.Constantes;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by joel on 6/27/16.
 */
public class Util{


    public static double expiroTiempoSinSincronizacion(){
        DispositivoBean dispositivoBean = DispositivoController.obtenerDispositivoPorIdDispositivo(1);
        String fechaUltimaSincronizacion = dispositivoBean.getFechaUltimaSincronizacion();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSSS");
        try{
            Date dateUltimaSincronizacion = simpleDateFormat.parse(fechaUltimaSincronizacion);
            Date dateAhora = new Date();
            double diferencia = dateAhora.getTime() - dateUltimaSincronizacion.getTime();
            ParametroBean tiempoSinSincronizacionParametroBean = ParametroController.obtenerParametroBeanPorIdParametro(9);
            double tiempoSinSincronizacion = tiempoSinSincronizacionParametroBean.getValorNumerico() * 60 * 60 * 1000;
            diferencia = diferencia - tiempoSinSincronizacion;
            return diferencia;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.i("TAG", "error");
        }
        return 0;
    }


    public static double redondearNumero(double numero, int decimales){
        int factor = (int)Math.pow(10, decimales);
        numero = numero * factor;
        double numeroRedondeado = Math.round(numero);
        return numeroRedondeado / factor;
    }

    public static String strNULL(Object object) {
        return object == null ? "" : object.toString();
    }

    public static String obtenerFechaActual(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar ahora = Calendar.getInstance();
        return simpleDateFormat.format(ahora.getTime());
    }

    public static void showToast(Context context, String mensaje) {

        Toast.makeText(context, mensaje,
                Toast.LENGTH_SHORT).show();

    }

    public static boolean esVacioONull(Object o)
    {
        if (o == null)
            return true;

        if (o.toString().trim().isEmpty())
            return true;

        return false;
    }

    public static String obtenerNumeroConDecimales(double numero){
        return obtenerDecimalFormat("###########.##").format(numero);
    }

    public static void mostrarAlertaConTitulo(String titulo, String mensaje, Context context){
        new AlertDialog.Builder(context)
                .setTitle(titulo)
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        }).show();
    }

    public static String capitalizedString(String string){

        if(string != null && string.length() > 0){
            String[] arrString = string.split(" ");
            String strFinal = "";
            for(int i = 0; i < arrString.length; i++){
                String tempString = arrString[i].trim();

                if (tempString.length() > 1)
                    strFinal = strFinal + String.format("%s%s ", tempString.substring(0, 1), tempString.substring(1).toLowerCase());
                else if (tempString.length() == 1)
                    strFinal = strFinal + tempString;
            }
            return strFinal.trim();
        }else{
            return "";
        }
    }

    public static String obtenerNumeroConFormato(double numero){
        return obtenerNumeroConFormatoDefinido(numero, "##,###,###");
    }
    public static String obtenerNumeroConFormatoDefinido(double numero, String formato){
        return obtenerDecimalFormat(formato).format(Math.round(numero));
    }
    public static DecimalFormat obtenerDecimalFormat(String formato){
        DecimalFormat formatter = new DecimalFormat(formato);
        DecimalFormatSymbols custom = new DecimalFormatSymbols(new Locale("es", "US"));
        custom.setDecimalSeparator('.');
        custom.setGroupingSeparator(',');
        formatter.setDecimalFormatSymbols(custom);
        return formatter;
    }

    public static int devolverRangoEdad(Calendar calFechaNacimiento)
    {
        if (calFechaNacimiento == null) return -1;

        Calendar calFechaActual = Calendar.getInstance();
        int edad = calFechaActual.get(Calendar.YEAR) - calFechaNacimiento.get(Calendar.YEAR);

        if (calFechaNacimiento.get(Calendar.DAY_OF_YEAR) < calFechaActual.get(Calendar.DAY_OF_YEAR))
            edad = edad -1;

        if (edad < 25)
            return Constantes.RangoEdad_menor25;
        else if (edad >= 25 && edad <= 30)
            return Constantes.RangoEdad_entre25_30;
        else if (edad >= 31 && edad <= 40)
            return Constantes.RangoEdad_entre31_40;
        else if (edad > 40)
            return Constantes.RangoEdad_mayor41;

        return -1;
    }
}