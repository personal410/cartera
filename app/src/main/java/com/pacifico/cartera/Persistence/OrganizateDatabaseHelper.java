package com.pacifico.cartera.Persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.dsbmobile.dsbframework.controller.persistence.DatabaseHelper;
import com.dsbmobile.dsbframework.controller.persistence.TableHelper;
import com.pacifico.cartera.Model.Bean.AdnBean;
import com.pacifico.cartera.Model.Bean.AjustesBean;
import com.pacifico.cartera.Model.Bean.CalendarioBean;
import com.pacifico.cartera.Model.Bean.CitaBean;
import com.pacifico.cartera.Model.Bean.CitaHistoricaBean;
import com.pacifico.cartera.Model.Bean.CitaMovimientoEstadoBean;
import com.pacifico.cartera.Model.Bean.EntidadBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.ProspectoMovimientoEtapaBean;
import com.pacifico.cartera.Model.Bean.DispositivoBean;
import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.IntermediarioBean;
import com.pacifico.cartera.Model.Bean.MensajeSistemaBean;
import com.pacifico.cartera.Model.Bean.ParametroBean;
import com.pacifico.cartera.Model.Bean.RecordatorioLlamadaBean;
import com.pacifico.cartera.Model.Bean.ReferidoBean;
import com.pacifico.cartera.Model.Bean.ReminderBean;
import com.pacifico.cartera.Model.Bean.ReunionInternaBean;
import com.pacifico.cartera.Model.Bean.TablaIdentificadorBean;
import com.pacifico.cartera.Model.Bean.TablaIndiceBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;

public class OrganizateDatabaseHelper extends DatabaseHelper{
	private ArrayList<TableHelper> tableHelpers;
	public OrganizateDatabaseHelper(Context context, String databaseName, int databaseVersion) {
		super(context, databaseName, databaseVersion);
		tableHelpers = new ArrayList<>();
		tableHelpers.add(AdnBean.tableHelper);
		tableHelpers.add(CalendarioBean.tableHelper);
		tableHelpers.add(CitaBean.tableHelper);
		tableHelpers.add(CitaHistoricaBean.tableHelper);
		tableHelpers.add(CitaMovimientoEstadoBean.tableHelper);
		tableHelpers.add(ProspectoBean.tableHelper);
		tableHelpers.add(ProspectoMovimientoEtapaBean.tableHelper);
		tableHelpers.add(DispositivoBean.tableHelper);
		tableHelpers.add(FamiliarBean.tableHelper);
		tableHelpers.add(IntermediarioBean.tableHelper);
		tableHelpers.add(MensajeSistemaBean.tableHelper);
		tableHelpers.add(ParametroBean.tableHelper);
		tableHelpers.add(ReferidoBean.tableHelper);
		tableHelpers.add(ReunionInternaBean.tableHelper);
		tableHelpers.add(TablaIndiceBean.tableHelper);
		tableHelpers.add(TablaTablasBean.tableHelper);
		tableHelpers.add(EntidadBean.tableHelper);
		tableHelpers.add(TablaIdentificadorBean.tableHelper);
		tableHelpers.add(RecordatorioLlamadaBean.tableHelper);
		tableHelpers.add(ReminderBean.tableHelper);
		tableHelpers.add(AjustesBean.tableHelper);
	}
	@Override
	public void executeCreates(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.beginTransaction();
		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getCreateSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
	@Override
	public void executeDrops(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.beginTransaction();
		for (TableHelper tableHelper : tableHelpers) {
			db.execSQL(tableHelper.getDropSentence());
		}
		db.setTransactionSuccessful();
		db.endTransaction();
	}
}
