package com.pacifico.cartera.Fragment.prospecto;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.ReferidosAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraReferidoController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.ReferidosBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;
import com.pacifico.cartera.utils.StringUtils;

/**
 * Created by dsb on 14/05/2016.
 */
public class ProspectoReferidosFragment extends Fragment {

    //Se cambio a dispositivo - private Integer idprospecto;
    private Integer idProspectoDispositivo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ppct_refe_frgm, container, false);

        Context ctx = getActivity().getApplicationContext();

        ReferidosAdapter adapter = new ReferidosAdapter(getActivity(), llenarListaReferidos(getListaReferidos()));

        ListView lvDatos = (ListView) v.findViewById(R.id.lvHistorial);
        lvDatos.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return v;
    }

    private ArrayList<ReferidosBean> getListaReferidos(){

        // Se cambio a dispositivo - if(getidprospecto()==null){
        if(getIdProspectoDispositivo()==null){
            return new ArrayList<>();
        }

        CarteraReferidoController carteraReferidoController = new CarteraReferidoController();

        // Se cambio a dispositivo - ArrayList<ReferidosBean> listaReferidos = carteraReferidoController.obtenerProspectoReferidos(getidprospecto());
        ArrayList<ReferidosBean> listaReferidos = carteraReferidoController.obtenerProspectoReferidos(getIdProspectoDispositivo());


        /*
        listaReferidos.add(new ReferidosBean("1001", "Doris Guerra", "912283923", Constantes.codHijosCon, 45, Constantes.codIngresosNivel2, true));
        listaReferidos.add(new ReferidosBean("1002", "Dusalisse Gutierrez", "3456789", Constantes.codHijosCon, 38, Constantes.codIngresosNivel1, false));
        listaReferidos.add(new ReferidosBean("1003", "German Chavez", "96789009", Constantes.codHijosSinDefinir, 67, Constantes.codIngresosNivel3, null));
        listaReferidos.add(new ReferidosBean("1004", "Julio Sanchez", "34356767", Constantes.codHijosSin, 56, Constantes.codIngresosNivel4, true));
        listaReferidos.add(new ReferidosBean("1005", "Gabriela Julls", "3435465", Constantes.codHijosSinDefinir, 50, Constantes.codIngresosNivel2, true));
        */
        return listaReferidos;
    }

    private ArrayList<ReferidosBean> llenarListaReferidos(ArrayList<ReferidosBean> listaReferidos){

        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaEdad = tablasController.obtenerMapaTablas(Constantes.codTablaRangoEdad);
        Map<Integer, TablaTablasBean> mapTablaIngresos = tablasController.obtenerMapaTablas(Constantes.codTablaRangoIngresos);
        Map<Integer, TablaTablasBean> mapTablaTipoEmpleo = tablasController.obtenerMapaTablas(Constantes.codTablaTipoEmpleo);

        ArrayList<ReferidosBean> listaReferidosProcesados = new ArrayList<>();
        String str_de = getResources().getString(R.string.fecha_de);


        for(ReferidosBean referidos : listaReferidos){
            referidos.setDesEdad(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEdad, referidos.getCodEdad()));
            referidos.setDesIngresos(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaIngresos, referidos.getCodIngresos()));
            referidos.setDesTipoEmpleo(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaTipoEmpleo, referidos.getTipoEmpleo()));
            referidos.setDesTieneHijos(StringUtils.obtenerTextoBooleano(referidos.getFlagTieneHijos()));

            referidos.setFecContactadoTexto(DateUtils.getDateFormat20(str_de, referidos.getFecContactado()));

        }

        return listaReferidos;
    }

    //Se cambio a dispositivo -
    /*public Integer getidprospecto() {
        return idprospecto;
    }

    public void setidprospecto(Integer idprospecto) {
        this.idprospecto = idprospecto;
    }*/

    public Integer getIdProspectoDispositivo() {
        return idProspectoDispositivo;
    }

    public void setIdProspectoDispositivo(Integer idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }
}
