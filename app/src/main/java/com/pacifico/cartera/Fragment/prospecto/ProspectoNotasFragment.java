package com.pacifico.cartera.Fragment.prospecto;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pacifico.cartera.R;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

/**
 * Created by dsb on 14/05/2016.
 */
public class ProspectoNotasFragment extends Fragment {

    private String notas;
    private TextViewCustomFont tvNotas;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ppct_nota_frgm, container, false);

        Context ctx = getActivity().getApplicationContext();
        tvNotas = (TextViewCustomFont) view.findViewById(R.id.tvNotas);
        tvNotas.setText(notas);

        return view;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }
}
