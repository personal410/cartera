package com.pacifico.cartera.Fragment.prospecto;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.HijoAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraFamiliarController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.FamiliarCarteraBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

import static com.pacifico.cartera.utils.Util.capitalizedString;

/**
 * Created by dsb on 14/05/2016.
 */
public class ProspectoDatosFamiliaresFragment extends Fragment {



    //private Integer idprospecto;
    private ProspectoBean prospectoBean;
    private FamiliarBean conyugeBean;
    private Integer idProspetcoDispositivo;
    private TextViewCustomFont tvConyugeNombre;
    private TextViewCustomFont tvConyugeEdad;
    private TextViewCustomFont tvConyugeOcupacion;
    private ListView lvDatos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ppct_dfam_frgm, container, false);
        setTvConyugeNombre((TextViewCustomFont) view.findViewById(R.id.tvConyugeNombre));
        setTvConyugeEdad((TextViewCustomFont) view.findViewById(R.id.tvConyugeEdad));
        setTvConyugeOcupacion((TextViewCustomFont) view.findViewById(R.id.tvConyugeOcupacion));
        lvDatos = (ListView) view.findViewById(R.id.lvHijos);
        actualizarPantalla();
        return view;
    }





    public void actualizarPantalla() {

        //Se cambio a dispositivo - Log.d("PF", "actualizarPantalla idProspecto: " + getidprospecto());
        Log.d("PF", "actualizarPantalla idProspecto: " + getIdProspetcoDispositivo());
        //if (getidprospecto() == null) {

        if (getIdProspetcoDispositivo() == null) {
            return;
        }

        CarteraFamiliarController carteraFamiliarController = new CarteraFamiliarController();
        //Se cambio a dispositivo -
        //ArrayList<FamiliarCarteraBean> familiaLista = carteraFamiliarController.obtenerProspectoFamiliar(getidprospecto());
        ArrayList<FamiliarCarteraBean> familiaLista = carteraFamiliarController.obtenerProspectoFamiliar(getIdProspetcoDispositivo());
        if (familiaLista == null || familiaLista.size() == 0) {
            return;
        }
        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaTipoFamiliar = tablasController.obtenerMapaTablas(Constantes.codTablaTipoFamiliar);

        FamiliarCarteraBean conyuge = null;
        ArrayList<FamiliarCarteraBean> hijoLista = new ArrayList<>();

        String str_de = getResources().getString(R.string.fecha_de);

        for (FamiliarCarteraBean familiarBean : familiaLista) {
            //familiarBean.setCodigoTipoFamiliar(StringUtils.convertStringToInt(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaTipoFamiliar, familiarBean.getCodigoTipoFamiliar())));
            String fecha = DateUtils.getDateFormat20(str_de, familiarBean.getFechaNacimientoDateTime());
            familiarBean.setFechaNacimientoTexto(fecha);

            if (familiarBean.getCodigoTipoFamiliar() == Constantes.codTipoFamiliar_conyuge && familiarBean.getFlagActivo()==1 ) {
                conyuge = familiarBean;
            } else if (familiarBean.getCodigoTipoFamiliar() == Constantes.codTipoFamiliar_hijos) {

               if(familiarBean.getFlagActivo()==1) {
                   hijoLista.add(familiarBean);
               }
            }

        }
        String edad;
        if(conyuge!=null ){
            tvConyugeNombre.setText(capitalizedString(conyuge.getNombres()+" "+capitalizedString(conyuge.getApellidoPaterno())+" "+conyuge.getApellidoMaterno()));
            if(conyuge.getEdad()==-1)
            {
                 edad = "";
            }
            else{
                 edad = ""+conyuge.getEdad();
            }
            if(edad.length()>0) {
                tvConyugeEdad.setText(edad + " Años");
            }
            tvConyugeOcupacion.setText(conyuge.getOcupacion());
        }else{
            tvConyugeNombre.setText("");
            tvConyugeEdad.setText("");
            tvConyugeOcupacion.setText("");
        }


        HijoAdapter adapter = new HijoAdapter(getActivity(), hijoLista);
        lvDatos.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }



    public Integer getIdProspetcoDispositivo() {
        return idProspetcoDispositivo;
    }

    public void setIdProspetcoDispositivo(Integer idProspetcoDispositivo) {
        this.idProspetcoDispositivo = idProspetcoDispositivo;
    }

    public TextViewCustomFont getTvConyugeNombre() {
        return tvConyugeNombre;
    }

    public void setTvConyugeNombre(TextViewCustomFont tvConyugeNombre) {
        this.tvConyugeNombre = tvConyugeNombre;
    }

    public TextViewCustomFont getTvConyugeEdad() {
        return tvConyugeEdad;
    }

    public void setTvConyugeEdad(TextViewCustomFont tvConyugeEdad) {
        this.tvConyugeEdad = tvConyugeEdad;
    }

    public TextViewCustomFont getTvConyugeOcupacion() {
        return tvConyugeOcupacion;
    }

    public void setTvConyugeOcupacion(TextViewCustomFont tvConyugeOcupacion) {
        this.tvConyugeOcupacion = tvConyugeOcupacion;
    }

    public ListView getLvDatos() {
        return lvDatos;
    }

    public void setLvDatos(ListView lvDatos) {
        this.lvDatos = lvDatos;
    }

    public ProspectoBean getProspectoBean() {
        return prospectoBean;
    }

    public void setProspectoBean(ProspectoBean prospectoBean) {
        this.prospectoBean = prospectoBean;
    }

    public FamiliarBean getConyugeBean() {
        return conyugeBean;
    }

    public void setConyugeBean(FamiliarBean conyugeBean) {
        this.conyugeBean = conyugeBean;
    }
}
