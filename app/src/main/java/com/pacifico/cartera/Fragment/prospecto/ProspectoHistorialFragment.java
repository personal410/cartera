package com.pacifico.cartera.Fragment.prospecto;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pacifico.cartera.Model.Bean.TablaTablasBean;

import java.util.ArrayList;
import java.util.Map;

import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.HistorialAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraHistorialController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.HistorialBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DrawableHelper;

/**
 * Created by dsb on 14/05/2016.
 */
public class ProspectoHistorialFragment extends Fragment {

    //private Integer idprospecto;
    private Integer idprospectodispositivo;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.ppct_hist_frgm, container, false);

        Context ctx = getActivity().getApplicationContext();

        HistorialAdapter adapter = new HistorialAdapter(getActivity(), llenarListaHistorial(getListaHitorial()));

        ListView lvDatos = (ListView) v.findViewById(R.id.lvHistorial);
        lvDatos.setAdapter(adapter);
        adapter.notifyDataSetChanged();

        return v;
    }

    private ArrayList<HistorialBean> getListaHitorial() {

        if (getIdprospectodispositivo() == null) {
            return new ArrayList<>();
        }

        CarteraHistorialController carteraMovimientoController = new CarteraHistorialController();
        ArrayList<HistorialBean> listaHistorial = carteraMovimientoController.obtenerProspectoHistorial(getIdprospectodispositivo());

        /*
        listaReferidos.add(new ReferidosBean("1001", "Doris Guerra", "912283923", Constantes.codHijosCon, 45, Constantes.codIngresosNivel2, true));
        listaReferidos.add(new ReferidosBean("1002", "Dusalisse Gutierrez", "3456789", Constantes.codHijosCon, 38, Constantes.codIngresosNivel1, false));
        listaReferidos.add(new ReferidosBean("1003", "German Chavez", "96789009", Constantes.codHijosSinDefinir, 67, Constantes.codIngresosNivel3, null));
        listaReferidos.add(new ReferidosBean("1004", "Julio Sanchez", "34356767", Constantes.codHijosSin, 56, Constantes.codIngresosNivel4, true));
        listaReferidos.add(new ReferidosBean("1005", "Gabriela Julls", "3435465", Constantes.codHijosSinDefinir, 50, Constantes.codIngresosNivel2, true));
        */


        /*
        listaHistorial.add(new HistorialBean(Constantes.codEtapaNuevo, Constantes.codEstadoPorContactar, DateUtils.createDateTimeFromString("2016-01-09T"),"9 Ene 2016, 9:30 a.m." ));
        listaHistorial.add(new HistorialBean(Constantes.codEtapaNuevo, Constantes.codIcnRecDeLlamado, DateUtils.createDateTimeFromString("2016-03-05T"), "5 Mar 2016, 11:00 a.m."));
        listaHistorial.add(new HistorialBean(Constantes.codEtapa1ra, Constantes.codEstadoAgendada, DateUtils.createDateTimeFromString("2016-03-10T"),"10 Mar 2016, 7:00 p.m." ));
        listaHistorial.add(new HistorialBean(Constantes.codEtapa1ra, Constantes.codEstadoRealizada, DateUtils.createDateTimeFromString("2016-03-15T"),"15 Mar 2016, 7:00 p.m." ));
        */

        return listaHistorial;
    }

    private ArrayList<HistorialBean> llenarListaHistorial(ArrayList<HistorialBean> listaHistorial) {
        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaEtapa= tablasController.obtenerMapaTablas(Constantes.codTablaEtapaProspecto);
        Map<Integer, TablaTablasBean> mapTablaEstado = tablasController.obtenerMapaTablas(Constantes.codTablaEstadoCita);
        DrawableHelper drawableHelper = new DrawableHelper(getContext(), getResources());
        Map<Integer, Drawable> mapDrawableEtapa = drawableHelper.obtenerMapaDrawableEtapa();
        Map<Integer, Drawable> mapDrawableEstado = drawableHelper.obtenerMapaDrawableEstado();

        ArrayList<HistorialBean> listaHistorialProcesados = new ArrayList<>();
        String str_de = getResources().getString(R.string.fecha_de);


        for(HistorialBean historialBean : listaHistorial){
            historialBean.setDesEtapa(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEtapa, historialBean.getCodEtapa()));
            historialBean.setDrwEtapa(mapDrawableEtapa.get(historialBean.getCodEtapa()));
            historialBean.setDesEstado("");
            historialBean.setDesEstado2(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEstado, historialBean.getCodEstado()));
            historialBean.setDrwEstado(mapDrawableEstado.get(historialBean.getCodEstado()));
            listaHistorialProcesados.add(historialBean);
        }

        return listaHistorialProcesados;
    }

    /*public Integer getidprospecto() {
        return idprospecto;
    }

    public void setidprospecto(Integer idprospecto) {
        this.idprospecto = idprospecto;
    }*/

    public Integer getIdprospectodispositivo() {
        return idprospectodispositivo;
    }

    public void setIdprospectodispositivo(Integer idprospectodispositivo) {
        this.idprospectodispositivo = idprospectodispositivo;
    }


}
