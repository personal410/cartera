package com.pacifico.cartera.Fragment.prospecto;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.pacifico.cartera.Model.Bean.FamiliarBean;
import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.TablaIdentificadorBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;
import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.FamiliarController;
import com.pacifico.cartera.Model.Controller.ProspectoController;
import com.pacifico.cartera.Model.Controller.TablaIdentificadorController;
import com.pacifico.cartera.Model.Controller.TablasGeneralesController;
import com.pacifico.cartera.Network.RespuestaSincronizacionListener;
import com.pacifico.cartera.Network.SincronizacionController;
import com.pacifico.cartera.Persistence.DatabaseConstants;
import com.pacifico.cartera.Util.Util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.pacifico.cartera.Activity.InicioSesionActivity;
import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.CampoDatosPersonalesArrayAdapter;
import com.pacifico.cartera.utils.CodigoTelefonoSorter;
import com.pacifico.cartera.utils.Constantes;

public class EditarProspectoFragment extends Fragment implements RadioGroup.OnCheckedChangeListener, View.OnFocusChangeListener {
    public static final String tipo_operacion = "tipo_operacion";
    public static final int operacion_MostrarADN = 3;
    public static final String parametro_idProspecto = "id_prospecto_dispositivo";
    final private Calendar calMax = Calendar.getInstance();
    final private Calendar calMin = Calendar.getInstance();
    private ArrayList<TablaTablasBean> arrTipoDocumentos, arrEstadosCiviles,arrCodigoTelefonoFijos;
    private Calendar calFechaNacimiento;

    private ArrayList<String> arrTitulosTipoDocumentos, arrTitulosCodigoTelefonoFijos, arrTitulosEstadosCiviles;
    private EditText edtPrimerNombre, edtApellidoPaterno, edtApellidoMaterno, edtNumeroDocumento, edtFechaNacimiento, edtTelefonoCelular, edtTelefonoFijo, edtAnexo;
    private EditText edtEdad, edtCorreoElectronico, edtEmpresa, edtActual, edtNotas, edtTipoDocumento, edtCodigoTelefonoFijo, edtEstadoCivil;
    private RadioGroup rgSexo, rgFumador, rgHijos;
    private EditText edtNombreHijo,edtEdadHijo;
    private EditText edtEdadHijo2,edtNombreHijo2;

    private ProspectoBean prospectoBean;
    private String codigoTelefonoFijo;
    private LayoutInflater layoutInflater;
    private ProgressDialog progressDialog;

    // conyugue
    private EditText edtConyugueNombre, edtConyugueApellidoPaterno, edtConyugueApellidoMaterno, edtConyugueFechaNacimiento, edtConyugueOcupacion;
    private Calendar calConyugueFechaNacimiento;
    private FamiliarBean conyugeBean;

    // hijo
    private ArrayList<FamiliarBean> arrHijos = new ArrayList<>();
    private ArrayList<FamiliarBean> arrHijosInicial = new ArrayList<>();
    private LinearLayout linHijos;
    int listaCampoActual;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        layoutInflater = inflater;
        if(prospectoBean.getFlagConyuge() == 1){
            ArrayList<FamiliarBean> arrFamiliaresTemp = FamiliarController.obtenerFamiliarPorIdProspectoPorTipoFamiliar(prospectoBean.getIdProspectoDispositivo(), 1);
            if(arrFamiliaresTemp.size() > 0){
                conyugeBean = arrFamiliaresTemp.get(0);
            }else{
                conyugeBean = new FamiliarBean();
                conyugeBean.inicializarValores();
            }
        }else{
            conyugeBean = new FamiliarBean();
            conyugeBean.inicializarValores();
        }
        if(prospectoBean.getFlagHijo() == 1){
            arrHijosInicial = FamiliarController.obtenerFamiliarPorIdProspectoPorTipoFamiliar(prospectoBean.getIdProspectoDispositivo(), 2);
            arrHijos = (ArrayList<FamiliarBean>)arrHijosInicial.clone();
        }
        arrTipoDocumentos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(5);
        arrTitulosTipoDocumentos = new ArrayList<>();
        int codigoTipoDocumento = prospectoBean.getCodigoTipoDocumento();
        if(codigoTipoDocumento == -1){
            prospectoBean.setCodigoTipoDocumento(1);
        }
        int indiceTipoDocumentoSeleccionado = 0;
        for (int i = 0; i < arrTipoDocumentos.size(); i++) {
            TablaTablasBean tablaTablasBean = arrTipoDocumentos.get(i);
            arrTitulosTipoDocumentos.add(tablaTablasBean.getValorCadena());
            if (codigoTipoDocumento == tablaTablasBean.getCodigoCampo()) {
                indiceTipoDocumentoSeleccionado = i;
            }
        }
        arrCodigoTelefonoFijos = TablasGeneralesController.obtenerTablaTablasPorIdTabla(20);
        Collections.sort(arrCodigoTelefonoFijos, new CodigoTelefonoSorter());
        arrTitulosCodigoTelefonoFijos = new ArrayList<>();
        DecimalFormat decimalFormat = new DecimalFormat("##");
        for(int i = 0; i < arrCodigoTelefonoFijos.size(); i++){
            TablaTablasBean tablaTablasBean = arrCodigoTelefonoFijos.get(i);
            int codigoTelefonoFijo = (int)tablaTablasBean.getValorNumerico();
            String cadenaCodigoTelefonoFijo = decimalFormat.format(codigoTelefonoFijo);
            if(cadenaCodigoTelefonoFijo.length() == 1){
                cadenaCodigoTelefonoFijo = "  " + cadenaCodigoTelefonoFijo;
            }
            arrTitulosCodigoTelefonoFijos.add(String.format("%s      %s", cadenaCodigoTelefonoFijo, tablaTablasBean.getValorCadena()));
        }
        String telefonoFijoTemp = "";
        String anexoTemp = "";
        String telefonoFijo = prospectoBean.getTelefonoFijo();
        codigoTelefonoFijo = "1";
        if(telefonoFijo != null){
            if(telefonoFijo.length() == 15){
                codigoTelefonoFijo = telefonoFijo.substring(0, 2).replace(" ", "");
                telefonoFijoTemp = telefonoFijo.substring(2, 10).replace(" ", "");
                anexoTemp = telefonoFijo.substring(10, 15).replace(" ", "");
            }
        }

        arrEstadosCiviles = TablasGeneralesController.obtenerTablaTablasPorIdTabla(2);
        arrTitulosEstadosCiviles = new ArrayList<>();
        int codigoEstadoCivil = prospectoBean.getCodigoEstadoCivil();
        int indiceEstadoCivilSeleccionado = -1;
        for(int i = 0; i < arrEstadosCiviles.size(); i++){
            TablaTablasBean tablaTablasBean = arrEstadosCiviles.get(i);
            arrTitulosEstadosCiviles.add(tablaTablasBean.getValorCadena());
            if(codigoEstadoCivil == tablaTablasBean.getCodigoCampo()){
                indiceEstadoCivilSeleccionado = i;
            }
        }
        View view = layoutInflater.inflate(R.layout.ppct_edit_frgm, container, false);
        edtPrimerNombre = (EditText) view.findViewById(R.id.edtPrimerNombre);
        edtApellidoPaterno = (EditText) view.findViewById(R.id.edtApellidoPaterno);
        edtApellidoMaterno = (EditText) view.findViewById(R.id.edtApellidoMaterno);
        edtTipoDocumento = (EditText) view.findViewById(R.id.edtTipoDocIdentidad);
        edtNumeroDocumento = (EditText) view.findViewById(R.id.edtNumeroDocumento);
        edtFechaNacimiento = (EditText) view.findViewById(R.id.edtFechaNacimiento);
        edtEdad = (EditText) view.findViewById(R.id.edtEdad);
        edtTelefonoCelular = (EditText) view.findViewById(R.id.edtTelefonoCelular);
        edtCodigoTelefonoFijo = (EditText) view.findViewById(R.id.edtCodigoTelefonoFijo);
        edtTelefonoFijo = (EditText) view.findViewById(R.id.edtTelefonoFijo);
        edtAnexo = (EditText) view.findViewById(R.id.edtAnexo);
        edtCorreoElectronico = (EditText) view.findViewById(R.id.edtCorreoElectronico);
        edtEstadoCivil = (EditText) view.findViewById(R.id.edtEstadoCivil);
        rgSexo = (RadioGroup) view.findViewById(R.id.rgSexo);
        rgFumador = (RadioGroup) view.findViewById(R.id.rgFumador);
        rgHijos = (RadioGroup) view.findViewById(R.id.rgHijos);
        edtEmpresa = (EditText) view.findViewById(R.id.edtEmpresa);
        linHijos = (LinearLayout)view.findViewById(R.id.linHijos);
        Button btnAgregarHijo = (Button)view.findViewById(R.id.btnAgregarHijo);

        edtConyugueNombre = (EditText) view.findViewById(R.id.edtConyugueNombre);
        edtConyugueApellidoPaterno = (EditText) view.findViewById(R.id.edtConyugueApellidoPaterno);
        edtConyugueApellidoMaterno = (EditText) view.findViewById(R.id.edtConyugueApellidoMaterno);
        edtConyugueFechaNacimiento = (EditText) view.findViewById(R.id.edtConyugueFechaNacimiento);
        edtConyugueOcupacion = (EditText) view.findViewById(R.id.edtConyugueOcupacion);
        edtNotas = (EditText)view.findViewById(R.id.edtNota) ;

        edtPrimerNombre.setText(Util.capitalizedString(getProspectoBean().getNombres()));
        edtPrimerNombre.setOnFocusChangeListener(this);
        edtApellidoPaterno.setText(Util.capitalizedString(getProspectoBean().getApellidoPaterno()));
        edtApellidoPaterno.setOnFocusChangeListener(this);
        edtTipoDocumento.setText(arrTitulosTipoDocumentos.get(indiceTipoDocumentoSeleccionado));
        edtTipoDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 0;
                cargarLista();
            }
        });
        edtCodigoTelefonoFijo.setText(codigoTelefonoFijo);
        edtCodigoTelefonoFijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 1;
                cargarLista();
            }
        });
        if(indiceEstadoCivilSeleccionado == -1){
            edtEstadoCivil.setText("");
        }else{
            edtEstadoCivil.setText(arrTitulosEstadosCiviles.get(indiceEstadoCivilSeleccionado));
        }
        edtEstadoCivil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaCampoActual = 2;
                cargarLista();
            }
        });
        edtNotas.setText(getProspectoBean().getNota());
        edtNotas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    String NotasNuevas = edtNotas.getText().toString();
                    if(!NotasNuevas.equals(getProspectoBean().getNota())){
                        getProspectoBean().setNota(NotasNuevas);
                        getProspectoBean().setFlagModificado(1);
                    }
                }
            }
        });

        String apellidoMaterno = getProspectoBean().getApellidoMaterno();
        if (apellidoMaterno != null) {
            edtApellidoMaterno.setText(Util.capitalizedString(apellidoMaterno));
        }
        edtApellidoMaterno.setOnFocusChangeListener(this);
        edtNumeroDocumento.setText(getProspectoBean().getNumeroDocumento());
        edtNumeroDocumento.setOnFocusChangeListener(this);

        final String fechaNacimiento = getProspectoBean().getFechaNacimiento();
        calMax.add(Calendar.YEAR, -18);
        calMin.add(Calendar.YEAR, -100);
        calMin.add(Calendar.DAY_OF_MONTH, -1);
        if (fechaNacimiento != null) {
            if (fechaNacimiento.length() == 10) {
                String[] arrFechaNacimiento = fechaNacimiento.split("-");
                calFechaNacimiento = Calendar.getInstance();
                calFechaNacimiento.set(Integer.parseInt(arrFechaNacimiento[0]), Integer.parseInt(arrFechaNacimiento[1]) - 1, Integer.parseInt(arrFechaNacimiento[2]));
                updateEdtFechaNacimiento();
            }
        }
        if (calFechaNacimiento == null) {
            calFechaNacimiento = (Calendar) calMax.clone();
        }
        edtFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calFechaNacimiento.set(Calendar.YEAR, year);
                        calFechaNacimiento.set(Calendar.MONTH, monthOfYear);
                        calFechaNacimiento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateEdtFechaNacimiento();
                    }
                }, calFechaNacimiento.get(Calendar.YEAR), calFechaNacimiento.get(Calendar.MONTH), calFechaNacimiento.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());
                datePickerDialog.setTitle("Seleccione una fecha");
                datePickerDialog.show();
            }
        });
        edtTelefonoCelular.setText(getProspectoBean().getTelefonoCelular());
        edtTelefonoCelular.setOnFocusChangeListener(this);
        edtTelefonoFijo.setText(telefonoFijoTemp);
        edtTelefonoFijo.setOnFocusChangeListener(this);
        edtAnexo.setText(anexoTemp);
        edtAnexo.setOnFocusChangeListener(this);
        edtCorreoElectronico.setText(getProspectoBean().getCorreoElectronico1());
        edtCorreoElectronico.setOnFocusChangeListener(this);
        if (getProspectoBean().getCodigoSexo() != -1) {
            int codigoSexo = getProspectoBean().getCodigoSexo();
            if (codigoSexo == 0 || codigoSexo == 1) {
                rgSexo.check(codigoSexo == 1 ? R.id.rbMasculino : R.id.rbFemenino);
            }
        }
        rgSexo.setOnCheckedChangeListener(this);
        if (getProspectoBean().getCondicionFumador() != -1) {
            rgFumador.check(getProspectoBean().getCondicionFumador() == 1 ? R.id.rbFumador : R.id.rbNoFumador);
        }
        rgFumador.setOnCheckedChangeListener(this);
        if (getProspectoBean().getFlagHijo() != -1) {
            rgHijos.check(getProspectoBean().getFlagHijo() == 1 ? R.id.rbConHijos : R.id.rbSinHijos);
        }
        rgHijos.setOnCheckedChangeListener(this);
        edtEmpresa.setText(getProspectoBean().getEmpresa());
        edtEmpresa.setOnFocusChangeListener(this);

        //conyugue
        if(conyugeBean != null){
            edtConyugueNombre.setText(conyugeBean.getNombres());
            edtConyugueApellidoPaterno.setText(conyugeBean.getApellidoPaterno());
            edtConyugueApellidoMaterno.setText(conyugeBean.getApellidoMaterno());
            String fechaNacimientoConyuge = conyugeBean.getFechaNacimiento();
            if(fechaNacimientoConyuge != null){
                if(fechaNacimientoConyuge.length() == 10){
                    String[] arrFechaNacimientoConyuge = conyugeBean.getFechaNacimiento().split("-");
                    calConyugueFechaNacimiento = Calendar.getInstance();
                    calConyugueFechaNacimiento.set(Integer.parseInt(arrFechaNacimientoConyuge[0]), Integer.parseInt(arrFechaNacimientoConyuge[1]) - 1, Integer.parseInt(arrFechaNacimientoConyuge[2]));
                    updateConyugueEdtFechaNacimiento();
                }
            }
            if(calConyugueFechaNacimiento == null){
                calConyugueFechaNacimiento = (Calendar) calMax.clone();
                calConyugueFechaNacimiento.add(Calendar.YEAR, -12);
            }
            edtConyugueOcupacion.setText(conyugeBean.getOcupacion());
        }
        edtConyugueNombre.setOnFocusChangeListener(this);
        edtConyugueApellidoPaterno.setOnFocusChangeListener(this);
        edtConyugueApellidoMaterno.setOnFocusChangeListener(this);
        edtConyugueFechaNacimiento.setOnFocusChangeListener(this);
        edtConyugueFechaNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), DatePickerDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        calConyugueFechaNacimiento.set(Calendar.YEAR, year);
                        calConyugueFechaNacimiento.set(Calendar.MONTH, monthOfYear);
                        calConyugueFechaNacimiento.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        updateConyugueEdtFechaNacimiento();
                    }
                }, calConyugueFechaNacimiento.get(Calendar.YEAR), calConyugueFechaNacimiento.get(Calendar.MONTH), calConyugueFechaNacimiento.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setCalendarViewShown(false);
                datePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());
                datePickerDialog.setTitle("Seleccione una fecha");
                datePickerDialog.show();
            }
        });
        edtConyugueOcupacion.setOnFocusChangeListener(this);

        //hijos
        for(int i = 0; i < arrHijos.size(); i++){
            final FamiliarBean hijoBean = arrHijos.get(i);
            final LinearLayout linHijo = (LinearLayout)layoutInflater.inflate(R.layout.row_nuevo_hijo, null);
            edtNombreHijo2 = (EditText)linHijo.findViewById(R.id.edtNombre);
            edtEdadHijo2 = (EditText)linHijo.findViewById(R.id.edtEdad);
            ImageButton btnEliminar =  (ImageButton)linHijo.findViewById(R.id.imgBtnEliminar);
            edtNombreHijo2.setText(Util.capitalizedString(hijoBean.getNombres()));
            edtEdadHijo2.setText(Integer.toString(hijoBean.getEdad()));
            edtNombreHijo2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String nuevoNombreHijo = edtNombreHijo2.getText().toString().toUpperCase();
                        if (nuevoNombreHijo.length() == 0) {
                            edtNombreHijo.setError(Constantes.MensajeErrorCampoVacio);
                        } else {
                            if (!nuevoNombreHijo.equals(hijoBean.getNombres())) {
                                hijoBean.setNombres(nuevoNombreHijo);
                                hijoBean.setFlagModificado(1);
                            }
                        }
                    }
                }
            });
            edtEdadHijo2.setOnFocusChangeListener(new View.OnFocusChangeListener(){
                @Override
                public void onFocusChange(View v, boolean hasFocus){
                    if(!hasFocus){
                        String nuevaEdad = edtEdadHijo2.getText().toString();
                        TextInputLayout textInputLayout = (TextInputLayout) v.getParent();
                        if(nuevaEdad.length() == 0){
                            textInputLayout.setError(Constantes.MensajeErrorCampoVacio);
                        }else{
                            int nuevaEdadTemp = Integer.parseInt(nuevaEdad);
                            if (nuevaEdadTemp != hijoBean.getEdad()) {
                                hijoBean.setEdad(nuevaEdadTemp);
                                hijoBean.setFlagModificado(1);
                            }
                            textInputLayout.setError(null);
                        }
                    }
                }
            });
            btnEliminar.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    linHijos.removeView(linHijo);
                    hijoBean.setFlagActivo(0);
                    hijoBean.setFlagModificado(1);
                }
            });
            linHijos.addView(linHijo);
        }
        btnAgregarHijo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final FamiliarBean hijoBean = new FamiliarBean();
                hijoBean.inicializarValores();
                arrHijos.add(hijoBean);
                final LinearLayout linHijo = (LinearLayout)layoutInflater.inflate(R.layout.row_nuevo_hijo, null);
                edtNombreHijo = (EditText)linHijo.findViewById(R.id.edtNombre);
                edtEdadHijo = (EditText)linHijo.findViewById(R.id.edtEdad);
                ImageButton btnEliminar =  (ImageButton)linHijo.findViewById(R.id.imgBtnEliminar);
                edtNombreHijo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            String nuevoNombreHijo = edtNombreHijo.getText().toString().toUpperCase();
                            if (nuevoNombreHijo.length() == 0) {
                                edtNombreHijo.setError(Constantes.MensajeErrorCampoVacio);
                            } else {
                                if (!nuevoNombreHijo.equals(hijoBean.getNombres())) {
                                    hijoBean.setNombres(nuevoNombreHijo);
                                    hijoBean.setFlagModificado(1);
                                }
                                edtNombreHijo.setError(null);
                            }
                        }

                        }

                });
                edtEdadHijo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (!hasFocus) {
                            String nuevaEdad = edtEdadHijo.getText().toString();
                            if (nuevaEdad.length() == 0) {
                                edtEdadHijo.setError(Constantes.MensajeErrorCampoVacio);
                            } else {
                                int nuevaEdadTemp = Integer.parseInt(nuevaEdad);
                                if (nuevaEdadTemp != hijoBean.getEdad()) {
                                    hijoBean.setEdad(nuevaEdadTemp);
                                    hijoBean.setFlagModificado(1);
                                }
                                edtEdadHijo.setError(null);
                            }
                        }
                    }
                });
                btnEliminar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        linHijos.removeView(linHijo);
                        hijoBean.setFlagActivo(0);
                        hijoBean.setFlagModificado(1);
                    }
                });
                linHijos.addView(linHijo);
            }
        });
        Button btnGuardar = (Button)view.findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                validarTodosCampos();
            }
        });

        Button btnAtras = (Button)view.findViewById(R.id.btnAtras);
        btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        TextView tvIrAlADN = (TextView)view.findViewById(R.id.tvIrAlADN);
        tvIrAlADN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(CitaReunionController.prospectoTieneCita(prospectoBean.getIdProspectoDispositivo())){
                    try{
                        Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.pacifico.adn");
                        if(launchIntent != null){
                            launchIntent.putExtra(tipo_operacion, operacion_MostrarADN);
                            launchIntent.putExtra(parametro_idProspecto, prospectoBean.getIdProspectoDispositivo());
                            launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(launchIntent);
                        }
                    }catch(Exception e){
                        Util.mostrarAlertaConTitulo("Alerta", "No se pudo abrir ADN", EditarProspectoFragment.this.getContext());
                    }
                }else{
                    Util.mostrarAlertaConTitulo("Alerta", "Es un prospecto nuevo, primero debe agendar una cita.", EditarProspectoFragment.this.getContext());
                }
            }
        });
        return view;
    }
    public void validarTodosCampos(){
        validarDatosPersonales();
        validarConyuge();
        validarHijos();

        ArrayList<String> arrCamposIncorrectos = new ArrayList<>();
        if(prospectoBean.getNombres().length() == 0){
            arrCamposIncorrectos.add("Primer Nombre");
        }
        if(prospectoBean.getApellidoPaterno().length() == 0){
            arrCamposIncorrectos.add("Apellido Paterno");
        }
        if(edtNumeroDocumento.getError() != null){
            arrCamposIncorrectos.add("Número de Documento");
        }
        if(prospectoBean.getCodigoEtapa() != 1){
            if(prospectoBean.getFechaNacimiento() == null){
                arrCamposIncorrectos.add("Fecha de Nacimiento");
            }else{
                if(prospectoBean.getFechaNacimiento().length() == 0){
                    arrCamposIncorrectos.add("Fecha de Nacimiento");
                }
            }
        }
        if(edtTelefonoCelular.getError() != null){
            arrCamposIncorrectos.add("Teléfono celular");
        }
        if(edtTelefonoFijo.getError() != null){
            arrCamposIncorrectos.add("Teléfono fijo");
        }
        if(edtCorreoElectronico.getError() != null){
            arrCamposIncorrectos.add("Correo electrónico");
        }
        if (prospectoBean.getCodigoEstadoCivil() == -1 && prospectoBean.getCodigoEtapa() != 1){
            arrCamposIncorrectos.add("Estado Civil");
        }
        if(prospectoBean.getCodigoSexo() == -1 && prospectoBean.getCodigoEtapa() != 1){
            arrCamposIncorrectos.add("Sexo");
        }
        if(prospectoBean.getCondicionFumador() == -1 && prospectoBean.getCodigoEtapa() != 1){
            arrCamposIncorrectos.add("Condición de Fumador");
        }
        if(prospectoBean.getFlagHijo() == -1 && prospectoBean.getCodigoEtapa() != 1){
            arrCamposIncorrectos.add("Hijos");
        }
        if(arrCamposIncorrectos.size() == 0){
            if(prospectoBean.getFlagConyuge() == 1 && prospectoBean.getCodigoEtapa() != 1){
                if(conyugeBean.getNombres().length() == 0){
                    arrCamposIncorrectos.add("Nombres del Cónyuge");
                }
                if(conyugeBean.getApellidoPaterno().length() == 0){
                    arrCamposIncorrectos.add("Apellido Paterno del Cónyuge");
                }
                if(conyugeBean.getFechaNacimiento().length() == 0){
                    arrCamposIncorrectos.add("Fecha de nacimiento del Cónyuge");
                }
            }
            if(arrCamposIncorrectos.size() == 0){
                if(prospectoBean.getFlagHijo() == 1 && prospectoBean.getCodigoEtapa() != 1){
                    if(arrHijos.size() > 0){
                        for(FamiliarBean hijo : arrHijos){
                            if(hijo.getNombres().length() == 0){
                                arrCamposIncorrectos.add("Nombre del hijo");
                            }
                            int edad = hijo.getEdad();
                            if(edad == -1 || edad > 100){
                                arrCamposIncorrectos.add("Edad del hijo");
                            }
                            if(arrCamposIncorrectos.size() > 0){
                                break;
                            }
                        }
                    }else{
                        String mensajeError = "Debe registrar al menos un hijo";
                        Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                }
                if(arrCamposIncorrectos.size() == 0){
                    guardar();
                }else{
                    String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
                    arrCamposIncorrectos.toArray(camposIncorrectos);
                    String mensajeError = "Por favor corregir los datos resaltados en: " + TextUtils.join(", ", camposIncorrectos);
                    Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
                }
            }else{
                String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
                arrCamposIncorrectos.toArray(camposIncorrectos);
                String mensajeError = "Por favor corregir los datos resaltados en: " + TextUtils.join(", ", camposIncorrectos);
                Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
            }
        }else{
            String[] camposIncorrectos = new String[arrCamposIncorrectos.size()];
            arrCamposIncorrectos.toArray(camposIncorrectos);
            String mensajeError = "Por favor corregir los datos resaltados en: " + TextUtils.join(", ", camposIncorrectos);
            Snackbar.make(getView(), mensajeError, Snackbar.LENGTH_SHORT).show();
        }
    }
    public void guardar(){
        if(prospectoBean.getFlagModificado() == 1){
            prospectoBean.setFlagModificado(0);
            prospectoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
            prospectoBean.setFlagEnviado(0);
            ProspectoController.actualizarProspecto(prospectoBean);
        }
        TablaIdentificadorBean familiarIdentificador = TablaIdentificadorController.obtenerTablaIdentificadorporTabla(DatabaseConstants.TBL_FAMILIAR);
        int idFamiliarDispositivoDisponible = familiarIdentificador.getIdentity() + 1;
        if(prospectoBean.getFlagConyuge() == 1){
            if(conyugeBean != null){
                if(conyugeBean.getFlagModificado() == 1){
                    conyugeBean.setFlagModificado(0);
                    int idFamiliarDispositivo = conyugeBean.getIdFamiliarDispositivo();
                    if(idFamiliarDispositivo == -1){
                        conyugeBean.setIdFamiliar(-1);
                        conyugeBean.setIdFamiliarDispositivo(idFamiliarDispositivoDisponible);
                        idFamiliarDispositivoDisponible++;
                        conyugeBean.setIdProspecto(prospectoBean.getIdProspecto());
                        conyugeBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                        conyugeBean.setCodigoTipoFamiliar(1);
                        conyugeBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                        FamiliarController.guardarFamiliar(conyugeBean);
                    }else{
                        conyugeBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                        conyugeBean.setFlagEnviado(0);
                        FamiliarController.actualizarFamiliar(conyugeBean);
                    }
                }
            }
        }else{
            if(conyugeBean != null){
                conyugeBean.setFlagActivo(0);
                conyugeBean.setFlagEnviado(0);
                FamiliarController.actualizarFamiliar(conyugeBean);
            }
        }
        if(prospectoBean.getFlagHijo() == 1){
            for(FamiliarBean hijoBean : arrHijos){
                if(hijoBean.getIdFamiliarDispositivo() == -1){
                    if(hijoBean.getNombres().length() > 0 && hijoBean.getEdad() > -1){
                        hijoBean.setFlagModificado(0);
                        hijoBean.setIdFamiliar(-1);
                        hijoBean.setIdFamiliarDispositivo(idFamiliarDispositivoDisponible);
                        idFamiliarDispositivoDisponible++;
                        hijoBean.setIdProspecto(prospectoBean.getIdProspecto());
                        hijoBean.setIdProspectoDispositivo(prospectoBean.getIdProspectoDispositivo());
                        hijoBean.setCodigoTipoFamiliar(2);
                        hijoBean.setFechaCreacionDispositivo(Util.obtenerFechaActual());
                        FamiliarController.guardarFamiliar(hijoBean);
                    }
                }else{
                    for(FamiliarBean hijoBeanTemp : arrHijosInicial){
                        if(hijoBeanTemp.getIdFamiliarDispositivo() == hijoBean.getIdFamiliarDispositivo()){
                            if(hijoBean.getNombres().length() > 0 && hijoBean.getEdad() > -1){
                                arrHijosInicial.remove(hijoBeanTemp);
                                if(hijoBean.getFlagModificado() == 1){
                                    hijoBean.setFechaModificacionDispositivo(Util.obtenerFechaActual());
                                    hijoBean.setFlagEnviado(0);
                                    FamiliarController.actualizarFamiliar(hijoBean);
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
        familiarIdentificador.setIdentity(idFamiliarDispositivoDisponible);
        TablaIdentificadorController.actualizarTablaIdentificador(familiarIdentificador);
        for(FamiliarBean hijoBean : arrHijosInicial){
            hijoBean.setFlagActivo(0);
            hijoBean.setFlagEnviado(0);
            FamiliarController.actualizarFamiliar(hijoBean);
        }
        sincronizar();
    }
    public void validarDatosPersonales(){
        onFocusChange(edtPrimerNombre, false);
        onFocusChange(edtApellidoPaterno, false);
        validarNumeroDocumento();
        if(edtFechaNacimiento.length() == 0){
            edtFechaNacimiento.setError(Constantes.MensajeErrorCampoVacio);
        }
        validarTelefonos();
        validarCorreoElectronico();
    }
    public void validarConyuge(){
        ArrayList<EditText> arrEdtTxts = new ArrayList<>();
        arrEdtTxts.add(edtConyugueNombre);
        arrEdtTxts.add(edtConyugueApellidoPaterno);
        for(EditText editText : arrEdtTxts){
            if(prospectoBean.getFlagConyuge() == 1){
                if(editText.getText().toString().trim().length() == 0){
                    editText.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    editText.setError(null);
                }
            }else{
                editText.setError(null);
            }
        }
        if(prospectoBean.getFlagConyuge() == 1 && edtConyugueFechaNacimiento.getText().toString().length() == 0){
            edtConyugueFechaNacimiento.setError(Constantes.MensajeErrorCampoVacio);
        }else{
            edtConyugueFechaNacimiento.setError(null);
        }
    }
    public void validarHijos(){
        for(int i = 0; i < linHijos.getChildCount(); i++){
            LinearLayout linHijo = (LinearLayout)linHijos.getChildAt(i);
            EditText edtNombreHijoTemp = (EditText)linHijo.findViewById(R.id.edtNombre);
            EditText edtEdadHijoTemp = (EditText)linHijo.findViewById(R.id.edtEdad);
            if(prospectoBean.getFlagHijo() == 1 && edtNombreHijoTemp.getText().length() == 0){
                edtNombreHijoTemp.setError(Constantes.MensajeErrorCampoVacio);
            }else{
                edtNombreHijoTemp.setError(null);
            }
            if(prospectoBean.getFlagHijo() == 1 && edtEdadHijoTemp.getText().length() == 0){
                edtEdadHijoTemp.setError(Constantes.MensajeErrorCampoVacio);
            }else{
                edtEdadHijoTemp.setError(null);
            }
        }
    }
    private void updateEdtFechaNacimiento(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy", new Locale("es", "ES"));
        edtFechaNacimiento.setText(simpleDateFormat.format(calFechaNacimiento.getTime()));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
        String nuevaFechaNacimiento = simpleDateFormat.format(calFechaNacimiento.getTime());
        if (!nuevaFechaNacimiento.equals(getProspectoBean().getFechaNacimiento())) {
            getProspectoBean().setFechaNacimiento(nuevaFechaNacimiento);
            getProspectoBean().setCodigoRangoEdad(Util.devolverRangoEdad(calFechaNacimiento));
            getProspectoBean().setFlagModificado(1);
        }
        Calendar calHoy = Calendar.getInstance();
        int diaHoy = calHoy.get(Calendar.DAY_OF_MONTH);
        int mesHoy = calHoy.get(Calendar.MONTH);
        int anioHoy = calHoy.get(Calendar.YEAR);
        int diaFecNac = calFechaNacimiento.get(Calendar.DAY_OF_MONTH);
        int mesFecNac = calFechaNacimiento.get(Calendar.MONTH);
        int anioFecNac = calFechaNacimiento.get(Calendar.YEAR);
        int edad = anioHoy - anioFecNac;
        if (mesHoy < mesFecNac) {
            edad -= 1;
        } else if (mesHoy == mesFecNac) {
            if (diaHoy < diaFecNac) {
                edad -= 1;
            }
        }
        edtEdad.setText(String.format("%d años", edad));
        TextInputLayout textInputLayout = (TextInputLayout) edtFechaNacimiento.getParent();
        textInputLayout.setError(null);
    }
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId){
        if(group.equals(rgSexo)){
            int nuevoCodigoSexo = checkedId == R.id.rbMasculino ? 1 : 0;
            if (nuevoCodigoSexo != getProspectoBean().getCodigoSexo()) {
                Log.i("TAG", "codigoSexo");
                getProspectoBean().setCodigoSexo(nuevoCodigoSexo);
                getProspectoBean().setFlagModificado(1);
            }
        }else if(group.equals(rgFumador)) {
            int nuevoCondicionFumador = checkedId == R.id.rbFumador ? 1 : 0;
            if (nuevoCondicionFumador != getProspectoBean().getCondicionFumador()) {
                Log.i("TAG", "condicionFumador");
                getProspectoBean().setCondicionFumador(checkedId == R.id.rbFumador ? 1 : 0);
                getProspectoBean().setFlagModificado(1);
            }
        }else if (group.equals(rgHijos)){
            int nuevoFlagHijo = checkedId == R.id.rbConHijos ? 1 : 0;
            if (nuevoFlagHijo != getProspectoBean().getFlagHijo()) {
                Log.i("TAG", "flagHijo");
                getProspectoBean().setFlagHijo(nuevoFlagHijo);
                getProspectoBean().setFlagModificado(1);
            }
        }
    }
    @Override
    public void onFocusChange(View v, boolean hasFocus){
        if(hasFocus){
            edtActual = (EditText)v;
        }else{
            edtActual = (EditText)v;
            String textoActual = edtActual.getText().toString().trim();
            if(edtActual.equals(edtPrimerNombre)){
                edtPrimerNombre.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getNombres())){
                    prospectoBean.setNombres(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "nombres");
                }
                if(edtPrimerNombre.length() == 0){
                    edtPrimerNombre.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtPrimerNombre.setError(null);
                }
            }else if(edtActual.equals(edtApellidoPaterno)){
                edtApellidoPaterno.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getApellidoPaterno())){
                    prospectoBean.setApellidoPaterno(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "apePat");
                }
                if(textoActual.length() == 0){
                    edtApellidoPaterno.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtApellidoPaterno.setError(null);
                }
            }else if(edtActual.equals(edtApellidoMaterno)){
                edtApellidoMaterno.setText(textoActual);
                if(!textoActual.toUpperCase().equals(prospectoBean.getApellidoMaterno())){
                    prospectoBean.setApellidoMaterno(textoActual.toUpperCase());
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "apeMat");
                }
            } else if (edtActual.equals(edtNumeroDocumento)) {
                if(!textoActual.equals(prospectoBean.getNumeroDocumento())){
                    prospectoBean.setNumeroDocumento(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "numDoc");
                }
                validarNumeroDocumento();
            }else if (edtActual.equals(edtTelefonoCelular)) {
                if(!textoActual.equals(prospectoBean.getTelefonoCelular())){
                    prospectoBean.setTelefonoCelular(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "telefonoCelular");
                    validarTelefonos();
                }
            } else if (edtActual.equals(edtTelefonoFijo) || edtActual.equals(edtAnexo)) {
                actualizarTelefonoFijo();
                validarTelefonos();
            } else if (edtActual.equals(edtCorreoElectronico)) {
                edtCorreoElectronico.setText(textoActual);
                if(!textoActual.equals(prospectoBean.getCorreoElectronico1())){
                    prospectoBean.setCorreoElectronico1(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "correoElectronico");
                }
                validarCorreoElectronico();
            }else if (edtActual.equals(edtEmpresa)){
                edtEmpresa.setText(textoActual);
                if(!textoActual.equals(prospectoBean.getEmpresa())){
                    prospectoBean.setEmpresa(textoActual);
                    prospectoBean.setFlagModificado(1);
                    Log.i("TAG", "empresa");
                }
            }else if(edtActual.equals(edtConyugueNombre)){
                edtConyugueNombre.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getNombres())){
                    conyugeBean.setNombres(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
                if(prospectoBean.getFlagConyuge() == 1 && textoActual.length() == 0){
                    edtConyugueNombre.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtConyugueNombre.setError(null);
                }
            }else if(edtActual.equals(edtConyugueApellidoPaterno)){
                edtConyugueApellidoPaterno.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getApellidoPaterno())){
                    conyugeBean.setApellidoPaterno(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
                if(prospectoBean.getFlagConyuge() == 1 && textoActual.length() == 0){
                    edtConyugueApellidoPaterno.setError(Constantes.MensajeErrorCampoVacio);
                }else{
                    edtConyugueApellidoPaterno.setError(null);
                }
            }else if(edtActual.equals(edtConyugueApellidoMaterno)){
                edtConyugueApellidoMaterno.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getApellidoMaterno())){
                    conyugeBean.setApellidoMaterno(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
            }else if(edtActual.equals(edtConyugueOcupacion)){
                edtConyugueOcupacion.setText(textoActual);
                if(!textoActual.equals(conyugeBean.getOcupacion())){
                    conyugeBean.setOcupacion(textoActual);
                    conyugeBean.setFlagModificado(1);
                }
            }
            edtActual = null;
        }
    }
    private void actualizarTelefonoFijo(){
        String telefonoFijo = edtTelefonoFijo.getText().toString();
        if(telefonoFijo.length() == 0){
            if(prospectoBean.getTelefonoFijo() != null){
                prospectoBean.setTelefonoFijo(null);
                prospectoBean.setFlagModificado(1);
            }
        }else{
            String codigoTelefonoFijoTemp = codigoTelefonoFijo;
            if (codigoTelefonoFijoTemp.length() == 1) {
                codigoTelefonoFijoTemp = " " + codigoTelefonoFijoTemp;
            }
            for (int i = telefonoFijo.length(); i < 8; i++) {
                telefonoFijo = telefonoFijo + " ";
            }
            String anexo = edtAnexo.getText().toString();
            for (int i = anexo.length(); i < 5; i++) {
                anexo = " " + anexo;
            }
            String telefonoFijoFinal = codigoTelefonoFijoTemp + telefonoFijo + anexo;
            if (!telefonoFijoFinal.equals(prospectoBean.getTelefonoFijo())) {
                prospectoBean.setTelefonoFijo(telefonoFijoFinal);
                prospectoBean.setFlagModificado(1);
                Log.i("TAG", "telefonoFijo");
            }
        }
    }
    private void validarNumeroDocumento(){
        if (edtNumeroDocumento.getText().toString().length() > 0){
            if(prospectoBean.getCodigoTipoDocumento() == 1){
                if(edtNumeroDocumento.getText().toString().length() != 8){
                    edtNumeroDocumento.setError("Por favor ingresa un DNI válido (ej: 44312341)");
                }else{
                    edtNumeroDocumento.setError(null);
                }
            }else if(prospectoBean.getCodigoTipoDocumento() == 2){
                if(edtNumeroDocumento.getText().toString().length() != 11){
                    edtNumeroDocumento.setError("Por favor ingresa un RUC válido (ej: 10443123418)");
                }else{
                    edtNumeroDocumento.setError(null);
                }
            }else if(prospectoBean.getCodigoTipoDocumento() == 3) {
                if (edtNumeroDocumento.getText().toString().length() != 9) {
                    edtNumeroDocumento.setError("Por favor ingresa un CE válido (ej: 123456789)");
                } else {
                    edtNumeroDocumento.setError(null);
                }
            }
        }else{
            edtNumeroDocumento.setError(null);
        }
    }
    private void validarCorreoElectronico(){
        if(prospectoBean.getCorreoElectronico1() != null && prospectoBean.getCorreoElectronico1().length() == 0){
            if(prospectoBean.getCodigoEtapa() == 1){
                edtCorreoElectronico.setError(null);
            }else {
                edtCorreoElectronico.setError(Constantes.MensajeErrorCampoVacio);
            }
        }else{
            final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
            Pattern pattern = Pattern.compile(EMAIL_PATTERN);
            Matcher matcher = pattern.matcher(edtCorreoElectronico.getText().toString());
            if(!matcher.matches()){
                edtCorreoElectronico.setError("Por favor ingresa una cuenta de correo válida (ej: hola@gmail.com)");
            }else{
                edtCorreoElectronico.setError(null);
            }
        }
    }
    private void updateConyugueEdtFechaNacimiento(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd 'de' MMMM, yyyy");
        edtConyugueFechaNacimiento.setText(simpleDateFormat.format(calConyugueFechaNacimiento.getTime()));
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String nuevaFechaNacimiento = simpleDateFormat.format(calConyugueFechaNacimiento.getTime());
        if(!nuevaFechaNacimiento.equals(conyugeBean.getFechaNacimiento())){
            conyugeBean.setFechaNacimiento(nuevaFechaNacimiento);
            conyugeBean.setFlagModificado(1);
        }
        Calendar calHoy = Calendar.getInstance();
        int diaHoy = calHoy.get(Calendar.DAY_OF_MONTH);
        int mesHoy = calHoy.get(Calendar.MONTH);
        int anioHoy = calHoy.get(Calendar.YEAR);
        int diaFecNac = calConyugueFechaNacimiento.get(Calendar.DAY_OF_MONTH);
        int mesFecNac = calConyugueFechaNacimiento.get(Calendar.MONTH);
        int anioFecNac = calConyugueFechaNacimiento.get(Calendar.YEAR);
        int edad = anioHoy - anioFecNac;
        if(mesHoy < mesFecNac){
            edad -= 1;
        }else if(mesHoy == mesFecNac){
            if(diaHoy < diaFecNac){
                edad -= 1;
            }
        }
        conyugeBean.setEdad(edad);
        edtConyugueFechaNacimiento.setError(null);
    }
    public ProspectoBean getProspectoBean() {
        return prospectoBean;
    }
    public void setProspectoBean(ProspectoBean prospectoBean) {
        this.prospectoBean = prospectoBean;
    }
    private void sincronizar(){
        SincronizacionController sincronizacionController = new SincronizacionController();
        if(progressDialog == null){
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setTitle("Enviando información");
            progressDialog.setMessage("Espere, por favor");
            progressDialog.setCancelable(false);

        }
        progressDialog.show();
        sincronizacionController.setRespuestaSincronizacionListener(new RespuestaSincronizacionListener() {
            @Override
            public void terminoSincronizacion(int codigo, String mensaje) {
                progressDialog.dismiss();
                if (codigo < 0) {
                    mostrarMensajeParaContinuar(mensaje);
                }else if(codigo == 1){
                    mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
                }else{
                    if(codigo == 4 || codigo == 6){
                        Intent inicioSesionIntent = new Intent(getActivity(), InicioSesionActivity.class);
                        inicioSesionIntent.putExtra("etapa", (codigo - 4)/2 + 1);
                        startActivity(inicioSesionIntent);
                    }else {
                        mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar\n" + mensaje);
                    }
                }
            }
        });
        int resultado = sincronizacionController.sincronizar(getContext());
        if(resultado == 0){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se sincronizó la información correctamente");
        }else if(resultado == 2){
            progressDialog.dismiss();
            mostrarMensajeParaContinuar("Se guardaron los datos sin sincronizar");
        }
    }
    private void mostrarMensajeParaContinuar(String mensaje){
        new AlertDialog.Builder(getContext())
                .setTitle("Alerta")
                .setMessage(mensaje).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                getActivity().onBackPressed();
            }
        }).show();
    }
    public void cargarLista(){
        if(edtActual != null){
            InputMethodManager inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(edtActual.getWindowToken(), 0);
            edtActual.clearFocus();
        }
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogCampoDatosPersonales = layoutInflater.inflate(R.layout.dialog_campo_datos_personales, null);
        ListView lstCampoDatosPersonales = (ListView) dialogCampoDatosPersonales.findViewById(R.id.lstCampoDatosPersonales);
        Button btnCancelar = (Button)dialogCampoDatosPersonales.findViewById(R.id.btnCancelar);
        ArrayList<String> arrCampoDatosPersonales;
        if(listaCampoActual == 0){
            arrCampoDatosPersonales = arrTitulosTipoDocumentos;
        }else if(listaCampoActual == 1){
            arrCampoDatosPersonales = arrTitulosCodigoTelefonoFijos;
        }else{
            arrCampoDatosPersonales = arrTitulosEstadosCiviles;
        }
        CampoDatosPersonalesArrayAdapter arrayAdapter = new CampoDatosPersonalesArrayAdapter(getContext(), arrCampoDatosPersonales, listaCampoActual);
        lstCampoDatosPersonales.setAdapter(arrayAdapter);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(dialogCampoDatosPersonales);
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        lstCampoDatosPersonales.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listaCampoActual == 0) {
                    edtTipoDocumento.setText(arrTitulosTipoDocumentos.get(position));
                    int nuevoCodigoTipoDocumento = arrTipoDocumentos.get(position).getCodigoCampo();
                    if (nuevoCodigoTipoDocumento != prospectoBean.getCodigoTipoDocumento()) {
                        prospectoBean.setCodigoTipoDocumento(nuevoCodigoTipoDocumento);
                        prospectoBean.setFlagModificado(1);
                        validarNumeroDocumento();
                    }
                } else if(listaCampoActual == 1) {
                    codigoTelefonoFijo = String.format("%.0f", arrCodigoTelefonoFijos.get(position).getValorNumerico());
                    edtCodigoTelefonoFijo.setText(codigoTelefonoFijo);
                    actualizarTelefonoFijo();
                    validarTelefonos();
                } else {
                    edtEstadoCivil.setText(arrTitulosEstadosCiviles.get(position));
                    int nuevoCodigoEstadoCivil = arrEstadosCiviles.get(position).getCodigoCampo();
                    int nuevoFlagConyuge = (position == 0 || position == 1) ? 1 : 0;
                    if(nuevoCodigoEstadoCivil != prospectoBean.getCodigoEstadoCivil()) {
                        prospectoBean.setCodigoEstadoCivil(nuevoCodigoEstadoCivil);
                        prospectoBean.setFlagConyuge(nuevoFlagConyuge);
                        prospectoBean.setFlagModificado(1);
                    }
                }
                dialog.dismiss();
            }
        });
        btnCancelar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void validarTelefonos(){
        String telefonoCelular = edtTelefonoCelular.getText().toString();
        String telefonoFijo = edtTelefonoFijo.getText().toString();
        if(telefonoCelular.length() > 0){
            if(telefonoCelular.length() != 9 || !telefonoCelular.substring(0, 1).equals("9")){
                edtTelefonoCelular.setError("Por favor ingresa un teléfono válido de celular (ej: 992665930)");
            }else{
                edtTelefonoCelular.setError(null);
            }
            if(telefonoFijo.length() > 0){
                int maximoTamano = codigoTelefonoFijo.equals("1") ? 7 : 6;
                if(telefonoFijo.length() != maximoTamano || telefonoFijo.substring(0, 1).equals("9")) {
                    edtTelefonoFijo.setError("Por favor ingresa un teléfono válido para el código de provincia seleccionado.");
                }else{
                    edtTelefonoFijo.setError(null);
                }
            }else{
                edtTelefonoFijo.setError(null);
            }
        }else{
            if(telefonoFijo.length() > 0){
                int maximoTamano = codigoTelefonoFijo.equals("1") ? 7 : 6;
                if(telefonoFijo.length() != maximoTamano || telefonoFijo.substring(0, 1).equals("9")) {
                    edtTelefonoFijo.setError("Por favor ingresa un teléfono válido para el código de provincia seleccionado.");
                }else{
                    edtTelefonoFijo.setError(null);
                    edtTelefonoCelular.setError(null);
                }
            }else{
                edtTelefonoCelular.setError(Constantes.MensajeErrorCampoVacio);
                edtTelefonoFijo.setError(null);
            }
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                sincronizar();
            }
        }
    }
}