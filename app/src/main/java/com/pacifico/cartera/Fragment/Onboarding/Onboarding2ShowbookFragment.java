package com.pacifico.cartera.Fragment.Onboarding;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pacifico.cartera.R;

public class Onboarding2ShowbookFragment extends Fragment {
    public Onboarding2ShowbookFragment(){}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_onboarding2_showbook, container, false);
    }
}