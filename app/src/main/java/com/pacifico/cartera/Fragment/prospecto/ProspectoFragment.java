package com.pacifico.cartera.Fragment.prospecto;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pacifico.cartera.Model.Bean.ProspectoBean;
import com.pacifico.cartera.Model.Bean.TablaTablasBean;
import com.pacifico.cartera.Model.Controller.CitaReunionController;
import com.pacifico.cartera.Model.Controller.ProspectoController;
import com.pacifico.cartera.Model.Controller.TablasGeneralesController;
import com.pacifico.cartera.Util.Util;

import java.util.Map;

import com.pacifico.cartera.Activity.MainActivity;
import com.pacifico.cartera.R;
import com.pacifico.cartera.adapter.ProspectoAdapter;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraAdnController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraProspectoController;
import com.pacifico.cartera.Model.Controller.Auxiliar.CarteraTablasGeneralesController;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProspectoADNBean;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProspectoDetalleBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.utils.DateUtils;
import com.pacifico.cartera.utils.StringUtils;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;
import com.pacifico.cartera.vista.controles.setFuente;

import static com.pacifico.cartera.Util.Util.capitalizedString;

/**
 * Created by dsb on 12/05/2016.
 */
public class ProspectoFragment extends Fragment {
    public static final String tipo_operacion = "tipo_operacion";
    public static final int operacion_MostrarADN = 3;
    public static final String parametro_idProspecto = "id_prospecto_dispositivo";
    public TabLayout tabLayout;
    public ViewPager viewPager;
    public LinearLayout llEditar;
    public LinearLayout llBack;

    public void setIdProspectoDispositivo(Integer idProspectoDispositivo) {
        this.idProspectoDispositivo = idProspectoDispositivo;
    }

    private Integer idProspectoDispositivo;

    // prospecto
    private TextViewCustomFont tvEtapaEstado;
    private TextViewCustomFont tvNombreProspecto;
    private TextViewCustomFont tvCargo;
    private TextViewCustomFont tvEmpresaCargo;
    private TextViewCustomFont tvReferenteTexto;
    private TextViewCustomFont tvReferente;
    private TextViewCustomFont tvProspectoEmail;
    private TextViewCustomFont tvProspectoTelefonoCelular;
    private TextViewCustomFont tvProspectoTelefonoFijo,tvAnexo;

    // adn
    private TextViewCustomFont tvIngresosMensuales;
    private TextViewCustomFont tvGastosMensuales;
    private TextViewCustomFont tvEfectivoYAhorros;
    private TextViewCustomFont tvCompromisoPago;
    private TextViewCustomFont tvDescobertura;

    private ProspectoDetalleBean prospectoDetalleBean;
    private ProspectoADNBean prospectoADNBean;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ((MainActivity) getActivity()).getSupportActionBar().hide();
        View view = inflater.inflate(R.layout.ppct_main_frgm, null);
        obtenerBeans();
        getResourceItems(view);
        inicializarPantalla();
        actualizarPantalla();
        updateResourceTabs(view);
        Log.i("TAG", "idProspectoDispositivo: " + idProspectoDispositivo);
        return view;
    }

    public void updateResourceTabs(View view){
        tabLayout = (TabLayout) view.findViewById(R.id.tabProspecto);
        viewPager = (ViewPager) view.findViewById(R.id.vpProspecto);

        viewPager.setOffscreenPageLimit(5);
        ProspectoAdapter adapter = new ProspectoAdapter(getActivity().getSupportFragmentManager());

        ProspectoDatosPersonalesFragment prospectoDatosPersonalesFragment = new ProspectoDatosPersonalesFragment();
        prospectoDatosPersonalesFragment.setProspectoDetalleBean(prospectoDetalleBean);

        ProspectoDatosFamiliaresFragment prospectoDatosFamiliaresFragment = new ProspectoDatosFamiliaresFragment();
        prospectoDatosFamiliaresFragment.setIdProspetcoDispositivo(prospectoDetalleBean.getIdProspectoDispositivo());

        ProspectoNotasFragment prospectoNotasFragment = new ProspectoNotasFragment();
        prospectoNotasFragment.setNotas(prospectoDetalleBean.getNota());

        ProspectoReferidosFragment prospectoReferidosFragment = new ProspectoReferidosFragment();
        prospectoReferidosFragment.setIdProspectoDispositivo(prospectoDetalleBean.getIdProspectoDispositivo());

        ProspectoHistorialFragment prospectoHistorialFragment = new ProspectoHistorialFragment();
        prospectoHistorialFragment.setIdprospectodispositivo(prospectoDetalleBean.getIdProspectoDispositivo());

        adapter.addFrag(prospectoDatosPersonalesFragment);
        adapter.addFrag(prospectoDatosFamiliaresFragment);
        adapter.addFrag(prospectoReferidosFragment);
        adapter.addFrag(prospectoNotasFragment);
        adapter.addFrag(prospectoHistorialFragment);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        RelativeLayout rel1 = (RelativeLayout) LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt1 = (TextView) rel1.findViewById(R.id.txt1);
        txt1.setText("DATOS PERSONALES");

        setFuente.setFuenteRg(getActivity(), txt1);

        txt1.setTextColor(getResources().getColor(R.color.black70));
        ImageView imgView1 = (ImageView) rel1.findViewById(R.id.img1);
        imgView1.setVisibility(View.GONE);
        RelativeLayout rel2 = (RelativeLayout) LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt2 = (TextView) rel2.findViewById(R.id.txt1);
        txt2.setText("DATOS FAMILIARES");

        setFuente.setFuenteRg(getActivity(), txt2);

        ImageView imgView2 = (ImageView) rel2.findViewById(R.id.img1);
        imgView2.setVisibility(View.GONE);

        RelativeLayout rel3 = (RelativeLayout) LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt3 = (TextView) rel3.findViewById(R.id.txt1);
        txt3.setText("REFERIDOS");
        setFuente.setFuenteRg(getActivity(), txt3);
        ImageView imgView3 = (ImageView) rel3.findViewById(R.id.img1);
        imgView3.setVisibility(View.GONE);

        RelativeLayout rel4 = (RelativeLayout) LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt4 = (TextView) rel4.findViewById(R.id.txt1);
        txt4.setText("NOTAS Y ADJUNTOS");
        setFuente.setFuenteRg(getActivity(), txt4);
        ImageView imgView4 = (ImageView) rel4.findViewById(R.id.img1);
        imgView4.setVisibility(View.GONE);

        RelativeLayout rel5 = (RelativeLayout) LayoutInflater.from(this.getActivity()).inflate(R.layout.custom_tab, null);
        TextView txt5 = (TextView) rel5.findViewById(R.id.txt1);
        txt5.setText("HISTORIAL");
        setFuente.setFuenteRg(getActivity(), txt5);
        ImageView imgView5 = (ImageView) rel5.findViewById(R.id.img1);
        imgView5.setVisibility(View.GONE);

        tabLayout.getTabAt(0).setCustomView(rel1);
        tabLayout.getTabAt(1).setCustomView(rel2);
        tabLayout.getTabAt(2).setCustomView(rel3);
        tabLayout.getTabAt(3).setCustomView(rel4);
        tabLayout.getTabAt(4).setCustomView(rel5);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                RelativeLayout relSelected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView)relSelected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.black));
                viewPager.setCurrentItem(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                RelativeLayout relUnselected = (RelativeLayout) tab.getCustomView();
                TextView txt1 = (TextView) relUnselected.findViewById(R.id.txt1);
                txt1.setTextColor(getResources().getColor(R.color.black70));
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){}
        });
    }
    public void getResourceItems(View view){
        llEditar = (LinearLayout)view.findViewById(R.id.llEditar);
        llBack = (LinearLayout)view.findViewById(R.id.llBack);
        llEditar.setOnClickListener(mEditarProspectoListener);
        llBack.setOnClickListener(mBackListener);

        tvEtapaEstado = (TextViewCustomFont) view.findViewById(R.id.tvEtapaEstado);
        tvNombreProspecto = (TextViewCustomFont) view.findViewById(R.id.tvNombreProspecto);
        tvCargo = (TextViewCustomFont) view.findViewById(R.id.tvCargo);
        tvEmpresaCargo = (TextViewCustomFont) view.findViewById(R.id.tvEmpresaCargo);
        tvReferenteTexto = (TextViewCustomFont) view.findViewById(R.id.tvReferenteTexto);
        tvReferente = (TextViewCustomFont) view.findViewById(R.id.tvReferente);
        tvProspectoEmail = (TextViewCustomFont) view.findViewById(R.id.tvProspectoEmail);
        tvProspectoTelefonoCelular = (TextViewCustomFont) view.findViewById(R.id.tvProspectoTelefonoCelular);
        tvProspectoTelefonoFijo = (TextViewCustomFont) view.findViewById(R.id.tvProspectoTelefonoFijo);
        tvAnexo = (TextViewCustomFont)view.findViewById((R.id.tvAnexo));

        // adn
        tvIngresosMensuales = (TextViewCustomFont) view.findViewById(R.id.tvIngresosMensuales);
        tvGastosMensuales = (TextViewCustomFont) view.findViewById(R.id.tvGastosMensuales);
        tvEfectivoYAhorros = (TextViewCustomFont) view.findViewById(R.id.tvEfectivoYAhorros);
        tvCompromisoPago = (TextViewCustomFont) view.findViewById(R.id.tvCompromisoPago);
        tvDescobertura = (TextViewCustomFont) view.findViewById(R.id.tvDescobertura);

        TextView tvIrAlADN = (TextView) view.findViewById(R.id.tvIrAlADN);
        tvIrAlADN.setOnClickListener(tvIrAADNListener);
    }
    private View.OnClickListener mEditarProspectoListener = new View.OnClickListener() {
        public void onClick(View v) {
            ProspectoController prospectoController = new ProspectoController();
            ProspectoBean prospectoBean = prospectoController.obtenerProspectoPorIdProspectoDispositivo(prospectoDetalleBean.getIdProspectoDispositivo());
            EditarProspectoFragment editarProspectoFragment = new EditarProspectoFragment();
            editarProspectoFragment.setProspectoBean(prospectoBean);
            ((MainActivity)getActivity()).changeFragment(editarProspectoFragment, Constantes.fragmentEditarProspecto);
        }
    };
    private View.OnClickListener mBackListener = new View.OnClickListener() {
        public void onClick(View v){
            getActivity().onBackPressed();
        }
    };
    private View.OnClickListener tvIrAADNListener = new View.OnClickListener() {
        public void onClick(View v) {
            if(CitaReunionController.prospectoTieneCita(idProspectoDispositivo)){
                try{
                    Intent launchIntent = getContext().getPackageManager().getLaunchIntentForPackage("com.pacifico.adn");
                    if(launchIntent != null){
                        launchIntent.putExtra(tipo_operacion, operacion_MostrarADN);
                        launchIntent.putExtra(parametro_idProspecto, idProspectoDispositivo);
                        launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(launchIntent);
                    }
                }catch(Exception e){
                    Util.mostrarAlertaConTitulo("Alerta", "No se pudo abrir ADN", ProspectoFragment.this.getContext());
                }
            }else{
                Util.mostrarAlertaConTitulo("Alerta", "Es un prospecto nuevo, primero debe agendar una cita", ProspectoFragment.this.getContext());
            }
        }
    };
    public void obtenerBeans() {
        CarteraProspectoController carteraProspectoController = new CarteraProspectoController();
        prospectoDetalleBean = llenarProspectoDetalle(carteraProspectoController.obtenerProspectoDetalle(idProspectoDispositivo));
        CarteraAdnController carteraAdnController = new CarteraAdnController();
        prospectoADNBean = llenarProspectoAdn(carteraAdnController.obtenerProspectoAdnResumen(idProspectoDispositivo));
    }
    public void inicializarPantalla() {
        tvEtapaEstado.setText("");
        tvNombreProspecto.setText("");
        tvCargo.setText("");
        tvEmpresaCargo.setText("");
        tvReferente.setText("");
        tvProspectoEmail.setText("");

        tvProspectoTelefonoCelular.setText("");
        tvProspectoTelefonoFijo.setText("");
        tvAnexo.setText("");

        tvIngresosMensuales.setText("");
        tvGastosMensuales.setText("");
        tvEfectivoYAhorros.setText("");
        tvCompromisoPago.setText("");
        tvDescobertura.setText("");
    }
    public void actualizarPantalla(){
        idProspectoDispositivo = ((MainActivity) getActivity()).getIdProspectoDispositivo();
        // prospecto
        String etapaReporte = ((MainActivity) getActivity()).getEtapaResultado();
        tvEtapaEstado.setText(etapaReporte);

        String nombreCompleto = prospectoDetalleBean.getNombres() + " " + prospectoDetalleBean.getApellidoPaterno();
        if(prospectoDetalleBean.getApellidoMaterno() != null){
            nombreCompleto = nombreCompleto + " " + prospectoDetalleBean.getApellidoMaterno();
        }
        tvNombreProspecto.setText(capitalizedString(nombreCompleto));
        if (prospectoDetalleBean.getCargo() != null)
            tvCargo.setText(prospectoDetalleBean.getCargo() + " en ");

        tvEmpresaCargo.setText(prospectoDetalleBean.getEmpresa());
        if(prospectoDetalleBean.getIdReferenciadorDispositivo() != 0){
            tvReferente.setText(capitalizedString(prospectoDetalleBean.getNombreReferenciador()));
        }else{
            tvReferenteTexto.setText("Fuente:");
            TablaTablasBean tablabean = TablasGeneralesController.obtenerTablaTablasPorIdTablaCodigoCampo(7, prospectoDetalleBean.getCodigoFuente());
            if (tablabean != null)
                tvReferente.setText(tablabean.getValorCadena());
        }

        tvProspectoEmail.setText(prospectoDetalleBean.getCorreoElectronico1());
        tvProspectoTelefonoCelular.setText(prospectoDetalleBean.getTelefonoCelular());

        String telefonoFijoTemp = "";
        String anexoTemp = "";
        String telefonoFijo = prospectoDetalleBean.getTelefonoFijo();
        String codigoTelefonoFijo = "1";
        String telefonoFijoFinal = "";
        if(telefonoFijo != null){
            if(telefonoFijo.length() == 15){
                codigoTelefonoFijo = telefonoFijo.substring(0, 2).replace(" ", "");
                telefonoFijoTemp = telefonoFijo.substring(2, 10).replace(" ", "");
                anexoTemp = telefonoFijo.substring(10, 15).replace(" ", "");
                telefonoFijoFinal = "(" + codigoTelefonoFijo + ") " + telefonoFijoTemp;
            }
        }
        tvProspectoTelefonoFijo.setText(telefonoFijoFinal);
        tvAnexo.setText(anexoTemp);

        // adn
        tvIngresosMensuales.setText(prospectoADNBean.getIngresosMensualesConTipoCambio());
        tvGastosMensuales.setText(prospectoADNBean.getGastosMensualesConTipoCambio());
        tvEfectivoYAhorros.setText(prospectoADNBean.getEfectivoYAhorrosConTipoCambio());
        tvCompromisoPago.setText(prospectoADNBean.getCompromisoPagoConTipoCambio());
        tvDescobertura.setText(prospectoADNBean.getDescoberturaConTipoCambio());
    }
    private ProspectoDetalleBean llenarProspectoDetalle(ProspectoDetalleBean bean){
        if (bean == null) {
            bean = new ProspectoDetalleBean();
        }
        CarteraTablasGeneralesController tablasController = new CarteraTablasGeneralesController();
        Map<Integer, TablaTablasBean> mapTablaEtapaProspecto = tablasController.obtenerMapaTablas(Constantes.codTablaEtapaProspecto);
        Map<Integer, TablaTablasBean> mapTablaEstadoCita = tablasController.obtenerMapaTablas(Constantes.codTablaEstadoCita);
        Map<Integer, TablaTablasBean> mapTablaSexo = tablasController.obtenerMapaTablas(Constantes.codTablaSexo);
        Map<Integer, TablaTablasBean> mapTablaEstadoCivil = tablasController.obtenerMapaTablas(Constantes.codTablaEstadoCivil);
        Map<Integer, TablaTablasBean> mapTablaNacionalidad = tablasController.obtenerMapaTablas(Constantes.codTablaNacionalidad);

        bean.setEtapa(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEtapaProspecto, bean.getCodigoEtapa()));
        bean.setEstado(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEstadoCita, bean.getCodigoEstado()));
        bean.setSexo(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaSexo, bean.getCodigoSexo()));
        bean.setEstadoCivil(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaEstadoCivil, bean.getCodigoEstadoCivil()));
        bean.setNacionalidad(tablasController.obtenerTablaValorCadenaByCodigoCampo(mapTablaNacionalidad, bean.getCodigoNacionalidad()));

        bean.setHijos(StringUtils.obtenerTextoBooleano(bean.getFlagHijos()));
        bean.setConyuge(StringUtils.obtenerTextoBooleano(bean.getFlagConyuge()));
        bean.setFumador(StringUtils.obtenerTextoBooleano(bean.getCondicionFumador()));

        String str_de = getResources().getString(R.string.fecha_de);
        bean.setFechaNacimientoTexto(DateUtils.getDateFormat20(str_de, bean.getFechaNacimiento()));
        bean.setEdad(DateUtils.calcularEdad(bean.getFechaNacimiento()));

        return bean;
    }
    private ProspectoADNBean llenarProspectoAdn(ProspectoADNBean bean){
        if(bean == null){
            bean = new ProspectoADNBean();
            bean.setTipoCambio(1);
        }
        int tipoCambio = bean.getTipoCambio();
        double factorEfectivoAhorros = bean.getIdMonedaEfectivoYAhorros() == Constantes.MonedaDolares ? 1 : tipoCambio;
        double montoEfectivoAhorros = Util.redondearNumero(bean.getEfectivoYAhorros() / factorEfectivoAhorros, 0);

        double factorIngresosMensuales = bean.getIdMonedaIngresosMensuales() == Constantes.MonedaDolares ? 1 : tipoCambio;
        double montoIngresosMensuales = Util.redondearNumero(bean.getIngresosMensuales() / factorIngresosMensuales, 0);

        double factorGastosMensuales = bean.getIdMonedaGastosMensuales() == Constantes.MonedaDolares ? 1 : tipoCambio;
        double montoGastosMensuales = Util.redondearNumero(bean.getGastosMensuales() / factorGastosMensuales, 0);

        double factorDescobertura = bean.getIdMonedaDescobertura() == Constantes.MonedaDolares ? 1 : tipoCambio;
        double montoDescobertura = Util.redondearNumero(bean.getDescobertura() / factorDescobertura, 0);

        double factorCompromisoPago = bean.getIdMonedaCompromisoPago() == Constantes.MonedaDolares ? 1 : tipoCambio;
        double montoCompromisoPago = Util.redondearNumero(bean.getCompromisoPago() / factorCompromisoPago, 0);

        bean.setEfectivoYAhorrosConTipoCambio(String.format("%s %s", this.getActivity().getString(R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoEfectivoAhorros)));
        bean.setIngresosMensualesConTipoCambio(String.format("%s %s", this.getActivity().getString(R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoIngresosMensuales)));
        bean.setGastosMensualesConTipoCambio(String.format("%s %s", this.getActivity().getString(R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoGastosMensuales)));
        bean.setDescoberturaConTipoCambio(String.format("%s %s", this.getActivity().getString(R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoDescobertura)));
        bean.setCompromisoPagoConTipoCambio(String.format("%s %s", this.getActivity().getString(R.string.simbolo_dolares), Util.obtenerNumeroConFormato(montoCompromisoPago)));
        return bean;
    }
}