package com.pacifico.cartera.Fragment.prospecto;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pacifico.cartera.R;
import com.pacifico.cartera.Model.Bean.Auxiliar.ProspectoDetalleBean;
import com.pacifico.cartera.utils.Constantes;
import com.pacifico.cartera.vista.controles.TextViewCustomFont;

import static com.pacifico.cartera.utils.Util.strNULL;

/**
 * Created by dsb on 14/05/2016.
 */
public class ProspectoDatosPersonalesFragment extends Fragment {

    private ProspectoDetalleBean prospectoDetalleBean;
    private TextViewCustomFont tvNumeroDocumento;
    private TextViewCustomFont tvFechaNacimiento;
    private TextViewCustomFont tvEdad;
    private TextViewCustomFont tvTieneHijos;
    private TextViewCustomFont tvEstadoCivil;
    private TextViewCustomFont tvGenero;
    private TextViewCustomFont tvNacionalidad;
    private TextViewCustomFont tvFumador;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.ppct_dper_frgm, container, false);

        Context ctx = getActivity().getApplicationContext();

        getResourceItems(view);
        actualizarPantalla();

        return view;
    }

    public void getResourceItems(View view) {

        tvNumeroDocumento = (TextViewCustomFont) view.findViewById(R.id.tvNumeroDocumento);
        tvFechaNacimiento = (TextViewCustomFont) view.findViewById(R.id.tvFechaNacimiento);
        tvEdad = (TextViewCustomFont) view.findViewById(R.id.tvEdad);
        tvTieneHijos = (TextViewCustomFont) view.findViewById(R.id.tvTieneHijos);
        tvEstadoCivil = (TextViewCustomFont) view.findViewById(R.id.tvEstadoCivil);
        tvGenero = (TextViewCustomFont) view.findViewById(R.id.tvGenero);
        tvFumador = (TextViewCustomFont) view.findViewById(R.id.tvFumador);

    }

    public void actualizarPantalla() {

        Log.d("PF", "actualizarPantalla: ");
        if (prospectoDetalleBean == null) {
            Log.d("PF", "prospectoDetalleBean null ");
            return;
        }

        // prospecto

        tvNumeroDocumento.setText(tipoDocumento()+" "+strNULL(prospectoDetalleBean.getNumeroDocumento()));

        Log.i("TAG","nuevos"+tipoDocumento());
        tvFechaNacimiento.setText(prospectoDetalleBean.getFechaNacimientoTexto());
        String edad = ""+prospectoDetalleBean.getEdad();
        if(edad.length()>0) {
            tvEdad.setText(String.valueOf(prospectoDetalleBean.getEdad()+" Años"));
        }
        tvTieneHijos.setText(prospectoDetalleBean.getHijos());

        if (prospectoDetalleBean.getSexo().equals(Constantes.codStrSexo_Masculino)) {
            tvGenero.setText(Constantes.desSexo_Masculino);
        } else if (prospectoDetalleBean.getSexo().equals(Constantes.codStrSexo_Femenino)) {
            tvGenero.setText(Constantes.desSexo_Femenino);
        }
        tvFumador.setText(prospectoDetalleBean.getFumador());
        tvEstadoCivil.setText(prospectoDetalleBean.getEstadoCivil());

    }

    public ProspectoDetalleBean getProspectoDetalleBean() {
        return prospectoDetalleBean;
    }

    public void setProspectoDetalleBean(ProspectoDetalleBean prospectoDetalleBean) {
        this.prospectoDetalleBean = prospectoDetalleBean;
    }

    public String  tipoDocumento(){

       String tipo =" " ;
int numero = prospectoDetalleBean.getCodigoTipoDocumento();
        switch (numero) {
            case 1:
                tipo = "DNI";
                break;
            case 2:
                tipo = "RUC";
                break;
            case 3:
                tipo ="CE";
                break;

        }
        return tipo;
    }




}
