package com.pacifico.cartera.Fragment.Ajustes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pacifico.cartera.R;

public class SincronizacionManualFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_sincronizacion_manual, container, false);
        return view ;
    }
}